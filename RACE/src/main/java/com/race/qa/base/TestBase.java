package com.race.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import com.race.qa.util.Log;
import com.race.qa.util.TestUtil;
import com.race.qa.util.WebEventListners;
import com.race.qa.util.extentreports.ExtentManager;
import com.race.qa.util.extentreports.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
//import net.lightbody.bmp.BrowserMobProxy;

/**
 * This is the base class of the project. Logics on excel, driver selection
 * config elements and Test Listeners are implemented here
 * 
 * @author Gautam Chakraborty
 */
public class TestBase {

	protected static WebDriver driver;
	protected static Properties configProperities;
	private static Properties translationProps;
	protected static Properties persistProperities;
	private static EventFiringWebDriver e_driver;
	private static WebEventListners eventListner;
	protected static String downloadFilepath;
	// public static BrowserMobProxy proxy;
	private static String configFilePath;
	private static String persistFilePath;
	private static String transFilePath;
	protected static WebDriverWait wait;
	protected static String methodName;
	protected static String Rand;
	private static String browserName;
	private static String environment;
	private static String deviceType = "web";
	protected static FirefoxProfile profile;

	// Constructor of test base class
	public TestBase() {
	}

	/**
	 * This is the initialization method which takes the browser information
	 * from the config file. By changing the browser key's value in config file
	 * other browsers can be invoked, default is chrome
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 **/
	public static void initialization() throws FileNotFoundException, IOException {
		/*
		 * To fetch values from config file, the file needs to be loaded in the
		 * memory first
		 */
		loadConfigFile();
			/*
		 * Persist files can be loaded if required from here - currently not
		 * used
		 */
		// loadPersisFile();

		/* Getting value of browser from config file */
		browserName = configProperities.getProperty("browser");

		// Browser Selection
		if (browserName.equals("chrome")) {
			startChrome(deviceType);
		} else if (browserName.equals("mozilla")) {
			startMozila();
		}

		// The logic to start the even firing web driver to get the TestNG Event
		// Listener Logs
		startEventFiringDriver();
		driver.manage().window().maximize();
		// deleting existing cookies before each tests
		driver.manage().deleteAllCookies();
		// Implicit wait and pge load time out
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);

		// Getting URL from config file
		driver.navigate().to(configProperities.getProperty("url"));
		wait = new WebDriverWait(driver, 1000);
	}

	/**
	 * This method loads the Config File. Config File is used to get the
	 * informations like Login Credentials, Browser Informations and Locale
	 * Informations. Gets values from com.race.qa.config>config.properties file
	 * 
	 * @throws FileNotFoundException
	 */
	public static void loadConfigFile() throws FileNotFoundException, IOException {
		configProperities = new Properties();

		// gets the file path from the project directory - Relative path
		configFilePath = System.getProperty("user.dir") + "/config.properties";

		FileInputStream configFile = new FileInputStream(configFilePath);
		configProperities.load(configFile);

		Log.info("Config File is loaded and ready to be used");
	}

	/**
	 * This method loads the persist file when a value and key needs to be
	 * stored from runtime and can be used in future. This a function for
	 * re-usability com.race.qa.config>persistance.properties file
	 */
	public static void loadPersisFile() throws FileNotFoundException, IOException {
		persistProperities = new Properties();
		persistFilePath = (System.getProperty("user.dir") + "/persistance.properties");
		FileInputStream persistFile = new FileInputStream(persistFilePath);
		persistProperities.load(persistFile);
	}

	/**
	 * This method is used to store values with keys to a properties file During
	 * the execution, if some values need to be reused which are generated at
	 * run time this method stores the value with key, and can be retrieved
	 * using the key.
	 * 
	 * @param key
	 * @param value
	 * @throws IOException
	 */
	public void setPersistValues(String key, String value) throws IOException {
		persistProperities.setProperty(key, value);
		FileWriter writer = null;
		writer = new FileWriter(persistFilePath);
		persistProperities.store(writer, "###########SAVED#############");
		writer.close();
	}

	/**
	 * This method is used to get the logs form the event firing webdriver and
	 * custom Log4j - these logs are stored in the log file.
	 */
	public static void startEventFiringDriver() {
		e_driver = new EventFiringWebDriver(driver);
		eventListner = new WebEventListners();
		e_driver.register(eventListner);
		driver = e_driver;
	}

	/**
	 * This methods starts chrome with different browser capabilities
	 * 
	 * @throws MalformedURLException
	 */
	public static void startChrome(String deviceType) throws FileNotFoundException, MalformedURLException {
		createProjectDir();
		if (deviceType == "web") {
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("download.default_directory", downloadFilepath);
			ChromeOptions options = new ChromeOptions();
			HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
			options.setExperimentalOption("prefs", chromePrefs);
			options.addArguments("test-type", System.getProperty("user.dir") + "customFolder");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			ChromeDriverService service = new ChromeDriverService.Builder()
					.usingDriverExecutable(new File((System.getProperty("user.dir") + "/chromedriver.exe")))
					.usingAnyFreePort().build();
			// Setting different capabilities for a stable automation
			cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
			cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			cap.setAcceptInsecureCerts(true);
			cap.setCapability(ChromeOptions.CAPABILITY, options);
			cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			// cap.setCapability("chrome.switches",
			// Arrays.asList("--incognito"));
			options.merge(cap);

			// File name is set from here to get the network traffic - Not in
			// Use
			// currently
			/*
			 * sFileName = workingDir + "\\ProxyLogs\\log.har";
			 * cap.setCapability(CapabilityType.PROXY, seleniumProxy);
			 * proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT,
			 * CaptureType.RESPONSE_CONTENT); proxy.newHar("RACE Dev01");
			 * 
			 * proxy.setTrustAllServers(true);
			 * proxy.setMitmManager(ImpersonatingMitmManager.builder().
			 * trustAllServers(true).build());
			 */

			environment = configProperities.getProperty("env");
			if (environment.equals("docker")) {
				String host = System.getProperty("seleniumHubHost");
				System.out.println(host);
				// "http://+host+"/wd/hub"
				driver = new RemoteWebDriver(new URL("http://192.168.99.100:4444/wd/hub"), cap);
			} else {
				driver = new ChromeDriver(service, options);// Instance of the
															// driver
															// launched
			}
		} else {
			Map<String, String> mobileEmulation = new HashMap<>();
			mobileEmulation.put("deviceName", configProperities.getProperty("deviceName"));
			ChromeDriverService service = new ChromeDriverService.Builder()
					.usingDriverExecutable(new File((System.getProperty("user.dir") + "/chromedriver.exe")))
					.usingAnyFreePort().build();
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

			driver = new ChromeDriver(service, chromeOptions);
		}
	}

	public static void startMozila() {
		try {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\geckodriver.exe");
			FirefoxProfile profile = new FirefoxProfile();
			profile.setAssumeUntrustedCertificateIssuer(false);

			profile.setPreference("browser.download.folderList", 2);
			profile.setPreference("browser.download.manager.showWhenStarting", false);
			profile.setPreference("browser.helperapps.neverAsk.saveToDisk",
					"application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/octet-stream;application/json");
			profile.setPreference("browser.download.dir", System.getProperty("user.dir") + "\\ProjectDownloadsDir\\");
			profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
			profile.setPreference("browser.download.panel.shown", false);
			profile.setPreference("browser.download.manager.focusWhenStarting", false);
			profile.setPreference("browser.download.useDownloadDir", true);
			profile.setPreference("browser.helperApps.alwaysAsk.force", false);
			profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
			profile.setPreference("browser.download.manager.closeWhenDone", true);
			profile.setPreference("browser.download.manager.showAlertOnComplete", false);
			profile.setPreference("browser.download.manager.useWindow", false);
			profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
			profile.setPreference("pdfjs.disabled", false);

			FirefoxOptions ffOptions = new FirefoxOptions();
			ffOptions.setProfile(profile);
			driver = new FirefoxDriver(ffOptions);

		} catch (Exception e) {
			Log.info(e.toString());
		}
	}

	/**
	 * This is to check for the downloaded file at the location or not
	 * 
	 * @param downloadPath
	 * @param fileName
	 * @return
	 */
	public boolean isFileDownloaded(String downloadPath, String fileName) {

		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
			Log.info("File Found Inside the Folder");
		}
		return flag;
	}

	/**
	 * This method deletes the file from the downloaded location The deletion of
	 * file is executed to get the re-usability
	 * 
	 * @param FILENAME
	 * @throws IOException
	 */
	public void deleteFile(String FILENAME) {

		File file = new File(System.getProperty("user.dir") + "/ProjectDownloadsDir/" + FILENAME);
		if (!file.exists()) {
			Log.info("File does not exists in folder");
		} else {
			// delete a file
			Log.info("File already exist");
			file.delete();
			Log.info("File Deleted Successfully");
		}

	}

	// These methods are called from here for the Event Listners
	public void onStart(ITestContext iTestContext) {

	}

	public void onFinish(ITestContext iTestContext) {

	}

	public void onTestStart(ITestResult iTestResult) {

	}

	public void onTestSuccess(ITestResult iTestResult) {

	}

	public void onTestFailure(ITestResult iTestResult) {

	}

	public void onTestSkipped(ITestResult iTestResult) {

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

	}

	public void onTestException(ITestResult iTestResult) {
		Object testClass = iTestResult.getInstance();
		WebDriver webDriver = ((TestBase) testClass).getDriver();

		// Take base64Screenshot screenshot.
		String base64Screenshot = "data:image/png;base64,"
				+ ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BASE64);

		// Extentreports log and screenshot operations for failed tests.
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Failed",
				ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		driver.close();

	}

	/**
	 * Returns the instance of the driver to each of the method that called
	 * 
	 * @return
	 */
	public WebDriver getDriver() {

		return driver;
	}

	/**
	 * This method reads the contents of excel and stores in the log file
	 * 
	 * @param FILENAME
	 * @throws IOException
	 */
	public void ExcelReader(String FILENAME) throws IOException {
		// Log.info("Inside ExcelReader Method");
		String excelFilePath = (System.getProperty("user.dir")) + "/ProjectDownloadsDir/" + FILENAME;
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		// Log.info("Excel File Path set");
		// Log.info("Reading Data");
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();
		Log.info("Total Number of Rows:" + iterator);
		firstSheet.getRow(0).getPhysicalNumberOfCells();
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			Log.info("--------Column Data Starts-----------");
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();

				if (cell.getCellTypeEnum() == CellType.STRING) {
					Log.info(cell.getStringCellValue());

				} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
					System.out.print(cell.getBooleanCellValue());
					Log.info(" - ");

				} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
					System.out.print(cell.getNumericCellValue());
					Log.info(" - ");

				}
			}
			Log.info("--------Column Data Ends-----------");
		}

		workbook.close();
		inputStream.close();
	}

	/**
	 * This method reads all the Labels in the modal pop up box like add Product
	 * etc and stores in log file
	 */
	public void getAllTextInPopup() {
		List<WebElement> texts = driver.findElements(By.tagName("label"));
		System.out.println(texts.size());
		java.util.Iterator<WebElement> i = texts.iterator();
		Log.info("-----------Data Starts-------------");
		while (i.hasNext()) {
			WebElement text = i.next();
			Log.info("Text is :   " + text.getText());
			Assert.assertTrue(text.getText() != null);
		}
		Log.info("-----------Data Ends-------------");
	}

	/**
	 * This method read all the options from the drop down list and stores in
	 * log file
	 * 
	 * @param element
	 * @param tag
	 */
	public void getDropdownData(WebElement element, String tag) {
		List<WebElement> Datas = element.findElements(By.tagName(tag));
		java.util.Iterator<WebElement> i = Datas.iterator();
		Log.info("-----------Data Starts-------------");

		while (i.hasNext()) {
			WebElement Data = i.next();
			Log.info("Text is :   " + Data.getText());
			Assert.assertTrue(Data.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Data);

		}
		Log.info("-----------Data Ends-------------");
	}

	/**
	 * This methods creates a random string of 10 digits and returns the string
	 * 
	 * @return
	 */
	public static String Random() {

		Random r = new Random();
		List<Integer> digits = new ArrayList<Integer>();
		String number = "";
		for (int i = 0; i < 10; i++) {
			digits.add(i);
		}
		for (int i = 10; i > 0; i--) {
			int randomDigit = r.nextInt(i);
			number += digits.get(randomDigit);
			digits.remove(randomDigit);
		}
		return number;

	}

	/**
	 * This methods reads the table data with headres and body and strores in
	 * log file
	 * 
	 * @param headerElement
	 * @param bodyElement
	 * @param headers
	 * @param body
	 */
	public void getTableData(WebElement headerElement, WebElement bodyElement, String headers, String body) {
		try {
			WebElement ele = driver.findElement(By.xpath("//*[@class='navigation ng-star-inserted']/ul"));
			List<WebElement> pagination = ele.findElements(By.tagName("li"));
			Log.info("                " + pagination.size());
			for (int l = 1; l <= pagination.size(); l++) {
				// WebElement page = k.next();
				// Log.info("Text is : " + page.getText());
				// Assert.assertTrue(page.getText() != null);
				List<WebElement> rows = headerElement.findElements(By.tagName(headers));
				java.util.Iterator<WebElement> i = rows.iterator();
				Log.info("----------HEADER DATA STARTS-------------------");
				while (i.hasNext()) {
					WebElement row = i.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
				}
				Log.info("----------HEADER DATA ENDS-------------------");
				List<WebElement> cols = bodyElement.findElements(By.tagName(body));
				Log.info("No. of Cols" + cols.size());
				java.util.Iterator<WebElement> j = cols.iterator();

				while (j.hasNext()) {
					Log.info("----------DATA STARTS-------------------");
					WebElement col = j.next();
					Log.info("Text is :   " + col.getText());
					Assert.assertTrue(col.getText() != null);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", col);
					Log.info("----------DATA ENDS-------------------");
				}
				if (driver.findElement(By.xpath("//button[@class='btn btn-input next']")).isDisplayed()
						&& driver.findElement(By.xpath("//button[@class='btn btn-input next']")).isEnabled()) {
					driver.findElement(By.xpath("//button[@class='btn btn-input next']")).click();
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath("//h3")));
				} else {
					Log.info("EOF");
				}
			}
		} catch (Exception e) {
			Log.info(e.toString());
		}

	}

	/**
	 * This methodCreates a custom Projects folder to store the downloaded excel
	 * file. and sets the download path for the chrome browser
	 * 
	 */
	public static void createProjectDir() {
		String DirectoryName = (System.getProperty("user.dir") + "/ProjectDownloadsDir");
		File Directory = new File(DirectoryName);
		if (!Directory.exists()) {
			Directory.mkdir();
		}
		downloadFilepath = (System.getProperty("user.dir") + "/ProjectDownloadsDir");

	}
	

}
