package com.race.qa.util;



import java.net.URL;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.race.qa.base.TestBase;

/**
 * This is the Logging Method using Log4j
 * @author Dell
 *
 */
public class Log extends TestBase{

//Initialize Log4j logs
	
	 private static Logger Log = Logger.getLogger(Log.class.getName());
	 public Log(){
		 URL LogUrl = getClass().getClassLoader().getResource("/src/main/resources/log4j.xml");
		 DOMConfigurator.configure(LogUrl);
	 }
	 
	 
// This is to print log for the beginning of the test case, as we usually run so many test cases as a test suite

public static void startTestCase(String sTestCaseName){
	
	Log.info("****************************************************************************************");

	Log.info("$$$$$$$$$$$$$$$$$$$$$                 "+sTestCaseName+ "       $$$$$$$$$$$$$$$$$$$$$$$$$");

	Log.info("****************************************************************************************");

	}

	//This is to print log for the ending of the test case

public static void endTestCase(String sTestCaseName){

	Log.info("XXXXXXXXXXXXXXXXXXXXXXX             "+"-E---N---D-"+"             XXXXXXXXXXXXXXXXXXXXXX");

	
	}

	// Need to create these methods, so that they can be called  

public static void info(String message) {

		Log.info(message);

		}

public static void warn(String message) {

   Log.warn(message);

	}

public static void error(String message) {

   Log.error(message);

	}

public static void fatal(String message) {

   Log.fatal(message);

	}

public static void debug(String message) {

   Log.debug(message);

	}

}
