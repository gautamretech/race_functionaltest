package com.race.qa.util.listners;

import com.race.qa.base.TestBase;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retry extends TestBase implements IRetryAnalyzer {

	private int count = 0;
	private static int maxTry =-1; // Run the failed test 2 times

	public boolean retry(ITestResult iTestResult) {
		if (!iTestResult.isSuccess()) { // Check if test not succeed
			if (count < maxTry) { // Check if maxtry count is reached
				count++; // Increase the maxTry count by 1
				iTestResult.setStatus(ITestResult.FAILURE); // Mark test as
															// failed
				//extendReportsFailOperations(iTestResult); // ExtentReports fail
															// operations
				return true; // Tells TestNG to re-run the test
			}
		} else {
			//com.race.qa.util.extentreports.ExtentTestManager.getTest()).setDescription("Test is retried");
			iTestResult.setStatus(ITestResult.SUCCESS); // If test passes,
														// TestNG marks it as
														// passed
		}
		return false;
	}
	/*public static void extendReportsFailOperations(ITestResult iTestResult) {
		WebDriver driver = DriverFactory.getInstance().getDriver();
		String base64Screenshot = "data:image/png;base64,"
				+ (((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64));
		com.race.qa.util.extentreports.ExtentTestManager.getTest().setDescription("Test is retried");
		com.race.qa.util.extentreports.ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Failed",
				com.race.qa.util.extentreports.ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
	}*/
}