package com.race.qa.pages.functional;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.ProjectTemplateLocators;
import com.race.qa.util.Log;
//import net.lightbody.bmp.proxy.jetty.html.Select;

public class FunctionalProjectTemplatePage extends TestBase {
	WebDriver driver;
	CommonLocators common;
	ProjectTemplateLocators ptl;
	String templatename;

	public FunctionalProjectTemplatePage(WebDriver driver) {
		this.driver = driver;
		this.driver = DriverFactory.getDriver();
	}

	public void getTemplate(String templatename) throws Throwable {
		System.out.println("inside getTemplate");
		try {
			System.out.println("getTeamplate"+templatename);
			awaitForElement(common.TEMPLATEGRAPH);
			Assert.assertTrue(common.TEMPLATEGRAPH.isDisplayed(), "Loading related issues");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", common.INPUTSEARCHFILTER);
			Assert.assertTrue(common.INPUTSEARCHFILTER.isDisplayed());
			
			common.INPUTSEARCHFILTER.sendKeys(templatename);
			waitForFilteredData(templatename);
			
			if (driver.findElements(By.xpath("//div[@class='no-data']")).size() == 1) {
				Log.info("Template does not exist");
			} else {
				Log.info("Template Found");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addTemplate_errorMessageSaveasDraft(String message) throws Throwable {
		try {
			Assert.assertTrue(common.TEMPLATEGRAPH.isDisplayed());
			Assert.assertEquals(ptl.LINKNEWTEMPLATE.getText(), selectPropertiesFile(true, locale, "newtemplate"));
			ptl.LINKNEWTEMPLATE.click();
			awaitForElement(ptl.LABELINPUTTEMPLATENAME);
			Assert.assertTrue(ptl.LABELINPUTTEMPLATENAME.isDisplayed());
			Assert.assertEquals(ptl.LABELINPUTTEMPLATENAME.getText(),
					selectPropertiesFile(true, locale, "templatename"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[@class='row page-buttons margin-bottom-sm']")));
			Assert.assertFalse(ptl.TEMPLATESAVEASDRAFTBUTTON.isEnabled());
			String errormessage = accessEditMessage(ptl.TEMPLATESAVEASDRAFTBUTTON,
					driver.findElement(By.xpath("//button[@class='btn btn-primary-blue']/following-sibling::div")));
			Assert.assertEquals(errormessage, message);

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public String createNewTemplate(String category, String templatetype, int numberOfPhases, int numberOfTasks,
			boolean isDuplicate, String templateName) throws Throwable {
			common=new CommonLocators();
			ptl=new ProjectTemplateLocators();
			String templatename = null;
		try {
			
			Assert.assertTrue(common.TEMPLATEGRAPH.isDisplayed());
			Assert.assertEquals(ptl.LINKNEWTEMPLATE.getText(), selectPropertiesFile(true, locale, "newtemplate"));
			ptl.LINKNEWTEMPLATE.click();
			awaitForElement(ptl.LABELINPUTTEMPLATENAME);
			Assert.assertTrue(ptl.LABELINPUTTEMPLATENAME.isDisplayed());
			Assert.assertEquals(ptl.LABELINPUTTEMPLATENAME.getText(),selectPropertiesFile(true, locale, "templatename"));
			if (isDuplicate && templateName != null) {
				templatename = templateName;
			} else {
				templatename = configProperities.getProperty("templatenameasinput") + Randomizer.generate(200, 2000);
			}
			System.out.println(templatename);
			ptl.INPUTTEMPLATENAME.sendKeys(templatename);
			
			selectOptionFromDropdown(ptl.DDCATEGORIES, common.CONTAINEROPTION, common.getCONTAINER(),
					ptl.DDSELECTTEMPLATECATEGORYOPTION, common.getDDSEARCHINPUT(), category);
			selectOptionFromDropdown(ptl.DDTEMPLATETYPE, common.CONTAINEROPTION, common.getCONTAINER(),
					ptl.DDSELECTTEPLATETYPEOPTION, common.getDDSEARCHINPUT(), templatetype);
			ptl.DESCRIPTION.sendKeys("This is generated from automated tests");
			Assert.assertEquals(ptl.HEADINGPHASES.getText(), selectPropertiesFile(true, locale, "phases"));
			addPhase(numberOfPhases, numberOfTasks);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return templatename;
	}

	public void addPhase(int numberOfPhases, int numberOfTasks) throws Throwable {
		Integer duration;
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ptl.ADDNEWPHASEBUTTON);
			ptl.ADDNEWPHASEBUTTON.isDisplayed();
			for (int i = 1; i <= numberOfPhases; i++) {
				Assert.assertTrue(ptl.ADDNEWPHASEBUTTON.isDisplayed());
				wait.until(ExpectedConditions.elementToBeClickable(ptl.ADDNEWPHASEBUTTON));
				ptl.ADDNEWPHASEBUTTON.click();
				awaitForElement(ptl.HEADINGPOP);
				Assert.assertEquals(ptl.HEADINGPOP.getText(), selectPropertiesFile(true, locale, "addphase"));
				Assert.assertEquals(ptl.LABELSELECTCOLOR.getText(),
						selectPropertiesFile(true, locale, "selectphasecolor"));
				Assert.assertEquals(ptl.LABELINPUTPHASENAME.getText(), selectPropertiesFile(true, locale, "phasename"));
				Assert.assertEquals(ptl.LABELINPUTDESCRIPTION.getText(),
						selectPropertiesFile(true, locale, "description"));
				ptl.INPUTNAME.sendKeys("Phase " + i);
				ptl.INPUTDESCRIPTION.sendKeys("Phase " + i + " description");
				wait.until(ExpectedConditions.elementToBeClickable(ptl.PHASESAVEBUTTON));
				Assert.assertTrue(ptl.PHASESAVEBUTTON.isEnabled());
				ptl.PHASESAVEBUTTON.click();
				awaitForElement(driver.findElement(By.xpath("//div[@class='header padding-sm']")));
				Assert.assertTrue(driver.findElement(By.xpath("//div[@class='header padding-sm']")).isDisplayed());
				Assert.assertEquals(ptl.ADDEDPHASEHEADING.getText(), "Phase 1 - " + "Phase " + i);
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(ptl.DELETEBUTTON)));
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(ptl.EDITBUTTON)));
				Thread.sleep(5000);
				Assert.assertTrue(ptl.EDITBUTTON.isDisplayed());
				Assert.assertTrue(ptl.DELETEBUTTON.isDisplayed());
				// Add task
				Assert.assertTrue(ptl.ADDTASKBUTTON.isDisplayed());
				for (int j = 1; j <= numberOfTasks; j++) {
					ptl.ADDTASKBUTTON.click();
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(ptl.INPUTNAME)));
					Assert.assertTrue(ptl.INPUTNAME.isDisplayed(), "Javascript Load issue");
					ptl.INPUTNAME.sendKeys(configProperities.getProperty("taskname") + j);
					Assert.assertTrue(ptl.INPUTDURATION.isDisplayed());
					duration = Randomizer.generate(1, 50);
					ptl.INPUTDURATION.sendKeys(String.valueOf(duration));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", ptl.BUTTONCREATETASK);
					Thread.sleep(10000);
				}
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					driver.findElement(By.xpath("(//div[@class='page-button']/button)[3]")));
			Thread.sleep(10000);
			Assert.assertEquals(driver.findElement(By.xpath("//span[@class='message']")).getText(),
					selectPropertiesFile(true, locale, "templatesaved"));
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(ptl.HEADINGTOPTEMPLATES)));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ptl.HEADINGTOPTEMPLATES);

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void templateGraphbyCategories(String familyname, String deptname) {

	}

	/**
	 * @throws Exception
	 * 
	 */
	public String saveAction(boolean saveAsDraft, boolean save) throws Exception {
		String message = null;
		try {
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//div[@class='row page-buttons margin-bottom-sm']"))));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[@class='row page-buttons margin-bottom-sm']")));
			if (saveAsDraft) {
				System.out.println("1111111111111111111111");
				System.out.println(ptl.TEMPLATESAVEASDRAFTBUTTON.isEnabled());
				if (ptl.TEMPLATESAVEASDRAFTBUTTON.isEnabled()) {

					Assert.assertTrue(ptl.TEMPLATESAVEASDRAFTBUTTON.isEnabled());
					ptl.TEMPLATESAVEASDRAFTBUTTON.click();
					Thread.sleep(10000);
					//awaitForElement(driver.findElement(By.xpath("//span[@class='message']")));
					message = driver.findElement(By.xpath("//span[@class='message']")).getText();
				} else {
					Assert.assertFalse(ptl.TEMPLATESAVEASDRAFTBUTTON.isEnabled());
					mouseHoverOnElement(driver.findElement(By.xpath("//button[@class='btn btn-primary-blue']/div")));
					wait.until(ExpectedConditions.visibilityOf(
							driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]"))));
					message = driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]")).getText();
				}
			} else if (save) {
				mouseHoverOnElement(driver.findElement(By.xpath("//button[@class='btn btn-primary-green']/div")));
				wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]"))));
				message = driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]")).getText();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return message;
	}

	public void addTemplate_errorMessageSave() throws Exception {
		try {
			Assert.assertTrue(common.TEMPLATEGRAPH.isDisplayed());
			Assert.assertEquals(ptl.LINKNEWTEMPLATE.getText(), selectPropertiesFile(true, locale, "newtemplate"));
			ptl.LINKNEWTEMPLATE.click();
			awaitForElement(ptl.LABELINPUTTEMPLATENAME);
			Assert.assertTrue(ptl.LABELINPUTTEMPLATENAME.isDisplayed());
			Assert.assertEquals(ptl.LABELINPUTTEMPLATENAME.getText(),
					selectPropertiesFile(true, locale, "templatename"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[@class='row page-buttons margin-bottom-sm']")));
			Assert.assertFalse(ptl.TEMPLATESAVEBUTTON.isEnabled());
			String errormessage = accessEditMessage(ptl.TEMPLATESAVEBUTTON,
					driver.findElement(By.xpath("//button[@class='btn btn-primary-green']/following-sibling::div")));
			Assert.assertEquals(errormessage, selectPropertiesFile(true, locale,
					"cannotsavetemplateasactivewithouttemplatenamecategoriestypeandatleastonephaseofprocess"));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public String addTemplate_Data1(boolean noData, String category, String templatetype, String templateName)
			throws Exception {
		System.out.println("inside addTemplate_Data1");
		String templatename = null;
		common = new CommonLocators();
		ptl=new ProjectTemplateLocators();
		try {
			Assert.assertTrue(common.TEMPLATEGRAPH.isDisplayed());
			Assert.assertEquals(ptl.LINKNEWTEMPLATE.getText(), selectPropertiesFile(true, locale, "newtemplate"));
			ptl.LINKNEWTEMPLATE.click();
			awaitForElement(ptl.LABELINPUTTEMPLATENAME);
			Assert.assertTrue(ptl.LABELINPUTTEMPLATENAME.isDisplayed());
			Assert.assertEquals(ptl.LABELINPUTTEMPLATENAME.getText(),
					selectPropertiesFile(true, locale, "templatename"));
			if (noData) {
				Log.info("No Data entered");
				templatename = null;
			} else {
				templatename = templateName;
				ptl.INPUTTEMPLATENAME.sendKeys(templateName);
				jsWait();
				selectOptionFromDropdown(ptl.DDCATEGORIES, common.CONTAINEROPTION, common.getCONTAINER(),
						ptl.DDSELECTTEMPLATECATEGORYOPTION, common.getDDSEARCHINPUT(), category);
				jsWait();
				selectOptionFromDropdown(ptl.DDTEMPLATETYPE, common.CONTAINEROPTION, common.getCONTAINER(),
						ptl.DDSELECTTEPLATETYPEOPTION, common.getDDSEARCHINPUT(), templatetype);
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return templatename;
	}

	public void waitForFilteredData(String templatename) throws InterruptedException {

		try {
			@SuppressWarnings("deprecation")
			FluentWait<WebDriver> tempwait = new FluentWait<WebDriver>(driver).withTimeout(1, TimeUnit.MINUTES)
					.pollingEvery(10, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);
			try {
				do {
					Thread.sleep(10);
				} while (driver.findElements(By.xpath("//table[@class='table table-striped']/tbody/tr")).size() != 1);
			} catch (StaleElementReferenceException e) {
				tempwait.until(
						ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ptl.TEMPLATEEDITACTION)));
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void rightClickonMenu(String message) throws Exception {
		try {
			jsWait();
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[7]/a/span"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[7]/a/span")).getText(),
					selectPropertiesFile(true, locale, "projecttemplates"));
			action.moveToElement(element);
			action.contextClick(element).build().perform();
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Set<String> handles = driver.getWindowHandles();
			String firstWinHandle = driver.getWindowHandle();
			System.out.println(firstWinHandle);
			handles.remove(firstWinHandle);
			Object winHandle = handles.iterator().next();
			if (winHandle != firstWinHandle) {
				String secondWinHandle = (String) winHandle;
				secondWinHandle = (String) winHandle;
				System.out.println(secondWinHandle);
				driver.switchTo().window(secondWinHandle);
			}
			Log.info("Switched to new Window");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[@class='panel-body bg-white']/div[2]/h3")));
			Thread.sleep(1000);
			mouseHoverOnElement(driver.findElement(By.xpath("(//div[@class='actions'])[1]")));
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]"))));
			String errormessage = driver.findElement(By.xpath("(//div[contains(@class,'race-tooltip')]/div)[1]"))
					.getText();
			Assert.assertEquals(errormessage, message);
			Log.info("Error Message displayed");
			driver.close();
			driver.switchTo().window(firstWinHandle);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public String accessEditMessage(WebElement parentElement, WebElement childElement) throws InterruptedException {
		try {
			Actions action = new Actions(driver);
			// Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(parentElement));
			action.moveToElement(parentElement).perform();
			// Thread.sleep(500);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(childElement)));
			// Thread.sleep(1000);
			childElement.getText();
			System.out.println(childElement.getText());
			return childElement.getText();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void validateActiveEditErrorMessage() throws Exception {
		System.out.println("inside validateActiveEditErrorMessage");
		Thread.sleep(10000);
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(
					By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]")));
			Assert.assertTrue(driver.findElement(By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]"))
					.isDisplayed());
			driver.findElement(By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]"))
					.click();
			Thread.sleep(10000);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='active-warning margin-top-md']/div[2]"))));
			awaitForElement(driver.findElement(By.xpath("//div[@class='active-warning margin-top-md']/div[2]")));
			System.out.println(driver.findElement(By.xpath("//div[@class='active-warning margin-top-md']/div[2]")).getText());
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public String getTemplatename() {
		String templatename = null;
		try {
			jsWait();

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("(//table[@class='table table-striped']/tbody/tr/td[2])[1]/div/span")));
			Assert.assertTrue(
					driver.findElement(By.xpath("(//table[@class='table table-striped']/tbody/tr/td[2])[1]/div/span"))
							.isDisplayed());
			templatename = driver
					.findElement(By.xpath("(//table[@class='table table-striped']/tbody/tr/td[2])[1]/div/span"))
					.getText();

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return templatename;
	}

	public String validateArchiveFunction() throws Exception {
		String currentState = null;
		try {
			jsWait();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(
					By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]")));
			Assert.assertTrue(driver
					.findElement(By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]"))
					.isDisplayed());
			mouseHoverOnElement(
					driver.findElement(By.xpath("(//button[contains(@class,'btn mouseover arrow-left')])[2]")));
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]"))));
			currentState = driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]")).getText();
			Log.info("Current state is :  " + currentState);
			if (currentState.equals(selectPropertiesFile(true, locale, "archive"))) {
				Log.info("The current State of Template is unarchieved, archieving now");
			} else if (currentState.equals(selectPropertiesFile(true, locale, "unarchive"))) {
				Log.info("The current State of Template is archieved, unarchieving now");
			}
			driver.findElement(By.xpath("(//button[contains(@class,'btn mouseover arrow-left')])[2]")).click();
			Thread.sleep(2000);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return currentState;
	}

	public void validateDeleteFunction() throws Exception {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(
					By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]")));
			Assert.assertTrue(driver.findElement(By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]"))
					.isDisplayed());
			mouseHoverOnElement(driver.findElement(By.xpath("(//button[contains(@class,'btn mouseover arrow-left')])[4]")));
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]"))));
			driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]")).getText();
			driver.findElement(By.xpath("(//button[contains(@class,'btn mouseover arrow-left')])[4]")).click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='modal-body']"))));
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-body']")).isDisplayed());
			driver.findElement(By.xpath("//div[contains(@class,'modal-footer')]/button[2]")).click();
			Thread.sleep(5000);
			awaitForElement(driver.findElement(By.xpath("//span[@class='message']")));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public String validateCopyFunction(String templateName) throws Exception {
		String copyTemplateName = null;
		try {
			jsWait();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(
					By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]")));
			Assert.assertTrue(driver
					.findElement(By.xpath("((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]"))
					.isDisplayed());
			mouseHoverOnElement(
					driver.findElement(By.xpath("(//button[contains(@class,'btn mouseover arrow-left')])[3]")));
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]"))));
			driver.findElement(By.xpath("//div[contains(@class,'race-tooltip')]/div[1]")).getText();
			driver.findElement(By.xpath("(//button[contains(@class,'btn mouseover arrow-left')])[3]")).click();
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//div[@class='modal-header padding-sm']"))));
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-header padding-sm']")).isDisplayed());
			Assert.assertTrue(driver.findElement(By.xpath("//textarea[@name='name']")).isDisplayed());
			copyTemplateName = "copyof" + templateName;
			driver.findElement(By.xpath("//textarea[@name='name']")).sendKeys(copyTemplateName);
			driver.findElement(By.xpath("//div[@class='modal-footer']/button[2]")).click();
			Thread.sleep(5000);
			awaitForElement(driver.findElement(By.xpath("//span[@class='message']")));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return copyTemplateName;
	}

	public String getTemplateStatus(String templatename) {
		String templateStatus = null;
		try {
			Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'status draft')]")).isDisplayed());
			templateStatus = driver.findElement(By.xpath("//span[contains(@class,'status draft')]")).getText();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return templateStatus;
	}
}
