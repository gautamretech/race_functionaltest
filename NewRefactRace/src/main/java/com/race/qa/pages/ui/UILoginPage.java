package com.race.qa.pages.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.util.Log;

public class UILoginPage extends TestBase {
	WebDriver driver;
	CommonLocators common;
	String locale;
	public UILoginPage(WebDriver driver, String locale) throws FileNotFoundException, IOException{
		try{
		this.driver=driver;
		this.driver=DriverFactory.getDriver();
		this.locale=locale;
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
	}
	}
	/**
	 * This Method is used for successful Login with the username and password
	 * 
	 * @param uname
	 * @param pwd
	 * @throws Exception 
	 */
	public void login(String loginUsername, String loginPassword) throws Exception {
		try{
		common = new CommonLocators();
		languageSelection(locale);
		common.getINPUTUSERNAME().clear();
		common.getINPUTUSERNAME().sendKeys(loginUsername);
		common.getINPUTPASSWORD().clear();
		common.getINPUTPASSWORD().sendKeys(loginPassword);
		common.getSIGNINBUTTON().click();
		waitForLandingPage();
		waitForPieChartData();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
		}
	}		

	/**
	 * This method logs out from the application - Avoid Session sharing issues
	 * 
	 * @throws InterruptedException
	 */
	public void logout() throws Exception {
		try{
		WebDriver driver = DriverFactory.getDriver();
		common = new CommonLocators();
		wait.until(ExpectedConditions.elementToBeClickable(common.getSETTINGSMENU()));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", common.getSETTINGSMENU());
		Assert.assertTrue(common.getSETTINGSMENU().isDisplayed());
		common.getSETTINGSMENU().click();
		wait.until(ExpectedConditions.elementToBeClickable(common.getLOGOUTBUTTON()));
		common.getLOGOUTBUTTON().click();
		wait.until(ExpectedConditions.visibilityOf(common.getRELOGINBUTTON()));
		Log.info("Text is :" + common.getRELOGINBUTTON().getText());
		Assert.assertTrue(common.getRELOGINBUTTON().getText() != null);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
		}
	}
	public void interim_logout() throws InterruptedException {
		try{
		WebDriver driver = DriverFactory.getDriver();
		common = new CommonLocators();
		wait.until(ExpectedConditions.elementToBeClickable(common.getSETTINGSMENU()));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", common.getSETTINGSMENU());
		Assert.assertTrue(common.getSETTINGSMENU().isDisplayed());
		common.getSETTINGSMENU().click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(common.getLOGOUTBUTTON())));
		Assert.assertTrue(common.getLOGOUTBUTTON().isDisplayed());
		common.getLOGOUTBUTTON().click();
		awaitForElement(common.getRELOGINBUTTON());
		Assert.assertTrue(common.getRELOGINBUTTON().isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(common.getRELOGINBUTTON()));
		common.getRELOGINBUTTON().click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(common.getSELECTLANGUAGE())));
		Assert.assertTrue(common.getSELECTLANGUAGE().isDisplayed());
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
		}
	}

	/**
	 * This is used for verifying the Logged in user
	 * 
	 * @param firstName
	 * @param lastName
	 */
	public void verifyLoggedUser(String firstName, String lastName) {
		wait.until(ExpectedConditions.visibilityOf(common.getDISPLAYNAME()));
		Assert.assertEquals(common.getDISPLAYNAME().getText(), firstName + " " + lastName);
	}

	/**
	 * This method is used for the registration of user
	 * @throws Exception 
	 */

	// Functional Tests
	public void validateForgetPasswordPage(String Locale) throws Exception {
		Locale = locale;
		languageSelection(locale);
		Log.info("The location selected is " + locale);
		wait.until(ExpectedConditions.visibilityOf(common.getLINKFORGOTPASSWORD()));
		Assert.assertTrue(common.getLINKFORGOTPASSWORD().isDisplayed());
		String emailPlaceHolder = common.getINPUTUSERNAME().getAttribute("placeholder");
		common.getLINKFORGOTPASSWORD().click();
		wait.until(ExpectedConditions.visibilityOf(common.getIMAGERESPONSIVE()));
		String retriveText = common.getBUTTONRETRIVEPASSWORD().getText();
		String lnkBacktoLogin = common.getLINKBACK2LOGIN().getText();
		Assert.assertTrue(common.getIMAGERESPONSIVE().isDisplayed());
		Assert.assertTrue(common.getIMAGERETRIVEPASSWORD().isDisplayed());
		Assert.assertEquals(emailPlaceHolder, selectPropertiesFile(false, Locale, "email"));
		Assert.assertEquals(retriveText, selectPropertiesFile(true,Locale,"retrievepassword"));
		Assert.assertEquals(lnkBacktoLogin, selectPropertiesFile(true,Locale,"backtologinpage"));
	}
	

	public void validatePreLoginPage(String Locale) throws Exception {
		Locale = locale;
		languageSelection(locale);
		Log.info("The location selected is " + locale);
		wait.until(ExpectedConditions.visibilityOf(common.getIMAGERESPONSIVE()));
		Assert.assertTrue(common.getIMAGERESPONSIVE().isDisplayed());
		Assert.assertTrue(common.getRETECHLOGO().isDisplayed());
		if(Locale.equals("JP")){
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='title-text ng-tns-c3-2 ng-star-inserted']")).isDisplayed());
		}else{
			Assert.assertTrue(common.getSIGNIN_IMAGE().isDisplayed());	
		}
		String selectLanguage = common.getTEXTLANGUAGESELECT().getText();
		String emailPlaceHolder = common.getINPUTUSERNAME().getAttribute("placeholder");
		String passwordPlaceHolder = common.getINPUTPASSWORD().getAttribute("placeholder");
		String signinButton = common.getSIGNINBUTTON().getText();
		String forgotPassword = common.getLINKFORGOTPASSWORD().getText();
		Assert.assertEquals(selectLanguage, selectPropertiesFile(true,Locale,"selectlanguage"));
		Assert.assertEquals(emailPlaceHolder, selectPropertiesFile(false,Locale,"email"));
		Assert.assertEquals(passwordPlaceHolder, selectPropertiesFile(false,Locale,"password"));
		Assert.assertEquals(signinButton, selectPropertiesFile(true,Locale,"signin"));
		Assert.assertEquals(forgotPassword, selectPropertiesFile(true,Locale,"forgetpassword"));
	}
	
}
