package com.race.qa.pages.functional;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.SupplierLocators;
import com.race.qa.util.Log;

public class FunctionalSupplierPage extends TestBase {
	protected CommonLocators common;
	protected SupplierLocators spLocators;
	protected String suppliername = null;
	protected String STATE;
	WebDriver driver;

	public FunctionalSupplierPage(WebDriver driver) {
		this.driver = driver;
		this.driver = DriverFactory.getDriver();
	}

	public void supplierTabs(String tabName) throws Exception {
		try {
			if (tabName == "General") {
				Assert.assertEquals(spLocators.GENERALTAB.getText(), selectPropertiesFile(true, locale, "general"));
				spLocators.GENERALTAB.click();
				Log.info("General Tab is selected");
				wait.until(ExpectedConditions.visibilityOf(spLocators.HEADINGOVERVIEW));
				Assert.assertEquals(spLocators.HEADINGOVERVIEW.getText(),
						selectPropertiesFile(true, locale, "overview"));
				Assert.assertEquals(spLocators.EDITBUTTON.getText(), selectPropertiesFile(true, locale, "edit"));

			} else if (tabName == "Business") {
				Assert.assertEquals(spLocators.BUSINESSTAB.getText(), selectPropertiesFile(true, locale, "business"));
				spLocators.BUSINESSTAB.click();
				Thread.sleep(1000);
				Assert.assertTrue(isImageLoaded(spLocators.BUSINESSTABIMAGE));
				Assert.assertEquals(spLocators.TEXTBUSINESSTYPE.getText(),
						selectPropertiesFile(true, locale, "typeofbusiness"), "WS load error related - Intermittent");

			} else if (tabName == "Factories") {
				Assert.assertEquals(spLocators.FACTORYTAB.getText(), selectPropertiesFile(true, locale, "factories"));
				spLocators.FACTORYTAB.click();
				Thread.sleep(1000);
				Assert.assertEquals(spLocators.ADDBUTTON.getText(), selectPropertiesFile(true, locale, "addfactory"),
						"WS load error related - Intermittent");

			} else if (tabName == "Contacts") {
				Assert.assertEquals(spLocators.CONTACTTAB.getText(), selectPropertiesFile(true, locale, "contacts"));
				spLocators.CONTACTTAB.click();
				Thread.sleep(1000);
				Assert.assertEquals(spLocators.ADDBUTTON.getText(), selectPropertiesFile(true, locale, "addcontact"),
						"WS load error related - Intermittent");

			} else if (tabName == "Certificates") {

			} else if (tabName == "Management") {

			} else if (tabName == "Catalog") {
				Assert.assertEquals(spLocators.CATALOGTAB.getText(), selectPropertiesFile(true, locale, "productlist"));
				spLocators.CATALOGTAB.click();
				awaitForElement(spLocators.PRODUCTLISTTITLE);
				// Assert.assertTrue(isImageLoaded(spLocators.PRODUCTLISTTITLE));
				Assert.assertEquals(spLocators.PRODUCTLISTTITLE.getText(),
						selectPropertiesFile(true, locale, "productlist_title"),
						"WS load error related - Intermittent");

			} else if (tabName == "Ratings") {

			} else if (tabName == "Settings") {

			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addContactDetails(String country) throws Exception {
		String firstName = "test";
		String lastName = "contact";
		String email = firstName + lastName + Randomizer.generate(200, 2000) + "@test.com";
		Integer phone = 1234567890;
		String title = "Mr";
		String CADDRESS = "Heqi Conjee Restaurant";
		String CZIP = "065001";
		String CCITY = "Beijing";
		// String ACCESS="NO";
		try {
			Assert.assertTrue(spLocators.ADDBUTTON.isDisplayed());
			Assert.assertTrue(spLocators.CONTACTINPUTSEARCH.isDisplayed());
			Assert.assertEquals(spLocators.TABLENODATA.getText(), selectPropertiesFile(true, locale, "factorynodata"));
			Log.info("No Contacts added");
			// Add contact
			spLocators.ADDBUTTON.click();
			awaitForElement(spLocators.ADDPOPUP);
			spLocators.INPUTCONTACTFIRSTNAME.sendKeys(firstName);
			spLocators.INPUTCONTACTLASTNAME.sendKeys(lastName);
			spLocators.INPUTCONTACTEMAIL.sendKeys(email);
			spLocators.INPUTCONTACTPHONE.sendKeys(String.valueOf(phone));
			// Linked in input not given
			spLocators.INPUTCONTACTTITLE.sendKeys(title);
			selectOptionFromDropdown(spLocators.DDCOUNTRY, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), country);
			spLocators.INPUTCONTACTADDRESS.sendKeys(CADDRESS);
			spLocators.INPUTCONTACTCITY.sendKeys(CCITY);
			spLocators.INPUTCONTACTSTATE.sendKeys(CCITY);
			spLocators.INPUTCONTACTZIP.sendKeys(CZIP);
			spLocators.CHECKBOXPRIMARYCONTACT.click();
			Assert.assertTrue(spLocators.CONTACTSAVEBUTTON.isEnabled());
			spLocators.CONTACTSAVEBUTTON.click();
			Thread.sleep(5000);
			Assert.assertEquals(driver.findElement(By.xpath("//table[@class='table table-striped table-hover bordered']/tbody/tr/td[1]/div/span"))
					.getText(), firstName + " " + lastName);
			Assert.assertEquals(driver.findElement(	By.xpath("//table[@class='table table-striped table-hover bordered']/tbody/tr/td[2]/a"))
					.getText(), (email));
			Assert.assertEquals(driver.findElement(By.xpath("//table[@class='table table-striped table-hover bordered']/tbody/tr/td[4]"))
					.getText(), title);
			Assert.assertEquals(driver.findElement(By.xpath("//table[@class='table table-striped table-hover bordered']/tbody/tr/td[5]"))
					.getText(), CADDRESS);
			
			Log.info("Contact added successfully");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void validateContact() throws Exception {
		try {
			Assert.assertTrue(spLocators.ADDBUTTON.isDisplayed());
			Assert.assertTrue(spLocators.CONTACTINPUTSEARCH.isDisplayed());
			Assert.assertEquals(spLocators.TABLENODATA.getText(), selectPropertiesFile(true, locale, "factorynodata"));
			Log.info("No Contacts added");
			// Add contact
			spLocators.ADDBUTTON.click();
			awaitForElement(spLocators.ADDPOPUP);
			Assert.assertEquals(spLocators.HEADINGADD.getText(), selectPropertiesFile(true, locale, "addcontact"));
			Assert.assertEquals(spLocators.LABELCONTACTFIRSTNAME.getText(),
					selectPropertiesFile(false, locale, "firstname"));
			Assert.assertEquals(spLocators.LABELCONTACTLASTNAME.getText(),
					selectPropertiesFile(false, locale, "lastname"));
			Assert.assertEquals(spLocators.LABELCONTACTEMAIL.getText(), selectPropertiesFile(false, locale, "email"));
			Assert.assertEquals(spLocators.LABELCONTACTPHONE.getText(), selectPropertiesFile(false, locale, "phone"));
			Assert.assertEquals(spLocators.LABELCONTACTTITLE.getText(), selectPropertiesFile(true, locale, "title"));
			Assert.assertEquals(spLocators.LABELCONTACTLINEDIN.getText(),
					selectPropertiesFile(true, locale, "linkedInprofilelink"));
			Assert.assertEquals(spLocators.LABELCONTACTCOUNTRY.getText(),
					selectPropertiesFile(true, locale, "country"));
			Assert.assertEquals(spLocators.LABELCONTACTADDRESS.getText(),
					selectPropertiesFile(false, locale, "address"));
			Assert.assertEquals(spLocators.LABELCONTACTCITY.getText(), selectPropertiesFile(false, locale, "city"));
			Assert.assertEquals(spLocators.LABELCONTACTSTATE.getText(),
					selectPropertiesFile(true, locale, "stateprovince"));
			Assert.assertEquals(spLocators.LABELCONTACTZIP.getText(), selectPropertiesFile(true, locale, "zip"));
			Assert.assertEquals(spLocators.LABELPRIMARYCONTACT.getText(),
					selectPropertiesFile(true, locale, "primarycontact"));
			Assert.assertEquals(spLocators.LABELACCESS.getText(), selectPropertiesFile(true, locale, "access"));
			Assert.assertFalse(spLocators.CONTACTSAVEBUTTON.isEnabled());
			Assert.assertTrue(common.getCLOSEBUTTON().isDisplayed());
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	/**
	 * @param factoryName
	 * @param status
	 * @param type
	 * @param country
	 *            > This has two values 1. China 2. United States of America>
	 *            The address will change as per country
	 * @throws Exception
	 */
	public void addNewFactorySuccess(String factoryName, String status, String type, String country) throws Exception {
		String CADDRESS = "Heqi Conjee Restaurant";
		String CZIP = "065001";
		String CCITY = "Beijing";
		// String UADDRESS = "Heqi Conjee Restaurant";
		// String UZIP = "065001";
		// String UCITY = "Beijing";
		try {
			Assert.assertTrue(spLocators.ADDBUTTON.isDisplayed());
			spLocators.ADDBUTTON.click();
			System.out.println("411111");
			awaitForElement(spLocators.ADDNEWFACTORYHEADING);
			Assert.assertTrue(spLocators.ADDNEWFACTORYHEADING.isDisplayed());
			spLocators.INPUTFACTORYNAME.sendKeys(factoryName);
			selectOptionFromDropdown(spLocators.DDSTATUS, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), status);
			System.out.println("42222");
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='status']/div/button//div/div[2]")).getText(),
					status);
			selectOptionFromDropdown(spLocators.DDTYPE, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), type);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='type']/div/button//div/div[2]")).getText(),
					type);
			selectOptionFromDropdown(spLocators.DDCATEGORIES, common.CONTAINEROPTION, common.getCONTAINER(),
					common.SELECTEDOPTION, common.getDDSEARCHINPUT(), configProperities.getProperty("familyname"));
			System.out.println("4333333");
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='categories']//div[2]")).getText(),
					configProperities.getProperty("familyname") + " - " + configProperities.getProperty("familyname"),
					"Option Mismatch due to synchronization error");
			selectOptionFromDropdown(spLocators.DDCOUNTRY, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), country);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='country']/div/button//div/div[2]")).getText(),
					country);
			spLocators.INPUTADDDRESS.sendKeys(CADDRESS);
			Log.info("Address Enetered");
			spLocators.INPUTCITY.sendKeys(CCITY);
			spLocators.INPUTZIP.sendKeys(CZIP);
			Assert.assertTrue(spLocators.BUTTONCOMPLETE.isEnabled());
			System.out.println("4444444");
			spLocators.BUTTONCOMPLETE.click();
			System.out.println("45555");
			Thread.sleep(5000);
			if (driver.findElements(By.xpath("(//div[contains(@class,'row details')])")).size() == 3) {
				Log.info("Factory Added successfully");
			} 
			else {
				Log.info("Factory not Added successfully");
			}
			// Check for the error message
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.TABLEFACTORYNAME);
			spLocators.TABLEFACTORYNAME.click();
			waitForRoundLoadertoDisappear();
			Thread.sleep(20000);
			boolean displayed = waitForMap(country);
			Assert.assertEquals(displayed, true);
			Log.info("Bing Map shown");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public boolean waitForMap(String country) throws InterruptedException {
		System.out.println("inside waitForMap");
		boolean found = false;
		//waitForRoundLoadertoDisappear();
		try {
			if (country.equals("China")) {
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='bing-map ng-star-inserted']/div"))));
				Thread.sleep(100);
				Log.info("Value of Found is false");

			} else {
				wait.until(ExpectedConditions.visibilityOf(
						driver.findElement(By.xpath("//a[contains(@href,'https://maps.google.com/maps?')]"))));
				Thread.sleep(100);
				Log.info("Value of Found is false");

			}
			found = true;
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return found;
	}

	/**
	 * This will yield an error message
	 * 
	 * @throws Exception
	 */
	public void addNewFactorywithoutAddress(String factoryName, String status, String type, String country)
			throws Exception {
		try {
			Assert.assertTrue(spLocators.ADDBUTTON.isDisplayed());
			spLocators.ADDBUTTON.click();
			awaitForElement(spLocators.ADDNEWFACTORYHEADING);
			Assert.assertTrue(spLocators.ADDNEWFACTORYHEADING.isDisplayed());
			spLocators.INPUTFACTORYNAME.sendKeys(factoryName);
			selectOptionFromDropdown(spLocators.DDSTATUS, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), status);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='status']/div/button//div/div[2]")).getText(),
					status);
			selectOptionFromDropdown(spLocators.DDTYPE, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), type);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='type']/div/button//div/div[2]")).getText(),
					type);
			selectOptionFromDropdown(spLocators.DDCOUNTRY, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), country);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='country']/div/button//div/div[2]")).getText(),
					country);
			Assert.assertTrue(spLocators.BUTTONCOMPLETE.isEnabled());
			spLocators.BUTTONCOMPLETE.click();
			Thread.sleep(5000);
			
			Log.info("Factory Added successfully");
			// Check for the error message
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.TABLEFACTORYNAME);
			spLocators.TABLEFACTORYNAME.click();
			Thread.sleep(10000);
			Assert.assertTrue(spLocators.NOMAPMESSAGE.isDisplayed());
			Assert.assertEquals(spLocators.NOMAPMESSAGE.getText(), selectPropertiesFile(true, locale,
					"thereisnoaddressaddedforthisfactorypleaseaddanaddresstoviewthemap"));
			Log.info("No address error message displayed");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	/**
	 * This method to validate blank Factory Page
	 * 
	 * @throws Exception
	 */
	public void validateFactoryPage() throws Exception {
		try {
			// Blank Factory
			Assert.assertTrue(spLocators.ADDBUTTON.isDisplayed());
			Assert.assertEquals(spLocators.ADDBUTTON.getText(), selectPropertiesFile(true, locale, "addfactory"));
			Assert.assertEquals(spLocators.FACTORYTABLENODATE.getText(),
					selectPropertiesFile(true, locale, "factorynodata"));
			Assert.assertTrue(spLocators.HEADINGFACTORYMAP.isDisplayed());
			Assert.assertEquals(spLocators.NOMAPMESSAGE.getText(),
					selectPropertiesFile(true, locale, "pleaseselectafactorytoshowmap"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.FACTORYSELECTEMPTYAREA);
			Assert.assertEquals(spLocators.SELECTFACTORYMESSAGE1.getText(),
					selectPropertiesFile(true, locale, "pleaseselectafactory"));
			Assert.assertEquals(spLocators.SELECTFACTORYMESSAGE2.getText(),
					selectPropertiesFile(true, locale, "formoreinformation"));

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void validateCatalogPage() throws Exception {
		try {
			// For new Supplier, the data will be blank
			Assert.assertTrue(spLocators.PRODUCTLISTTITLE.isDisplayed());
			Assert.assertTrue(spLocators.ADDPRODUCTBUTTON.isDisplayed());
			Assert.assertEquals(spLocators.ADDPRODUCTBUTTON.getText(),
					selectPropertiesFile(true, locale, "addnewproduct"));
			Assert.assertTrue(spLocators.PRODLISTNODATA.isDisplayed());
			Assert.assertEquals(spLocators.PRODLISTNODATA.getText(),
					selectPropertiesFile(true, locale, "nodataavailable"));
			Log.info("No Data added");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void deleteCatalogData() throws Exception {
		System.out.println("indide deleteCatalogData");
		int numberOfCatalog = 0;
		String xpath_1="//div[contains(@class,'panel-body product-list-item')]";
		String xpath_2="//div[@class='checkbox-indicator ng-star-inserted']";
		try {
			Assert.assertTrue(driver.findElement(By.xpath(xpath_1)).isDisplayed());
			numberOfCatalog = driver.findElements(By.xpath(xpath_1)).size();
			System.out.println(numberOfCatalog+"hdkhdkhd");
			for (int i = 1; i <= numberOfCatalog; i++) {
				System.out.println("inside for loop");
				System.out.println(xpath_2+"["	+ i + "]"+"  jejejjejejejejej");
				Assert.assertTrue(driver.findElement(By.xpath(xpath_2+"["	+ i + "]")).isDisplayed());
				Assert.assertTrue(driver.findElement(By.xpath("("+xpath_2+")"+"[" + i + "]")).isDisplayed());
				driver.findElement(By.xpath("("+xpath_2+")"+"[" + i + "]")).click();
				System.out.println("Stop Here");
				Thread.sleep(10000);
				System.out.println(driver.findElement(By.xpath("(//div[@class='checkbox-indicator ng-star-inserted'])[" + i + "]")).getCssValue("background"));
				System.out.println(i + " Catalog selected");
			}
			awaitForElement(spLocators.REMOVEBUTTON);
			Assert.assertTrue(spLocators.REMOVEBUTTON.isEnabled());
			spLocators.REMOVEBUTTON.click();
			awaitForElement(spLocators.ERRORMESSAGEDIV);
			Assert.assertTrue(spLocators.ERRORMESSAGEDIV.isDisplayed());
			Assert.assertEquals(spLocators.CANCELBUTTON.getText(), selectPropertiesFile(true, locale, "cancel"));
			Assert.assertEquals(spLocators.DELETBUTTON.getText(), selectPropertiesFile(true, locale, "delete"));
			spLocators.DELETBUTTON.click();
			awaitForElement(spLocators.PRODLISTNODATA);
			Assert.assertEquals(spLocators.PRODLISTNODATA.getText(),selectPropertiesFile(true, locale, "nodataavailable"));
			Log.info("Product List deleted successfully");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void recomendProductAsAdmin() throws Exception {
		int numberOfCatalog = 0;
		String xpath_1="//div[contains(@class,'panel-body product-list-item')]";
		try {
			Assert.assertTrue(driver.findElement(By.xpath(xpath_1)).isDisplayed());
			numberOfCatalog = driver.findElements(By.xpath(xpath_1)).size();
			for (int i = 1; i <= numberOfCatalog; i++) {
				Assert.assertTrue(driver.findElement(By.xpath(xpath_1+"["+ i + "]")).isDisplayed());
				Assert.assertTrue(driver.findElement(By.xpath("("+xpath_1+")[" + i + "]")).isDisplayed());
				driver.findElement(By.xpath("(//div[@class='checkbox-indicator ng-star-inserted'])[" + i + "]")).click();
				Thread.sleep(5000);
				Log.info(i + " Catalog selected");
			}
			Assert.assertTrue(spLocators.RECOMMENDBUTTON.isEnabled());
			spLocators.RECOMMENDBUTTON.click();
			awaitForElement(driver.findElement(By.xpath("//div[@class='modal-content']")));
			Assert.assertEquals(spLocators.PRODUCTINFOHEADING.getText(),selectPropertiesFile(true, locale, "productinfo"));

			for (int i = 1; i <= 4; i++) {
				Assert.assertFalse(driver.findElement(By.xpath("(//div[@class='form-group no-margin']/input)[" + i + "]")).isEnabled());
			}
			Assert.assertEquals(spLocators.LABELOEM.getText(), selectPropertiesFile(true, locale, "OEM"));
			Assert.assertEquals(spLocators.LABELINSTOCK.getText(), selectPropertiesFile(true, locale, "instock"));
			Assert.assertEquals(spLocators.LABELMOQ.getText(), selectPropertiesFile(true, locale, "MOQ"));
			Assert.assertEquals(spLocators.LABELORIGIN.getText(), selectPropertiesFile(true, locale, "origin"));
			Assert.assertEquals(spLocators.LABELTEXTAREA.getText(),	selectPropertiesFile(true, locale, "recommendedreason"));
			// enter inputs
			spLocators.INPUTMOQ.sendKeys("1");
			spLocators.INPUTORIGIN.sendKeys("1");
			spLocators.TEXTAREAREASON.sendKeys("Testing Purpose");
			Assert.assertTrue(spLocators.CONFIRMBUTTON.isEnabled());
			spLocators.CONFIRMBUTTON.click();
			awaitForElement(driver.findElement(By.xpath("(//div[@class='modal-content'])[2]")));
			String ActualMessage = spLocators.RECOMENDCONFIRMMESSAGE1.getText();
			String ExpectedMessage_1 = selectPropertiesFile(true, locale, "areyousureyouwouldliketochoose");
			String ExpectedMessage_2 = selectPropertiesFile(true, locale, "thisproduct");
			Assert.assertEquals(ActualMessage, (ExpectedMessage_1 + " " + ExpectedMessage_2) + "?");
			Assert.assertTrue(spLocators.CONFIRMBUTTON.isDisplayed());
			Assert.assertTrue(spLocators.CANCELBUTTON.isDisplayed());
			spLocators.RECOMENDCONFIRMBUTTON.click();
			awaitForElement(driver.findElement(By.xpath(xpath_1)));
			Assert.assertTrue(spLocators.RECOMENDEDICON.isDisplayed());
			Log.info("Product is Recomended");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	/**
	 * These will yeild an error message on permission
	 * 
	 * @throws Exception
	 */
	public void chooseTopProductAsAdmin() throws Exception {
		int numberOfCatalog = 0;
		String xpath_1="//div[contains(@class,'panel-body product-list-item')]";
		try {
			Assert.assertTrue(driver.findElement(By.xpath(xpath_1)).isDisplayed());
			
			numberOfCatalog = driver.findElements(By.xpath(xpath_1)).size();
			System.out.println(numberOfCatalog);
			for (int i = 1; i <= numberOfCatalog; i++) {
				Assert.assertTrue(driver.findElement(By.xpath(xpath_1+"["+ i + "]")).isDisplayed());
				Assert.assertTrue(driver.findElement(By.xpath("(//button[@class='btn top-product btn-banner ng-star-inserted'])[" + i + "]"))
						.isDisplayed());
				driver.findElement(By.xpath("(//button[@class='btn top-product btn-banner ng-star-inserted'])[" + i + "]")).isEnabled();
				mouseHoverOnElement(driver.findElement(By.xpath("(//button[@class='btn top-product btn-banner ng-star-inserted'])[" + i + "]")));
				driver.findElement(By.xpath("(//button[@class='btn top-product btn-banner ng-star-inserted'])[" + i + "]"))
						.click();
				do{
					Thread.sleep(100);
					System.out.println(driver.findElements(By.xpath("//div[@class='list']/div/span[2]")).size());
				}while(driver.findElements(By.xpath("//div[@class='list']/div/span[2]")).size()!=1);
				Assert.assertTrue(spLocators.TOPADMINPERMISSIONDENIEDMESSAGE.isDisplayed());
				Assert.assertEquals(spLocators.TOPADMINPERMISSIONDENIEDMESSAGE.getText(),
						selectPropertiesFile(true, locale, "youdonothavepermissiontoperformthisaction"));
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addCatalogData(boolean isBatchUpload, String filename) throws Exception {
		String catalogName = "IndividualUpload";
		String size = "100";
		double price = 23.99;
		String packageType = "Test Package";
		spLocators = new SupplierLocators();
		try {
			Assert.assertTrue(spLocators.ADDPRODUCTBUTTON.isDisplayed());
			spLocators.ADDPRODUCTBUTTON.click();
			awaitForElement(spLocators.TABADDNEWPRODUCT);
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-content']")).isDisplayed());
			Assert.assertEquals(spLocators.TABADDNEWPRODUCT.getText(),selectPropertiesFile(true, locale, "addnewproduct"));
			Assert.assertEquals(spLocators.LABELUPLOADPRODUCTIMAGE.getText(),selectPropertiesFile(true, locale, "productimage"));
			Assert.assertEquals(spLocators.LABELINPUTPRODUCTNAME.getText(),selectPropertiesFile(true, locale, "productname"));
			Assert.assertEquals(spLocators.LABELINPUTSIZE.getText(), selectPropertiesFile(true, locale, "size"));
			Assert.assertEquals(spLocators.LABELINPUTPRICE.getText(), selectPropertiesFile(true, locale, "price"));
			Assert.assertEquals(spLocators.LABELINPUTPACKAGETYPE.getText(),selectPropertiesFile(true, locale, "packagetype"));
			Assert.assertEquals(spLocators.LINKADDMOREPRODUCTS.getText(),selectPropertiesFile(true, locale, "addadditionalproduct"));
			if (isBatchUpload && filename != null) {
				Log.info("Catalog will be uploaded as Batch");
				Assert.assertTrue(spLocators.TABBATCHUPLOAD.isDisplayed());
				Assert.assertEquals(spLocators.TABBATCHUPLOAD.getText(),selectPropertiesFile(true, locale, "batchupload"));
				spLocators.TABBATCHUPLOAD.click();
				awaitForElement(spLocators.TEXTDOWNLOADTEMPLATE);
				Assert.assertTrue(spLocators.TEXTDOWNLOADTEMPLATE.isDisplayed());
				Assert.assertEquals(spLocators.TEXTDOWNLOADTEMPLATE.getText(),selectPropertiesFile(true, locale, "downloadtemplate"));
				Assert.assertEquals(spLocators.TEXTUPLOADBATCH.getText(),selectPropertiesFile(true, locale, "addanewbatchfile"));
				spLocators.INPUTBATCHUPLOAD.sendKeys(System.getProperty("user.dir") + "\\ProjectData\\" + filename);
				awaitForElement(spLocators.TEXTUPLOADEDFILES);
				Assert.assertTrue(spLocators.TEXTUPLOADEDFILES.isDisplayed());
				Assert.assertEquals(spLocators.TEXTUPLOADEDFILES.getText(),selectPropertiesFile(true, locale, "uploadedfiles"));
				Assert.assertEquals(spLocators.TITLEFILENAME.getText(), filename);
				if (spLocators.SUBTITLEMESSAGE.getText().equals("The file contains errors and has not been uploaded")) {
					Log.info("The file Not uploaded because of Errors, no product to be added");
					Assert.assertTrue(spLocators.CLOSEBUTTON.isDisplayed());
					spLocators.CLOSEBUTTON.click();
					//Thread.sleep(2000);
					awaitForElement(spLocators.PRODLISTNODATA);
					Assert.assertEquals(spLocators.PRODLISTNODATA.getText(),
							selectPropertiesFile(true, locale, "nodataavailable"));
				} 
				else if (spLocators.SUBTITLEMESSAGE.getText().equals("The file has been uploaded")) {
					Log.info("The file uploaded successfully, product added");
					Assert.assertTrue(spLocators.CLOSEBUTTON.isDisplayed());
					spLocators.CLOSEBUTTON.click();
					Thread.sleep(10000);
					Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'panel-body product-list-item')]")).isDisplayed());
				}
			} 
			else if (filename == null) {
				Log.info("Catalog will be uploaded as individual products");
				spLocators.INPUTPRODUCTNAME.sendKeys(catalogName);
				spLocators.INPUTSIZE.sendKeys(size);
				spLocators.INPUTPRICE.sendKeys(String.valueOf(price));
				spLocators.INPUTPACKAGETYPE.sendKeys(packageType);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						driver.findElement(By.xpath("//div[@class='modal-footer footer-btn-container']")));
				Assert.assertTrue(spLocators.SAVEBUTTONCATALOG.isEnabled());
				spLocators.SAVEBUTTONCATALOG.click();
				Thread.sleep(10000);
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'panel-body product-list-item')]")).isDisplayed());
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addBusinessType(String businesstype) throws Exception {
		try {
			selectOptionFromDropdown(spLocators.DDBUSINESSTYPE, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDBUSINESSOPTION, common.getDDSEARCHINPUT(), businesstype);
			common.BUTTONDONE.click();
			jsWait();
			Assert.assertTrue(spLocators.HEADINGBUSINESSTYPESELECTINFO.getText()
					.contains(selectPropertiesFile(true, locale, "selectedbusiness")));
			Assert.assertTrue(
					spLocators.BUSINESSCHIPTEXT.getText().contains(configProperities.getProperty("businesstype")));
			Assert.assertTrue(spLocators.BUTTONBUSINESSCHIPCLOSE.isDisplayed());
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void deleteAnnualSalesData() throws Exception {
		try {
			Assert.assertEquals(spLocators.HEADINGANNUALSALES.getText(),
					selectPropertiesFile(true, locale, "annualsales"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.HEADINGPOTENTIALCATEGORIES);
			// System.out.println(driver.findElement(By.xpath("((//div[@class='no-data'])/preceding::h3)[2]")).getText());
			if (driver
					.findElements(By
							.xpath("(//h3)[2]/following::table[@class='table table-striped table-hover'][1]/tbody/tr/td[6]"))
					.size() > 0) {
				Log.info("Annual sales Data is available");
				for (int i = 1; i <= driver
						.findElements(By
								.xpath("(//h3)[2]/following::table[@class='table table-striped table-hover'][1]/tbody/tr/td[6]"))
						.size(); i++) {
					Assert.assertTrue(driver
							.findElement(By.xpath("(//*[@name='saleYear']/following::td[4]/div/button)[" + i + "]"))
							.isDisplayed());
					driver.findElement(By.xpath("(//*[@name='saleYear']/following::td[4]/div/button)[" + i + "]"))
							.click();
					Thread.sleep(500);
					jsWait();
					Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-content']")).isDisplayed(),
							"WS Error Intermittent issue");
					Assert.assertEquals(spLocators.CONFIRMDELETEMESSAGE.getText(),
							selectPropertiesFile(true, locale, "areyousureyouwouldliketodeletethis?"));
					Assert.assertTrue(spLocators.CANCELBUTTON.isDisplayed());
					Assert.assertTrue(spLocators.DELETBUTTON.isDisplayed());
					Assert.assertEquals(spLocators.CANCELBUTTON.getText(),
							selectPropertiesFile(true, locale, "cancel"));
					Assert.assertEquals(spLocators.DELETBUTTON.getText(), selectPropertiesFile(true, locale, "delete"));
					spLocators.DELETBUTTON.click();
					Thread.sleep(500);
					jsWait();
				}
				Assert.assertTrue(driver.findElements(By.xpath("(//div[@class='no-data'])[1]")).size() == 1);

			} else {
				Log.info("Annual sales Data is Not available");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addAnnualSalesData(String categoryname, String year, int annualsales, double percent) throws Exception {
		try {
			Assert.assertEquals(spLocators.HEADINGANNUALSALES.getText(),
					selectPropertiesFile(true, locale, "annualsales"));
			if (driver.findElements(By.xpath("(//div[@class='no-data'])[1]")).size() == 0) {
				Log.info("Annual sales Data is available and added");
			} else {
				Log.info("Annual sales Data is not available and adding");
				spLocators.BUTTONADDANNUNALSALES.click();
				System.out.println("qqq111111111");
				Assert.assertTrue(spLocators.DDSALECATEGORY.isDisplayed());
				System.out.println("qqq111111111");
				selectOptionFromDropdown(spLocators.DDSALECATEGORY, common.CONTAINEROPTION, common.getCONTAINER(),
						spLocators.DDCATEGORYOPTION, common.getDDSEARCHINPUT(), categoryname);
				System.out.println("qqq22222");
				Assert.assertEquals(spLocators.DDCATEGORYSELECTED.getText(), "Auto_Family - Auto_Family");
				System.out.println("qqq33333");
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",spLocators.DDYEAR);
				Thread.sleep(1000);
				selectOptionFromDropdown(spLocators.DDYEAR, common.CONTAINEROPTION, common.getCONTAINER(),
						common.CONTAINEROPTION, common.getDDSEARCHINPUT(), year);
				System.out.println("qqq444444");
				Assert.assertEquals(spLocators.DDCATEGORYSELECTED.getText(), "Auto_Family - Auto_Family");
				Assert.assertEquals(spLocators.DDYEARSELECTED.getText(), year);
				spLocators.INPUTSALES.clear();
				System.out.println("qqq155555");
				spLocators.INPUTSALES.sendKeys(String.valueOf(annualsales));
				System.out.println("qqq66666");
				spLocators.INPUTPERCENTAGE.clear();
				System.out.println("qqq777777");
				spLocators.INPUTPERCENTAGE.sendKeys(String.valueOf(percent));
				System.out.println("qqq88888");
				spLocators.CALCULATEDVALUE.click();
				System.out.println("qqq99999");
				double calculated = ((annualsales) * (percent)) / 100;
				System.out.println("herere" + calculated);
				
				String calc = spLocators.CALCULATEDVALUE.getText();
				calc = calc.replaceAll("¥", "").replaceAll(".00", "");
				if (calculated == Integer.parseInt(calc)) {
					Log.info("Calculated correct");
				} else {
					Log.info("Not Calculated correct");
				}
				Assert.assertTrue(spLocators.SALESDELETEBUTTON.isDisplayed());
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addPotentialCategoriesData(String categoryname, String year, int capacity, int used) throws Exception {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.HEADINGPOTENTIALCATEGORIES);
			Assert.assertEquals(spLocators.HEADINGPOTENTIALCATEGORIES.getText(),
					selectPropertiesFile(true, locale, "potentialcategories"));
			if (driver.findElement(By.xpath("(//div[@class='no-data'])[1]/preceding::div[1]/h3"))
					.getText() == selectPropertiesFile(true, locale, "annualsales")) {
				Log.info("Annual sales is empty");
				if (driver.findElements(By.xpath("(//div[@class='no-data'])[2]")).size() == 0) {
					Log.info("Potential categories Data is available and added");
				} else {
					Log.info("Potential categories Data is not available and adding");
				}
			} else {
				Log.info("Annual sales has Data");
				if (driver.findElements(By.xpath("(//div[@class='no-data'])[1]")).size() == 0) {
					Log.info("Potential categories Data is available and added");
				} else {
					Log.info("Potential categories Data is not available and adding");
				}
			}
			spLocators.ADDBUTTONPOTENTALCATEGORY.click();
			jsWait();
			Assert.assertTrue(spLocators.DDPOTENTIALCATEGORIES.isDisplayed());
			selectOptionFromDropdown(spLocators.DDPOTENTIALCATEGORIES, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDCATEGORYOPTION, common.getDDSEARCHINPUT(), categoryname);
			Assert.assertEquals(spLocators.DDPOTENTIALCATSELECTED.getText(), "Auto_Family - Auto_Family");
			selectOptionFromDropdown(spLocators.DDPOTENTIALYEAR, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDYEAROPTION, common.getDDSEARCHINPUT(), year);
			Assert.assertEquals(spLocators.DDPOTENTIALYEARSELECTED.getText(), year);
			spLocators.INPUTCAPACITY.clear();
			Thread.sleep(1000);
			spLocators.INPUTCAPACITY.sendKeys(String.valueOf(capacity));
			Thread.sleep(1000);
			jsWait();
			spLocators.INPUTUSED.clear();
			Thread.sleep(1000);
			spLocators.INPUTUSED.sendKeys(String.valueOf(used));
			Thread.sleep(5000);
			jsWait();
			spLocators.INPUTCOMMENT.sendKeys("This is from Automated test");
			Thread.sleep(1000);
			jsWait();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void supplierBusinessTab(String supliername) throws Exception {
		Assert.assertEquals(spLocators.BUSINESSTABSUPPLIERNAME.getText(), supliername);
	}

	public void validateRequiredFields_AddSupplier() throws Exception {
		common=new  CommonLocators();
		spLocators=new SupplierLocators();
		try {
			waitForPieChartData();
			Assert.assertTrue(common.getPIECHART().isDisplayed());
			Assert.assertTrue(spLocators.ADDSUPPLIERBUTTON.isDisplayed());
			spLocators.ADDSUPPLIERBUTTON.click();
			awaitForElement(spLocators.ADDPOPUP);
			Assert.assertTrue(spLocators.ADDPOPUP.isDisplayed());
			Assert.assertEquals(spLocators.HEADINGADD.getText(), selectPropertiesFile(true, locale, "addsupplier"),
					"The Texts are not matching");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", spLocators.NEXTBUTTON);
			Assert.assertTrue(spLocators.NEXTBUTTON.isDisplayed());
			spLocators.NEXTBUTTON.click();
			awaitForElement(spLocators.HEADINGADD);
			Assert.assertEquals(spLocators.HEADINGADD.getText(),selectPropertiesFile(true, locale, "suppliersettings"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",spLocators.SAVESUPPLIERBUTTON);
			Assert.assertTrue(spLocators.SAVESUPPLIERBUTTON.isDisplayed());
			spLocators.SAVESUPPLIERBUTTON.click();
			Thread.sleep(5000);
			//awaitForElement(driver.findElement(By.xpath("//div[@class='errors hoverable ng-star-inserted']")));
			int num = driver.findElements(By.xpath("//div[@class='errors hoverable ng-star-inserted']")).size();
			System.out.println(num+"gdgdgdhshsghge");
			for (int i = 1; i <= num; i++) {
				Log.info("Required Fields" + driver.findElement(By.xpath("(//div[@class='form-group has-error']/label)[" + i + "]")).getText());
				mouseHoverOnElement(driver.findElement(By.xpath("(//div[@class='errors hoverable ng-star-inserted']/div)[" + i + "]")));
				Thread.sleep(3000);
				awaitForElement(driver.findElement(By.xpath("(//div[@class='errors hoverable ng-star-inserted']/div)["
						+ i + "]/following-sibling::ul/li")));
				Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='errors hoverable ng-star-inserted']/div)[" + i + "]/following-sibling::ul/li"))
						.getText(), "This field is required.");
			}
			Assert.assertEquals(
					driver.findElement(By.xpath("(//div[@class='form-group has-error']/label)[1]")).getText(),
					selectPropertiesFile(true, locale, "suppliername"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//div[@class='form-group has-error']/label)[2]")).getText(),
					selectPropertiesFile(true, locale, "country"));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public String createSupplier(String status, String country) throws Exception {
		String ID = null;
		String name = null;
		common=new CommonLocators();
		spLocators=new SupplierLocators();
		try {
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed(), "Loading related error");
			Assert.assertTrue(spLocators.ADDSUPPLIERBUTTON.isDisplayed());
			spLocators.ADDSUPPLIERBUTTON.click();
			awaitForElement(spLocators.ADDPOPUP);
			Assert.assertTrue(spLocators.ADDPOPUP.isDisplayed());
			Assert.assertEquals(spLocators.HEADINGADD.getText(), selectPropertiesFile(true, locale, "addsupplier"),
					"The Texts are not matching");
			// Add Logo
			spLocators.UPLOADLOGOLINK.sendKeys(System.getProperty("user.dir") + "\\ProjectData\\2.jpg");
			Thread.sleep(5000);
			
			Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'logo-img ng-star-inserted')]")).isDisplayed());
			name = configProperities.getProperty("suppliername") + Randomizer.generate(200, 2000);
			spLocators.SUPPLIERNAME.sendKeys(name);
			ID = "ID" + Randomizer.generate(200, 2000);
			spLocators.SUPPLIERID.sendKeys(ID);
			// Adding Status
			selectOptionFromDropdown(spLocators.DDSTATUS, common.CONTAINEROPTION, common.getCONTAINER(),
					spLocators.DDSELECTOPTION, common.getDDSEARCHINPUT(), status);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='status']/div/button//div/div[2]")).getText(),
					status);
			// Select Country
			selectOptionFromDropdown(spLocators.DDCOUNTRY, common.CONTAINEROPTION, common.getCONTAINER(),
					common.CONTAINEROPTION, common.getDDSEARCHINPUT(), country);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='country']/div/button//div/div[2]")).getText(),
					country);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", spLocators.NEXTBUTTON);
			spLocators.NEXTBUTTON.click();
			waitForProgressBar(driver.findElement(By.xpath("//div[@class='race-progress']/div")));
			Thread.sleep(10000);
			waitForElement(driver.findElements(By.xpath("//div[@class='modal-header']/span")));
			Assert.assertEquals(spLocators.HEADINGADD.getText(), selectPropertiesFile(true, locale, "suppliersettings"),
					"The Texts are not matching");
			spLocators.SAVESUPPLIERBUTTON.click();
			Thread.sleep(10000);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return name;
}
	public void AssertStatement(WebElement element,  String text){
		Assert.assertTrue(element.getText().contains(text));
		}

	public void changeSupplierStatus(String supplierStatusTobe, String supplierName) throws Exception {
		try {
			Thread.sleep(1500);
			Assert.assertTrue(common.PROJECTSUPPLIERSIDEMENU.isDisplayed());
			Assert.assertEquals(common.PROJECTSUPPLIERSIDEMENU.getText(), supplierName);
			common.PROJECTSUPPLIERSIDEMENU.click();
			awaitForElement(spLocators.TEXTSUPPLIERDETAILS);
			Assert.assertTrue(spLocators.TEXTSUPPLIERDETAILS.isDisplayed());
			Assert.assertEquals(spLocators.TEXTSUPPLIERDETAILS.getText(),
					selectPropertiesFile(true, locale, "supplierdetails"));
			Assert.assertEquals(spLocators.SUPPLIERSTATUSBUTTON.getText(),
					selectPropertiesFile(false, locale, "invited"));
			Assert.assertEquals(spLocators.BLOCKSUPPLIERNAME.getText(), supplierName);
			spLocators.SUPPLIERCHANGESTATUSBUTTON.click();
			Thread.sleep(1000);
			if (supplierStatusTobe == "SELECT") {
				awaitForElement(spLocators.CHANGEOPTIONS);
				Assert.assertTrue(spLocators.SELECTSUPPLIEROPTION.isDisplayed());
				spLocators.SELECTSUPPLIEROPTION.click();
				Log.info("Supplier Selected");
				Thread.sleep(5000);
			}
			new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
					.executeScript("return document.readyState").equals("complete"));
			Assert.assertEquals(spLocators.SUPPLIERSTATUSBUTTON.getText(),
					selectPropertiesFile(false, locale, "selected"));

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void getSupplier(String supplierName) throws Exception {

		this.suppliername = supplierName;
		try {
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed(), "Loading related issues");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", common.INPUTSEARCHFILTER);
			Assert.assertTrue(common.INPUTSEARCHFILTER.isDisplayed());
			Log.info(suppliername);
			common.INPUTSEARCHFILTER.sendKeys(suppliername);
			waitForFilteredData(suppliername);
			Thread.sleep(500);
			System.out.println(driver.findElements(By.xpath("//div[@class='no-data']")).size() == 1);
			if (driver.findElements(By.xpath("//div[@class='no-data']")).size() == 1) {
				Log.info("Supplier does not exist");
			} else {
				Log.info("Supplier Found");
			}
			spLocators.SUPPLIERLINKTABLE.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.SUPPLIERSECTIONTABS));
			Assert.assertTrue(spLocators.SUPPLIEREDITBUTTON.isDisplayed());
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void preRatingsAddProduct_Click(String projectname, String suppliername) throws Exception {
		spLocators=new SupplierLocators();
		try {
			awaitForElement(spLocators.HEADINGPRERATINGPAGE);

			Assert.assertTrue(spLocators.HEADINGPRERATINGPAGE.isDisplayed());
			Assert.assertEquals(spLocators.HEADINGPRERATINGPAGE.getText(),
					selectPropertiesFile(true, locale, "supplierrating"));
			Assert.assertTrue(spLocators.TABLEPRERATINGPAGE.isDisplayed());
			Assert.assertEquals(spLocators.RATINGSTATUSPRERATINGPAGE.getText(),
					selectPropertiesFile(true, locale, "ratingrequired"),
					"The status is not matching, only allowed is Ratings required");
			Assert.assertEquals(spLocators.PROJECTNAMEPRERATINGPAGE.getText(), projectname);
			if (driver.findElements(By.xpath("(//div[@class='danger add-product text-center font-weight-700']/div)[2]"))
					.size() == 0) {
				Log.info("Product Already Added");
			} else {
				Assert.assertTrue(spLocators.CLICKHERELINKPRERATINGPAGE.isDisplayed());
				Assert.assertEquals(spLocators.CLICKHERELINKPRERATINGPAGE.getText(),
						selectPropertiesFile(true, locale, "clickhere"));
				spLocators.CLICKHERELINKPRERATINGPAGE.click();
				waitForRoundLoadertoDisappear();
				//Log.info("Navigated to Project space Page");
				//waitForTextToAppear(driver.findElement(By.xpath("(//p[@class='text-ellipsis no-margin-bottom'])[1]")));
				//awaitForElement(driver.findElement(By.xpath("(//p[@class='text-ellipsis no-margin-bottom'])[1]")));
				awaitForElement(spLocators.TEXTSUPPLIERDETAILS);
				Assert.assertEquals(spLocators.TEXTSUPPLIERDETAILS.getText(),
						selectPropertiesFile(true, locale, "supplierdetails"));
				Assert.assertEquals(spLocators.BLOCKSUPPLIERNAME.getText(), suppliername);
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void clickBacktoRatings() throws Exception {
		try {
			awaitForElement(common.BUTTONBACKTORATING);
			Assert.assertTrue(common.BUTTONBACKTORATING.isDisplayed());
			common.BUTTONBACKTORATING.click();
			awaitForElement(spLocators.HEADINGPRERATINGPAGE);
			Assert.assertTrue(spLocators.HEADINGPRERATINGPAGE.isDisplayed());
			Assert.assertEquals(spLocators.HEADINGPRERATINGPAGE.getText(),
					selectPropertiesFile(true, locale, "supplierrating"));
			Log.info("Moved on Pre-Ratings Screen from ProjectSpace");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addProductRatings() throws Exception {
		try {
			Assert.assertTrue(spLocators.PRODUCTSRATINGSAREA.isDisplayed());
			Assert.assertEquals(spLocators.TITLEPRODUCTRATINGS.getText(),
					selectPropertiesFile(true, locale, "productrating"));
			mouseHoverOnElement(spLocators.PRODUCTRATINGINFO_POPUP);
			awaitForElement(spLocators.PRODUCTRATINGINFO_POPUPTEXT);
			jsWait();
			Assert.assertTrue(spLocators.PRODUCTRATINGINFO_POPUPTEXT.isDisplayed());
			Assert.assertEquals(spLocators.PRODUCTRATINGINFO_POPUPTEXT.getText(),
					selectPropertiesFile(true, locale, "scoreofproductsdevelopedwithcurrentsupplier"));
			// Product Rating
			System.out.println(spLocators.PRODUCTRATINGRANGE.getLocation());
			spLocators.PRODUCTRATINGRANGE.click();
			Thread.sleep(1000);
			jsWait();
			Assert.assertEquals(spLocators.PRODUCTSCORETYPE.getText(), selectPropertiesFile(true, locale, "average"));
			Log.info("PRODUCT RAting Added");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addSupplierRating(boolean withDefaultCriteria, int numberofCriteria, String usercriteria)
			throws Exception {
		try {
			Assert.assertTrue(spLocators.TITLESUPPLIERRATINGS.isDisplayed());
			Assert.assertEquals(spLocators.TITLESUPPLIERRATINGS.getText(),
					selectPropertiesFile(true, locale, "supplierrating"));
			mouseHoverOnElement(spLocators.SUPPLIERRATINGINFO_POPUP);
			awaitForElement(spLocators.SUPPLIERRATINGSINFO_TEXT);
			jsWait();
			Assert.assertTrue(spLocators.SUPPLIERRATINGSINFO_TEXT.isDisplayed());
			Assert.assertEquals(spLocators.SUPPLIERRATINGSINFO_TEXT.getText(), selectPropertiesFile(true, locale,
					"scoreofspecificcriteriaforcurrentsupplieryoucanratethesupplierviathesixdefaultcriteriaoraddingnewcriteriondefaultcriteriacannotbedeleted"));
			for (int i = 1; i <= numberofCriteria; i++) {
				String criteria_Coomon = "//div[@class='rating-container min-height-60 padding-left-5 ng-star-inserted']";
				if (withDefaultCriteria) {
					Assert.assertTrue(driver.findElement(By.xpath(criteria_Coomon + "[" + i + "]")).isDisplayed());
					Assert.assertTrue(
							driver.findElement(By.xpath(criteria_Coomon + "[" + i + "]/race-checkbox")).isDisplayed());
					System.out.println(
							driver.findElement(By.xpath(criteria_Coomon + "[" + i + "]/div/div[1]")).getText());
					Assert.assertTrue(
							driver.findElement(By.xpath(criteria_Coomon + "[" + i + "]/div[2]")).isDisplayed());
					driver.findElement(By.xpath(criteria_Coomon + "[" + i + "]/race-checkbox")).click();
					Thread.sleep(1000);
					jsWait();
					Log.info("default critiria CheckBox checked");
					driver.findElement(By.xpath(criteria_Coomon + "[" + i + "]/div[2]//input")).click();
					Thread.sleep(1000);
					jsWait();
					Assert.assertEquals(
							driver.findElement(By.xpath(criteria_Coomon + "[" + i + "]/div[2]//span")).getText(),
							selectPropertiesFile(true, locale, "average"));
					Log.info(i + "    Rated");
				} else {
					// Adding New Criterion
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							spLocators.BUTTONADDCRITERIA);
					Log.info(i + "   clicked this");
					spLocators.BUTTONADDCRITERIA.click();
					Thread.sleep(1000);
					jsWait();
					Assert.assertTrue(
							driver.findElement(By.xpath(criteria_Coomon + "[" + (i + 6) + "]")).isDisplayed());
					Assert.assertTrue(driver.findElement(By.xpath(criteria_Coomon + "[" + (i + 6) + "]/race-checkbox"))
							.isDisplayed());
					// System.out.println(driver.findElement(By.xpath(criteria_Coomon+"["+
					// (i + 6) + "]/div/div[1]")).getText());
					Assert.assertTrue(
							driver.findElement(By.xpath(criteria_Coomon + "[" + (i + 6) + "]/div[2]")).isDisplayed());
					driver.findElement(By.xpath("(//*[@name='textItem'])[1]")).sendKeys(usercriteria + i);
					Thread.sleep(500);
					driver.findElement(By.xpath(criteria_Coomon + "[" + (i + 6) + "]/race-checkbox")).click();
					Thread.sleep(1000);
					jsWait();
					Log.info("default critiria CheckBox checked");
					driver.findElement(By.xpath(criteria_Coomon + "[" + (i + 6) + "]/div[2]//input")).click();
					Thread.sleep(1000);
					jsWait();
					Assert.assertEquals(
							driver.findElement(By.xpath(criteria_Coomon + "[" + (i + 6) + "]/div[2]//span")).getText(),
							selectPropertiesFile(true, locale, "average"));
					Log.info(i + "    Rated");
				}

			}
			spLocators.BUTTONSAVESUPPLIERRATINGS.click();
			jsWait();
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.RATINGSTATUSPRERATINGPAGE);
			awaitForElement(spLocators.RATINGSTATUSPRERATINGPAGE);
			Assert.assertEquals(spLocators.RATINGSTATUSPRERATINGPAGE.getText(),
					selectPropertiesFile(true, locale, "ratingcompleted"));
			Assert.assertTrue(spLocators.FINALRATINGSCORE.isDisplayed());
			Assert.assertEquals(spLocators.FINALRATINGSCORE.getText(), "3");
			Log.info("Supplier Rating completed");
			Thread.sleep(1000);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void waitForFilteredData(String suppliername) throws InterruptedException {
		try {
			@SuppressWarnings("deprecation")
			FluentWait<WebDriver> tempwait = new FluentWait<WebDriver>(driver).withTimeout(1, TimeUnit.MINUTES)
					.pollingEvery(10, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);
			try {
				do {
					Thread.sleep(10);
				} while (driver.findElements(By.xpath("//table[@class='table table-striped']/tbody/tr")).size() != 1);
			} catch (StaleElementReferenceException e) {
				tempwait.until(ExpectedConditions
						.refreshed(ExpectedConditions.elementToBeClickable(spLocators.SUPPLIERLINKTABLE)));
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void ratinsPageErrorMessageOnSave() throws Exception {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.BUTTONSAVESUPPLIERRATINGS);
			spLocators.BUTTONSAVESUPPLIERRATINGS.click();
			jsWait();
			Thread.sleep(1000);
			Assert.assertTrue(spLocators.PRODUCTERROR.isDisplayed());
			Assert.assertEquals(spLocators.PRODUCTERROR.getText(),
					selectPropertiesFile(true, locale, "productratingisrequiredpleaseaddarating"));
			Log.info("PRODUCT RATING ERROR DISPLAYED");
			Assert.assertTrue(spLocators.SUPPLIERERROR.isDisplayed());
			Assert.assertEquals(spLocators.SUPPLIERERROR.getText(),
					selectPropertiesFile(true, locale, "supplierratingisrequiredpleaseaddarating"));
			Log.info("SUPPLIER RATING ERROR DISPLAYED");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

}
