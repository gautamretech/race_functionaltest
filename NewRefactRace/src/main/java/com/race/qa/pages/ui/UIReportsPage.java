package com.race.qa.pages.ui;


import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.util.Log;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.ReportsLocators;

public class UIReportsPage extends TestBase {
	WebDriver driver;
	UIDashboardPage dashboardPage;
	CommonLocators common;
	ReportsLocators rl;

	public UIReportsPage(WebDriver driver) {
		this.driver = driver;
		this.driver = DriverFactory.getDriver();
	}

	// Project's Tab
	public void validateProjectTabScreen() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PROJECTSTAB.isDisplayed();
			rl.PROJECTSTAB.click();
			jsWait();
			Assert.assertEquals(rl.PROJECTSTAB.getText(), selectPropertiesFile(true, locale, "projects"));
			Assert.assertEquals(rl.TEXTOVERVIEW.getText(), selectPropertiesFile(true, locale, "overview"));
			Assert.assertEquals(rl.DDTOGGLENAME.getText(), selectPropertiesFile(true, locale, "byproject"));

			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			Log.info("Text is :" + rl.HEADING.getText());
			Assert.assertTrue(rl.HEADING.getText() != null);
			rl.SEARCHPROJECTINPUT.isDisplayed();
			rl.SEARCHBUTTON.isDisplayed();
			Log.info("Text is :" + rl.SEARCHBUTTON.getText());
			Assert.assertTrue(rl.SEARCHBUTTON.getText() != null);
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			Log.info("Text is :" + rl.FILTERBUTTON.getText());
			Assert.assertTrue(rl.FILTERBUTTON.getText() != null);
			// ICONPROJSUPPLIER.isDisplayed();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void completedProjectGraph(boolean tobeclicked) throws Exception {
		try {
			Assert.assertTrue(rl.COMPLETEDPROJECTGRAPHAREA.isDisplayed());
			Assert.assertEquals(rl.COMPLETEDTEXT, selectPropertiesFile(true, locale, "completed"));
			if (tobeclicked) {
				rl.COMPLETEDPROJECTGRAPHAREA.click();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	// Team Tab
	public String validateLineGraph_TeamTab(String Month) {

		String firstLetter = Month.substring(0, 1).toUpperCase();
		String restLetters = Month.substring(1).toLowerCase();
		return firstLetter + restLetters;
	}
	/*
	 * public String currentMonth(){ Month currentMonth =
	 * LocalDate.now().getMonth(); String cm=currentMonth.toString(); return cm;
	 * //System.out.println(currentMonth); Month PreviousMonth
	 * =currentMonth.minus(1); String pm = PreviousMonth.toString(); Month
	 * NextMonth =currentMonth.plus(1); String nm=NextMonth.toString();
	 * System.out.println(cm+"..............."+pm+".........."+nm); }
	 */

	public void validateTeamTabScreen() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement(rl.LINEGRAPH);
			rl.LINEGRAPH.isDisplayed();
			rl.TEAMTAB.isDisplayed();
			rl.HEADINGWORKLOAD.isDisplayed();
			Log.info("Text is :" + rl.TEAMTAB.getText());
			Log.info("Text is :" + rl.HEADINGWORKLOAD.getText());
			Assert.assertTrue(rl.TEAMTAB.getText() != null);
			Assert.assertTrue(rl.HEADINGWORKLOAD.getText() != null);
			// Log.info("Text is :" + rl.SELECTSTATUSOFTASK.getText());
			// Assert.assertTrue(rl.SELECTSTATUSOFTASK.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			rl.HEADING.isDisplayed();
			Log.info("Text is :" + rl.HEADING.getText());
			Log.info("Text is :" + rl.FILTERBUTTON.getText());
			Log.info("Text is :" + rl.SEARCHUSERSINPUTBOX.getText());
			Log.info("Text is :" + rl.SEARCHBUTTON.getText());
			Assert.assertTrue(rl.HEADING.getText() != null);
			Assert.assertTrue(rl.FILTERBUTTON.getText() != null);
			Assert.assertTrue(rl.SEARCHUSERSINPUTBOX.getText() != null);
			Assert.assertTrue(rl.SEARCHBUTTON.getText() != null);
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.SEARCHUSERSINPUTBOX.isDisplayed();
			rl.SEARCHBUTTON.isDisplayed();
			// rl.GROUPSICON.isDisplayed();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

		}


	public void validateUserTableInTeamTab() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.TEAMTAB.isDisplayed();
			rl.HEADINGWORKLOAD.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			userTableHeaders();
			userTableData();
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateSupplierTabScreen() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.SUPPLIERSTAB.isDisplayed();
			Log.info("Text is :  " + rl.SUPPLIERSTAB.getText());
			Assert.assertTrue(rl.SUPPLIERSTAB.getText() != null);
			rl.SUPPLIERSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			Log.info("Text is :  " + rl.HEADING.getText());
			Assert.assertTrue(rl.HEADING.getText() != null);
			rl.SEARCHBOXSUPPLIER.isDisplayed();
			rl.SEARCHBUTTON.isDisplayed();
			Log.info("Text is :  " + rl.SEARCHBUTTON.getText());
			Assert.assertTrue(rl.SEARCHBUTTON.getText() != null);
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			Log.info("Text is :  " + rl.FILTERBUTTON.getText());
			Assert.assertTrue(rl.FILTERBUTTON.getText() != null);
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateProductTabScreen() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PRODUCTSTAB.isDisplayed();
			Log.info("Text is   : " + rl.PRODUCTSTAB.getText());
			Assert.assertTrue(rl.PRODUCTSTAB.getText() != null);
			rl.PRODUCTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			Log.info("Text is   : " + rl.HEADING.getText());
			Assert.assertTrue(rl.HEADING.getText() != null);
			rl.SEARCPRODUCTINPUTBOX.isDisplayed();
			rl.SEARCHBUTTON.isDisplayed();
			Log.info("Text is   : " + rl.SEARCHBUTTON.getText());
			Assert.assertTrue(rl.SEARCHBUTTON.getText() != null);
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			Log.info("Text is   : " + rl.FILTERBUTTON.getText());
			Assert.assertTrue(rl.FILTERBUTTON.getText() != null);
			rl.ICONCOUNTRYNBANNER.isDisplayed();
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateDownloadUserReport() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.DOWNLOADBUTTON.click();

		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}


	}

	public void validateDownloadProjectReport() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PROJECTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.DOWNLOADBUTTON.click();

		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateDownloadProductReport() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PRODUCTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.DOWNLOADBUTTON.click();
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}


	}

	public void validateDownloadSupplierReport() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.SUPPLIERSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.DOWNLOADBUTTON.click();

		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}


	}

	public void validateDownloadUserReportWithFilterOnStatus() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTSTATUS));
			rl.SELECTSTATUS.isDisplayed();
			rl.SELECTSTATUS.click();
			wait.until(ExpectedConditions.visibilityOf(rl.OPTIONACTIVESTATUS));
			rl.OPTIONACTIVESTATUS.isDisplayed();
			Log.info("Text is :" + rl.OPTIONACTIVESTATUS);
			Assert.assertTrue(rl.OPTIONACTIVESTATUS != null);
			rl.OPTIONACTIVESTATUS.click();
			common.getAPPLYFILTERBUTTON().isDisplayed();
			common.getAPPLYFILTERBUTTON().click();
			rl.DOWNLOADBUTTON.click();

		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateDownloadProjectReportWithFilterOnStatus() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PROJECTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTSTATUS));
			rl.SELECTSTATUS.isDisplayed();
			rl.SELECTSTATUS.click();
			wait.until(ExpectedConditions.visibilityOf(rl.OPTIONINPROGRESSSTATE));
			rl.OPTIONINPROGRESSSTATE.isDisplayed();
			Log.info("Text is : " + rl.OPTIONINPROGRESSSTATE.getText());
			Assert.assertTrue(rl.OPTIONINPROGRESSSTATE.getText() != null);
			rl.OPTIONINPROGRESSSTATE.click();
			common.getAPPLYFILTERBUTTON().isDisplayed();
			common.getAPPLYFILTERBUTTON().click();
			wait.until(ExpectedConditions.visibilityOf(rl.DOWNLOADBUTTON));
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.DOWNLOADBUTTON.click();

		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateDownloadProductReportWithFilterOnStatus() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PRODUCTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTSTATUS));
			rl.SELECTSTATUS.isDisplayed();
			rl.SELECTSTATUS.click();
			wait.until(ExpectedConditions.visibilityOf(rl.OPTIONINPROGRESSSTATUS));
			rl.OPTIONINPROGRESSSTATUS.isDisplayed();
			Log.info("Text is " + rl.OPTIONINPROGRESSSTATUS.getText());
			Assert.assertTrue(rl.OPTIONINPROGRESSSTATUS.getText() != null);
			rl.OPTIONINPROGRESSSTATUS.click();
			common.getAPPLYFILTERBUTTON().isDisplayed();
			common.getAPPLYFILTERBUTTON().click();
			wait.until(ExpectedConditions.visibilityOf(rl.DOWNLOADBUTTON));
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.DOWNLOADBUTTON.click();

		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}


	}

	public void validateDownloadSupplierReportWithFilterOnStatus() throws Exception {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.SUPPLIERSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTSTATUS));
			rl.SELECTSTATUS.isDisplayed();
			rl.SELECTSTATUS.click();
			wait.until(ExpectedConditions.visibilityOf(rl.OPTIONACTIVESTATUS));
			rl.OPTIONACTIVESTATUS.isDisplayed();
			rl.OPTIONACTIVESTATUS.click();
			common.getAPPLYFILTERBUTTON().isDisplayed();
			common.getAPPLYFILTERBUTTON().click();
			wait.until(ExpectedConditions.visibilityOf(rl.DOWNLOADBUTTON));
			rl.DOWNLOADBUTTON.isDisplayed();
			rl.DOWNLOADBUTTON.click();

		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateUserFilterPopupScreen() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.TEAMTAB.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			rl.HEADING.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTUSERGROUPS));
			rl.SELECTUSERGROUPS.isDisplayed();
			Log.info("Text is :   " + rl.SELECTUSERGROUPS.getText());
			rl.SELECTUSERGROUPS.click();
			getAllSelectOptions();
			rl.SELECTUSERGROUPS.click();
			// User Role
			rl.SELECTUSERPROJECT.isDisplayed();
			Log.info("Text is :   " + rl.SELECTUSERPROJECT.getText());
			rl.SELECTUSERPROJECT.click();
			getAllSelectOptions();
			rl.SELECTUSERPROJECT.click();
			Log.info("Text is :   " + rl.SELECTUSERROLE.getText());
			rl.SELECTUSERROLE.isDisplayed();
			rl.SELECTUSERROLE.click();
			getAllSelectOptions();
			rl.SELECTUSERROLE.click();
			Log.info("Text is :   " + rl.SELECTSTATUS.getText());
			rl.SELECTSTATUS.isDisplayed();
			rl.SELECTSTATUS.click();
			getAllStatus();
			rl.SELECTSTATUS.click();
			common.getCLEARFILTERBUTTON().isDisplayed();
			Log.info("Text is :   " + common.getCLEARFILTERBUTTON().getText());
			common.getCANCELFILTERBUTTON().isDisplayed();
			Log.info("Text is :   " + common.getCANCELFILTERBUTTON().getText());
			common.getAPPLYFILTERBUTTON().isDisplayed();
			Log.info("Text is :   " + common.getAPPLYFILTERBUTTON().getText());
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateProjectFilterPopupScreen() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PROJECTSTAB.isDisplayed();
			rl.PROJECTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTSTATUS));
			rl.SELECTSTATUS.isDisplayed();
			Log.info(rl.SELECTSTATUS.getText());
			Assert.assertTrue(rl.SELECTSTATUS.getText() != null);
			rl.SELECTSTATUS.click();
			getAllSelectOptions();
			rl.SELECTSTATUS.click();
			rl.SELECTTEMPLATETYPE.isDisplayed();
			Log.info(rl.SELECTTEMPLATETYPE.getText());
			Assert.assertTrue(rl.SELECTTEMPLATETYPE.getText() != null);
			//// SELECTTEMPLATETYPE.click();
			// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@placeholder='Filter']"))));
			// getAllSelectOptions();
			rl.SELECTTEMPLATETYPE.click();
			rl.SELECTMANAGER.isDisplayed();
			Log.info(rl.SELECTMANAGER.getText());
			Assert.assertTrue(rl.SELECTMANAGER.getText() != null);
			rl.SELECTMANAGER.click();
			getAllSelectOptions();
			rl.SELECTMANAGER.click();
			rl.SELECTCATEGORIES.isDisplayed();
			rl.SELECTCATEGORIES.isDisplayed();
			Log.info(rl.SELECTCATEGORIES.getText());
			Assert.assertTrue(rl.SELECTCATEGORIES.getText() != null);
			rl.SELECTCATEGORIES.click();
			getAllSelectOptions();
			rl.SELECTCATEGORIES.click();
			common.getCANCELFILTERBUTTON().isDisplayed();
			common.getAPPLYFILTERBUTTON().isDisplayed();
			common.getCLEARFILTERBUTTON().isDisplayed();
			common.getCLEARFILTERBUTTON().click();
			jsWait();
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProductFilterPopupScreen() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.PRODUCTSTAB.isDisplayed();
			rl.PRODUCTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTSTATUS));
			rl.SELECTSTATUS.isDisplayed();
			Log.info(rl.SELECTSTATUS.getText());
			Assert.assertTrue(rl.SELECTSTATUS.getText() != null);
			rl.SELECTSTATUS.click();
			getAllSelectOptions();
			rl.SELECTSTATUS.click();
			rl.SELECTSUPPLIER.isDisplayed();
			Log.info(rl.SELECTSUPPLIER.getText());
			Assert.assertTrue(rl.SELECTSUPPLIER.getText() != null);
			rl.SELECTSUPPLIER.click();
			getAllSelectOptions();
			rl.SELECTSUPPLIER.click();
			rl.SELECTINGREDIENTS.isDisplayed();
			Log.info(rl.SELECTINGREDIENTS.getText());
			Assert.assertTrue(rl.SELECTINGREDIENTS.getText() != null);
			rl.SELECTINGREDIENTS.click();
			getAllSelectOptions();
			rl.SELECTINGREDIENTS.click();
			rl.SELECTALLERGENS.isDisplayed();
			Log.info(rl.SELECTALLERGENS.getText());
			Assert.assertTrue(rl.SELECTALLERGENS.getText() != null);
			// SELECTALLERGENS.click();
			// getAllSelectOptions();
			// SELECTALLERGENS.click();
			common.getCLEARFILTERBUTTON().isDisplayed();
			common.getAPPLYFILTERBUTTON().isDisplayed();
			common.getCANCELFILTERBUTTON().isDisplayed();
			common.getCLEARFILTERBUTTON().click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateSupplierFilterPopupScreen() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.SUPPLIERSTAB.isDisplayed();
			rl.SUPPLIERSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(rl.HEADING));
			rl.HEADING.isDisplayed();
			rl.FILTERBUTTON.isDisplayed();
			rl.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(rl.SELECTSTATUS));
			rl.SELECTSTATUS.isDisplayed();
			Log.info(rl.SELECTSTATUS.getText());
			Assert.assertTrue(rl.SELECTSTATUS.getText() != null);
			rl.SELECTSTATUS.click();
			getAllStatus();
			rl.SELECTSTATUS.click();
			rl.SELECTCATEGORIES.isDisplayed();
			Log.info(rl.SELECTCATEGORIES.getText());
			Assert.assertTrue(rl.SELECTCATEGORIES.getText() != null);
			rl.SELECTCATEGORIES.click();
			getAllSelectOptions();
			rl.SELECTCATEGORIES.click();
			rl.SELECTUSERPROJECT.isDisplayed();
			Log.info(rl.SELECTUSERPROJECT.getText());
			Assert.assertTrue(rl.SELECTUSERPROJECT.getText() != null);
			rl.SELECTUSERPROJECT.click();
			getAllSelectOptions();
			rl.SELECTUSERPROJECT.click();
			rl.SELECTCOUNTRY.isDisplayed();
			Log.info(rl.SELECTCOUNTRY.getText());
			Assert.assertTrue(rl.SELECTCOUNTRY.getText() != null);
			rl.SELECTCOUNTRY.click();
			// getAllSelectOptions();
			rl.SELECTCOUNTRY.click();
			rl.SELECTPARENTCOMPANY.isDisplayed();
			Log.info(rl.SELECTPARENTCOMPANY.getText());
			Assert.assertTrue(rl.SELECTPARENTCOMPANY.getText() != null);
			rl.SELECTPARENTCOMPANY.click();
			getAllSelectOptions();
			rl.SELECTPARENTCOMPANY.click();
			common.getCLEARFILTERBUTTON().isDisplayed();
			common.getAPPLYFILTERBUTTON().isDisplayed();
			common.getCANCELFILTERBUTTON().isDisplayed();
			common.getCLEARFILTERBUTTON().click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void userTableHeaders() throws Throwable{
		try {
			WebElement table = driver.findElement(By.xpath("//*[contains(@class,'table table-striped table-hover')]"));
			List<WebElement> rows = table.findElements(By.tagName("th"));
			java.util.Iterator<WebElement> i = rows.iterator();
			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info(row.getText());
				Assert.assertTrue(row.getText() != null);
			}
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void userTableData()throws Throwable {
		try {

			List<WebElement> firstCol = driver
					.findElements(By.xpath("//tr[contains(@class,'clickable')]/following::td"));
			// Log.info("No. of Rows" + firstCol.size());
			java.util.Iterator<WebElement> i = firstCol.iterator();
			while (i.hasNext()) {
				Log.info("----------DATA STARTS-------------------");
				WebElement first = i.next();
				Log.info("Text is :   " + first.getText());
				Assert.assertTrue(first.getText() != null);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true)", first);
				Log.info("----------DATA ENDS-------------------");
			}

		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getAllSelectOptions() throws Throwable{
		try{
		awaitForElement(rl.FILTERSECTIONDROPDOWN);
		List<WebElement> rows = rl.FILTERSECTIONDROPDOWN.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> i = rows.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			Log.info("Text is :" + row.getText());
			Assert.assertTrue(row.getText() != null);
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].scrollIntoView(true);",
			// driver.findElement(By.xpath("//div[@class='race-select-body
			// ps-container ps-theme-default ps-active-y']/ul/li")));
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getAllStatus() throws Throwable{
		try{
		WebElement STATUS = driver.findElement(By.xpath("//div[@class='race-select-body ps']/ul"));
		List<WebElement> rows = STATUS.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> i = rows.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			Log.info("Text is :" + row.getText());
			Assert.assertTrue(row.getText() != null);
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateProjectTableInTeamTab() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.TEAMTAB.isDisplayed();
			rl.HEADINGWORKLOAD.isDisplayed();
			rl.PROJECTSTAB.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			userTableHeaders();
			userTableData();
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateProductTable() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.TEAMTAB.isDisplayed();
			rl.HEADINGWORKLOAD.isDisplayed();
			rl.PRODUCTSTAB.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			userTableHeaders();
			userTableData();
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateSupplierTable() throws Throwable {
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() != 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_REPORTS());
			common.getMENU_REPORTS().click();
			awaitForElement((rl.LINEGRAPH));
			rl.LINEGRAPH.isDisplayed();
			rl.TEAMTAB.isDisplayed();
			rl.HEADINGWORKLOAD.isDisplayed();
			rl.SUPPLIERSTAB.click();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", rl.HEADING);
			userTableHeaders();
			userTableData();
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}
}
