package com.race.qa.pages.functional;

import java.util.List;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.DashboardLocators;
import com.race.qa.util.Log;
import com.sun.javafx.image.ByteToBytePixelConverter;

public class FunctionalDashboardPage extends TestBase {
	WebDriver driver;
	protected CommonLocators common;
	protected DashboardLocators dbLocators;
	protected String STATUS;
	protected int count;
	protected int interCount;
	protected int designcount;
	protected int deliverCount = 0;
	protected String projectname = null;
	protected String Locale;
	protected Integer TotalDeliverables = 0;

	public FunctionalDashboardPage(WebDriver driver) {
		this.driver = driver;
		this.driver = DriverFactory.getDriver();
	}

	public void getProject(String projectname, boolean isSupplier) throws Throwable {
		try {
			dbLocators = new DashboardLocators();
			common = new CommonLocators();

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", common.INPUTSEARCHFILTER);
			Assert.assertTrue(common.INPUTSEARCHFILTER.isDisplayed());
			Log.info(projectname);
			common.INPUTSEARCHFILTER.sendKeys(projectname);
			waitForFilteredData(projectname);
			if (driver.findElements(By.xpath("//div[@class='no-data']")).size() == 1) {
				System.out.println("Project Not found as Running Project");
				common.INPUTSEARCHFILTER.clear();
				System.out.println("Ch3cking in Complete Projecrts");
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						common.PROJECTSTATUSCOMPLETE);
				waitForElementToAppear(common.PROJECTSTATUSCOMPLETE, driver);
				wait.until(ExpectedConditions.elementToBeClickable(common.PROJECTSTATUSCOMPLETE));
				common.PROJECTSTATUSCOMPLETE.click();
				Assert.assertTrue(common.PROJECTSTATUSCOMPLETESELECTED.isDisplayed());
				common.INPUTSEARCHFILTER.sendKeys(projectname);
				common.INPUTSEARCHFILTER_BUTTON.click();
				waitForFilteredData(projectname);
				Thread.sleep(5000);
				if (driver.findElements(By.xpath("//div[@class='no-data']")).size() == 1) {
					Log.info("Projects not assigned to the user as no informed group is used");

				} else if (driver.findElements(By.xpath("//div[@class='no-data']")).size() == 0) {
					Log.info("Project is completed");
					wait.until(ExpectedConditions.elementToBeClickable(dbLocators.PROJECTLINKINTABLE));
					dbLocators.PROJECTLINKINTABLE.click();
					if (isSupplier) {
						waitForElementToAppearWithText(common.SUPPLIERLANDINGPAGE, driver);
						Assert.assertTrue(common.SUPPLIERLANDINGPAGE.isDisplayed());
					} else {
						waitForElementToAppear(dbLocators.TASKNAME, driver);
						Assert.assertTrue(dbLocators.TASKNAME.isDisplayed());
					}
				}
			} else {
				Log.info("Project Not completed");
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.PROJECTLINKINTABLE));
				dbLocators.PROJECTLINKINTABLE.click();
				if (isSupplier) {
					waitForElementToAppearWithText(common.SUPPLIERLANDINGPAGE, driver);
					common.SUPPLIERLANDINGPAGE.click();
					waitForElementToAppearWithText(common.SUPPLIERHEADINGPJOJECTSPACE, driver);
					Assert.assertEquals(common.SUPPLIERHEADINGPJOJECTSPACE.getText(),
							selectPropertiesFile(true, locale, "supplierdetails"));
					Assert.assertTrue(common.SUPPLIERHEADINGPJOJECTSPACE.isDisplayed());
				} else {
					Thread.sleep(2000);
					//waitForElementToAppear(dbLocators.TASKNAME, driver);
					Assert.assertTrue(dbLocators.TASKNAME.isDisplayed());
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  : " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  : "
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public String getTaskName() throws Throwable {
		try {
			dbLocators = new DashboardLocators();
			waitForElementToAppear(dbLocators.TASKNAME, driver);
			Assert.assertTrue(dbLocators.TASKNAME.isDisplayed());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  : " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  : "
							+ e.getClass().getSimpleName());
			throw e;
		}
		return dbLocators.TASKNAME.getText();
	}

	public int getTaskCounts(String taskType) throws Throwable {
		Integer TASKCOUNT = 0;
		try {
			dbLocators = new DashboardLocators();
			// waitForPieChart();
			if (taskType == "opentasks") {
				List<WebElement> BOXOPENTASKCOUNTS = driver
						.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[1]"));
				if (BOXOPENTASKCOUNTS.size() > 0 && Integer.parseInt(dbLocators.BOXOPENTASKCOUNT.getText()) != 0) {
					dbLocators.BOXOPENTASKS.click();
					String OpenTaskCount = dbLocators.TASKSCOUNT.getText();
					OpenTaskCount = OpenTaskCount.replaceAll("[^\\d]", "");
					Assert.assertEquals(dbLocators.BOXOPENTASKCOUNT.getText(), OpenTaskCount);
					Log.info(dbLocators.BOXOPENTASKCOUNT.getText());
					TASKCOUNT = Integer.parseInt(OpenTaskCount);
				} else {
					Log.info("No Open Tasks");
				}
			} else if (taskType == "taskending") {
				List<WebElement> BOXENDINGTASKSS = driver
						.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[2]"));
				if (BOXENDINGTASKSS.size() > 0 && Integer.parseInt(dbLocators.BOXENDINGTASKCOUNT.getText()) != 0) {
					dbLocators.BOXENDINGTASKS.click();
					Log.info("Text is :   " + dbLocators.BOXENDINGTASKCOUNT.getText());
					String EndingTaskCount = dbLocators.TASKSCOUNT.getText();
					Log.info(EndingTaskCount);
					EndingTaskCount = EndingTaskCount.replaceAll("[^\\d]", "");
					Assert.assertEquals(dbLocators.BOXENDINGTASKCOUNT.getText(), EndingTaskCount);
					TASKCOUNT = Integer.parseInt(EndingTaskCount);
				} else {
					Log.info("No Task Ending");
				}
			} else if (taskType == "taskdelayed") {
				List<WebElement> BOXTASKSDELAYEDS = driver
						.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[3]"));
				if (BOXTASKSDELAYEDS.size() > 0 && Integer.parseInt(dbLocators.BOXTASKDELAYEDCOUNT.getText()) != 0) {
					dbLocators.BOXTASKSDELAYED.click();
					Log.info("Text is :   " + dbLocators.BOXTASKDELAYEDCOUNT.getText());
					String DelayedTaskCount = dbLocators.TASKSCOUNT.getText();
					Log.info(DelayedTaskCount);
					DelayedTaskCount = DelayedTaskCount.replaceAll("[^\\d]", "");
					Assert.assertEquals(dbLocators.BOXTASKDELAYEDCOUNT.getText(), DelayedTaskCount);
					TASKCOUNT = Integer.parseInt(DelayedTaskCount);
				} else {
					Log.info("No Task Delayed");
				}
			} else if (taskType == "pendingspecs") {
				List<WebElement> BOXPENDINGCPECS = driver
						.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[4]"));
				if (BOXPENDINGCPECS.size() > 0 && Integer.parseInt(dbLocators.BOXNEWPENDINGSPECCOUNT.getText()) != 0) {
					dbLocators.BOXPENDINGSPECS.click();
					Log.info("Text is :   " + dbLocators.BOXNEWPENDINGSPECCOUNT.getText());
					String PendinSpecCount = dbLocators.TASKSCOUNT.getText();
					Log.info(PendinSpecCount);
					PendinSpecCount = PendinSpecCount.replaceAll("[^\\d]", "");
					Assert.assertEquals(dbLocators.BOXNEWPENDINGSPECCOUNT.getText(), PendinSpecCount);
					TASKCOUNT = Integer.parseInt(PendinSpecCount);
				} else {
					Log.info("No Pending Specs");
				}
			} else if (taskType == "newsubmissions") {
				List<WebElement> NEWSUB = driver
						.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[5]"));
				if (NEWSUB.size() > 0 && Integer.parseInt(dbLocators.BOXNEWSUBMISSIONCOUNT.getText()) != 0) {
					dbLocators.BOXNEWSUBMISSIONS.click();
					Log.info("Text is :   " + dbLocators.BOXNEWSUBMISSIONCOUNT.getText());
					String NewSubmissionCount = dbLocators.TASKSCOUNT.getText();
					Log.info(NewSubmissionCount);
					NewSubmissionCount = NewSubmissionCount.replaceAll("[^\\d]", "");
					Assert.assertEquals(dbLocators.BOXNEWSUBMISSIONCOUNT.getText(), NewSubmissionCount);
					TASKCOUNT = Integer.parseInt(NewSubmissionCount);
				} else {
					Log.info("No Recent Submissions");
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  : " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  : "
							+ e.getClass().getSimpleName());
			throw e;
		}
		return TASKCOUNT;
	}

	public void assignTask(String user) throws Throwable {
		dbLocators = new DashboardLocators();
		String assigneduser = null;
		boolean isassigned;
		try {
			// awaitForElement(dbLocators.DDASSIGNTASK);
			Assert.assertTrue(dbLocators.DDASSIGNTASK.isDisplayed());
			Assert.assertEquals(dbLocators.ASSIGNEDTEXT.getText(),
					(selectPropertiesFile(true, Locale, "noassignment")));
			Log.info("No assignment now");
			isassigned = selectOptionFromDropdown(dbLocators.DDASSIGNTASK, common.CONTAINEROPTION,
					common.getCONTAINER(), common.CONTAINEROPTION, common.getDDSEARCHINPUT(), user);
			if (isassigned == true) {
				assigneduser = driver
						.findElement(
								By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']/div/div[2]"))
						.getText();
				if (assigneduser.equals(configProperities.get("pmname"))) {
					Log.info("Task Assigned to PM user");
				} else if (assigneduser.equals(configProperities.get("colabname"))) {
					Log.info("Task Assigned to Colaborator user");
				} else if (assigneduser.equals(configProperities.get("groupleadname"))) {
					Log.info("Task Assigned to Admin User");
				}
			} else {
				Log.info("Assignment option for the user is not found");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method : " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  : "
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public String currentAssignedUser() throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		dbLocators = new DashboardLocators();
		String assigneduser = null;
		try {
			awaitForElement(driver.findElement(By.xpath("//*[@name='assignedTo']")));
			Assert.assertTrue(driver.findElement(By.xpath("//*[@name='assignedTo']")).isDisplayed());
			assigneduser = driver
					.findElement(
							By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']/div/div[2]"))
					.getText();
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method : " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  : "
							+ e.getClass().getSimpleName());
			throw e;
		}
		return assigneduser;
	}

	public String reAssignTask(String assigneduser, boolean isleadassign, String reassignuser, boolean ismemberassign)
			throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		String reassigneduserfinal = null;
		try {
			if (assigneduser.equals(configProperities.getProperty("5249lead"))) {
				Log.info("Task assigned to Group Lead - Colab user");
				// Group Lead re-assigning task to team member
				if (isleadassign) {
					if (reassignuser.equals(configProperities.getProperty("5249member1"))) {
						selectOptionFromDropdown(
								driver.findElement(
										By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']")),
								common.CONTAINEROPTION, common.getCONTAINER(), common.CONTAINEROPTION,
								common.getDDSEARCHINPUT(), reassignuser);
						reassigneduserfinal = driver
								.findElement(By.xpath("(//span[contains(@class,'text-ellipsis')])[2]")).getText();
					} else if (reassignuser.equals(configProperities.getProperty("5249member2"))) {
						selectOptionFromDropdown(
								driver.findElement(
										By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']")),
								common.CONTAINEROPTION, common.getCONTAINER(), common.SELECTEDOPTION,
								common.getDDSEARCHINPUT(), reassignuser);
						reassigneduserfinal = driver
								.findElement(By.xpath("(//span[contains(@class,'text-ellipsis')])[2]")).getText();
					}
				}
			} else if (assigneduser.equals(configProperities.getProperty("5249member1"))
					|| assigneduser.equals(configProperities.getProperty("5249member2"))) {
				Log.info("Task assigned to Group member");
				// Group Member assigning task back to Team lead
				if (ismemberassign) {
					selectOptionFromDropdown(driver.findElement(By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']")),
							common.CONTAINEROPTION, common.getCONTAINER(), common.CONTAINEROPTION,common.getDDSEARCHINPUT(), reassignuser);
					if (assigneduser.equals(configProperities.getProperty("5249member2"))) {
						reassigneduserfinal = driver.findElement(By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']/div/div[2]"))
								.getText();
					} else {
						reassigneduserfinal = driver.findElement(By.xpath("(//span[contains(@class,'text-ellipsis')])[2]")).getText();
					}
				}
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method : " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  : "
							+ e.getClass().getSimpleName());
			throw e;
		}
		return reassigneduserfinal;
	}

	public void navigateToRatingTabFromTasks(boolean toBeNavigate, String supplierName, String PROJECTNAME)
			throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		int round = 0;
		boolean count;
		int j;
		dbLocators = new DashboardLocators();
		try {
			Assert.assertTrue(dbLocators.TASKLISTSECTION.isDisplayed());
			Search: do {
				count = false;
				List<WebElement> ele1_rate = driver
						.findElements(By.xpath("//h4[contains(@class,'no-margin inline-block text-ellipsis')]"));
				List<WebElement> ele_task = driver
						.findElements(By.xpath("//h4[contains(@class,'no-margin text-ellipsis')]"));
				System.out.println(ele1_rate.size());
				System.out.println(ele_task.size());
				for (j = (1 + round); j <= (ele1_rate.size()); j++) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath(
									"(//h4[contains(@class,'no-margin inline-block text-ellipsis')])[" + j + "]")));
					String FirstText = driver
							.findElement(By.xpath(
									"(//h4[contains(@class,'no-margin inline-block text-ellipsis')])[" + j + "]"))
							.getText();
					if (FirstText.equals(selectPropertiesFile(true, locale, "supplier_message") + " " + supplierName
							+ "/" + " " + PROJECTNAME + " " + "needs rating.")) {
						Log.info("Rating Task  found");
						Thread.sleep(2000);
						break Search;
					} else {
						Log.info("Ratings Task not found");
					}
				}
				if (driver
						.findElements(By
								.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
						.size() > 0) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath(
									"//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button")));
					driver.findElement(
							By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
							.click();
					count = true;
					jsWait();
				}
			} while ((driver
					.findElements(
							By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
					.size() > 0)
					|| (driver
							.findElements(By
									.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
							.size() == 0 && count == true));
			if (toBeNavigate) {
				driver.findElement(
						By.xpath("(//h4[contains(@class,'no-margin inline-block text-ellipsis')])[" + j + "]")).click();
				Thread.sleep(1000);
				Log.info("Navigated to the ratings page");
			} else {
				Log.info("Navigation not selected");
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method : " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  : "
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void searchTaskinTaskSection(boolean toBeNavigate, String projectname) throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		String TASKNAME = "Auto_Task";
		int round = 0;
		boolean count;
		int j;
		dbLocators = new DashboardLocators();
		try {
			awaitForElement(dbLocators.TASKLISTSECTION);
			Assert.assertTrue(dbLocators.TASKLISTSECTION.isDisplayed());
			Search: do {
				count = false;
				List<WebElement> ele1 = driver
						.findElements(By.xpath("//h4[contains(@class,'no-margin inline-block ellipsis')]"));
				List<WebElement> ele = driver.findElements(By.xpath("//h4[contains(@class,'no-margin ellipsis')]"));
				System.out.println(ele1.size());
				System.out.println(ele.size());
				for (j = (1 + round); j <= (ele.size()); j++) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath("(//h4[contains(@class,'no-margin ellipsis')])[" + j + "]")));
					String FirstText = driver
							.findElement(By.xpath("(//h4[contains(@class,'no-margin ellipsis')])[" + j + "]"))
							.getText();
					System.out.println("hhherrerr" + FirstText);
					System.out.println(TASKNAME + " /" + " " + projectname);
					if (FirstText.equals(TASKNAME + " /" + " " + projectname)) {
						Log.info("Task  found");
						if (!driver
								.findElement(By.xpath(
										"(//h4[contains(@class,'no-margin ellipsis')])[" + j + "]/following::h4[1]"))
								.getText().equals("Approval Required")) {
							Log.info("This is for task assignment");
						} else {
							Log.info("This is for task approval");
						}

						break Search;
					} else {
						Log.info("Task not found");
					}
				}
				if (driver
						.findElements(By
								.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
						.size() > 0) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath(
									"//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button")));
					driver.findElement(
							By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
							.click();
					count = true;
					jsWait();
				}
			} while ((driver
					.findElements(
							By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
					.size() > 0)
					|| (driver
							.findElements(By
									.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button"))
							.size() == 0 && count == true));
			if (toBeNavigate) {
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(
						driver.findElement(By.xpath("(//h4[contains(@class,'no-margin ellipsis')])[" + j + "]")))));
				driver.findElement(By.xpath("(//h4[contains(@class,'no-margin ellipsis')])[" + j + "]")).click();
				Thread.sleep(1000);
				Log.info("Navigated to the ratings page");
			} else {
				Log.info("Navigation not selected");
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	/**
	 * This method is used for creating a Project with Locale and boolean value
	 * for supplier to be selected or not. True = Supplier selected.
	 * 
	 * @param Locale
	 * @param withSupplier
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public String createProject(boolean withSupplier) throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		boolean Flag1;
		dbLocators = new DashboardLocators();
		String INPUTPROJECTNAME = (configProperities.getProperty("projectname") + Randomizer.generate(200, 2000));
		try {
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed());
			Assert.assertTrue(dbLocators.getDB_STARTPROJECTBUTTON().isDisplayed());
			dbLocators.getDB_STARTPROJECTBUTTON().click();
			selectOptionFromDropdown(dbLocators.getDB_DDCATEGORY(), common.CONTAINEROPTION, common.getCONTAINER(),
					common.SELECTEDOPTION, common.getDDSEARCHINPUT(), configProperities.getProperty("familyname"));
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='category']//div[2]")).getText(),
					configProperities.getProperty("familyname") + " - " + configProperities.getProperty("familyname"),
					"Option Mismatch due to synchronization error");
			Assert.assertTrue(dbLocators.getDB_DDTEMPLATE().isEnabled());
			selectOptionFromDropdown(dbLocators.getDB_DDTEMPLATE(), common.CONTAINEROPTION, common.getCONTAINER(),
					common.SELECTEDOPTION, common.getDDSEARCHINPUT(), configProperities.getProperty("templatename"));
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='template']//div[2]")).getText(),
					configProperities.getProperty("templatename"));

			dbLocators.getDB_INPUTPROJECTNAME().sendKeys(INPUTPROJECTNAME);
			Log.info("Project Name is  " + INPUTPROJECTNAME);
			selectOptionFromDropdown(dbLocators.getDB_DDTEAMMANEGER(), common.CONTAINEROPTION, common.getCONTAINER(),
					common.SELECTEDOPTION, common.getDDSEARCHINPUT(), configProperities.getProperty("groupleadname"));
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='manager']//div[2]")).getText(),
					configProperities.getProperty("groupleadname"));
			if (withSupplier) {
				selectOptionFromDropdown(dbLocators.getDB_DDSUPPLIER(), common.CONTAINEROPTION, common.getCONTAINER(),
						common.SELECTEDOPTION, common.getDDSEARCHINPUT(),
						configProperities.getProperty("suppliername"));
				Assert.assertTrue(driver.findElement(By.xpath("//*[@name='suppliers']//div[2]")).getText()
						.contains(configProperities.getProperty("suppliername")));
			}
			dbLocators.getDB_BUTTONNEXT().click();
			// In case of Error message for duplicate Name> will Log that
			// Project is
			// not created.
			if (driver.findElements(By.xpath("//div[@class='alert alert-danger ng-star-inserted']")).size() == 0) {
				Flag1 = true;
				Log.info("Project created successfully");
				awaitForElement(dbLocators.TASKNAME);
				// wait.until(ExpectedConditions.visibilityOf(dbLocators.TASKNAME));
				Assert.assertTrue(dbLocators.TASKNAME.isDisplayed());
			} else {
				Flag1 = false;
				Log.info("Project name is duplicate");
				// Assert.assertEquals(Flag, true, "Project Name is already
				// Exists");
				INPUTPROJECTNAME = (configProperities.getProperty("projectname") + Randomizer.generate(200, 2000));
				dbLocators.getDB_INPUTPROJECTNAME().clear();
				Thread.sleep(10);
				dbLocators.getDB_INPUTPROJECTNAME().sendKeys(INPUTPROJECTNAME);
				Log.info("New Project Name is  " + INPUTPROJECTNAME);
				dbLocators.getDB_BUTTONNEXT().click();
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}

		return INPUTPROJECTNAME;
	}

	/**
	 * This function is used to modify tasks with different Parameters like auto
	 * complete, PM Task, Informed Group designs and deliverables. Will add
	 * dependency later on
	 * 
	 * @throws Exception
	 */

	public void waitForFilteredData(String projectname) throws Throwable {
		common = new CommonLocators();
		try {
			@SuppressWarnings("deprecation")
			FluentWait<WebDriver> tempwait = new FluentWait<WebDriver>(driver).withTimeout(1, TimeUnit.MINUTES)
					.pollingEvery(10, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);
			try {
				do {
					Thread.sleep(100);
					System.out.println("\r.");

				} while (driver.findElements(By.xpath("//table[@class='table table-striped bordered']/tbody/tr"))
						.size() != 1);

			} catch (StaleElementReferenceException e) {
				tempwait.until(ExpectedConditions
						.refreshed(ExpectedConditions.elementToBeClickable(dbLocators.PROJECTLINKINTABLE)));

			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void clickOnEditTaskButton() throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			dbLocators = new DashboardLocators();

			awaitForElement(dbLocators.EDITTASKBUTTON);
			// wait.until(ExpectedConditions.elementToBeClickable(dbLocators.EDITTASKBUTTON));
			Assert.assertTrue(dbLocators.EDITTASKBUTTON.isDisplayed());
			Assert.assertEquals(dbLocators.EDITTASKBUTTON.getText(), selectPropertiesFile(true, locale, "edit"));
			wait.until(ExpectedConditions.elementToBeClickable(dbLocators.EDITTASKBUTTON));
			dbLocators.EDITTASKBUTTON.click();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(dbLocators.EDITTASKPHASEDD)));
			// awaitForElement(dbLocators.EDITTASKPHASEDD);
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public boolean isTaskStausButtonDisplayed() {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		boolean flag;
		try {
			// driver.navigate().refresh();
			if (driver.findElements(By.xpath("//div[@class='change-status up race-dropdown']/button")).size() == 0) {
				flag = true;
				Log.info("Task Status button is not displaying");
			} else {
				flag = false;
				Log.info("Task Status button is displaying");
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return flag;
	}

	public String getTaskStatus() throws Throwable {
		try {
			dbLocators = new DashboardLocators();
			waitForElementToAppear(dbLocators.TD_BUTTON, driver);
			Assert.assertTrue(dbLocators.TD_BUTTON.isDisplayed());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return driver.findElement(By.xpath("//div[@class='model-status']")).getText();
	}

	public boolean changeTaskStatus(String taskStatusTobe) throws Throwable {
		boolean flag = false;
		dbLocators = new DashboardLocators();
		try {
			dbLocators = new DashboardLocators();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					dbLocators.CHANGESTATUSBUTTON);
			awaitForElement(dbLocators.CHANGESTATUSBUTTON);
			Assert.assertTrue(dbLocators.CHANGESTATUSBUTTON.isDisplayed());
			Thread.sleep(2000);
			// wait.until(ExpectedConditions.elementToBeClickable(dbLocators.CHANGESTATUSBUTTON));
			dbLocators.CHANGESTATUSBUTTON.click();
			Thread.sleep(2000);

			if (taskStatusTobe == "STARTTASK") {
				System.out.println("join here");
				awaitForElement(dbLocators.OPTIONSTARTTASK);
				Assert.assertTrue(dbLocators.OPTIONSTARTTASK.isDisplayed());
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.OPTIONSTARTTASK));
				dbLocators.OPTIONSTARTTASK.click();
				Thread.sleep(1000);
				driver.navigate().refresh();
				Log.info("Task Started");
				flag = true;
			} else if (taskStatusTobe == "COMPLETETASK") {
				System.out.println("join here");
				awaitForElement(dbLocators.OPTIONCOMPLETETASK);
				Assert.assertTrue(dbLocators.OPTIONCOMPLETETASK.isDisplayed());
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.OPTIONCOMPLETETASK));
				dbLocators.OPTIONCOMPLETETASK.click();
				Thread.sleep(1000);
				driver.navigate().refresh();
				Log.info("Task Completed");

			}
			new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
					.executeScript("return document.readyState").equals("complete"));
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return flag;
	}

	// Notifications
	public int getNotificationCount() throws Throwable {
		int count = 0;
		dbLocators = new DashboardLocators();
		try {
			awaitForElement(dbLocators.DB_NOTIFICATIONBUTTON);
			Assert.assertTrue(dbLocators.DB_NOTIFICATIONBUTTON.isDisplayed());
			awaitForElement(dbLocators.DB_NOTIFICATIONCOUNT);
			Assert.assertTrue(dbLocators.DB_NOTIFICATIONCOUNT.isDisplayed());
			count = Integer.parseInt(dbLocators.DB_NOTIFICATIONCOUNT.getText());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return count;
	}

	public String readNotification(int count) throws Throwable {

		String message = null;
		dbLocators = new DashboardLocators();
		try {
			awaitForElement(dbLocators.DB_NOTIFICATIONBUTTON);
			Assert.assertTrue(dbLocators.DB_NOTIFICATIONBUTTON.isDisplayed());
			if (count == 0) {
				Log.info("Notification tray is empty");
			} else {
				Log.info("Notification tray has messages");
				dbLocators.DB_NOTIFICATIONBUTTON.click();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(common.NOTIFICATIONICONMODEL)));
				Assert.assertTrue(common.NOTIFICATIONICONMODEL.isDisplayed());
				wait.until(
						ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(common.COMPLETEFIRSTNOTIFICATON)));
				message = (common.COMPLETEFIRSTNOTIFICATON.getText());
				dbLocators.DB_NOTIFICATIONBUTTON.click();
				waitForElementToDisappear(driver.findElements(By.xpath("common.NOTIFICATIONICONMODEL")));

			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return message;
	}

	public synchronized void deleteNotificationMessage(boolean all, int count) throws Throwable {
		dbLocators = new DashboardLocators();
		common = new CommonLocators();
		int i = 1;
		try {
			if (count == 0) {
				Log.info("No Notifications");
			} else {
				if (driver
						.findElements(
								By.xpath("//div[@class='notifications-title text-center']/following-sibling::div"))
						.size() == 1) {
					System.out.println("Notification Model open Already");
				} else {
					System.out.println("Not open");
					awaitForElement(dbLocators.DB_NOTIFICATIONBUTTON);
					Assert.assertTrue(dbLocators.DB_NOTIFICATIONBUTTON.isDisplayed());
					dbLocators.DB_NOTIFICATIONBUTTON.click();
					waitForRoundLoadertoDisappear();
				}
				Assert.assertTrue(common.NOTIFICATIONICONMODEL.isDisplayed());
				awaitForElement(common.COMPLETEFIRSTNOTIFICATONTEXT);
				if (all) {
					System.out.println(driver.findElements(By.xpath("//a[contains(@class,'readAll')]")).size() == 1);
					Log.info("Deleting all notifications from the tray");
					if (driver.findElements(By.xpath("//a[contains(@class,'readAll')]")).size() == 1) {
						Log.info("All Notification are not read");
						awaitForElement(common.NOTIFICATION_READALLBUTTON);
						common.NOTIFICATION_READALLBUTTON.click();
						awaitForElement(common.NOTIFICATION_DELETEALLBUTTON);
					} else {
						Log.info("Some Notification read");
						Assert.assertTrue(common.NOTIFICATION_DELETEALLBUTTON.isDisplayed());

					}
					common.NOTIFICATION_DELETEALLBUTTON.click();
					awaitForElement(common.NO_NOTICATIONTEXT);
					Assert.assertTrue(common.NO_NOTICATIONTEXT.isDisplayed());
					Log.info("All Notifications are deleted from the tray");
				} else {
					Log.info("Deleting first notification from the tray");
					moveToElement(common.COMPLETEFIRSTNOTIFICATON, common.REMOVEFIRSTNOTIFICATION);
				}
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.DB_NOTIFICATIONBUTTON));
				dbLocators.DB_NOTIFICATIONBUTTON.click();
				do {
					System.out.println("inside while");
					Thread.sleep(100);
					System.out.println("value od f i is  " + i);
					System.out
							.println(driver
									.findElements(By
											.xpath("//div[@class='notifications-title text-center']/following-sibling::div"))
									.size() == 1 && i == 5);
					i += 1;
				} while (driver
						.findElements(
								By.xpath("//div[@class='notifications-title text-center']/following-sibling::div"))
						.size() == 1 && i == 5);
				driver.navigate().refresh();
				// waitForPieChart();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}

	}

	public boolean messageTypeAnalyse(String message, String taskname, String assigneduser) throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		boolean flag = false;
		try {
			if (message.equals(null)) {
				Log.info("No Messages found");
			} else {
				if (message.equals((selectPropertiesFile(true, locale, "youvebeenassignedtotask") + " " + taskname))) {
					Log.info("Task Assignment Message for user");
					flag = true;
				} else if (message.equals(selectPropertiesFile(true, locale, "task") + " " + taskname + " "
						+ selectPropertiesFile(true, locale, "hasbeenreassignedto") + " " + assigneduser)) {
					Log.info("Project Owner Re-assignment notification");
					flag = true;
				} else if (message
						.equals("Design deliverableLOOP1 approval has been specifically assigned to myPM myPM")) {
					Log.info("Group lead re-assignment message");
					flag = true;
				} else {
					Log.info("No matching message found");
				}
				// test.get().log(Status.INFO, "Completed Test Case
				// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return flag;
	}

	public boolean messageTypeAnalyse(String message, String taskname) throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		boolean flag = false;
		try {
			if (message.equals(null)) {
				Log.info("No Messages found");
			} else {
				message = message.replaceAll("\\s", "");
				if (message.equals((selectPropertiesFile(true, locale, "youvebeenassignedtotask") + taskname)
						.replaceAll("\\s", ""))) {
					Log.info("Task Assignment Message for user");
					flag = true;
				} else if (message.equals(selectPropertiesFile(true, locale, "newdesignversionon") + " " + taskname)) {
					Log.info("Design submission Message");
					flag = true;
				} else if (message.equals(selectPropertiesFile(true, locale, "task") + " " + taskname + " "
						+ selectPropertiesFile(true, locale, "hasbeenstarted"))) {
					Log.info("Task started message for project owner");
					flag = true;
				} else if (message.equals("The design review design1 has been specifically assigned to you")) {
					Log.info("Member self assign notification as approval assignee");
					flag = true;
				} else if (message.equals(selectPropertiesFile(true, locale, "designreview") + " design1" + " "
						+ selectPropertiesFile(true, locale, "needsapproval"))) {
					Log.info("Lead notification for approval required");
					flag = true;
				}

				else {
					Log.info("No matching message found");
				}
				// test.get().log(Status.INFO, "Completed Test Case
				// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}

		return flag;
	}

	public boolean getInformedDropDown() throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		boolean isdropDown = false;
		try {
			if (driver.findElements(By.xpath("(//span[contains(@class,'text-ellipsis')])[2]")).size() == 1) {
				Log.info("No DropDown");
			} else if (driver.findElements(By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']"))
					.size() == 1) {
				Log.info("DropDown");
				isdropDown = true;
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return isdropDown;
	}

	public void getReassignUserlist(boolean isdropdown) throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			if (isdropdown == false) {
				Log.info("No dropdown");
			} else {
				driver.findElement(By.xpath("//*[@class='btn toggle-select assign-collaboration btn-primary']"))
						.click();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(
						driver.findElement(By.xpath("//div[@class='race-select-body ps']/ul/li")))));
				Assert.assertTrue(
						driver.findElement(By.xpath("//div[@class='race-select-body ps']/ul/li")).isDisplayed());
				System.out.println(
						driver.findElements(By.xpath("(//div[@class='race-select-body ps']/ul/li/a/span)")).size());
				Log.info("Number of users displaying"
						+ driver.findElements(By.xpath("(//div[@class='race-select-body ps']/ul/li/a/span)")).size());
				for (int i = 1; i <= driver.findElements(By.xpath("(//div[@class='race-select-body ps']/ul/li/a/span)"))
						.size(); i++) {
					Log.info(driver
							.findElement(By.xpath("(//div[@class='race-select-body ps']/ul/li/a/span)[" + i + "]"))
							.getText());
				}
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public String modifyTask(boolean autoComplete, boolean informed, String groupname) throws Throwable {
		String selectedGroup = null;
		try {
			dbLocators = new DashboardLocators();
			if (autoComplete) {
				waitForElementToAppear(dbLocators.AUTOCOMPLETEYES, driver);
				dbLocators.AUTOCOMPLETEYES.click();
				Log.info("Auto Complete selected");
			} else {
				Log.info("Auto Complete not selected");
			}
			if (informed) {
				if (groupname != null) {
					selectOptionFromDropdown(dbLocators.DB_DDINFORMEDGROUP, dbLocators.INFORMEDCONTAINEROPTION,
							common.getCONTAINER(), dbLocators.INFORMEDDOPTION, common.getDDSEARCHINPUT(), groupname);
					Assert.assertTrue(dbLocators.INFORMEDGROUPDONEBUTOON.isDisplayed());
					dbLocators.INFORMEDGROUPDONEBUTOON.click();
					System.out.println("done button clicked");
					Log.info("Group Selected");

				} else if (groupname == null) {
					Log.info("Please enter a group to select");
				}
			} else {
				Log.info("No Informed group used");
			}
			wait.until(ExpectedConditions.elementToBeClickable(dbLocators.TASKSAVEBUTTON_EDITTASK));
			dbLocators.TASKSAVEBUTTON_EDITTASK.click();
			wait.until(ExpectedConditions
					.invisibilityOf(driver.findElement(By.xpath("//div[@class='modal-footer']/button"))));
			// (driver.findElements(By.xpath("//div[@class='modal-footer']/button")));
			Log.info("Task modification saved");
			if (driver.findElements(By.xpath("//span[contains(@class,'group ')]/preceding::h5[1]")).size() == 0) {
				System.out.println("Informed Group not used");
			} else {
				Assert.assertTrue(dbLocators.INFORMEDGROUPHEADINGPROJECTSPACE.isDisplayed());
				selectedGroup = dbLocators.INFORMEDGROUPVALUEPROJECTSPACE.getText();
			}
			return selectedGroup;

		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void submitDeliverables(Integer totalDeliverables, Integer numberOfDeliverables) throws Throwable {
		dbLocators = new DashboardLocators();
		boolean Flag = false;
		try {
			for (int r = 1; r <= totalDeliverables; r++) {
				int submitBlock = driver
						.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + r + "]")).size();
				if (submitBlock == 0) {
					Log.info("New Submission");
					if ((totalDeliverables - numberOfDeliverables) > 0) {
						System.out.println("All folders will not be submitted");
						totalDeliverables = totalDeliverables - numberOfDeliverables;
					} else if ((totalDeliverables - numberOfDeliverables) == 0) {
						System.out.println("All folders will be submitted");
					} else if ((totalDeliverables - numberOfDeliverables) < 0) {
						Log.info("Invalid combination");
						Flag = true;
						Assert.assertEquals(Flag, false, "Please check the input params");
					}
					for (int d = 1; d <= totalDeliverables; d++) {
						if (driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + d + "]"))
								.isDisplayed()
								&& driver
										.findElements(
												By.xpath("(//div[contains(@class,'submitted-block')])[" + d + "]"))
										.size() == 0) {
							int before_countOfSubmission = Integer.parseInt(driver
									.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + d + "]"))
									.getText());
							Log.info("Before Submisssion count " + before_countOfSubmission);
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + d + "]"))
									.click();
							Thread.sleep(5000);
							awaitForElement(driver.findElement(By.xpath("//span[@class='text-ellipsys']")));
							System.out.println("Modal Displayed");
							waitForElementToAppear(driver.findElement(By.xpath("//label[@class='upload-link']")),
									driver);
							// Assert.assertEquals(driver.findElement(By.xpath("((//div[@class='modal-body']/div)[1]/div/h5)[1]")).getText(),
							// selectPropertiesFile(true, Locale, "category"));
							Assert.assertEquals(driver
									.findElement(By.xpath("((//div[@class='modal-body']/div)[1]/div/h5)[2]")).getText(),
									selectPropertiesFile(true, Locale, "projectmanager"));
							Assert.assertEquals(driver
									.findElement(By.xpath("((//div[@class='modal-body']/div)[1]/div/h5)[3]")).getText(),
									selectPropertiesFile(true, Locale, "type"));
							driver.findElement(By.xpath("//label[@class='upload-link']/input"))
									.sendKeys(System.getProperty("user.dir") + "\\ProjectData\\2.jpg");
							waitForProgressBar(driver.findElement(By.xpath("//div[@class='race-progress']/div")));
							Thread.sleep(10000);
							Assert.assertTrue(
									driver.findElement(By.xpath("//ul[@class='attachment-list ng-star-inserted']/li"))
											.isDisplayed());

							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
									driver.findElement(By.xpath("//button[@class='btn btn-primary-green wide-btn']")));
							driver.findElement(By.xpath("//button[@class='btn btn-primary-green wide-btn']")).click();
							Log.info("Deliverable Submitted");
							Thread.sleep(10000);
						}
						if (totalDeliverables > 1 && Integer.parseInt(
								driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + d + "]"))
										.getText()) == 0) {
							Log.info("Further submission");
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + d + "]"))
									.click();
							awaitForElement(driver.findElement(By.xpath("//span[@class='text-ellipsys']")));
							System.out.println("Modal Displayed");
							Assert.assertEquals(driver
									.findElement(By.xpath("((//div[@class='modal-body']/div)[1]/div/h5)[1]")).getText(),
									selectPropertiesFile(true, Locale, "category"));
							Assert.assertEquals(driver
									.findElement(By.xpath("((//div[@class='modal-body']/div)[1]/div/h5)[2]")).getText(),
									selectPropertiesFile(true, Locale, "projectmanager"));
							Assert.assertEquals(driver
									.findElement(By.xpath("((//div[@class='modal-body']/div)[1]/div/h5)[3]")).getText(),
									selectPropertiesFile(true, Locale, "type"));
							driver.findElement(By.xpath("//label[@class='upload-link']/input"))
									.sendKeys(System.getProperty("user.dir") + "\\ProjectData\\2.jpg");
							waitForProgressBar(driver.findElement(By.xpath("//div[@class='race-progress']/div")));
							Thread.sleep(10000);
							Assert.assertTrue(
									driver.findElement(By.xpath("//ul[@class='attachment-list ng-star-inserted']/li"))
											.isDisplayed());
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
									driver.findElement(By.xpath("//button[@class='btn btn-primary-green wide-btn']")));
							driver.findElement(By.xpath("//button[@class='btn btn-primary-green wide-btn']")).click();
							Log.info("Further Deliverable Submitted");
							Thread.sleep(10000);
						}
						if (driver.findElements(By.xpath("//ul[@class='attachment-list ng-star-inserted']"))
								.size() > 0) {
							Log.info(d + " All required  Deliverable Submitted");
						} else {
							Log.info("No submission");
						}
					}
				} else if (submitBlock > 0) {
					Log.info("Already Submission");
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addDeliverables(Integer numberOfDeliverables, String approvalType, String pathname, String groupname)
			throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		dbLocators = new DashboardLocators();
		boolean Flag = false;
		Integer count = 0;
		deliverCount = 0;
		try {
			Log.info("Inside addDeliverables method");
			for (int i = 1; i <= numberOfDeliverables; i++) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						dbLocators.DB_ADDDELIVERABLE);
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.DB_ADDDELIVERABLE));
				Thread.sleep(1000);
				System.out.println(driver.findElements(By.xpath("(//h5[@class='toggle-collapse'])[1]")).size());
				if (driver.findElements(By.xpath("(//h5[@class='toggle-collapse'])[1]")).size() > 0
						&& driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]")).getText()
								.contains(selectPropertiesFile(true, Locale, "deliverables"))) {
					Log.info("1 Deliverable already added");
					count = getDeliverableCountOnTaskScreen();// 1
				} else {
					Log.info("Deliverable adding first time");
					// count=0
				}
				dbLocators.DB_ADDDELIVERABLE.click();
				Thread.sleep(1000);
				TotalDeliverables = getDeliverableCountOnTaskScreen();
				System.out.println("Herererere    " + TotalDeliverables);// 2
				if (numberOfDeliverables - (count + TotalDeliverables) == 0) {
					Log.info("First Time Addition, No change");
				} else if (numberOfDeliverables - (count + TotalDeliverables) < 0) {
					Log.info("Adding further");
				} else if (((i + count) - numberOfDeliverables) < 0) {
					Flag = true;
					Assert.assertEquals(Flag, false, "This is not a valid case");
				}
				for (int j = 1; j <= i; j++) {
					System.out.println("1" + driver
							.findElement(By
									.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
											+ j + "]//div[@class='select-info-body media-middle']"))
							.getText().equals(selectPropertiesFile(true, Locale, "deliverabletype")));
					System.out.println("2" + driver
							.findElement(By
									.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
											+ (j + 1) + "]//div[@class='select-info-body media-middle']"))
							.getText().equals(selectPropertiesFile(true, Locale, "approvalpath")));
					System.out.println("3" + driver
							.findElement(By
									.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
											+ (j + 2) + "]//div[@class='select-info-body media-middle']"))
							.getText().equals(selectPropertiesFile(true, Locale, "approvalgroups_deliverable")));
					if (driver
							.findElement(By
									.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
											+ j + "]//div[@class='select-info-body media-middle']"))
							.getText().equals(selectPropertiesFile(true, Locale, "deliverabletype"))
							&& driver
									.findElement(By
											.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
													+ (j + 1) + "]//div[@class='select-info-body media-middle']"))
									.getText().equals(selectPropertiesFile(true, Locale, "approvalpath"))
							&& (driver
									.findElement(By
											.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
													+ (j + 2) + "]//div[@class='select-info-body media-middle']"))
									.getText()
									.equals(selectPropertiesFile(true, Locale, "approvalgroups_deliverable")))) {
						Log.info("First Time addition will use this");
						System.out.println("aaaaaaaaaaaaaaaaaaaaa");
						selectOptionFromDropdown(
								driver.findElement(By
										.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
												+ j + "]")),
								common.CONTAINEROPTION, common.getCONTAINER(), common.CONTAINEROPTION,
								common.getDDSEARCHINPUT(), configProperities.getProperty("deliverabletype"));
						Log.info("Deliverable Type Selected");
						Thread.sleep(1000);
						if (approvalType == "NOAPPROVAL" && pathname == null && groupname == null) {
							Log.info("No Approval Type selected");
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("NOAPPROVAL" + j);
						} else if (approvalType == "GROUP" && pathname == null) {
							Log.info("GROUP Type selected");
							selectOptionFromDropdown(
									driver.findElement(By
											.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
													+ (j + 1) + "]")),
									common.CONTAINEROPTION, common.getCONTAINER(), common.SELECTEDOPTION,
									common.getDDSEARCHINPUT(), groupname);
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("GROUP" + j);
						} else if (approvalType == "LOOP" && groupname == null) {
							Log.info("LOOP Type selected");
							selectOptionFromDropdown(
									driver.findElement(By
											.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
													+ j + "]")),
									common.CONTAINEROPTION, common.getCONTAINER(), common.CONTAINEROPTION,
									common.getDDSEARCHINPUT(), pathname);
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("LOOP" + j);
						}
					} else {
						System.out.println("Adding Further");
						Log.info("Filling information in further dropdowns");
						selectOptionFromDropdown(
								driver.findElement(By
										.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
												+ (j + 3) + "]")),
								common.CONTAINEROPTION, common.getCONTAINER(), common.CONTAINEROPTION,
								common.getDDSEARCHINPUT(), configProperities.getProperty("deliverabletype"));
						Thread.sleep(1000);
						Log.info("Deliverable Type Selected");
						if (approvalType == "NOAPPROVAL" && pathname == null && groupname == null) {
							Log.info("No Approval Type selected");
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("NOAPPROVAL" + (j + 1));
						} else if (approvalType == "GROUP" && pathname == null) {
							Log.info("GROUP Type selected");
							selectOptionFromDropdown(
									driver.findElement(By
											.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
													+ (j + 4) + "]")),
									common.CONTAINEROPTION, common.getCONTAINER(), common.SELECTEDOPTION,
									common.getDDSEARCHINPUT(), groupname);
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("GROUP" + (j + 1));
						} else if (approvalType == "LOOP" && groupname == null) {
							Log.info("LOOP Type selected");
							selectOptionFromDropdown(
									driver.findElement(By
											.xpath("(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])["
													+ (j + 3) + "]")),
									common.CONTAINEROPTION, common.getCONTAINER(), common.SELECTEDOPTION,
									common.getDDSEARCHINPUT(), pathname);
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("LOOP" + (j + 1));
						}
					}
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath("//div[@class='modal-footer']/button")));
					wait.until(ExpectedConditions
							.elementToBeClickable(driver.findElement(By.xpath("//div[@class='modal-footer']/button"))));
					Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-footer']/button")).isEnabled());

					driver.findElement(By.xpath("//div[@class='modal-footer']/button")).click();
					Assert.assertFalse(driver.findElement(By.xpath("//div[@class='modal-footer']/button")).isEnabled());
					wait.until(ExpectedConditions
							.invisibilityOf(driver.findElement(By.xpath("//div[@class='modal-footer']/button"))));
					Log.info("Task modification saved");
					Thread.sleep(1000);
					Log.info("Deliverable Saved");
				}
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void addDesigns(String approvalType, Integer numberOfDesigns, String pathname, String groupname)
			throws Throwable {
		dbLocators = new DashboardLocators();
		designcount = 1;
		try {
			for (int i = 1; i <= numberOfDesigns; i++) {
				Log.info("Design selected");
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.DB_ADDDESIGN));
				dbLocators.DB_ADDDESIGN.click();
				awaitForElement(dbLocators.DB_DDDESIGNLOOP);
				if (approvalType == "NOAPPROVAL" && pathname == null && groupname == null) {
					Log.info("No Approval Type selected");
					dbLocators.DB_INPUTDESIGNNAME.sendKeys("NOAPPROVAL" + designcount);
				} else if (approvalType == "GROUP" && pathname == null) {
					selectOptionFromDropdown(dbLocators.DB_DDDESIGNGROUP, common.CONTAINEROPTION, common.getCONTAINER(),
							common.SELECTEDOPTION, common.getDDSEARCHINPUT(), groupname);
					dbLocators.DB_INPUTDESIGNNAME.sendKeys("GROUP" + designcount);
					driver.findElement(By.xpath("//button[@class='btn btn-block btn-primary-green']")).click();
				} else if (approvalType == "LOOP" && groupname == null) {
					selectOptionFromDropdown(dbLocators.DB_DDDESIGNLOOP, common.CONTAINEROPTION, common.getCONTAINER(),
							common.CONTAINEROPTION, common.getDDSEARCHINPUT(), pathname);
					dbLocators.DB_INPUTDESIGNNAME.sendKeys("LOOP" + designcount);
				}
				awaitForElement(dbLocators.DB_DESIGNSAVEBUTTON);
				dbLocators.DB_DESIGNSAVEBUTTON.click();
				wait.until(ExpectedConditions
						.invisibilityOf(driver.findElement(By.xpath("//div[@class='modal-content']"))));
				// Assert.assertFalse(driver.findElement(By.xpath("//div[@class='modal-content']")).isDisplayed());
				designcount = designcount + 1;
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public int getDesignCount() throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		String TotDesign = null;
		try {
			jsWait();
			awaitForElement(driver.findElement(By.xpath("//h5[@class='toggle-collapse']")));

			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(driver.findElement(By.xpath("//h5[@class='toggle-collapse']")).getText());
			while (m.find()) {
				TotDesign = m.group();
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return Integer.parseInt(TotDesign);
	}

	public int getDeliverableCount() throws Throwable {
		String TotDeliver = null;
		int countDeliver = 0;
		try {

			if (driver.findElements(By.xpath("(//h5[@class='toggle-collapse'])[1]")).size() > 0) {
				System.out.println(driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]")).getText());
				Pattern p = Pattern.compile("\\d+");
				Matcher m = p.matcher(driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]")).getText());
				while (m.find()) {
					TotDeliver = m.group();
				}
				countDeliver = Integer.parseInt(TotDeliver);
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			countDeliver = 0;
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return countDeliver;
	}

	public int getDeliverableCountOnTaskScreen() throws Throwable {
		String TotDeliver = null;
		int countDeliver = 0;
		try {

			if (driver.findElements(By.xpath("//div[contains(@class,'deliverable-item ng-star-inserted')]/h5"))
					.size() > 0) {
				System.out.println(
						driver.findElement(By.xpath("//div[contains(@class,'deliverable-item ng-star-inserted')]/h5"))
								.getText());
				Pattern p = Pattern.compile("\\d+");
				Matcher m = p.matcher(
						driver.findElement(By.xpath("//div[contains(@class,'deliverable-item ng-star-inserted')]/h5"))
								.getText());
				while (m.find()) {
					TotDeliver = m.group();
				}
				countDeliver = Integer.parseInt(TotDeliver);
			}

		} catch (Exception e) {
			countDeliver = 0;
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return countDeliver;

	}

	public String getDesignName(int i) throws Throwable {
		String DesignName = null;
		try {

			System.out.println("3333333333333333");
			Thread.sleep(10000);
			DesignName = driver
					.findElement(
							By.xpath("//div[contains(@class,'deliverable-slot')][" + i + "]//span[@class='title']"))
					.getText();
			System.out.println("44444444444444");
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return DesignName;
	}

	public String addDesignFolder(int numberOfFolder, int numberOfDesigns, boolean isSecondAddition) throws Throwable {
		int limit = getDesignCount();
		System.out.println("limit" + limit);
		System.out.println(numberOfDesigns);
		String designname = null;
		try {
			if (isSecondAddition) {
				Log.info("2nd Run");
			} else {
				if ((limit - numberOfDesigns) > 0) {
					limit = limit - numberOfDesigns;
				} else if ((limit - numberOfDesigns) == 0) {

				}
			}
			System.out.println(limit + "gfgfgfgfgf");
			for (int i = 1; i <= limit; i++) {
				System.out.println("hereree 0" + "Time of running   :" + i);
				getDesignName(i);
				System.out.println(numberOfFolder);
				numberOfFolder: for (int m = 1; m <= numberOfFolder; m++) {
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]"))
							.isDisplayed());
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")).click();
					waitForRoundLoadertoDisappear();
					Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-content']")).isDisplayed());
					Thread.sleep(10000);

					// check if design folder already added or not
					System.out.println(
							driver.findElements(By.xpath("//div[@class='inner-container']/button")).size() + " c1");
					System.out
							.println(driver.findElements(By.xpath("//div[@class='upload-container']/div/button")).size()
									+ " c2");
					System.out.println(
							driver.findElements(By.xpath("//button[@class='btn btn-success']")).size() + " c3");
					System.out.println(driver.findElements(By.xpath("//div[@class='overlay']")).size() + " c4");

					if (driver.findElements(By.xpath("//div[@class='upload-container']/div/button")).size() == 1) {
						System.out.println("hereree 1");
						Log.info("Supplier addition");
						Log.info("No designs folder added");
						Assert.assertTrue(driver.findElement(By.xpath("//div[@class='upload-container']/div/button"))
								.isDisplayed());
						wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(
								driver.findElement(By.xpath("//div[@class='upload-container']/div/button")))));
						driver.findElement(By.xpath("//div[@class='upload-container']/div/button")).click();
						System.out.println("now here");
						awaitForElement(driver.findElement(By.xpath("//input[@name='title']")));
						Assert.assertTrue(driver.findElement(By.xpath("//input[@name='title']")).isDisplayed());
						designname = "design" + m;
						driver.findElement(By.xpath("//input[@name='title']")).sendKeys(designname);
						driver.findElement(By.xpath("//*[@name='description']")).sendKeys(designname);
						driver.findElement(By.xpath("//button[@class='btn btn-primary-green btn-width-120']")).click();
						Log.info(m + " Folder added");
						System.out.println("Stop 1");
					} else if (driver.findElements(By.xpath("//div[@class='inner-container']/button")).size() == 1) {
						System.out.println("hereree 2");
						Log.info("Non-Supplier login");
						Log.info("No designs folder added");
						Assert.assertTrue(driver
								.findElement(
										By.xpath("//div[@class='upload-container upload-design-review']/div/button"))
								.isDisplayed());
						wait.until(ExpectedConditions
								.refreshed(ExpectedConditions.elementToBeClickable(driver.findElement(By
										.xpath("//div[@class='upload-container upload-design-review']/div/button")))));
						driver.findElement(By.xpath("//div[@class='upload-container upload-design-review']/div/button"))
								.click();
						System.out.println("now here");
						awaitForElement(driver.findElement(By.xpath("//input[@name='title']")));
						Assert.assertTrue(driver.findElement(By.xpath("//input[@name='title']")).isDisplayed());
						designname = "design" + m;
						driver.findElement(By.xpath("//input[@name='title']")).sendKeys(designname);
						driver.findElement(By.xpath("//*[@name='description']")).sendKeys(designname);
						driver.findElement(By.xpath("//button[@class='btn btn-primary-green btn-width-120']")).click();
						Log.info(m + " Folder added");
					} else if (driver.findElement(By.xpath("//button[@class='btn btn-success']")).isDisplayed()
							&& driver.findElements(By.xpath("//div[@class='overlay']")).size() > 0) {
						System.out.println("hereree 3");
						Log.info("Folder already added");
						int existingFolder = driver.findElements(By.xpath("//div[@class='overlay']")).size();
						if ((numberOfFolder - existingFolder) <= 0) {
							System.out.println("hereree 4");
							Log.info("Required Folders already created");
							driver.findElement(By.xpath("//div[@class='button close']")).click();
							break numberOfFolder;
						} else {
							System.out.println("hereree 5");
							for (int l = 1; l <= (numberOfFolder - existingFolder); l++) {
								System.out.println("hereree 5");
								awaitForElement(driver.findElement(By.xpath("//button[@class='btn btn-success']")));
								driver.findElement(By.xpath("//button[@class='btn btn-success']")).click();
								Thread.sleep(1000);
								awaitForElement(driver.findElement(By.xpath("//input[@name='title']")));
								Assert.assertTrue(driver.findElement(By.xpath("//input[@name='title']")).isDisplayed());
								designname = "design" + (l + 1);
								driver.findElement(By.xpath("//input[@name='title']")).sendKeys(designname);
								driver.findElement(By.xpath("//*[@name='description']")).sendKeys(designname);
								driver.findElement(By.xpath("//button[@class='btn btn-primary-green btn-width-120']"))
										.click();
								Log.info("1 more Folder added");
							}
						}
					}
					System.out.println("Stop- partial");
					Thread.sleep(10000);
					Assert.assertTrue(driver.findElement(By.xpath("//div[@class='button close']")).isEnabled());
					driver.findElement(By.xpath("//div[@class='button close']")).click();
					System.out.println("Stop- final");
					Thread.sleep(10000);
				}

			}

		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return designname;
	}

	public void submitDesign(int numberOfFolders, int numberOfDesigns, boolean isSecond) throws Throwable {
		try {
			int limit = getDesignCount();
			if ((limit - numberOfDesigns) > 0) {
				limit = limit - numberOfDesigns;
			} else if ((limit - numberOfDesigns) == 0) {

			} else if ((limit - numberOfDesigns) < 0) {
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of " + "designs");
			}
			for (int i = 1; i <= limit; i++) {
				getDesignName(i);
				String xpath_1 = "//div[contains(@class,'submitted-block')]";
				if (driver.findElements(By.xpath(xpath_1)).size() == 1 && numberOfFolders == 1 && isSecond == false) {
					Log.info("Design is already submitted");
					break;
				} else if (driver.findElements(By.xpath(xpath_1)).size() == 1 && numberOfFolders == 1
						&& isSecond == true) {
					continue;
				} else if (driver.findElements(By.xpath(xpath_1)).size() == 0 && numberOfFolders == 1) {
					Log.info("Design not submitted and Number of folders is 1");
				} else if (driver.findElements(By.xpath(xpath_1)).size() == 1 && numberOfFolders > 1) {
					Log.info("Other Folders available");
				}
				String xpath_2 = "(//div[@class='info-right flex flex-align-center']/button)[";
				wait.until(ExpectedConditions
						.refreshed(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xpath_2 + i + "]")))));
				Assert.assertTrue(driver.findElement(By.xpath(xpath_2 + i + "]")).isDisplayed());
				wait.until(ExpectedConditions.refreshed(
						ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(xpath_2 + i + "]")))));
				driver.findElement(By.xpath(xpath_2 + i + "]")).click();
				waitForRoundLoadertoDisappear();
				Thread.sleep(10000);
				int folders = driver.findElements(By.xpath("//div[@class='overlay']")).size();
				System.out.println(folders + "hshshskhskhs");
				if ((folders - numberOfFolders) > 0) {
					folders = folders - numberOfFolders;
					Log.info("Number of Folder selcted is " + folders);
				} else if ((folders - numberOfFolders) == 0) {
					Log.info("Number of Folder selcted is " + folders);
				} else if ((folders - numberOfFolders) < 0) {
					boolean Flag = false;
					Assert.assertEquals(Flag, true, "Please enter a valid number");
					break;
				}
				for (int q = 1; q <= folders; q++) {
					if (driver.findElements(By.xpath("(//div[@class='overlay'])[" + q + "]/label")).size() > 0) {
						System.out.println("now here 1");
						Assert.assertEquals(
								driver.findElement(By.xpath("(//div[@class='overlay'])[" + q + "]/label")).getText(),
								selectPropertiesFile(true, locale, "addnewversion"));
						driver.findElement(By.xpath("(//div[@class='overlay'])[" + q + "]/label/input"))
								.sendKeys(System.getProperty("user.dir") + "\\ProjectData\\2.jpg");
						waitForProgressBar(driver
								.findElement(By.xpath("//div[@class='upload-progress ng-star-inserted']/div/div")));
						Thread.sleep(10000);
						// waitForElementToAppear(driver.findElement(By.xpath("//*[@name='description']")),
						// driver);
						// waitForDatainDriopdown(driver.findElements(By.xpath("//*[@name='description']")));
						Assert.assertTrue(driver.findElement(By.xpath("//*[@name='description']")).isDisplayed());
						driver.findElement(By.xpath("//*[@name='description']")).sendKeys("Test");
						driver.findElement(By.xpath("//button[@class='btn btn-primary-green btn-width-120']")).click();
						Log.info("1 Design Added added");
						//// waitForElementToAppearWithText(driver.findElement(By.xpath("(//span[@class='finalize
						//// ng-star-inserted'])[" + q + "]")), driver);
						Thread.sleep(10000);
						Assert.assertTrue(
								driver.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
										.isDisplayed());
						String designStatus = driver
								.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
								.getText();
						designStatus = designStatus.replaceAll("\\d", "").trim();
						Assert.assertTrue(designStatus.contains(selectPropertiesFile(true, locale, "statussubmitted")));

					}
				}
				wait.until(ExpectedConditions.refreshed(ExpectedConditions
						.elementToBeClickable(driver.findElement(By.xpath("//div[@class='button close']")))));
				driver.findElement(By.xpath("//div[@class='button close']")).click();
				wait.until(ExpectedConditions
						.invisibilityOf(driver.findElement(By.xpath("//div[@class='button close']"))));

			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void selectDesignVersion(int numberOfFolders, int numberOfDesigns) throws Throwable {
		try {
			Log.info("Inside selectDesignVersion method");
			int limit = getDesignCount();
			System.out.println(limit);
			if ((limit - numberOfDesigns) > 0) {
				limit = limit - numberOfDesigns;
			} else if ((limit - numberOfDesigns) == 0) {
			} else if ((limit - numberOfDesigns) < 0) {
				Log.info("Please enter a valid number");
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of desifns");
			}
			for (int i = 1; i <= limit; i++) {
				getDesignName(i);
				if (driver.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]"))
						.size() == 0) {
					Log.info("Design not submitted, Please Submit design");
				} else {
					Log.info("Design Submitted");
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]"))
							.isDisplayed());
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")).click();
					Thread.sleep(3000);
					int folders = driver.findElements(By.xpath("//div[@class='overlay']")).size();
					System.out.println(folders + "sslect folder count");
					if (folders - numberOfFolders > 0) {
						folders = folders - numberOfFolders;
					} else if (folders - numberOfFolders == 0) {
					} else if (folders - numberOfFolders < 0) {
						Log.info("Not a valid selection");
					}
					for (int q = 1; q <= folders; q++) {
						if (driver.findElements(By.xpath("(//div[@class='overlay'])[" + q + "]/label")).size() == 0
								&& driver.findElements(By.xpath("(//div[@class='overlay'])[" + q + "]/button"))
										.size() > 0) {
							Assert.assertTrue(driver
									.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
									.isDisplayed());
							if (selectPropertiesFile(true, Locale, "statussubmitted") == null) {
								Assert.assertTrue(driver
										.findElement(
												By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
										.getText().contains("Submitted"));
							} else {
								Assert.assertTrue(driver
										.findElement(
												By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
										.getText().contains(selectPropertiesFile(true, Locale, "statussubmitted")));
							}
							Log.info("Submissions are completed");
							moveToElement(
									driver.findElement(By
											.xpath("(//div[@class='image-box flex flex--center flex--align--center ng-star-inserted'])["
													+ q + "]")),
									driver.findElement(By
											.xpath("(//div[@class='image-box flex flex--center flex--align--center ng-star-inserted'])["
													+ q + "]/following::div[@class='overlay'][1]")));
							wait.until(ExpectedConditions
									.visibilityOf(driver.findElement(By.xpath("//div[@class='modal-header']/span"))));
							if (driver
									.findElements(By.xpath("//button[@class='btn btn-danger select ng-star-inserted']"))
									.size() == 0) {
								Log.info("Design not selected yet");
								Thread.sleep(10000);
								Assert.assertTrue(driver
										.findElement(
												By.xpath("//button[@class='btn btn-success select ng-star-inserted']"))
										.isDisplayed());
								driver.findElement(
										By.xpath("//button[@class='btn btn-success select ng-star-inserted']")).click();
								Log.info("Select version button clicked");
								wait.until(ExpectedConditions.visibilityOf(
										driver.findElement(By.xpath("(//div[@class='modal-content'])[2]"))));
								wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(
										"//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']"))));
								driver.findElement(By
										.xpath("//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']"))
										.click();
								Log.info("Confirmation Given");
								Thread.sleep(10000);
								Assert.assertEquals(driver
										.findElement(
												By.xpath("//button[@class='btn btn-danger select ng-star-inserted']"))
										.getText(), (selectPropertiesFile(true, locale, "removeselection")));
								driver.findElement(By.xpath("(//button[@class='btn btn-input'])[1]")).click();

								Assert.assertTrue(driver.findElement(By.xpath("//button[@class='btn btn-success']"))
										.isDisplayed());
								Log.info("1 Design version selected");

							} else {
								Log.info(q + "Inside first else clause");
								Log.info("Design Already selected");
								driver.findElement(By.xpath("(//button[@class='btn btn-input'])[1]")).click();

								Assert.assertTrue(driver.findElement(By.xpath("//button[@class='btn btn-success']"))
										.isDisplayed());
								Log.info("Moved Back");
							}
						} else {
							Log.info("Already selected design");
						}
					}
					wait.until(ExpectedConditions.refreshed(ExpectedConditions
							.elementToBeClickable(driver.findElement(By.xpath("//div[@class='button close']")))));
					driver.findElement(By.xpath("//div[@class='button close']")).click();
					System.out.println("struck by 1");
					// waitForElementToDisappear(driver.findElements(By.xpath("//div[@class='modal-content']")));
					wait.until(ExpectedConditions
							.invisibilityOf(driver.findElement(By.xpath("//div[@class='modal-content']"))));
					System.out.println("struck by 2");
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void approvalActionDeliverable(String action, int numberOfDelivers, boolean isSupplier) throws Throwable {
		Log.info("Inside approvalActionDeliverable method");

		try {
			if (isSupplier) {
				Assert.assertTrue(driver
						.findElement(
								By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.isDisplayed());
				driver.findElement(By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.click();
				Thread.sleep(1000);
			}
			int limit = getDeliverableCount();
			if ((limit - numberOfDelivers) > 0) {
				limit = limit - numberOfDelivers;
			} else if ((limit - numberOfDelivers) == 0) {
			} else if ((limit - numberOfDelivers) < 0) {
				Log.info("Please enter a valid number");
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of desifns");
			}
			for (int i = 1; i <= limit; i++) {
				if (driver.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]"))
						.size() == 0) {
					Log.info("Deliverable not submitted, Please Submit Deliverable");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "finalized"))) {
					Log.info("Deliverable Finalized");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "statussubmitted"))) {
					Log.info("Deliverable Submitted");

					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")))));
					Assert.assertTrue(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]"))
									.isDisplayed());
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")))));
					int noOfSubmits = Integer.parseInt(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]"))
									.getText());
					driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")).click();
					Thread.sleep(5000);
					outerForLoop: for (int e = 1; e <= noOfSubmits; e++) {
						awaitForElement(driver.findElement(By.xpath("//div[@class='ng-star-inserted']")));
						Assert.assertTrue(
								driver.findElement(By.xpath("//div[@class='ng-star-inserted']")).isDisplayed());
						int noVersion = driver.findElements(By.xpath("//div[@class='ng-star-inserted']")).size();
						Log.info("Number of versions are  " + noVersion);

						if (driver.findElement(By.xpath("(//div[@class='ng-star-inserted']/h4)[" + e + "]")).getText()
								.equals(selectPropertiesFile(true, Locale, "latestsubmission"))) {
							Log.info("Latest Submission block Found");

							if (driver
									.findElements(By
											.xpath("//div[@class='flexStyle deliverable-path approval-container ng-star-inserted']"))
									.size() > 0) {
								System.out.println("Approval Loop is used");
								int steps = driver.findElements(By.xpath("//div[@class='stage-body']")).size();
								System.out.println("Total Number of Steps for Approval  " + steps);
								System.out.println("Approval Path used is  " + driver
										.findElement(By.xpath("//h5[contains(@class,'text-ellipsis')]")).getText());

								innerForLoop: for (int f = 1; f <= steps; f++) {
									((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
											driver.findElement(By.xpath("(//div[@class='stage-body'])[" + (f) + "]")));
									// check for Status of Step
									if (driver.findElement(By.xpath("(//div[@class='marker cursor'])[" + f + "]"))
											.getCssValue("border-color").equals("rgb(63, 197, 157)")) {
										String ApprovalStepName = driver
												.findElement(By.xpath("(//div[@class='stage-body']/h3)[" + f + "]"))
												.getText();
										System.out.println("Current Approval Step name is   :" + ApprovalStepName);

										if (action == "APPROVE") {
											((JavascriptExecutor) driver).executeScript(
													"arguments[0].scrollIntoView(true);",
													driver.findElement(By.xpath("//img[@alt='Finalize']")));
											driver.findElement(By.xpath("//img[@alt='Finalize']")).click();
											awaitForElement(
													driver.findElement(By.xpath("//button[@class='btn btn-danger']")));
											driver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
											Thread.sleep(5000);
											Assert.assertTrue(driver
													.findElement(
															By.xpath("(//div[@class='status-log'])[1]/div/span[1]"))
													.getText().contains("Step Approved:"));
											Log.info("Item has been Approved");
											break innerForLoop;
										} else if (action == "UPDATE") {
											Log.info("Request update");
											((JavascriptExecutor) driver).executeScript(
													"arguments[0].scrollIntoView(true);",
													driver.findElement(By.xpath("//img[@alt='Request Update']")));
											driver.findElement(By.xpath("//img[@alt='Request Update']")).click();
											awaitForElement(
													driver.findElement(By.xpath("//button[@class='btn btn-danger']")));
											driver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
											Thread.sleep(5000);
											Assert.assertTrue(driver
													.findElement(
															By.xpath("(//div[@class='status-log'])[1]/div/span[1]"))
													.getText().contains("Request Updated:"));
											Log.info("Item has been requested update");
											break innerForLoop;
										} else if (action == "null") {
											Log.info("Admin will not take any action");
										}
									} else if (driver.findElement(By.xpath("//img[@alt='Step Approved'][" + f + "]"))
											.isDisplayed()) {
										System.out.println("Step is approved");
										continue innerForLoop;
									} else if (driver.findElement(By.xpath("//img[@alt='Request Update'][" + f + "]"))
											.isDisplayed()) {
										System.out.println("Step is Request Updated");
										continue innerForLoop;
									}
								}
								System.out.println("Saving");
								driver.findElement(By.xpath("//button[@class='btn btn-primary-green wide-btn']"))
										.click();
								Thread.sleep(5000);
							} else {
								System.out.println("Approval Group is Used");
							}

						} else if (driver
								.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + e + "]"))
								.getText().contains("Update requested")) {
							System.out.println("Request Updated as Folder Status");
							break outerForLoop;
						} else if (driver
								.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + e + "]"))
								.getText().contains("Finalized")) {
							System.out.println("Finalized as Folder Status");
							break outerForLoop;
						}
					}

				}
			}

		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void assignApprovalUserDeliverable(String user, String adminAction, int numberOfDelivers, boolean selfAssign,
			boolean isSupplier, String leadUser) throws Throwable {

		Log.info("Inside assignApprovalUserDeliverable method");
		try {
			if (isSupplier) {
				Assert.assertTrue(driver
						.findElement(
								By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.isDisplayed());
				driver.findElement(By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.click();
				Thread.sleep(1000);
			}
			int limit = getDeliverableCount();
			if ((limit - numberOfDelivers) > 0) {
				limit = limit - numberOfDelivers;
			} else if ((limit - numberOfDelivers) == 0) {
			} else if ((limit - numberOfDelivers) < 0) {
				Log.info("Please enter a valid number");
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of desifns");
			}
			outerForLoop: for (int i = 1; i <= limit; i++) {
				if (driver.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]"))
						.size() == 0) {
					Log.info("Deliverable not submitted, Please Submit Deliverable");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "finalized"))) {
					Log.info("Deliverable Finalized");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "statussubmitted"))) {
					Log.info("Deliverable Submitted");
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")))));
					Assert.assertTrue(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]"))
									.isDisplayed());
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")))));
					int noOfSubmits = Integer.parseInt(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]"))
									.getText());
					driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")).click();
					Thread.sleep(5000);
					System.out.println("Total Submissions are   :" + noOfSubmits);
					for (int e = 1; e <= noOfSubmits; e++) {
						Assert.assertTrue(
								driver.findElement(By.xpath("//div[@class='ng-star-inserted']")).isDisplayed());
						int noVersion = driver.findElements(By.xpath("//div[@class='ng-star-inserted']")).size();
						Log.info("Number of versions are  " + noVersion);

						if (driver.findElement(By.xpath("(//div[@class='ng-star-inserted']/h4)[" + e + "]")).getText()
								.equals(selectPropertiesFile(true, Locale, "latestsubmission"))) {
							Log.info("Latest Submission block Found");
							if (driver
									.findElements(By
											.xpath("//div[@class='flexStyle deliverable-path approval-container ng-star-inserted']"))
									.size() > 0) {
								Log.info("Loop is Used");
								int steps = driver.findElements(By.xpath("//div[@class='stage-body']")).size();
								System.out.println("Total Steps are  : " + steps);
								innerForLoop: for (int f = 1; f <= 1; f++) {
									Log.info("Inside for loop" + f);
									((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
											driver.findElement(By.xpath("(//div[@class='stage-body'])[" + (f) + "]")));
									if (driver.findElement(By.xpath(
											"(//div[@class='wrap not-started ng-star-inserted']/span)[" + f + "]"))
											.getText().contains("Not Started")) {
										System.out.println("Task in Not started, can be assigned");
									} else {
										System.out.println("No Match");
									}
									if (driver
											.findElement(By
													.xpath("//div[@class='race-select-container form-select']/button"))
											.getCssValue("background-color").contains("rgba(46, 61, 82, 1)")) {
										System.out.println("Earlier Assignments Happened");
										String currentUser = driver
												.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
												.getText();
										System.out.println(currentUser);
										Log.info("The current Assigned User is Admin  User :" + currentUser);
									} else {
										System.out.println("First Time Assignment");
										String currentUser = driver
												.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
												.getText();
										Assert.assertEquals(currentUser, leadUser);
										Log.info("The current Assigned User is Admin  User");
									}
									if (user == "PM") {
										selectOptionFromDropdown(
												driver.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button/div")),
												common.CONTAINEROPTION, common.getCONTAINER(), common.CONTAINEROPTION,
												common.getDDSEARCHINPUT(), configProperities.getProperty("pmname"));
										Thread.sleep(2000);
										System.out.println("herererer    " + driver
												.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button"))
												.getCssValue("background-color"));
										Assert.assertEquals(driver
												.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
												.getText(), ("myPM myPM"));
										driver.findElement(
												By.xpath("//button[@class='btn btn-primary-green wide-btn']")).click();
										Thread.sleep(5000);
										Log.info("Approval Assigned to PM");
										break innerForLoop;
									} else if (user == "COLAB") {
										selectOptionFromDropdown(
												driver.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button/div")),
												common.CONTAINEROPTION, common.getCONTAINER(), common.CONTAINEROPTION,
												common.getDDSEARCHINPUT(), configProperities.getProperty("colabname"));
										Thread.sleep(2000);
										Assert.assertEquals(driver
												.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
												.getText(), "my collab my collab");
										driver.findElement(
												By.xpath("//button[@class='btn btn-primary-green wide-btn']")).click();
										Thread.sleep(5000);
										Log.info("Approval Assigned to Colaborator");
										break innerForLoop;
									} else if (user == "ADMIN") {
										Log.info("Approval is already assigned to Admin");
										if (adminAction == "APPROVE") {
											driver.findElement(By.xpath("//img[@alt='Finalize']")).click();
											awaitForElement(
													driver.findElement(By.xpath("//button[@class='btn btn-danger']")));
											driver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
											Thread.sleep(5000);
											Assert.assertTrue(
													driver.findElement(By.xpath("//img[@alt='Step Approved']"))
															.isDisplayed());
											driver.findElement(
													By.xpath("//button[@class='btn btn-primary-green wide-btn']"))
													.click();
											Thread.sleep(2000);
											break innerForLoop;
										} else if (adminAction == "UPDATE") {
											Log.info("Request update");
											driver.findElement(By.xpath("//img[@alt='Request Update']")).click();
											awaitForElement(
													driver.findElement(By.xpath("//button[@class='btn btn-danger']")));
											driver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
											Thread.sleep(5000);
											Assert.assertTrue(
													driver.findElement(By.xpath("//img[@alt='Request Update']"))
															.isDisplayed());
											driver.findElement(
													By.xpath("//button[@class='btn btn-primary-green wide-btn']"))
													.click();
											Thread.sleep(2000);
											break innerForLoop;
										} else if (adminAction == "null") {
											Log.info("Admin will not take any action");
											break innerForLoop;
										}
									}

								}
								System.out.println("Out of inner loop");
							}
						} else {
							System.out.println("Not selected");
						}
					}

				}
				/*
				 * System.out.println("out of outer loop");
				 * ((JavascriptExecutor)
				 * driver).executeScript("arguments[0].scrollIntoView(true);",
				 * driver.findElement(By.xpath("//div[@class='button close']"
				 * ))); awaitForElement(driver.findElement(By.xpath(
				 * "//div[@class='button close']")));
				 * driver.findElement(By.xpath("//div[@class='button close']"
				 * )).click(); Thread.sleep(2000);
				 */
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void approvalGroupDeliver(int numberOfDelivers, boolean approve, boolean isSupplier) throws Throwable {
		try {
			if (isSupplier) {
				Assert.assertTrue(driver
						.findElement(
								By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.isDisplayed());
				driver.findElement(By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.click();
			}
			int limit = getDeliverableCount();
			if ((limit - numberOfDelivers) > 0) {
				limit = limit - numberOfDelivers;
			} else if ((limit - numberOfDelivers) == 0) {
			} else if ((limit - numberOfDelivers) < 0) {
				Log.info("Please enter a valid number");
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of deliverables");
			}
			for (int i = 1; i <= limit; i++) {
				if (driver.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]"))
						.size() == 0) {
					Log.info("Deliverable not submitted, Please Submit Deliverable");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "finalized"))) {
					Log.info("Deliverable Finalized");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "statussubmitted"))) {
					Log.info("Deliverable Submitted");
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")))));
					Assert.assertTrue(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]"))
									.isDisplayed());
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(
							driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")))));
					int noOfSubmits = driver
							.findElements(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")).size();
					driver.findElement(By.xpath("(//*[@alt='Folder']/following-sibling::span)[" + i + "]")).click();
					for (int e = 1; e <= noOfSubmits; e++) {
						awaitForElement(driver.findElement(By.xpath("//div[@class='ng-star-inserted']")));
						Assert.assertTrue(
								driver.findElement(By.xpath("//div[@class='ng-star-inserted']")).isDisplayed());
						int noVersion = driver.findElements(By.xpath("//div[@class='ng-star-inserted']")).size();
						Log.info("Number of versions are  " + noVersion);
						// Filtering the Latest Submission and
						// Finalizing/Updating it.
						if (driver.findElement(By.xpath("(//div[@class='ng-star-inserted']/h4)[" + e + "]")).getText()
								.equals(selectPropertiesFile(true, Locale, "latestsubmission"))) {
							Log.info("Latest Submission block Found");
							if (approve) {
								driver.findElement(By.xpath("//a/img[@alt='Finalize']")).click();
								awaitForElement(driver.findElement(
										By.xpath("(//div[@class='deliverable padding-xs pointer']/div/p/span)[1]")));
								Assert.assertEquals(driver
										.findElement(By
												.xpath("(//div[@class='deliverable padding-xs pointer']/div/p/span)[1]"))
										.getText(), selectPropertiesFile(true, Locale, "finalizedeliverable?"));
								Assert.assertTrue(
										driver.findElement(By.xpath("//span[@class='pull-right']")).isDisplayed()
												&& driver.findElement(By.xpath("//button[@class='btn btn-danger']"))
														.isDisplayed()
												&& driver.findElement(By.xpath("//button[@class='btn btn-input']"))
														.isDisplayed());
								Assert.assertEquals(
										driver.findElement(By.xpath("//button[@class='btn btn-danger']")).getText(),
										selectPropertiesFile(true, Locale, "finalize"));
								Assert.assertEquals(
										driver.findElement(By.xpath("//button[@class='btn btn-input']")).getText(),
										selectPropertiesFile(true, Locale, "cancel"));
								driver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
								Log.info("Finalize button clicked");
								waitForElementToAppear(
										driver.findElement(By.xpath("//div[@class='deliverable padding-xs pointer']")),
										driver);
								Assert.assertTrue(
										driver.findElement(By.xpath("//div[@class='deliverable padding-xs pointer']"))
												.isDisplayed());
							} else {
								Log.info("Request update");
								driver.findElement(By.xpath("//a/img[@alt='Request Update']")).click();
								awaitForElement(driver.findElement(
										By.xpath("(//div[@class='deliverable padding-xs pointer']/div/p/span)[1]")));
								Assert.assertEquals(driver
										.findElement(By
												.xpath("(//div[@class='deliverable padding-xs pointer']/div/p/span)[1]"))
										.getText(), selectPropertiesFile(true, Locale, "requestupdate?"));
								Assert.assertTrue(
										driver.findElement(By.xpath("//span[@class='pull-right']")).isDisplayed()
												&& driver.findElement(By.xpath("//button[@class='btn btn-danger']"))
														.isDisplayed()
												&& driver.findElement(By.xpath("//button[@class='btn btn-input']"))
														.isDisplayed());
								Assert.assertEquals(
										driver.findElement(By.xpath("//button[@class='btn btn-danger']")).getText(),
										selectPropertiesFile(true, Locale, "requests"));
								Assert.assertEquals(
										driver.findElement(By.xpath("//button[@class='btn btn-input']")).getText(),
										selectPropertiesFile(true, Locale, "cancel"));
								driver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
								Log.info("Request Update button clicked");
								waitForElementToAppear(
										driver.findElement(By
												.xpath("//div[@class='//div[@class='deliverable padding-xs pointer']")),
										driver);
								Assert.assertTrue(
										driver.findElement(By.xpath("//div[@class='deliverable padding-xs pointer']"))
												.isDisplayed());
								Assert.assertTrue(
										driver.findElements(By.xpath("//span[@class='pull-right']")).size() == 0);
							}
						} else {
							Log.info("Latest Block Not found");
						}
					}
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath("//button[@class='btn btn-primary-green wide-btn']")));
					driver.findElement(By.xpath("//button[@class='btn btn-primary-green wide-btn']")).click();
					Thread.sleep(10000);
					Assert.assertTrue(driver
							.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
							.getText().contains(selectPropertiesFile(true, Locale, "finalized"))
							|| driver
									.findElement(
											By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
									.getText().contains(selectPropertiesFile(true, Locale, "updaterequested")));
					Log.info("Deliverable Action Taken");
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void approvalGroup(int numberOfFolders, int numberOfDesigns, boolean approve, boolean isSupplier)
			throws Throwable {
		Log.info("Inside approvalGruop method");
		try {
			if (isSupplier) {
				System.out.println("Supplier");
				Assert.assertTrue(driver
						.findElement(
								By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.isDisplayed());
				driver.findElement(By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.click();
			}
			int limit = getDesignCount();
			if ((limit - numberOfDesigns) > 0) {
				limit = limit - numberOfDesigns;
			} else if ((limit - numberOfDesigns) == 0) {
			} else if ((limit - numberOfDesigns) < 0) {
				Log.info("Please enter a valid number");
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of designs");
			}
			for (int i = 1; i <= limit; i++) {
				getDesignName(i);
				System.out.println(
						driver.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
								.getText());
				System.out.println("inside first for loop");
				if (driver.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]"))
						.size() == 0) {
					Log.info("Design not submitted, Please Submit design");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "finalized"))) {
					Log.info("Design Finalized");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "statussubmitted"))) {
					Log.info("Design Submitted");
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]"))
							.isDisplayed());
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")).click();
					System.out.println("hehehhehe");
					Thread.sleep(5000);
					wait.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath("//div[@class='modal-header']/span"))));
					int folders = driver.findElements(By.xpath("//div[@class='overlay']")).size();
					System.out.println(folders);
					for (int q = 1; q <= folders; q++) {
						if (driver.findElements(By.xpath("(//div[@class='overlay'])[" + q + "]/label")).size() == 0
								&& driver.findElements(By.xpath("(//div[@class='overlay'])[" + q + "]/button"))
										.size() > 0) {
							Log.info("Folders Exisits with submisiions and view design Tag");

							if (driver.findElement(By.xpath("(//div[@class='overlay']/following::div[1])[" + q + "]"))
									.getCssValue("background-color").equals("rgba(63, 197, 157, 1)")
									|| driver
											.findElements(By.xpath("(//img[@class='fnl ng-star-inserted'])[" + q + "]"))
											.size() < 0) {
								Log.info("Approval Group/ Loop used");
								driver.findElement(By.xpath("(//div[@class='overlay']/following::div[1])[" + q + "]"))
										.click();
								if (driver.findElements(By.xpath("//div[@class='deliverable-slot']")).size() > 0) {
									Log.info("Group Used");
									if (driver
											.findElements(By.xpath("//div[@class='actions choices ng-star-inserted']"))
											.size() > 0) {
										Log.info("Approval Actions are available ");
										Assert.assertTrue(driver
												.findElement(By
														.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Finalize']"))
												.isDisplayed());
										Assert.assertTrue(driver
												.findElement(By
														.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Request Update']"))
												.isDisplayed());
										if (approve) {
											System.out.println("inside approve");
											driver.findElement(By
													.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Finalize']"))
													.click();
											awaitForElement(driver.findElement(By.xpath(
													"//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']")));
											driver.findElement(By
													.xpath("//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']"))
													.click();
											Thread.sleep(2000);
											// awaitForElement(driver.findElement(By.xpath("//div[@class='statuses
											// approved ng-star-inserted']")));
											Assert.assertTrue(driver
													.findElement(By
															.xpath("//div[@class='statuses approved ng-star-inserted']"))
													.getCssValue("background-color").equals("rgba(63, 197, 157, 1)"));
											String designStatus = driver
													.findElement(By.xpath(
															"(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
													.getText();
											System.out.println("I am hereree" + designStatus);
											Assert.assertTrue(designStatus
													.contains(selectPropertiesFile(true, Locale, "finalized")));
											Log.info("Design Finalized");
											Assert.assertTrue(
													driver.findElement(By.xpath("//img[@class='fnl ng-star-inserted']"))
															.isDisplayed());
										} else {
											Log.info("Request update");
											driver.findElement(By
													.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Request Update']"))
													.click();
											awaitForElement(driver.findElement(By.xpath(
													"//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']")));
											driver.findElement(By
													.xpath("//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']"))
													.click();
											String designStatus = driver
													.findElement(By.xpath(
															"(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
													.getText();
											Assert.assertTrue(designStatus
													.contains(selectPropertiesFile(true, Locale, "updaterequested"))
													|| designStatus.contains("Update requested"));
											Log.info("Design Update Requested");
											Assert.assertTrue(
													driver.findElement(By.xpath("//img[@class='fnl ng-star-inserted']"))
															.isDisplayed());
										}
										Assert.assertTrue(common.getCLOSEBUTTON().isDisplayed());
										common.getCLOSEBUTTON().click();
										Thread.sleep(3000);

									}
								} else {
									Log.info("Approval Loop is used");
									Assert.assertEquals(driver
											.findElement(By
													.xpath("//div[@class='approval-container bg-white padding-tools']/h3"))
											.getText(), configProperities.getProperty("pathname"));
									Assert.assertTrue(driver.findElement(By.xpath("//div[@class='stages-list flex']"))
											.isDisplayed());
									int numberOfSteps = driver.findElements(By.xpath("(//div[@class='marker cursor'])"))
											.size();
									Log.info("Total Steps are :" + numberOfSteps);
									// ("By Default the first stepp will be
									// green");
									Assert.assertEquals(
											driver.findElement(By.xpath("(//div[@class='marker cursor'])[1]"))
													.getCssValue("background-color"),
											"rgba(63, 197, 157, 1)");
									Log.info("First Step started");
									if (driver.findElement(By.xpath("(//div[@class='marker cursor'])[2]"))
											.getCssValue("background-color").equals("rgba(255, 185, 0, 1)")) {
										Log.info("Second Step not started");
										driver.findElement(
												By.xpath("(//div[@class='deliverable-slot ng-star-inserted'])[1]"))
												.isDisplayed();
										Assert.assertTrue(driver
												.findElement(By
														.xpath("//button[@class='btn toggle-select btn-input form-control']/div"))
												.isDisplayed());
										Assert.assertEquals(driver
												.findElement(By
														.xpath("//button[@class='btn toggle-select btn-input form-control']/div/div[2]"))
												.getText(), configProperities.getProperty("groupleadname"));
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void getNumberOfApprovalSteps() throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		int completedSteps = 1;
		try {
			awaitForElement(driver.findElement(By.xpath("//div[@class='stages-list flex']/div")));
			int steps = driver.findElements(By.xpath("//div[@class='stages-list flex']/div")).size();
			int actionableSteps = driver
					.findElements(By.xpath("//div[@class='row margin-top-md ng-star-inserted']/div/div")).size();
			int totalSteps = steps - 1;
			Log.info("steps" + totalSteps);
			Assert.assertEquals(actionableSteps, totalSteps);
			for (int f = 1; f <= steps; f++) {
				if (driver.findElement(By.xpath("//div[@class='marker cursor']")).getCssValue("background-color")
						.equals("GREEN")) {
					completedSteps = (completedSteps + 1);
				}
			}
			Log.info("completed steps are" + completedSteps);
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void approvalAction(int numberOfDesigns, int numberOfFolders, String action, boolean isSupplier)
			throws Throwable {
		Log.info("Inside approvalAction method");
		try {
			if (isSupplier) {
				Assert.assertTrue(driver
						.findElement(
								By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.isDisplayed());
				driver.findElement(By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.click();
				Thread.sleep(1000);
			}
			int limit = getDesignCount();
			if ((limit - numberOfDesigns) > 0) {
				limit = limit - numberOfDesigns;
			} else if ((limit - numberOfDesigns) == 0) {
			} else if ((limit - numberOfDesigns) < 0) {
				Log.info("Please enter a valid number");
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of designs");
			}
			for (int i = 1; i <= limit; i++) {
				getDesignName(i);

				if (driver.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]"))
						.size() == 0) {
					Log.info("Design not submitted, Please Submit design");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "finalized"))) {
					Log.info("Design Finalized");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "statussubmitted"))) {
					Log.info("Design Submitted");
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]"))
							.isDisplayed());
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")).click();
					Thread.sleep(5000);
					int folders = driver.findElements(By.xpath("//div[@class='overlay']")).size();
					System.out.println(folders + "Number of Folders");
					outerForLoop: for (int q = 1; q <= folders; q++) {
						System.out.println("started with folder number   " + q);

						if (driver.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
								.isDisplayed()) {
							Log.info("Design is Submitted");
							if (driver.findElement(By.xpath("(//div[@class='overlay']/following::div[1])[" + q + "]"))
									.getCssValue("background-color").equals("rgba(63, 197, 157, 1)")) {
								Log.info("Approval Group / Loop used");
								driver.findElement(By.xpath("(//div[@class='overlay']/following::div[1])[" + q + "]"))
										.click();
								Thread.sleep(5000);
								if (driver
										.findElements(
												By.xpath("//div[@class='approval-container bg-white padding-tools']"))
										.size() > 0) {
									System.out.println("Approval Loop is used");
									int steps = driver.findElements(By.xpath("//div[@class='stage-body']")).size();
									System.out.println("Total Number of Steps for Approval  " + steps);
									System.out.println("Approval Path used is  "
											+ driver.findElement(By.xpath("//h5[@class='text-ellipsis']")).getText());
									innerForLoop: for (int f = 1; f <= steps; f++) {
										System.out.println("Current step number being used" + f);
										((JavascriptExecutor) driver)
												.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(
														By.xpath("(//div[@class='stage-body'])[" + (f) + "]")));
										// check for Status of Step
										if (driver.findElement(By.xpath("(//div[@class='marker cursor'])[" + f + "]"))
												.getCssValue("border-color").equals("rgb(63, 197, 157)")) {
											String ApprovalStepName = driver
													.findElement(By.xpath("(//div[@class='stage-body']/h3)[" + f + "]"))
													.getText();
											System.out.println("Current Approval Step name is   :" + ApprovalStepName);
											Assert.assertTrue(driver
													.findElement(By
															.xpath("//div[@class='actions choices ng-star-inserted']"))
													.isDisplayed());
											if (action == "APPROVE") {
												driver.findElement(By
														.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Finalize']"))
														.click();
												awaitForElement(driver.findElement(By.xpath(
														"//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']")));
												driver.findElement(By
														.xpath("//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']"))
														.click();
												Thread.sleep(5000);
												Assert.assertTrue(driver
														.findElement(
																By.xpath("(//div[@class='status-log'])[1]/div/span[1]"))
														.getText().contains("Step Approved:"));
												Log.info("Item has been Approved");
												break innerForLoop;
											} else if (action == "UPDATE") {
												Log.info("Request update");
												driver.findElement(By
														.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Request Update']"))
														.click();
												Thread.sleep(1000);
												awaitForElement(driver.findElement(By.xpath(
														"//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']")));
												driver.findElement(By
														.xpath("//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']"))
														.click();
												Thread.sleep(5000);
												Log.info("Item has been requested update");
												break innerForLoop;
											} else if (action == "null") {
												Log.info("Admin will not take any action");
											}

										} else if (driver
												.findElement(By.xpath("//img[@alt='Step Approved'][" + f + "]"))
												.isDisplayed()) {
											System.out.println("Step is approved");
											continue innerForLoop;
										} else if (driver
												.findElement(By.xpath("//img[@alt='Request Update'][" + f + "]"))
												.isDisplayed()) {
											System.out.println("Step is Request Updated");
											continue innerForLoop;
										}
									}
								} else {
									System.out.println("Approval Group is Used");
								}

							} else {
								System.out.println("No Approval Type Selected or design not selected");

							}

						} else if (driver
								.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
								.getText().contains("Update requested")) {
							System.out.println("Request Updated as Folder Status");
							break outerForLoop;
						} else if (driver
								.findElement(By.xpath("(//span[@class='finalize ng-star-inserted'])[" + q + "]"))
								.getText().contains("Finalized")) {
							System.out.println("Finalized as Folder Status");
							break outerForLoop;
						}
					}
				}
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						driver.findElement(By.xpath("//div[@class='button close']")));
				awaitForElement(driver.findElement(By.xpath("//div[@class='button close']")));
				driver.findElement(By.xpath("//div[@class='button close']")).click();
				Thread.sleep(2000);

			}

		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void assignApprovalStepUser(String user, String adminAction, int numberOfDesigns, int numberOfFolders,
			boolean selfAssign, boolean isSupplier, String leadUser) throws Throwable {
		Log.info("Inside assignApprovalStepUser method");
		try {
			if (isSupplier) {
				Assert.assertTrue(driver
						.findElement(
								By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.isDisplayed());
				driver.findElement(By.xpath("//h4[contains(@class,'suppliers-title')]/following-sibling::div[1]/ul/li"))
						.click();
				Thread.sleep(1000);
			}
			int limit = getDesignCount();
			System.out.println(limit + "Here is approval");
			if ((limit - numberOfDesigns) > 0) {
				limit = limit - numberOfDesigns;
			} else if ((limit - numberOfDesigns) == 0) {
			} else if ((limit - numberOfDesigns) < 0) {
				Log.info("Please enter a valid number");
				boolean Flag = false;
				Assert.assertEquals(Flag, true, "Please enter a valid number of designs");
			}
			for (int i = 1; i <= limit; i++) {

				getDesignName(i);
				if (driver.findElements(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]"))
						.size() == 0) {
					Log.info("Design not submitted, Please Submit design");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "finalized"))) {
					Log.info("Design Finalized");
				} else if (driver
						.findElement(By.xpath("(//div[contains(@class,'submitted-block')])[" + i + "]/span[1]"))
						.getText().contains(selectPropertiesFile(true, Locale, "statussubmitted"))) {
					Log.info("Design Submitted");
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")))));
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]"))
							.isDisplayed());
					driver.findElement(
							By.xpath("(//div[@class='info-right flex flex-align-center']/button)[" + i + "]")).click();
					Thread.sleep(5000);

					int folders = driver.findElements(By.xpath("//div[@class='overlay']")).size();
					System.out.println(folders + "Here is approval");
					for (int q = 1; q <= folders; q++) {
						if (driver.findElements(By.xpath("(//div[@class='overlay'])[" + q + "]/button")).size() > 0) {
							Log.info("Folders Exisits with submisiions and view design Tag");
							if (driver.findElement(By.xpath("(//div[@class='overlay']/following::div[1])[" + q + "]"))
									.getCssValue("background-color").equals("rgba(63, 197, 157, 1)")) {
								Log.info("Approval Group/ Loop used");
								driver.findElement(By.xpath("(//div[@class='overlay']/following::div[1])[" + q + "]"))
										.click();
								Thread.sleep(1000);
								if (driver
										.findElements(
												By.xpath("//div[@class='approval-container bg-white padding-tools']"))
										.size() > 0) {
									Log.info("Loop is Used");
									int steps = driver.findElements(By.xpath("//div[@class='stage-body']")).size();
									System.out.println("Total Steps are  : " + steps);
									innerForLoop: for (int f = 1; f <= 1; f++) {
										Log.info("Inside for loop" + f);
										((JavascriptExecutor) driver)
												.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(
														By.xpath("(//div[@class='stage-body'])[" + (f) + "]")));
										if (driver.findElement(By.xpath(
												"(//div[@class='wrap not-started ng-star-inserted']/span)[" + f + "]"))
												.getText().contains("Not Started")) {
											System.out.println("Task in Not started, can be assigned");
										} else {
											System.out.println("No Match");
										}
										System.out.println("old code");
										Log.info("Current Actionable step is being performed for " + f);
										if (driver
												.findElement(By
														.xpath("//div[@class='race-select-container form-select']/button"))
												.getCssValue("background-color").contains("rgba(46, 61, 82, 1)")) {
											System.out.println("Earlier Assignments Happened");
											String currentUser = driver
													.findElement(By
															.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
													.getText();
											System.out.println(currentUser);
											Log.info("The current Assigned User is Admin  User :" + currentUser);
										} else {
											System.out.println("First Time Assignment");
											String currentUser = driver
													.findElement(By
															.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
													.getText();
											Assert.assertEquals(currentUser, leadUser);
											Log.info("The current Assigned User is Admin  User");
										}
										if (user == "PM") {
											selectOptionFromDropdown(
													driver.findElement(By
															.xpath("//div[@class='race-select-container form-select']/button/div")),
													common.CONTAINEROPTION, common.getCONTAINER(),
													common.CONTAINEROPTION, common.getDDSEARCHINPUT(),
													configProperities.getProperty("pmname"));
											Thread.sleep(2000);
											System.out.println("herererer    " + driver
													.findElement(By
															.xpath("//div[@class='race-select-container form-select']/button"))
													.getCssValue("background-color"));
											Assert.assertEquals(driver
													.findElement(By
															.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
													.getText(), ("myPM myPM"));
											Log.info("Approval Assigned to PM");
											break innerForLoop;
										} else if (user == "COLAB") {
											selectOptionFromDropdown(
													driver.findElement(By
															.xpath("//div[@class='race-select-container form-select']/button/div")),
													common.CONTAINEROPTION, common.getCONTAINER(),
													common.CONTAINEROPTION, common.getDDSEARCHINPUT(),
													configProperities.getProperty("colabname"));
											Thread.sleep(2000);
											Assert.assertEquals(driver
													.findElement(By
															.xpath("//div[@class='race-select-container form-select']/button/div/div[2]"))
													.getText(), "my collab my collab");
											Log.info("Approval Assigned to Colaborator");
											break innerForLoop;
										} else if (user == "ADMIN") {
											String xpath_1 = "//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']";
											Log.info("Approval is already assigned to Admin");
											if (adminAction == "APPROVE") {
												driver.findElement(By
														.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Finalize']"))
														.click();
												awaitForElement(driver.findElement(By.xpath(xpath_1)));
												driver.findElement(By.xpath(xpath_1)).click();
												Thread.sleep(2000);
												Assert.assertTrue(
														driver.findElement(By.xpath("//img[@alt='Step Approved']"))
																.isDisplayed());
												Assert.assertTrue(driver
														.findElement(By
																.xpath("//div[@class='wrap ng-star-inserted']/span[1]"))
														.getText().contains("Step Approved:"));
												System.out.println(driver
														.findElement(By
																.xpath("//div[@class='wrap ng-star-inserted']/span[2]"))
														.getText());
												break innerForLoop;
											} else if (adminAction == "UPDATE") {
												Log.info("Request update");
												driver.findElement(By
														.xpath("//div[@class='actions choices ng-star-inserted']/a/img[@alt='Request Update']"))
														.click();
												awaitForElement(driver.findElement(By.xpath(
														"//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']")));
												driver.findElement(By
														.xpath("//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700 ng-star-inserted']"))
														.click();
												awaitForElement(driver.findElement(By
														.xpath("//div[@class='statuses ng-star-inserted requested']")));
												Assert.assertTrue(driver
														.findElement(By
																.xpath("//div[@class='statuses approved ng-star-inserted']"))
														.getCssValue("background-color")
														.equals("rgba(255, 255, 255, 1)"));
												break innerForLoop;
											} else if (adminAction == "null") {
												Log.info("Admin will not take any action");
												break innerForLoop;
											}
										}

									}
									System.out.println("Out of inner loop");
								}
							} else {
								System.out.println("Not selected");
							}
						}

					}
					System.out.println("out of outer loop");
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath("//div[@class='button close']")));
					awaitForElement(driver.findElement(By.xpath("//div[@class='button close']")));
					driver.findElement(By.xpath("//div[@class='button close']")).click();
					Thread.sleep(2000);
				}
				// test.get().log(Status.INFO, "Completed Test Case
				// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public String loginAPI() throws Throwable {
		String authToken = null;
		JSONObject login = new JSONObject();
		login.put("email", configProperities.getProperty("adminusername"));
		login.put("password", configProperities.getProperty("commonpassword"));
		CloseableHttpClient client = HttpClients.createDefault();
		try {
			HttpPost request = new HttpPost(configProperities.getProperty("url") + "/users/mfa/authtoken/login/");
			StringEntity params = new StringEntity(login.toString());
			request.setEntity(params);
			request.setHeader("Content-type", "application/json");
			CloseableHttpResponse response = client.execute(request);
			System.out.println(request);
			HttpEntity responseEntity = response.getEntity();
			authToken = EntityUtils.toString(responseEntity).substring(10, 50);
			System.out.println(authToken);
			client.close();
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return authToken;

	}

	public Object getJsonData() {
		String startDate = getStartDate();
		String launchDate = getLaunchDate();
		String INPUTPROJECTNAME = (configProperities.getProperty("projectname") + "API"
				+ Randomizer.generate(200, 2000));
		System.out.println(INPUTPROJECTNAME);
		JSONObject projectData = new JSONObject();
		// test01 Data
		try {
			projectData.put("category", 393);
			projectData.put("manager", 497);
			projectData.put("supplier_links", Collections.EMPTY_LIST);
			projectData.put("template", 474);
			// dev01 Data
			/*
			 * projectData.put("category", 232); projectData.put("manager",
			 * 311); projectData.put("supplier_links", Collections.EMPTY_LIST);
			 * projectData.put("template", 592);
			 */
			projectData.put("name", INPUTPROJECTNAME);
			projectData.put("launch_date", launchDate);
			projectData.put("start_date", startDate);
			projectData.put("sku_count", 0);
			System.out.println(projectData);
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return projectData;
	}

	public CloseableHttpResponse postAPI() throws Throwable {
		String token = loginAPI();
		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		try {

			HttpPost request = new HttpPost(configProperities.getProperty("url") + "/projects/api/v0/projects/");
			StringEntity params = new StringEntity(getJsonData().toString());
			request.setEntity(params);
			request.setHeader("Content-type", "application/json");
			request.setHeader("authorization", "Token" + " " + token);
			response = client.execute(request);
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
		return response;

	}

	public String createProjectAPI() throws Throwable {
		String project = null;
		try {
			postapi: for (int i = 1; i <= 3; i++) {
				System.out.println(project + "  22");
				CloseableHttpResponse tempRespose = postAPI();
				System.out.println(tempRespose);
				System.out.println(tempRespose.getStatusLine().getReasonPhrase());
				System.out.println(tempRespose.getStatusLine());
				if (tempRespose.getStatusLine().getStatusCode() == 400 && i <= 3) {
					Log.info("Duplicate will try one more time" + i);
					continue postapi;

				} else {
					HttpEntity responseEntity = tempRespose.getEntity();
					JSONObject jo = new JSONObject(EntityUtils.toString(responseEntity));
					System.out.println(jo);
					project = jo.getString("name");
					break postapi;
				}
				// System.out.println(project+" 44");
			}
			System.out.println(project + "  33");
			System.out.println(project != null);
			if (project != null) {
				test.get().log(Status.INFO, project);
			} else {
				test.get().log(Status.FAIL,
						"Project Not created after 3 attempts due to Duplicate Project Name, please check the Random Number Generator"
								+ "Project Name is   :" + project);
				Assert.fail();

			}

		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}

		return project;
	}

	public void addDeliverables_supplier(Integer numberOfDeliverables, String approvalType, String pathname,
			String groupname) throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		dbLocators = new DashboardLocators();
		boolean Flag = false;
		Integer count = 0;
		deliverCount = 0;
		Log.info("Inside addDeliverables method");
		try {
			for (int i = 1; i <= numberOfDeliverables; i++) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						dbLocators.DB_ADDDELIVERABLE);
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.DB_ADDDELIVERABLE));
				Thread.sleep(1000);
				System.out.println(driver.findElements(By.xpath("(//h5[@class='toggle-collapse'])[1]")).size());
				if (driver.findElements(By.xpath("(//h5[@class='toggle-collapse'])[1]")).size() > 0
						&& driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]")).getText()
								.contains(selectPropertiesFile(true, Locale, "deliverables"))) {
					Log.info("1 Deliverable already added");
					count = getDeliverableCountOnTaskScreen();// 1
				} else {
					Log.info("Deliverable adding first time");
					// count=0
				}
				dbLocators.DB_ADDDELIVERABLE.click();
				Thread.sleep(1000);
				TotalDeliverables = getDeliverableCountOnTaskScreen();
				System.out.println("Herererere    " + TotalDeliverables);// 2
				if (numberOfDeliverables - (count + TotalDeliverables) == 0) {
					Log.info("First Time Addition, No change");
				} else if (numberOfDeliverables - (count + TotalDeliverables) < 0) {
					Log.info("Adding further");
				} else if (((i + count) - numberOfDeliverables) < 0) {
					Flag = true;
					Assert.assertEquals(Flag, false, "This is not a valid case");
				}
				for (int j = 1; j <= i; j++) {
					if (driver
							.findElement(
									By.xpath("((//h5[@class='toggle-collapse']/following-sibling::div)[2]//button)[" + j
											+ "]//div[@class='select-info-body media-middle']"))
							.getText().equals(selectPropertiesFile(true, Locale, "deliverabletype"))
							&& driver
									.findElement(By
											.xpath("((//h5[@class='toggle-collapse']/following-sibling::div)[2]//button)["
													+ (j + 2) + "]//div[@class='select-info-body media-middle']"))
									.getText().equals(selectPropertiesFile(true, Locale, "approvalpath"))
							&& (driver
									.findElement(By
											.xpath("((//h5[@class='toggle-collapse']/following-sibling::div)[2]//button)["
													+ (j + 3) + "]//div[@class='select-info-body media-middle']"))
									.getText()
									.equals(selectPropertiesFile(true, Locale, "approvalgroups_deliverable")))) {
						Log.info("First Time addition will use this");
						System.out.println("aaaaaaaaaaaaaaaaaaaaa");
						selectOptionFromDropdown(
								driver.findElement(
										By.xpath("((//h5[@class='toggle-collapse']/following-sibling::div)[2]//button)["
												+ j + "]")),
								common.CONTAINEROPTION, common.getCONTAINER(), common.SELECTEDOPTION,
								common.getDDSEARCHINPUT(), configProperities.getProperty("deliverabletype"));
						Log.info("Deliverable Type Selected");
						Thread.sleep(1000);
						if (approvalType == "NOAPPROVAL" && pathname == null && groupname == null) {
							Log.info("No Approval Type selected");
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("NOAPPROVAL" + j);
						} else if (approvalType == "GROUP" && pathname == null) {
							Log.info("GROUP Type selected");
							selectOptionFromDropdown(
									driver.findElement(By
											.xpath("((//h5[@class='toggle-collapse']/following-sibling::div)[2]//button)["
													+ (j + 3) + "]")),
									common.CONTAINEROPTION, common.getCONTAINER(), common.SELECTEDOPTION,
									common.getDDSEARCHINPUT(), groupname);
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("GROUP" + j);
						} else if (approvalType == "LOOP" && groupname == null) {
							Log.info("LOOP Type selected");
							selectOptionFromDropdown(
									driver.findElement(By
											.xpath("((//h5[@class='toggle-collapse']/following-sibling::div)[2]//button)["
													+ (j + 2) + "]")),
									common.CONTAINEROPTION, common.getCONTAINER(), common.SELECTEDOPTION,
									common.getDDSEARCHINPUT(), pathname);
							dbLocators.DB_INPUTDELIVERABLENAME.sendKeys("LOOP" + j);
						}
					}

				}
				// test.get().log(Status.INFO, "Completed Test Case
				// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	public void supplierAdditions_(String approvalType, Integer numberOfDesigns, String pathname, String groupname)
			throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		dbLocators = new DashboardLocators();
		designcount = 1;
		try {
			for (int i = 1; i <= numberOfDesigns; i++) {
				Log.info("Design selected");
				wait.until(ExpectedConditions.elementToBeClickable(dbLocators.DB_ADDDESIGN_SUPPLIER));
				Thread.sleep(1000);
				dbLocators.DB_ADDDESIGN_SUPPLIER.click();
				awaitForElement(dbLocators.SUPPLIERDESIGNLOOPDD);
				if (approvalType == "NOAPPROVAL" && pathname == null && groupname == null) {
					Log.info("No Approval Type selected");
					dbLocators.SUPPLIERDESIGNNAMEINPUT.sendKeys("NOAPPROVAL" + designcount);
				} else if (approvalType == "GROUP" && pathname == null) {
					selectOptionFromDropdown(dbLocators.SUPPLIERDESIGNGROUPDD, common.CONTAINEROPTION,
							common.getCONTAINER(), common.SELECTEDOPTION, common.getDDSEARCHINPUT(), groupname);
					dbLocators.SUPPLIERDESIGNNAMEINPUT.sendKeys("GROUP" + designcount);
					driver.findElement(By.xpath("//button[@class='btn btn-block btn-primary-green']")).click();
				} else if (approvalType == "LOOP" && groupname == null) {
					selectOptionFromDropdown(dbLocators.SUPPLIERDESIGNLOOPDD, common.CONTAINEROPTION,
							common.getCONTAINER(), common.SELECTEDOPTION, common.getDDSEARCHINPUT(), pathname);
					dbLocators.SUPPLIERDESIGNNAMEINPUT.sendKeys("LOOP" + designcount);
				}
				awaitForElement(driver
						.findElement(By.xpath("(//div[contains(@class,'modal-footer no-padding-top')]/button)[2]")));
				driver.findElement(By.xpath("(//div[contains(@class,'modal-footer no-padding-top')]/button)[2]"))
						.click();
				Thread.sleep(2000);
				designcount = designcount + 1;
			}
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}

	}

	public void addSupplierFromProjectScreen(String supplierName, boolean addSupplierContact, String contactname,
			boolean addDesign, boolean addDeliverable) throws Throwable {

		dbLocators = new DashboardLocators();
		try {
			Assert.assertTrue(common.PROJECTSPACEMULTIADDBUTTTON.isDisplayed());
			common.PROJECTSPACEMULTIADDBUTTTON.click();
			awaitForElement(common.MULTIBUTTONDROPDOWN);
			Thread.sleep(500);
			Assert.assertTrue(common.MULTIADD_EDITSUPPLIEROPTION.isDisplayed());
			System.out.println("hererre" + driver
					.findElement(By
							.xpath("//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul/li[2]/a"))
					.getText());
			common.MULTIADD_EDITSUPPLIEROPTION.click();
			System.out.println("11111");
			awaitForElement(driver.findElement(By.xpath("//div[@class='modal-content']")));
			System.out.println("2222");
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-content']")).isDisplayed());
			Assert.assertEquals(common.MODALHEADING.getText(), selectPropertiesFile(true, locale, "addsupplier"));
			Assert.assertTrue(common.DDSELECTSUPPLIER.isDisplayed());
			System.out.println("3333");
			selectOptionFromDropdown(common.DDSELECTSUPPLIER, common.CONTAINEROPTION, common.getCONTAINER(),
					common.DDSUPPLIEROPTION, common.getDDSEARCHINPUT(), supplierName);
			System.out.println("44444");
			Thread.sleep(1000);
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='supplier']/div/button/div/div[2]")).getText(),
					supplierName, "Search related issue");
			if (addSupplierContact && contactname != null) {
				Assert.assertTrue(driver.findElement(By.xpath("//*[@name='contacts']")).isDisplayed());
				Log.info("Contact select dd is displayed");
				selectOptionFromDropdown(driver.findElement(By.xpath("//*[@name='contacts']")), common.CONTAINEROPTION,
						common.getCONTAINER(), common.DDSUPPLIEROPTION, common.getDDSEARCHINPUT(), contactname);
				Assert.assertTrue(driver.findElement(By.xpath("//button[@class='btn btn-block btn-primary-green']"))
						.isDisplayed());
				driver.findElement(By.xpath("//button[@class='btn btn-block btn-primary-green']")).click();
			}
			if (addDesign) {
				supplierAdditions_("LOOP", 1, configProperities.getProperty("pathname"), null);
			}
			if (addDeliverable) {
				addDeliverables_supplier(1, "LOOP", configProperities.getProperty("pathname"), null);
			}
			awaitForElement(common.ADDSUPPLIERBUTTON);

			common.ADDSUPPLIERBUTTON.click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[@class='button close']")));
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath("//div[@class='button close']"))));
			driver.findElement(By.xpath("//div[@class='button close']")).click();
			Thread.sleep(10000);
			wait.until(ExpectedConditions.visibilityOf(
					driver.findElement(By.xpath("//span[contains(@class,'toolbar-header project-space')]"))));
			Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'toolbar-header project-space')]"))
					.isDisplayed());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}

	/**
	 * 2 Params - Existing Product or New Product
	 * 
	 * @param typeOfProduct
	 * @throws Exception
	 */
	public void addProductFromProjectScreen(boolean isExistingProduct, String productname, String projectname)
			throws Throwable {
		// test.get().log(Status.INFO, "Starting Test Case
		// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		dbLocators = new DashboardLocators();
		try {
			Assert.assertTrue(common.PROJECTSPACEMULTIADDBUTTTON.isDisplayed());
			common.PROJECTSPACEMULTIADDBUTTTON.click();
			awaitForElement(common.MULTIBUTTONDROPDOWN);
			Thread.sleep(500);
			Assert.assertTrue(common.MULTIADD_EDITSUPPLIEROPTION.isDisplayed());
			common.MULTIADD_NEWPRODUCT.click();
			awaitForElement(driver.findElement(By.xpath("//div[@class='modal-content']")));
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-content']")).isDisplayed());
			Assert.assertEquals(common.MODALHEADING.getText(), selectPropertiesFile(true, locale, "newproduct"));
			Assert.assertTrue(common.LABELEXISTINGPRODUCT.isDisplayed());
			Assert.assertEquals(common.LABELEXISTINGPRODUCT.getText(),
					selectPropertiesFile(true, locale, "existingproduct"));

			if (isExistingProduct) {
				Log.info("Adding Exiting Product");
				// Click on Yes Radio button for Existing Product
				awaitForElement(common.DDEXISTINGPRODUCT);
				Assert.assertTrue(common.DDEXISTINGPRODUCT.isDisplayed());
				Assert.assertTrue(common.CATEGORYDROPDOWN.isDisplayed());
				Assert.assertTrue(common.BRANDDROPDOWN.isDisplayed());
				// Select Product
				selectOptionFromDropdown(common.DDEXISTINGPRODUCT, common.CONTAINEROPTION, common.getCONTAINER(),
						common.DDEXISTINGPRODUCTOPTION, common.getDDSEARCHINPUT(), productname);
				jsWait();
				// Assert.assertEquals(common.DDEXISTINGPRODUCTOPTION.getText(),
				// productname);
				Assert.assertTrue(common.BUTTONCONFIRM.isEnabled());
				common.BUTTONCONFIRM.click();
				jsWait();
				Thread.sleep(1000);
				awaitForElement(common.ADDEDPRODUCT);
				Assert.assertEquals(common.ADDEDPRODUCT.getText(), productname, "Search Related issues");
				Assert.assertTrue(common.ADDEDPRODUCT_CLOSEBUTTON.isDisplayed());
				// Assert.assertTrue(common.SECONDADDPRODUCTBUTTON.isDisplayed());
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						common.getCLOSEBUTTON());
				common.getCLOSEBUTTON().click();
				jsWait();
			} else {
				Log.info("Adding New Product");
				driver.findElement(By.xpath("(//div[@class='switch-field inline-flex'])[1]/label[2]")).click();
				jsWait();
				Thread.sleep(1000);
				awaitForElement(common.PROJECTDROPDOWN);
				Assert.assertEquals(common.DDPROJECTNAME.getText(), projectname);
				// To be written further
			}
			awaitForElement(common.BUTTONBACKTORATING);
			Assert.assertEquals(common.SELECTEDPRODUCT.getText(),
					selectPropertiesFile(true, locale, "selectedproducts"));
			Assert.assertTrue(driver.findElement(By.xpath("//*[@alt='Bar Code']/following-sibling::span[2]")).getText()
					.equals("1"));
			Assert.assertEquals(common.BUTTONBACKTORATING.getText(),
					selectPropertiesFile(true, locale, "backtorating"));
			// test.get().log(Status.INFO, "Completed Test Case
			// :"+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR,
					"Error in the method  " + Thread.currentThread().getStackTrace()[1].getMethodName() + " Error  :"
							+ e.getClass().getSimpleName());
			throw e;
		}
	}
}
