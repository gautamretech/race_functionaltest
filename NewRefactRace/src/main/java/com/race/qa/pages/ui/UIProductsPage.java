package com.race.qa.pages.ui;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.ProductsPageLocators;
import com.race.qa.util.Log;



public class UIProductsPage extends TestBase {
	WebDriver driver;
	CommonLocators common;
	ProductsPageLocators pl;
	
		public UIProductsPage(WebDriver driver) {
			this.driver=driver;
			this.driver=DriverFactory.getDriver();
	}
	public void AddNewProductInProjectDetailsPage() throws Exception{
		try{
		pl = new ProductsPageLocators();
		wait.until(ExpectedConditions.visibilityOf(pl.ADDNEWPRODUCTOPTION));
		pl.ADDNEWPRODUCTOPTION.click();
		wait.until(ExpectedConditions.visibilityOf(pl.POPUPHEADINGPRODUCT));
		pl.POPUPHEADINGPRODUCT.isDisplayed();
		Log.info("Text Is :  " + pl.POPUPHEADINGPRODUCT.getText());
		Assert.assertTrue(pl.POPUPHEADINGPRODUCT.getText() != null);
		getAllTextInPopup();
		//Clicking on New Product Yes radio
		pl.YESNEWPRODUCTRADIO.click();
		Log.info("Add New Product");
		wait.until(ExpectedConditions.visibilityOf(pl.PRODUCTPAGESELECTPROJECT));
		pl.PRODUCTPAGESELECTPROJECT.isDisplayed();
		// Dropdown is not selecteable
		/*PRODUCTPAGESELECTPROJECT.click();
		getDropdownData(DROPDOWNDATA, "li");
		PRODUCTPAGESELECTPROJECT.click();*/
		pl.PRODUCTPAGEPRODUCTNAME.isDisplayed();
		pl.PRODUCTPAGEPRODUCTID.isDisplayed();
		pl.PRODUCTPAGESELECTDEPARTMENT.isDisplayed();
		pl.PRODUCTPAGESELECTFAMILY.isDisplayed();
		pl.PRODUCTPAGESELECTCATEGORY.isDisplayed();
		pl.PRODUCTPAGESELECTSUBCATEGORY.isDisplayed();
		pl.PRODUCTPAGESELECTSEGMENT.isDisplayed();
		pl.PRODUCTPAGESELCTBRAND.isDisplayed();
		pl.PRODUCTPAGESELCTBRAND.click();
		//getDropdownData(DROPDOWNDATA, "li");
		pl.PRODUCTPAGESELCTBRAND.click();
		pl.PRODUCTPAGEDESCRIPTION.isDisplayed();
		pl.PRODUCTPAGEMARKETPOSITION.isDisplayed();
		pl.PRODUCTPAGESELECTCOUNTRY.isDisplayed();
		pl.PRODUCTPAGESELECTBANNER.isDisplayed();
		pl.PRODUCTPAGESELECTSTATUS.isDisplayed();
		pl.PRODUCTPAGESELECTSTATUS.click();
		getDropdownData(pl.DROPDOWNDATA, "li");
		pl.PRODUCTPAGESELECTSTATUS.click();
		pl.PRODUCTPAGESELECTSPECIFICATION.isDisplayed();
		pl.PRODUCTPAGENEXTBUTTON.isDisplayed();
		Log.info("Text is    :" + pl.PRODUCTPAGENEXTBUTTON.getText());
		Assert.assertTrue(pl.PRODUCTPAGENEXTBUTTON.getText() != null);
		//Add Existing Product
		Log.info("Add Existing Product");
		//common.getCLOSEBUTTON().click();
		pl.YESEXISTINGPRODUCTRADIO.click();
		wait.until(ExpectedConditions.visibilityOf(pl.FILTERCATEGORY));
		pl.FILTERCATEGORY.click();
		getDropdownData(pl.DROPDOWNDATA, "li");
		pl.FILTERCATEGORY.click();
		pl.FILTERBRAND.isDisplayed();
		pl.FILTERBRAND.click();
		//getDropdownData(DROPDOWNDATA, "li");
		pl.FILTERBRAND.click();
		pl.EXISTINGPRODUCTDROPDOWN.click();
		getDropdownData(pl.DROPDOWNDATA, "li");
		pl.EXISTINGPRODUCTDROPDOWN.click();
		pl.EXISTINGPRODUCTCONFIRMBUTTON.isDisplayed();
		Log.info("Text is    :" + pl.EXISTINGPRODUCTCONFIRMBUTTON.getText());
		Assert.assertTrue(pl.EXISTINGPRODUCTCONFIRMBUTTON.getText() != null);
		common.getCLOSEBUTTON().click();
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
		throw e;
	}
	}

	public void validateAddNewProductPage() throws Exception{
		common = new CommonLocators();
		pl=new ProductsPageLocators();
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PRODUCTS());
			common.getMENU_PRODUCTS().click();
			awaitForElement((common.getPIECHART()));
			awaitForElement(pl.ADDPRODUCTBUTTON);
			pl.ADDPRODUCTBUTTON.isDisplayed();
			Log.info("Text Is :  " + pl.ADDPRODUCTBUTTON.getText());
			Assert.assertTrue(pl.ADDPRODUCTBUTTON.getText() != null);
			pl.ADDPRODUCTBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(pl.POPUPHEADINGPRODUCT));
			pl.POPUPHEADINGPRODUCT.isDisplayed();
			Log.info("Text Is :  " + pl.POPUPHEADINGPRODUCT.getText());
			Assert.assertTrue(pl.POPUPHEADINGPRODUCT.getText() != null);
			getAllTextInPopup();
			pl.PRODUCTPAGESELECTPROJECT.isDisplayed();
			pl.PRODUCTPAGESELECTPROJECT.click();
			getDropdownData(pl.DROPDOWNDATA, "li");
			pl.PRODUCTPAGESELECTPROJECT.click();
			pl.PRODUCTPAGEPRODUCTNAME.isDisplayed();
			pl.PRODUCTPAGEPRODUCTID.isDisplayed();
			pl.PRODUCTPAGESELECTDEPARTMENT.isDisplayed();
			pl.PRODUCTPAGESELECTFAMILY.isDisplayed();
			pl.PRODUCTPAGESELECTCATEGORY.isDisplayed();
			pl.PRODUCTPAGESELECTSUBCATEGORY.isDisplayed();
			pl.PRODUCTPAGESELECTSEGMENT.isDisplayed();
			pl.PRODUCTPAGESELCTBRAND.isDisplayed();
			/*PRODUCTPAGESELCTBRAND.click();
			getDropdownData(DROPDOWNDATA, "li");
			PRODUCTPAGESELCTBRAND.click();*/
			pl.PRODUCTPAGEDESCRIPTION.isDisplayed();
			pl.PRODUCTPAGEMARKETPOSITION.isDisplayed();
			pl.PRODUCTPAGESELECTCOUNTRY.isDisplayed();
			pl.PRODUCTPAGESELECTBANNER.isDisplayed();
			pl.PRODUCTPAGESELECTSTATUS.isDisplayed();
			pl.PRODUCTPAGESELECTSTATUS.click();
			getDropdownData(pl.DROPDOWNDATA, "li");
			pl.PRODUCTPAGESELECTSTATUS.click();
			pl.PRODUCTPAGENEXTBUTTON.isDisplayed();
			Log.info("Text is    :" + pl.PRODUCTPAGENEXTBUTTON.getText());
			Assert.assertTrue(pl.PRODUCTPAGENEXTBUTTON.getText() != null);
			pl.PRODUCTPAGENEXTBUTTON.isDisplayed();
			/*awaitForElement(pl.VARIANTNAME);
			Assert.assertTrue(pl.VARIANTNAME.isDisplayed());
			Assert.assertTrue(pl.VARIANTID.isDisplayed());
			Assert.assertTrue(pl.ADDANOTHERVARIANTID.isDisplayed());
			Assert.assertTrue(pl.ESTANNUALQUANTITY.isDisplayed());
			Assert.assertTrue(pl.SIZE.isDisplayed());
			Assert.assertTrue(pl.SIZEUNIT.isDisplayed());
			Assert.assertTrue(pl.HEIGHT.isDisplayed());
			Assert.assertTrue(pl.LENGTH.isDisplayed());
			Assert.assertTrue(pl.WIDTH.isDisplayed());*/
			common.getCLOSEBUTTON().click();
			Thread.sleep(1000);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateFilterPopupScreen() throws Exception{
		common = new CommonLocators();
		pl=new ProductsPageLocators();
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PRODUCTS());
			common.getMENU_PRODUCTS().click();
			awaitForElement((common.getPIECHART()));
			awaitForElement(pl.ADDPRODUCTBUTTON);
			pl.ADDPRODUCTBUTTON.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.PRODUCTSPAGEFILTERBUTTON);
			pl.PRODUCTSPAGEFILTERBUTTON.isDisplayed();
			Log.info("Text is   :" + pl.PRODUCTSPAGEFILTERBUTTON.getText());
			Assert.assertTrue(pl.PRODUCTSPAGEFILTERBUTTON.getText() != null);
			pl.PRODUCTSPAGEFILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(pl.FILTERCATEGORYDROPDOWN));
			getAllTextInPopup();
			//FILTERPRODUCTCODE.isDisplayed();
			pl.FILTERCATEGORYDROPDOWN.isDisplayed();
			pl.FILTERCATEGORYDROPDOWN.click();
			getDropdownData(pl.DROPDOWNDATA, "li");
			pl.FILTERCATEGORYDROPDOWN.click();
			pl.FILTERPRODUCTINGID.isDisplayed();
			pl.PRODUCTPAGESELECTSTATUS.isDisplayed();
			pl.PRODUCTPAGESELECTSTATUS.click();
			getDropdownData(pl.DROPDOWNDATA, "li");
			pl.PRODUCTPAGESELECTSTATUS.click();
			common.getCANCELFILTERBUTTON().isDisplayed();
			Log.info("Text is   :" + common.getCANCELFILTERBUTTON().getText());
			Assert.assertTrue(common.getCANCELFILTERBUTTON().getText() != null);
			common.getCLEARFILTERBUTTON().isDisplayed();
			Log.info("Text is   :" + common.getCLEARFILTERBUTTON().getText());
			Assert.assertTrue(common.getCLEARFILTERBUTTON().getText() != null);
			common.getAPPLYFILTERBUTTON().isDisplayed();
			Log.info("Text is   :" + common.getAPPLYFILTERBUTTON().getText());
			Assert.assertTrue(common.getAPPLYFILTERBUTTON().getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.FILTERPOPUP);
			common.getCLEARFILTERBUTTON().click();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProductTableData() throws Exception{
		common = new CommonLocators();
		pl=new ProductsPageLocators();	
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PRODUCTS());
			common.getMENU_PRODUCTS().click();
			awaitForElement((common.getPIECHART()));
			awaitForElement(pl.ADDPRODUCTBUTTON);
			pl.ADDPRODUCTBUTTON.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.HEADING);
			// HEADING.isDisplayed();
			pl.HEADING.isDisplayed();
			Log.info("Text is    :" + pl.HEADING.getText());
			Assert.assertTrue(pl.HEADING.getText() != null);
			productTableData();
			}catch(Exception e){
				test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
				throw e;
			}
	}
	
	public void validateProductGeneralTab() throws StackOverflowError,Exception{
		try{
		common = new CommonLocators();
		pl=new ProductsPageLocators();
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PRODUCTS());
			common.getMENU_PRODUCTS().click();
			awaitForElement((common.getPIECHART()));
			awaitForElement((pl.ADDPRODUCTBUTTON));
			pl.ADDPRODUCTBUTTON.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);"
					, driver.findElement(By.xpath("//div[contains(@class,'project-heading')]/h3")));
			awaitForElement(pl.PRODUCTLINK);
			pl.PRODUCTLINK.isDisplayed();
			Log.info("Text is   :" + pl.PRODUCTLINK.getText());
			Assert.assertTrue(pl.PRODUCTLINK.getText() != null);
			pl.PRODUCTLINK.click();
			awaitForElement((pl.GENERALTAB));
			pl.GENERALTAB.isDisplayed();
			Log.info("Text is   :" + pl.GENERALTAB.getText());
			Assert.assertTrue(pl.GENERALTAB.getText() != null);
			// GENERALTAB.click();
			//ADDPRODUCTBUTTONINGENTAB.isDisplayed();
			//Log.info("Text is   :" + ADDPRODUCTBUTTONINGENTAB.getText());
			//Assert.assertTrue(ADDPRODUCTBUTTONINGENTAB.getText() != null);
			pl.HEADINGOVERVIEW.isDisplayed();
			Log.info("Text is   :" + pl.HEADINGOVERVIEW.getText());
			Assert.assertTrue(pl.HEADINGOVERVIEW.getText() != null);
			pl.EDITBUTTON.isDisplayed();
			Log.info("Text is   :" + pl.EDITBUTTON.getText());
			Assert.assertTrue(pl.EDITBUTTON.getText() != null);
			getOverviewSection();
			getSoldSection();
			getProductDescription();
			//UPLOADFILEIMAGE.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.ADDVARIENTBUTTON);
			pl.ADDVARIENTBUTTON.isDisplayed();
			Log.info("Text is   :" + pl.ADDVARIENTBUTTON.getText());
			Assert.assertTrue(pl.ADDVARIENTBUTTON.getText() != null);
			variantTableData();
			//Log.info(UPLOADCONTAINER.getText());
			//Assert.assertTrue(UPLOADCONTAINER.getText() != null);
			//Log.info("Text is   :" + UPLOADLINK.getText());
			//Assert.assertTrue("Text is   :" + UPLOADLINK.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.PROJECTSTAB);
			Log.info("Text is :" + pl.PROJECTSTAB.getText());
			String NumberOfProjects = pl.PROJECTSTAB.getText();
			NumberOfProjects = NumberOfProjects.replaceAll("[^\\d]", "");
			Log.info(NumberOfProjects);
			Integer Number = Integer.parseInt(NumberOfProjects);
			Assert.assertTrue(pl.PROJECTSTAB.getText() != null);
			pl.PROJECTSTAB.click();
			if (Number == 0) {
				Log.info("No Data");
			} else {
				projectTableData();
			}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProductEditPage() throws Exception{
		common = new CommonLocators();
		pl=new ProductsPageLocators();
		try{		
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PRODUCTS());
			common.getMENU_PRODUCTS().click();
			awaitForElement((common.getPIECHART()));
			awaitForElement(pl.ADDPRODUCTBUTTON);
			pl.ADDPRODUCTBUTTON.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);"
					, driver.findElement(By.xpath("//div[contains(@class,'project-heading')]/h3")));
			awaitForElement(pl.PRODUCTLINK);
			pl.PRODUCTLINK.click();
			awaitForElement((pl.EDITBUTTON));
			pl.EDITBUTTON.isDisplayed();
			pl.EDITBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(pl.HEADINGPRODUCTEDITPAGE));
			pl.HEADINGPRODUCTEDITPAGE.isDisplayed();
			Log.info(pl.HEADINGPRODUCTEDITPAGE.getText());
			Assert.assertTrue(pl.HEADINGPRODUCTEDITPAGE.getText() != null);
			getAllTextInPopup();
			pl.PRODUCTPAGESELECTPROJECT.isDisplayed();
			pl.PRODUCTPAGESELECTPROJECT.click();
			Log.info(pl.PRODUCTPAGESELECTPROJECT.getText());
			getDropdownData(pl.DROPDOWNDATA, "li");
			pl.PRODUCTPAGESELECTPROJECT.click();
			pl.PRODUCTPAGESELECTDEPARTMENT.isDisplayed();
			pl.PRODUCTPAGESELECTFAMILY.isDisplayed();
			pl.PRODUCTPAGESELECTCATEGORY.isDisplayed();
			pl.PRODUCTPAGESELECTSUBCATEGORY.isDisplayed();
			pl.PRODUCTPAGESELECTSEGMENT.isDisplayed();
			pl.PRODUCTPAGESELCTBRAND.isDisplayed();
			pl.PRODUCTPAGESELCTBRAND.click();
			//Log.info(PRODUCTPAGESELCTBRAND.getText());
			//getDropdownData(DROPDOWNDATA, "li");
			//getSelectBrandData();
			pl.PRODUCTPAGESELCTBRAND.click();
			pl.PRODUCTPAGESELECTCOUNTRY.isDisplayed();
			pl.PRODUCTPAGESELECTCOUNTRY.click();
			Log.info(pl.PRODUCTPAGESELECTCOUNTRY.getText());
			//getDropdownData(DROPDOWNDATA, "li");
			pl.PRODUCTPAGESELECTCOUNTRY.click();
			pl.PRODUCTPAGESELECTBANNER.isDisplayed();
			pl.PRODUCTPAGESELECTBANNER.click();
			Log.info(pl.PRODUCTPAGESELECTBANNER.getText());
			//getDropdownData(DROPDOWNDATA, "li");
			pl.PRODUCTPAGESELECTBANNER.click();
			pl.PRODUCTPAGESELECTSTATUS.isDisplayed();
			pl.PRODUCTPAGESELECTSTATUS.click();
			Log.info(pl.PRODUCTPAGESELECTSTATUS.getText());
			getDropdownData(pl.DROPDOWNDATA, "li");
			pl.PRODUCTPAGESELECTSTATUS.click();
			//pl.PRODUCTPAGESELECTSPECIFICATION.isDisplayed();
			//pl.PRODUCTPAGESELECTSPECIFICATION.click();
			//Log.info(pl.PRODUCTPAGESELECTSPECIFICATION.getText());
			//getDropdownData(DROPDOWNDATA, "li");
			//pl.PRODUCTPAGESELECTSPECIFICATION.click();
			common.getCLOSEBUTTON().isDisplayed();
			Log.info(common.getCLOSEBUTTON().getText());
			Assert.assertTrue(common.getCLOSEBUTTON().getText() != null);
			Assert.assertTrue(driver.findElement(By.xpath("//button[@class='float-right positi btn btn-primary-green btn-width-120 ng-star-inserted']")).isDisplayed());
			//common.getSAVEBUTTON().isDisplayed();
			Log.info(driver.findElement(By.xpath("//button[@class='float-right positi btn btn-primary-green btn-width-120 ng-star-inserted']")).getText());
			Assert.assertTrue(driver.findElement(By.xpath("//button[@class='float-right positi btn btn-primary-green btn-width-120 ng-star-inserted']")).getText() != null);
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
			
		}

	public void validateAddEditVariant() throws Exception{
		common = new CommonLocators();
		pl=new ProductsPageLocators();
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PRODUCTS());
			common.getMENU_PRODUCTS().click();
			awaitForElement((common.getPIECHART()));
			awaitForElement((pl.ADDPRODUCTBUTTON));
			pl.ADDPRODUCTBUTTON.isDisplayed();
			Log.info("Text is :" + pl.ADDPRODUCTBUTTON.getText());
			Assert.assertTrue(pl.ADDPRODUCTBUTTON.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.PRODUCTLINK);
			pl.PRODUCTLINK.click();
			awaitForElement(pl.GENERALTAB);
			//wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(pl.GENERALTAB)));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);"
					,pl.ADDVARIENTBUTTON);
			pl.VARIENTSTAB.isDisplayed();
			wait.until(ExpectedConditions.visibilityOf(pl.ADDVARIENTBUTTON));
			Log.info("Text is : " + pl.VARIENTSTAB.getText());
			Assert.assertTrue(pl.VARIENTSTAB.getText() != null);
			wait.until(ExpectedConditions.visibilityOf(pl.ADDVARIENTBUTTON));
			if(driver.findElements(By.xpath("//table[@class='table table-striped table-hover no-margin-bottom']/tbody/tr[1]/td[1]")).size()==0){
				Log.info("No Varient Data");
			}
			else{
			Log.info("Text is : " + pl.VARIENTNAMELINK.getText());
			Assert.assertTrue(pl.VARIENTNAMELINK.getText() != null);
			//New edit varient link
			driver.findElement(By.xpath("//table[@class='table table-striped table-hover no-margin-bottom']/tbody/tr[1]/td[9]/div/a[2]")).click();
			wait.until(ExpectedConditions.visibilityOf(pl.HEADINGEDITVARIENT));
			pl.HEADINGEDITVARIENT.isDisplayed();
			Log.info("Text is : " + pl.HEADINGEDITVARIENT.getText());
			Assert.assertTrue(pl.HEADINGEDITVARIENT.getText() != null);
			getAllTextInPopup();
			pl.VARIANTNAME.isDisplayed();
			pl.VARIANTID.isDisplayed();
			pl.ADDANOTHERVARIANTID.isDisplayed();
			pl.ESTANNUALQUANTITY.isDisplayed();
			pl.NUMBEROFUNITS.isDisplayed();
			//pl.WIDTH.isDisplayed();
			//pl.HEIGHT.isDisplayed();
			//pl.LENGTH.isDisplayed();
			getRadioButton();
			//pl.DESCRIPTION.isDisplayed();
			common.getCLOSEBUTTON().isDisplayed();
			//common.getSAVEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
			}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateSpecManagementTabAndRequetSpecs() throws Exception{
		common = new CommonLocators();
		pl=new ProductsPageLocators();
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PRODUCTS());
			common.getMENU_PRODUCTS().click();
			awaitForElement((common.getPIECHART()));
			awaitForElement(pl.ADDPRODUCTBUTTON);
			pl.ADDPRODUCTBUTTON.isDisplayed();
			Log.info("Text is :" + pl.ADDPRODUCTBUTTON.getText());
			Assert.assertTrue(pl.ADDPRODUCTBUTTON.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.PRODUCTLINK);
			awaitForElement(pl.PRODUCTLINK);
			pl.PRODUCTLINK.click();
			Thread.sleep(500);
			awaitForElement((pl.GENERALTAB));
			pl.SPECMANAGEMENTTAB.isDisplayed();
			Log.info("Text is  :" + pl.SPECMANAGEMENTTAB.getText());
			Assert.assertTrue(pl.SPECMANAGEMENTTAB.getText() != null);
			pl.SPECMANAGEMENTTAB.click();
			wait.until(ExpectedConditions.visibilityOf(pl.REQUESTSPECIFICATIONBUTTON));
			pl.REQUESTSPECIFICATIONBUTTON.isDisplayed();
			Log.info("Text is :" + pl.REQUESTSPECIFICATIONBUTTON.getText());
			Assert.assertTrue(pl.REQUESTSPECIFICATIONBUTTON.getText() != null);
			pl.REQUESTSPECIFICATIONBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(pl.SELECTPRODUCTVARIANT));
			getAllTextInPopup();
			pl.HEADINGREQUESTSPECIFICATION.isDisplayed();
			Log.info(pl.HEADINGREQUESTSPECIFICATION.getText());
			Assert.assertTrue(pl.HEADINGREQUESTSPECIFICATION.getText() != null);
			pl.SELECTPRODUCTVARIANT.isDisplayed();
			Log.info("Text is :" + pl.SELECTPRODUCTVARIANT.getText());
			Assert.assertTrue(pl.SELECTPRODUCTVARIANT.getText() != null);
			pl.SELECTPRODUCTVARIANT.click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'race-popup-container')]"))));
					//elementToBeClickable(driver.findElement(By.xpath("//div[contains(@class,'race-popup-container')]"))));
			getDropdownData(pl.VARDATA, "li");
			pl.SELECTPRODUCTVARIANT.click();
			pl.SELECTSUPPLIER.isDisplayed();
			Log.info("Text is :" + pl.SELECTSUPPLIER.getText());
			Assert.assertTrue(pl.SELECTSUPPLIER.getText() != null);
			pl.SELECTSUPPLIER.click();
			getDropdownData(pl.VARDATA, "li");
			pl.SELECTSUPPLIER.click();
			pl.CONFIRMBUTTON.isDisplayed();
			Log.info("Text is :" + pl.CONFIRMBUTTON.getText());
			Assert.assertTrue(pl.CONFIRMBUTTON.getText() != null);
			pl.ADDMORESPECBUTTON.isDisplayed();
			Log.info("Text is :" + pl.ADDMORESPECBUTTON.getText());
			Assert.assertTrue(pl.ADDMORESPECBUTTON.getText() != null);
			pl.SUBMITBUTTON.isDisplayed();
			Log.info("Text is :" + pl.SUBMITBUTTON.getText());
			Assert.assertTrue(pl.SUBMITBUTTON.getText() != null);
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);		
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
	/*
	 * public void validateSubmissionAndSpecRequestTab() { dashboardPage = new
	 * DashboardPage();
	 * wait.until(ExpectedConditions.visibilityOf(dashboardPage.PIECHART));
	 * MENU_PRODUCTS.click();
	 * wait.until(ExpectedConditions.visibilityOf(dashboardPage.PIECHART));
	 * ADDPRODUCTBUTTON.isDisplayed(); ((JavascriptExecutor)
	 * driver).executeScript("arguments[0].scrollIntoView(true);",
	 * PRODUCTSPAGEFILTERBUTTON); PRODUCTLINK.click();
	 * wait.until(ExpectedConditions.visibilityOf(GENERALTAB));
	 * SPECMANAGEMENTTAB.isDisplayed(); SPECMANAGEMENTTAB.click();
	 * wait.until(ExpectedConditions.visibilityOf(REQUESTSPECIFICATIONBUTTON));
	 * ((JavascriptExecutor)
	 * driver).executeScript("arguments[0].scrollIntoView(true);",
	 * SUBMISSIONTAB); SUBMISSIONTAB.isDisplayed();
	 * SEARCHSUBMISSION.isDisplayed(); DROPDOWNALLSUBMISSION.isDisplayed();
	 * SPECREQUESTTAB.click(); }
	 */

	/*public void getSelectBrandData() {
		List<WebElement> Datas = BRANDDATA.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> i = Datas.iterator();
		Log.info("-----------Data Starts-------------");

		while (i.hasNext()) {
			WebElement Data = i.next();
			Log.info("Text is :   " + Data.getText());
			Assert.assertTrue(Data.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Data);

		}
		Log.info("-----------Data Ends-------------");
	}*/

	/*public void getDropdownData() {
		List<WebElement> Datas = DROPDOWNDATA.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> i = Datas.iterator();
		Log.info("-----------Data Starts-------------");

		while (i.hasNext()) {
			WebElement Data = i.next();
			Log.info("Text is :   " + Data.getText());
			Assert.assertTrue(Data.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Data);

		}
		Log.info("-----------Data Ends-------------");
	}*/

	public void productTableData() {
		try{
		String DataElement = "//table[@class='table table-striped table-hover']/tbody/tr[";
		List<WebElement> rows = driver
				.findElements(By.xpath("//table[@class='table table-striped table-hover']/tbody/tr"));
		Log.info("No. of Rows" + rows.size());
		for (int k = 1; k <= 5; k++) {
			Log.info("----------DATA STARTS-------------------");
			for (int j = 1; j <= 8; j++) {
				Log.info(
						"Text is :" + driver.findElement(By.xpath(DataElement + k + "]" + "/td[" + j + "]")).getText());
				Assert.assertTrue(
						driver.findElement(By.xpath(DataElement + k + "]" + "/td[" + j + "]")).getText() != null);
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(DataElement + k + "]")));
			// driver.findElement(By.xpath(DataElement+k+"]"
			Log.info("----------DATA ENDS-------------------");
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getOverviewSection() {
		try{
		String Element = "//div[@class='badges-section clearfix']/div";
		List<WebElement> rows = driver.findElements(By.xpath(Element));
		Log.info("No. of Rows" + rows.size());
		for (int k = 1; k <= rows.size(); k++) {
			WebElement OV_value1 = driver.findElement(By.xpath(Element + "[" + k + "]" + "/div/h6"));
			WebElement OV_value2 = driver.findElement(By.xpath(Element + "[" + k + "]" + "/div/h5"));
			Log.info("Text is :   " + OV_value1.getText());
			Assert.assertTrue(OV_value1.getText() != null);
			Log.info("Text is :   " + OV_value2.getText());
			Assert.assertTrue(OV_value2.getText() != null);
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getProductDescription() {
		try{
		String Element = "//div[@class='row product-description']/div";
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				driver.findElement(By.xpath(Element)));
		List<WebElement> rows = driver.findElements(By.xpath(Element));
		Log.info("No. of Rows" + rows.size());
		for (int k = 1; k <= rows.size(); k++) {
			WebElement PD_value1 = driver.findElement(By.xpath(Element + "[" + k + "]/h5"));
			WebElement PD_value2 = driver.findElement(By.xpath(Element + "[" + k + "]//p"));
			Log.info("Text is :   " + PD_value1.getText());
			Assert.assertTrue(PD_value1.getText() != null);
			Log.info("Text is :   " + PD_value2.getText());
			Assert.assertTrue(PD_value2.getText() != null);
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getSoldSection() {
		try{
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.COUNTRYSOLD);
			Log.info("Text is :   " + pl.COUNTRYSOLD.getText());
			Log.info("Text is : " + pl.BANNERSOLD.getText());
			Assert.assertTrue(pl.BANNERSOLD.getText() != null);
			Assert.assertTrue(pl.COUNTRYSOLD.getText() != null);
			String CS = "(//h5[@class='no-margin font-weight-600 padding-xs padding-left-xs bg-linear-gradient'])[1]";
			String BS = "(//h5[@class='no-margin font-weight-600 padding-xs padding-left-xs bg-linear-gradient'])[2]";
			List<WebElement> css = driver.findElements(By.xpath(CS + "/following-sibling::ul/li"));
			if (css.size() > 0) {
				java.util.Iterator<WebElement> i = css.iterator();
				while (i.hasNext()) {
					WebElement cs = i.next();
					Log.info("Text is :" + cs.getText());
					Assert.assertTrue(cs.getText() != null);
				}
			} else {
				Log.info("No records found");
			}
			List<WebElement> bss = driver.findElements(By.xpath(BS + "/following-sibling::ul/li"));
			if (bss.size() > 0) {
				java.util.Iterator<WebElement> j = bss.iterator();
				while (j.hasNext()) {
					WebElement bs = j.next();
					Log.info("Text is :" + bs.getText());
					Assert.assertTrue(bs.getText() != null);
				}
			} else {
				Log.info("No records found");
			}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
		
		}
	

	public void variantTableData() {
		try{
		String DataElement = "//table[@class='table table-striped table-hover no-margin-bottom']/tbody/tr[";
		List<WebElement> rows = driver
				.findElements(By.xpath("//table[@class='table table-striped table-hover no-margin-bottom']/tbody/tr"));
		Log.info("No. of Rows" + rows.size());
		for (int k = 1; k <= rows.size(); k++) {
			Log.info("----------DATA Starts-------------------");
			for (int j = 1; j <= 9; j++) {
				Log.info(
						"Text is :" + driver.findElement(By.xpath(DataElement + k + "]" + "/td[" + j + "]")).getText());
				Assert.assertTrue(
						driver.findElement(By.xpath(DataElement + k + "]" + "/td[" + j + "]")).getText() != null);
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(DataElement + k + "]")));
			Log.info("----------DATA ENDS-------------------");
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void projectTableData() {
		try{
		String Element = "//*[@class='table table-striped table-hover no-margin-bottom']/tbody/tr";
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(Element))));
		String DataElement = "//*[@class='table table-striped table-hover no-margin-bottom']/tbody/tr[";
		List<WebElement> rows = driver.findElements(By.xpath(Element));
		Log.info("No. of Rows" + rows.size());
		for (int k = 1; k <= rows.size(); k++) {
			Log.info("----------DATA Starts-------------------");
			for (int j = 1; j <= 6; j++) {
				Log.info(
						"Text is :" + driver.findElement(By.xpath(DataElement + k + "]" + "/td[" + j + "]")).getText());
				Assert.assertTrue(
						driver.findElement(By.xpath(DataElement + k + "]" + "/td[" + j + "]")).getText() != null);
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(DataElement + k + "]")));
			// driver.findElement(By.xpath(DataElement+k+"]"
			Log.info("----------DATA ENDS-------------------");
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getRadioButton() {
		try{
		List<WebElement> Element = driver
				.findElements(By.xpath("//div[@class='switch-title inline']/following-sibling::label"));
		java.util.Iterator<WebElement> i = Element.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			Log.info("Text is :  " + row.getText());
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getRequestData() {
		try{
		List<WebElement> rows = driver.findElements(By.xpath("//h4/following::label/span"));
		java.util.Iterator<WebElement> i = rows.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			Log.info("Text is :" + row.getText());
			//Assert.assertTrue(row.getText() != null);
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getSpecInformation() {
		try{
		//driver.navigate().refresh();
		for (int j = 1; j <= 2; j++) {
			for (int i = 1; i <= 2; i++) {
				Log.info("Text is :" + driver
						.findElement(By.xpath("//div[contains(@class,'border-light-grey')]/div/table/tbody/tr["
								+ j + "]/td[" + i + "]/div"))
						.getText());
				driver.navigate().refresh();
			}

		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getAllSpecsData() {
		try{
		Log.info(pl.INUSETEXT.getText());
		Log.info(pl.TOTALINUSETEXT.getText());
		getTableData();
		getSubmissionTabData();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getTableData() {
		try{
		WebElement DataElement = driver
				.findElement(By.xpath("//table[@class='table table-striped hover-table custom-checkbox']/tbody/tr"));
		List<WebElement> rows = DataElement.findElements(By.tagName("td"));
		WebElement HeadElement = driver
				.findElement(By.xpath("//table[@class='table table-striped hover-table custom-checkbox']/thead/tr"));
		List<WebElement> heads = HeadElement.findElements(By.tagName("th"));
		java.util.Iterator<WebElement> j = heads.iterator();
		while (j.hasNext()) {
			WebElement head = j.next();
			Log.info("Text is    :" + head.getText());
			Assert.assertTrue(head.getText() != null);
		}
		Log.info("Text is :" + rows.size());

		java.util.Iterator<WebElement> i = rows.iterator();
		while (i.hasNext()) {
			if (rows.size() < 2) {
				Log.info("No Records Found");
				WebElement row = i.next();
				Log.info("Text is :" + row.getText());
			} else {
				WebElement row = i.next();
				Log.info("Text is :" + row.getText());
			}
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getSubmissionTabData() {
		try{
		common = new CommonLocators();
		pl=new ProductsPageLocators();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.SUBMISSIONTAB);
		pl.SUBMISSIONTAB.isDisplayed();
		Log.info("Text is :" + pl.SUBMISSIONTAB.getText());
		Assert.assertTrue(pl.SUBMISSIONTAB.getText() != null);
		getSubmissionTableData();
		pl.SEARCHSUBMISSION.isDisplayed();
		Log.info("Text is :" + pl.SEARCHSUBMISSION.getText());
		Assert.assertTrue(pl.SEARCHSUBMISSION.getText() != null);
		Log.info("Text is :" + pl.TEXTDROPDOWNALLSUBMISSION.getText());
		Assert.assertTrue(pl.TEXTDROPDOWNALLSUBMISSION.getText() != null);
		// DROPDOWNALLSUBMISSION.click();
		// getAllSubmissionDropDownData();
		// DROPDOWNALLSUBMISSION.click();
		// next tab
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", pl.SPECREQUESTTAB);
		pl.SPECREQUESTTAB.click();
		Log.info("Text is :" + pl.SPECREQUESTTAB.getText());
		Assert.assertTrue(pl.SPECREQUESTTAB.getText() != null);
		getTableData();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getAllSubmissionDropDownData() {
		try{
		List<WebElement> rows = driver
				.findElements(By.xpath("//ul[@class='dropdown-menu']/li//div[@class='media-body media-middle']"));
		java.util.Iterator<WebElement> i = rows.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			Log.info("Text Is :" + row.getText());
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getSubmissionTableData() {
		try{
		WebElement DataElement = driver
				.findElement(By.xpath("//table[@class='table table-striped hover-table custom-checkbox']/tbody/tr"));
		List<WebElement> rows = DataElement.findElements(By.tagName("td"));
		List<WebElement> HeadElement = driver.findElements(
				By.xpath("//table[@class='table table-striped hover-table custom-checkbox']/thead/tr/th/div/span"));
		// List<WebElement>heads = HeadElement.findElements(By.tagName("span"));
		for (int j = 1; j <= HeadElement.size(); j++) {
			Log.info("Text is :" + driver
					.findElement(By
							.xpath("//table[@class='table table-striped hover-table custom-checkbox']//span[@class='font-weight-700']"))
					.getText());
		}
		Log.info("Text is :" + rows.size());

		java.util.Iterator<WebElement> i = rows.iterator();
		while (i.hasNext()) {
			if (rows.size() < 2) {
				Log.info("No Records Found");
				WebElement row = i.next();
				Log.info("Text is :" + row.getText());
			} else {
				WebElement row = i.next();
				Log.info("Text is :" + row.getText());
			}
		}
	
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
		throw e;
	}
}
}
