package com.race.qa.pages.functional;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.AdministrativeLocators;
import com.race.qa.locator.CommonLocators;


public class FunctionalAdministrativePage extends TestBase {
	protected AdministrativeLocators adl;
	protected CommonLocators common;
	WebDriver driver;
	
	public FunctionalAdministrativePage(WebDriver driver){
		this.driver=driver;
		this.driver=DriverFactory.getDriver();
	}
	public String selectCountryAdmin(String country) throws Exception{
		String selectedCountry=null;
		try{
		selectOptionFromDropdown(common.DDCOUNTRY, common.CONTAINEROPTION, common.getCONTAINER(),
				common.DDSELECTOPTION, common.getDDSEARCHINPUT(), country);
		jsWait();
		Assert.assertEquals(driver.findElement(By.xpath("//*[@name='country']/div/button/div/div[2]")).getText()
				, country);
		selectedCountry = driver.findElement(By.xpath("//*[@name='country']/div/button/div/div[2]")).getText();
		}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return selectedCountry;
	}
}
