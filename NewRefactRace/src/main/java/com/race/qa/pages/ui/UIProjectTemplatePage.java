package com.race.qa.pages.ui;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.util.Log;
import com.race.qa.locator.ProjectTemplateLocators;


public class UIProjectTemplatePage extends TestBase {
	WebDriver driver;
	protected CommonLocators common ;
	protected ProjectTemplateLocators ptl;
	
	
	public UIProjectTemplatePage(WebDriver driver) {
		this.driver=driver;
		this.driver=DriverFactory.getDriver();
	}

	public void validateExistingTemplateTabScreen() throws Throwable{
		common=new CommonLocators();
		ptl=new ProjectTemplateLocators();
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PROJECTTEMPLATES());
			common.getMENU_PROJECTTEMPLATES().click();
			awaitForElement((ptl.BARCHART));
			awaitForElement(ptl.EXISTINGTEMPLATETAB);
			ptl.EXISTINGTEMPLATETAB.isDisplayed();
			Log.info("Text is  :" + ptl.EXISTINGTEMPLATETAB.getText());
			Assert.assertTrue(ptl.EXISTINGTEMPLATETAB.getText() != null);
			ptl.HEADING.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ptl.HEADINGEXISTINGTEMPLATE);
			ptl.HEADINGEXISTINGTEMPLATE.isDisplayed();
			Log.info("Text is  :" + ptl.HEADINGEXISTINGTEMPLATE.getText());
			Assert.assertTrue(ptl.HEADINGEXISTINGTEMPLATE.getText() != null);
			verifyTable();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
	public void addNewTemplate()throws StackOverflowError,Throwable{
		//Need category before creating this
		common=new CommonLocators();
		ptl=new ProjectTemplateLocators();
		try{
		if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
			Log.info("PIE chart available");
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed());
			}
			else{
				Log.info("PIE CHART not available");
			}
		awaitForElement(common.getMENU_PROJECTTEMPLATES());
		common.getMENU_PROJECTTEMPLATES().click();
		wait.until(ExpectedConditions.visibilityOf(ptl.BARCHART));
		ptl.EXISTINGTEMPLATETAB.isDisplayed();
		Log.info("Text is :" + ptl.NEWTEMPLATETAB.getText());
		Assert.assertTrue(ptl.NEWTEMPLATETAB.getText() != null);
		ptl.NEWTEMPLATETAB.click();
		wait.until(ExpectedConditions.visibilityOf(ptl.INPUTTEMPLATENAME));
		ptl.INPUTTEMPLATENAME.isDisplayed();
		ptl.INPUTTEMPLATENAME.sendKeys(configProperities.getProperty("templatename"));
		ptl.SELECTCATEGORIES.click();
		getDropdownData(ptl.DROPDOWNDATA, "li");
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[@class='race-select-body ps-container ps-theme-default ps-active-y']/ul/li/a[text()='testinggs']")));
		//driver.findElement(By.xpath("//div[@class='race-select-body ps-container ps-theme-default ps-active-y']/ul/li/a[text()='testinggs']")).click();
		ptl.SELECTTEMPLATETYPE.click();
		driver.findElement(By.xpath("//a[text()='New Product Development']")).click();
		ptl.DESCRIPTION.sendKeys("Test");
		driver.findElement(By.xpath("//div[@class='row page-buttons']/div/button[3]")).click();
		ptl.EXISTINGTEMPLATETAB.click();
		wait.until(ExpectedConditions.visibilityOf(ptl.BARCHART));
		driver.findElement(By.xpath("//input[@class='form-control']")).sendKeys(configProperities.getProperty("templatename"));
		verifyTable();
	}
		catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}	

	public void validateNewTemplateTabScreen() throws Throwable{
		common=new CommonLocators();
		ptl=new ProjectTemplateLocators();
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PROJECTTEMPLATES());
			common.getMENU_PROJECTTEMPLATES().click();
			awaitForElement(ptl.BARCHART);
			awaitForElement(ptl.EXISTINGTEMPLATETAB);
			ptl.EXISTINGTEMPLATETAB.isDisplayed();
			Log.info("Text is :" + ptl.NEWTEMPLATETAB.getText());
			Assert.assertTrue(ptl.NEWTEMPLATETAB.getText() != null);
			ptl.NEWTEMPLATETAB.click();
			wait.until(ExpectedConditions.visibilityOf(ptl.SELECTCATEGORIES));
			getAllTextInPopup();
			// INPUTTEMPLATENAME.isDisplayed();
			ptl.SELECTCATEGORIES.isDisplayed();
			awaitForElement(ptl.SELECTCATEGORIES);
			ptl.SELECTCATEGORIES.click();
			getDropdownData(ptl.CATEGORYDD, "li");
			ptl.SELECTCATEGORIES.click();
			ptl.SELECTTEMPLATETYPE.isDisplayed();
			ptl.SELECTTEMPLATETYPE.click();
			// getDropdownData();
			ptl.SELECTTEMPLATETYPE.click();
			ptl.DESCRIPTION.isDisplayed();
			ptl.HEADINGPHASES.isDisplayed();
			Log.info("Text is :" + ptl.HEADINGPHASES.getText());
			Assert.assertTrue(ptl.HEADINGPHASES.getText() != null);
			//getButtons();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateAddNewPhaseScreen() throws Throwable{
		common=new CommonLocators();
		ptl=new ProjectTemplateLocators();
		try{
			
			if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
			awaitForElement(common.getMENU_PROJECTTEMPLATES());
			common.getMENU_PROJECTTEMPLATES().click();
			awaitForElement(ptl.BARCHART);
			awaitForElement(ptl.EXISTINGTEMPLATETAB);
			ptl.EXISTINGTEMPLATETAB.isDisplayed();
			ptl.NEWTEMPLATETAB.click();
			wait.until(ExpectedConditions.visibilityOf(ptl.ADDNEWPHASEBUTTON));
			ptl.ADDNEWPHASEBUTTON.isDisplayed();
			Log.info(ptl.ADDNEWPHASEBUTTON.getText());
			Assert.assertTrue(ptl.ADDNEWPHASEBUTTON.getText() != null);
			ptl.ADDNEWPHASEBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(ptl.HEADINGPOP));
			ptl.HEADINGPOP.isDisplayed();
			Log.info(ptl.HEADINGPOP.getText());
			Assert.assertTrue(ptl.HEADINGPOP.getText() != null);
			getAllTextInPopup();
			ptl.CHECKBOXPHASECOLOR.isDisplayed();
			common.getCLOSEBUTTON().isDisplayed();
			common.getSAVEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
	
	public void verifyTable()throws Throwable {
		try{
		WebElement Table = driver.findElement(By.xpath("//*[@class='table table-striped']/tbody"));
		List<WebElement> rows = Table.findElements(By.tagName("td"));
			if (rows.size() == 0) {
				Log.info("No Data");
			} else {
				java.util.Iterator<WebElement> i = rows.iterator();
				Log.info("----------DATA STARTS-------------------");
				while (i.hasNext()) {
					WebElement row = i.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
				}
			}
			Log.info("----------DATA ENDS-------------------");
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getButtons() throws Throwable{
		WebElement Table = driver.findElement(By.xpath("//*[@class='row page-buttons']/div"));
		try {
			List<WebElement> rows = Table.findElements(By.tagName("button"));
			if (rows.size() == 0) {
				Log.info("No Data");
			} else {
				java.util.Iterator<WebElement> i = rows.iterator();
				Log.info("----------DATA STARTS-------------------");
				while (i.hasNext()) {
					WebElement row = i.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
					// ((JavascriptExecutor)
					// driver).executeScript("arguments[0].scrollIntoView(true);",
					// row);
				}
			}
			Log.info("----------DATA ENDS-------------------");
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
}
