package com.race.qa.pages.ui;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.DashboardLocators;
import com.race.qa.util.Log;

public class UIDashboardPage extends TestBase {
	WebDriver driver;
	CommonLocators common;
	DashboardLocators dbLocators;

	public UIDashboardPage(WebDriver driver) {
		this.driver = driver;
		this.driver = DriverFactory.getDriver();
		// PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30),
		// this);

	}

	public void createDuplicateProject(boolean withSupplier) throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			Assert.assertTrue(dbLocators.getDB_STARTPROJECTBUTTON().isDisplayed());
			Assert.assertEquals(dbLocators.getDB_STARTPROJECTBUTTON().getText(),
					selectPropertiesFile(true, locale, "startnewproject"));
			dbLocators.getDB_STARTPROJECTBUTTON().click();
			awaitForElement(dbLocators.getDB_DDCATEGORY());
			// Next button is disabled
			Assert.assertFalse(dbLocators.getDB_BUTTONNEXT().isEnabled());
			// All the labels
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='form-group']/label)[1]")).getText(),
					selectPropertiesFile(true, locale, "category"));
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='form-group']/label)[2]")).getText(),
					selectPropertiesFile(true, locale, "template"));
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='form-group']/label)[3]")).getText(),
					selectPropertiesFile(true, locale, "projectname"));
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='form-group']/label)[4]")).getText(),
					selectPropertiesFile(true, locale, "projectmanager"));
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='form-group']/label)[5]")).getText(),
					selectPropertiesFile(true, locale, "supplier"));
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='form-group']/label)[6]")).getText(),
					selectPropertiesFile(true, locale, "startdate"));
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='form-group']/label)[7]")).getText(),
					selectPropertiesFile(true, locale, "targetlaunchdate"));
			// Dropdowns internal texts
			Assert.assertEquals(driver.findElement(By.xpath("//ul[@class='race-tabs']/li/a")).getText(),
					selectPropertiesFile(true, locale, "newproject"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//div[@class='select-info-body media-middle tall'])[1]")).getText(),
					selectPropertiesFile(true, locale, "selectcategories"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//div[@class='select-info-body media-middle tall'])[2]")).getText(),
					selectPropertiesFile(true, locale, "selecttemplate"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//div[@class='select-info-body media-middle tall'])[3]")).getText(),
					selectPropertiesFile(true, locale, "none"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//div[@class='select-info-body media-middle tall'])[4]")).getText(),
					selectPropertiesFile(true, locale, "selectsuppliers"));
			// Enter existing Project Name
			selectOptionFromDropdown(dbLocators.getDB_DDCATEGORY(), common.CONTAINEROPTION, common.getCONTAINER(),
					common.SELECTEDOPTION, common.getDDSEARCHINPUT(), configProperities.getProperty("familyname"));
			Assert.assertTrue(dbLocators.getDB_DDTEMPLATE().isEnabled());
			selectOptionFromDropdown(dbLocators.getDB_DDTEMPLATE(), common.CONTAINEROPTION, common.getCONTAINER(),
					common.SELECTEDOPTION, common.getDDSEARCHINPUT(), configProperities.getProperty("templatename"));
			String INPUTPROJECTNAME = "First Project";
			dbLocators.getDB_INPUTPROJECTNAME().sendKeys(INPUTPROJECTNAME);
			Log.info("Project Name is  " + INPUTPROJECTNAME);
			selectOptionFromDropdown(dbLocators.getDB_DDTEAMMANEGER(), common.CONTAINEROPTION, common.getCONTAINER(),
					common.SELECTEDOPTION, common.getDDSEARCHINPUT(), configProperities.getProperty("groupleadname"));
			if (withSupplier) {
				selectOptionFromDropdown(dbLocators.getDB_DDSUPPLIER(), common.CONTAINEROPTION, common.getCONTAINER(),
						common.SELECTEDOPTION, common.getDDSEARCHINPUT(),
						configProperities.getProperty("suppliername"));
			}
			Assert.assertEquals(dbLocators.getDB_BUTTONNEXT().getText(), selectPropertiesFile(true, locale, "next"));
			dbLocators.getDB_BUTTONNEXT().click();
			awaitForElement(driver.findElement(By.xpath("//div[@class='alert alert-danger ng-star-inserted']")));
			// Assert.assertEquals(driver.findElement(By.xpath("//div[@class='alert
			// alert-danger ng-star-inserted']")).getText()
			// , selectPropertiesFile(true, locale, ""));
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[@class='alert alert-danger ng-star-inserted']")).isDisplayed());
			System.out.println(
					driver.findElement(By.xpath("//div[@class='alert alert-danger ng-star-inserted']")).getText());
			Assert.assertEquals(
					driver.findElement(By.xpath("//div[@class='alert alert-danger ng-star-inserted']")).getText(),
					selectPropertiesFile(false, locale, "projectwiththesamenamealreadyexists"));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateDashboardMenus() throws Exception {
		Integer menuCount;
		WebDriver driver = DriverFactory.getDriver();
		try {
			menuCount = driver.findElements(By.xpath("//div[@class='main-nav-items']/ul/li")).size();
			for (int u = 2; u <= menuCount; u++) {
				if (u == 2) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "dashboard"));
				} else if (u == 3) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "strategy"));
				} else if (u == 4) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "products"));
				} else if (u == 5) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "supplier"));
				} else if (u == 6) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "teammanagement"));
				} else if (u == 7) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "projecttemplates"));
				} else if (u == 8) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "administration"));
				} else if (u == 9) {
					Assert.assertEquals(
							driver.findElement(By.xpath("//div[@class='main-nav-items']/ul/li[" + u + "]")).getText(),
							selectPropertiesFile(true, locale, "reports"));
				} else {
					Log.info("Matching data not found");
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProjectStatus() throws Exception {
		Integer projectStatus;
		common = new CommonLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			projectStatus = driver
					.findElements(By
							.xpath("(//div[@class='row no-margin task-row projects-filter']/div/following-sibling::div)//h4"))
					.size();
			for (int u = 1; u <= projectStatus; u++) {
				if (u == 1) {
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin task-row projects-filter']//h4)[" + u + "]"))
							.getCssValue("border").contains("rgb(255, 185, 0)"));
				} else if (u == 2) {
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin task-row projects-filter']//h4)[" + u + "]"))
							.getCssValue("border").contains("rgb(103, 211, 224)"));
				} else if (u == 3) {
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin task-row projects-filter']//h4)[" + u + "]"))
							.getCssValue("border").contains("rgb(63, 197, 157)"));
				} else if (u == 4) {
					Assert.assertTrue(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin task-row projects-filter']//h4)[" + u + "]"))
							.getCssValue("border").contains("rgb(242, 62, 70)"));
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateDashboardTaskMenu() throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		Integer menuTask;
		common = new CommonLocators();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			Thread.sleep(1000);
			Assert.assertEquals(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")).getText(),
					selectPropertiesFile(true, locale, "mydashboard"));
			Assert.assertEquals(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[2]")).getText(),
					selectPropertiesFile(true, locale, "supplier"));
			menuTask = driver.findElements(By.xpath("(//div[@class='row no-margin'])/h2/following-sibling::span"))
					.size();
			for (int u = 1; u <= menuTask; u++) {
				if (u == 1) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin'])[" + u + "]/h2/following-sibling::span"))
							.getText(), selectPropertiesFile(true, locale, "opentasks"));
					if (Integer.parseInt(
							driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]/div/h2"))
									.getText()) != 0) {
						Log.info("Task Box has data and clickable");
						driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]")).click();
						Log.info("Selected Task Section Clicked");
						wait.until(ExpectedConditions.visibilityOf(driver.findElement(
								By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div"))));
						Assert.assertTrue(
								driver.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/h2"))
										.getText().contains(selectPropertiesFile(true, locale, "opentasks")),
								"No Match found");
						if (driver
								.findElements(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
								.size() != 0) {
							Assert.assertTrue(
									driver.findElement(By
											.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
											.getText().contains(selectPropertiesFile(true, locale, "viewmore")),
									"No Match found");
						} else {
							Log.info("View More Button is not available");
						}
						Assert.assertTrue(driver
								.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm fix-height-400']/div/h2"))
								.getText().contains(selectPropertiesFile(true, locale, "top5projectsbyopentasks")),
								"No Match found");
						Assert.assertTrue(
								driver.findElement(By
										.xpath("//div[@class='race-select-container form-select left']//div[@class='select-info']"))
										.getText().contains(selectPropertiesFile(true, locale, "project_1")),
								"No Match found");
					} else {
						Log.info("No Task Data found");
					}
				} else if (u == 2) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin'])[" + u + "]/h2/following-sibling::span"))
							.getText(), selectPropertiesFile(true, locale, "tasksending"));
					if (Integer.parseInt(
							driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]/div/h2"))
									.getText()) != 0) {
						Log.info("Task Box has data and clickable");
						driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]")).click();
						Log.info("Selected Task Section Clicked");
						wait.until(ExpectedConditions.visibilityOf(driver.findElement(
								By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div"))));
						Assert.assertTrue(
								driver.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/h2"))
										.getText().contains(selectPropertiesFile(true, locale, "tasksending")),
								"No Match found");
						if (driver
								.findElements(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
								.size() != 0) {
							Assert.assertTrue(
									driver.findElement(By
											.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
											.getText().contains(selectPropertiesFile(true, locale, "viewmore")),
									"No Match found");
						} else {
							Log.info("View More Button is not available");
						}
						Assert.assertTrue(driver
								.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm fix-height-400']/div/h2"))
								.getText().contains(selectPropertiesFile(true, locale, "tasksendingbyprojects")),
								"No Match found");
					} else {
						Log.info("No Task Data found");
					}
				} else if (u == 3) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin'])[" + u + "]/h2/following-sibling::span"))
							.getText(), selectPropertiesFile(true, locale, "tasksdelayed"));
					if (Integer.parseInt(
							driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]/div/h2"))
									.getText()) != 0) {
						Log.info("Task Box has data and clickable");
						driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]")).click();
						Log.info("Selected Task Section Clicked");
						wait.until(ExpectedConditions.visibilityOf(driver.findElement(
								By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div"))));
						Assert.assertTrue(
								driver.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/h2"))
										.getText().contains(selectPropertiesFile(true, locale, "tasksdelayed")),
								"No Match found");
						if (driver
								.findElements(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
								.size() != 0) {
							Assert.assertTrue(
									driver.findElement(By
											.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
											.getText().contains(selectPropertiesFile(true, locale, "viewmore")),
									"No Match found");
						} else {
							Log.info("View More Button is not available");
						}
						Assert.assertTrue(
								driver.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm fix-height-400']/div/h2"))
										.getText().contains(selectPropertiesFile(true, locale, "topdelayedtasks")),
								"No Match found");
					} else {
						Log.info("No Task Data found");
					}
				} else if (u == 4) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin'])[" + u + "]/h2/following-sibling::span"))
							.getText(), selectPropertiesFile(true, locale, "pendingspecs"));
					if (Integer.parseInt(
							driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]/div/h2"))
									.getText()) != 0) {
						Log.info("Task Box has data and clickable");
						driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]")).click();
						Log.info("Selected Task Section Clicked");
						wait.until(ExpectedConditions.visibilityOf(driver.findElement(
								By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div"))));
						Assert.assertTrue(driver
								.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/h2"))
								.getText().contains(selectPropertiesFile(true, locale, "pendingspecifications")),
								"No Match found");
						if (driver
								.findElements(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
								.size() != 0) {
							Assert.assertTrue(
									driver.findElement(By
											.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
											.getText().contains(selectPropertiesFile(true, locale, "viewmore")),
									"No Match found");
						} else {
							Log.info("View More Button is not available");
						}
						Assert.assertTrue(
								driver.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm fix-height-400']/div/h2"))
										.getText().contains(selectPropertiesFile(true, locale, "specsbyproduct")),
								"No Match found");
					} else {
						Log.info("No Task Data found");
					}
				} else if (u == 5) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("(//div[@class='row no-margin'])[" + u + "]/h2/following-sibling::span"))
							.getText(), selectPropertiesFile(true, locale, "newsubmissions"));
					if (Integer.parseInt(
							driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]/div/h2"))
									.getText()) != 0) {
						Log.info("Task Box has data and clickable");
						driver.findElement(By.xpath("(//*[@class='row no-margin task-row']/div)[" + u + "]")).click();
						Log.info("Selected Task Section Clicked");
						wait.until(ExpectedConditions.visibilityOf(driver.findElement(
								By.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div"))));
						Assert.assertTrue(driver
								.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/h2"))
								.getText().contains(selectPropertiesFile(true, locale, "recentdeliverablesubmissions")),
								"No Match found");
						if (driver
								.findElements(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
								.size() != 0) {
							Assert.assertTrue(
									driver.findElement(By
											.xpath("//div[@class='panel-body bg-white margin-bottom-sm status-column']/div/button/span"))
											.getText().contains(selectPropertiesFile(true, locale, "viewmore")),
									"No Match found");
						} else {
							Log.info("View More Button is not available");
						}
						Assert.assertTrue(driver
								.findElement(By
										.xpath("//div[@class='panel-body bg-white margin-bottom-sm fix-height-400']/div/h2"))
								.getText().contains(selectPropertiesFile(true, locale, "recentdeliverablesubmissions")),
								"No Match found");
					} else {
						Log.info("No Task Data found");
					}
				} else {
					Log.info("No matching values displayed");
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateDashboardFilters() throws Exception {
		Integer label;
		common = new CommonLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//h3[@class='font-size-16 text-2e3d51 font-weight-400 no-margin']")));
			Assert.assertTrue(
					driver.findElement(By.xpath("//h3[@class='font-size-16 text-2e3d51 font-weight-400 no-margin']"))
							.getText().contains(selectPropertiesFile(true, locale, "projects")));
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='filter-container']//span")).isDisplayed());
			System.out.println("1" + driver.findElement(By.xpath("//div[@class='filter-container']//span")).getText());
			System.out.println("2" + selectPropertiesFile(true, locale, "filter"));
			Assert.assertEquals(driver.findElement(By.xpath("//div[@class='filter-container']//span")).getText(),
					selectPropertiesFile(true, locale, "filter"));
			wait.until(ExpectedConditions.refreshed(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath("//div[@class='filter-container']/button")))));
			driver.findElement(By.xpath("//div[@class='filter-container']/button")).click();
			Thread.sleep(1000);
			awaitForElement(driver.findElement(By.xpath("//div[contains(@class,'filter-box')]")));
			label = driver
					.findElements(By.xpath("//div[contains(@class,'filter-box')]//div[@class='form-group']/label"))
					.size();
			for (int u = 1; u <= label; u++) {
				if (u == 1) {
					awaitForElement(driver.findElement(By.xpath(
							"(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]")));
					Assert.assertEquals(driver.findElement(By
							.xpath("(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "templatetype"));
					System.out.println(
							"hsjshsj" + driver.findElement(By.xpath("//*[@name='templateType']//div[2]")).getText());
					System.out.println(selectPropertiesFile(true, locale, "selecttemplatetype"));
					Assert.assertEquals(driver.findElement(By.xpath("//*[@name='templateType']//div[2]")).getText(),
							(selectPropertiesFile(true, locale, "selecttemplatetype")));

				} else if (u == 2) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "category"));
					Assert.assertTrue(driver.findElement(By.xpath("//*[@name='category']//div[2]")).getText()
							.equals(selectPropertiesFile(true, locale, "selectcategory")));
				} else if (u == 3) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "manager"));
					Assert.assertTrue(driver.findElement(By.xpath("//*[@name='manager']//div[2]")).getText()
							.equals(selectPropertiesFile(true, locale, "selectmanager")));
				} else if (u == 4) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "task"));
				} else if (u == 5) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "STATUS"));
					Assert.assertTrue(driver.findElement(By.xpath("//*[@name='status']//div[2]")).getText()
							.equals(selectPropertiesFile(true, locale, "selectstatus")));
				} else if (u == 6) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "from"));
					Assert.assertTrue(driver.findElement(By.xpath("//*[@name='fromDate']/div/a/div/div[1]")).getText()
							.equals(selectPropertiesFile(true, locale, "targetlaunch")));

				} else if (u == 7) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//div[contains(@class,'filter-box')]//div[@class='form-group']/label)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "To"));
					Assert.assertTrue(driver.findElement(By.xpath("//*[@name='toDate']/div/a/div/div[1]")).getText()
							.equals(selectPropertiesFile(true, locale, "targetlaunch")));
				} else {
					Log.info("No matching record found");
				}

			}
			Assert.assertEquals(driver.findElement(By.xpath("//button[@class='btn btn-blank btn-danger']")).getText(),
					selectPropertiesFile(true, locale, "clearallfilters"));
			Assert.assertEquals(driver.findElement(By.xpath("//button[@class='btn btn-primary-green']")).getText(),
					selectPropertiesFile(true, locale, "applyfilter"));
			Assert.assertEquals(driver.findElement(By.xpath("//button[@class='btn btn-input']")).getText(),
					selectPropertiesFile(true, locale, "cancel"));
			// check on apply the filter button gets black
			driver.findElement(By.xpath("//button[@class='btn btn-primary-green']")).click();
			Thread.sleep(500);
			Assert.assertEquals(driver
					.findElement(By
							.xpath("//button[contains(@class,'btn flex flex-space-between flex-align-center padding-left-xs padding-right-xs fix-mozilla min-width')]"))
					.getCssValue("background-color"), "rgba(46, 61, 82, 1)");
			// clear Filter will make disappear the black back ground color
			driver.findElement(By.xpath("//div[@class='filter-container']")).click();
			awaitForElement(driver.findElement(By.xpath("//div[contains(@class,'filter-box')]")));
			Assert.assertTrue(
					driver.findElement(By.xpath("//button[@class='btn btn-blank btn-danger']")).isDisplayed());
			driver.findElement(By.xpath("//button[@class='btn btn-blank btn-danger']")).click();
			Thread.sleep(500);
			Assert.assertEquals(driver
					.findElement(By
							.xpath("//button[contains(@class,'btn flex flex-space-between flex-align-center padding-left-xs padding-right-xs fix-mozilla min-width')]"))
					.getCssValue("background-color"), "rgba(255, 255, 255, 1)");

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProjectSpace(String projectName) throws Exception {
		Integer countMenu;
		Integer countView;
		Integer countChatMenu;
		WebDriver driver = DriverFactory.getDriver();
		try {
			// Project Space side menu
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='sidebar']")).isDisplayed());
			Assert.assertTrue(driver.findElement(By.xpath("//h5[@class='sidebar-title']/span")).getText()
					.contains(selectPropertiesFile(true, locale, "project")));
			Assert.assertEquals(
					driver.findElement(By.xpath("//div[@class='race-select-container projects']//div[2]")).getText(),
					projectName);
			Assert.assertTrue(driver
					.findElement(By.xpath("//h4[@class='suppliers-title race-popup flex flex-align-center']/span"))
					.getText().contains(selectPropertiesFile(true, locale, "suppliers")));
			Assert.assertTrue(
					driver.findElement(By.xpath("(//div[@class='media-middle overflow-initial race-popup']//p)[1]"))
							.getText().contains(selectPropertiesFile(true, locale, "phase")));
			Assert.assertEquals(driver.findElement(By.xpath("//div[@class='top-bar-container']/div/span")).getText(),
					selectPropertiesFile(true, locale, "projectspace"));
			// Project space >top section
			Assert.assertEquals(
					driver.findElement(By.xpath("//*[@alt='Bar Code']/following-sibling::span[1]")).getText(),
					selectPropertiesFile(true, locale, "noproductsselected"));
			Assert.assertTrue(driver.findElement(By.xpath("(//*[@alt='Settings Menu'])[2]")).isDisplayed());
			driver.findElement(By.xpath("(//*[@alt='Settings Menu'])[2]")).click();
			awaitForElement(driver.findElement(By.xpath(
					"//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul")));
			Assert.assertTrue(driver
					.findElement(By
							.xpath("//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul"))
					.isDisplayed());
			countMenu = driver
					.findElements(By
							.xpath("//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul/li"))
					.size();
			for (int u = 1; u <= countMenu; u++) {
				if (u == 1) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul/li/a)["
									+ u + "]"))
							.getText(), selectPropertiesFile(true, locale, "newtask"));
				} else if (u == 2) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul/li/a)["
									+ u + "]"))
							.getText(), selectPropertiesFile(true, locale, "editsupplier"));
				} else if (u == 3) {
					// Assert.assertEquals(driver.findElement(By.xpath("(//button[@class='btn
					// btn-primary-green btn-round
					// race-dropdown-toggle']/following-sibling::ul/li/a)["+u+"]"))
					// .getText(), selectPropertiesFile(true, locale,
					// "newphase"));
				} else if (u == 4) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul/li/a)["
									+ u + "]"))
							.getText(), selectPropertiesFile(true, locale, "newproduct"));
				} else {
					Log.info("No matching record found");
				}
			}
			driver.findElement(By.xpath("(//*[@alt='Settings Menu'])[2]")).click();
			Thread.sleep(100);
			Assert.assertTrue(
					driver.findElement(By.xpath("//button[@class='btn btn-warning view-btn race-dropdown-toggle']"))
							.isDisplayed());
			Assert.assertEquals(
					driver.findElement(By.xpath("//button[@class='btn btn-warning view-btn race-dropdown-toggle']"))
							.getText(),
					selectPropertiesFile(true, locale, "view"));
			driver.findElement(By.xpath("//button[@class='btn btn-warning view-btn race-dropdown-toggle']")).click();
			awaitForElement(driver.findElement(By
					.xpath("//button[@class='btn btn-warning view-btn race-dropdown-toggle']/following-sibling::ul")));
			Assert.assertTrue(driver
					.findElement(By
							.xpath("//button[@class='btn btn-warning view-btn race-dropdown-toggle']/following-sibling::ul"))
					.isDisplayed());
			countView = driver
					.findElements(By
							.xpath("//button[@class='btn btn-warning view-btn race-dropdown-toggle']/following-sibling::ul/li"))
					.size();
			for (int u = 1; u <= countView; u++) {
				if (u == 1) {
					Assert.assertEquals(
							driver.findElement(By
									.xpath("(//button[@class='btn btn-warning view-btn race-dropdown-toggle']/following-sibling::ul/li/a)["
											+ u + "]"))
									.getText(),
							selectPropertiesFile(true, locale, "ganttchartview"));
				} else if (u == 2) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//button[@class='btn btn-warning view-btn race-dropdown-toggle']/following-sibling::ul/li/a)["
									+ u + "]"))
							.getText(), selectPropertiesFile(true, locale, "gridview"));
				} else if (u == 3) {
					Assert.assertEquals(driver.findElement(By
							.xpath("(//button[@class='btn btn-warning view-btn race-dropdown-toggle']/following-sibling::ul/li/a)["
									+ u + "]"))
							.getText(), selectPropertiesFile(true, locale, "summary"));
				} else {
					Log.info("No matching record found");
				}
			}
			driver.findElement(By.xpath("//button[@class='btn btn-warning view-btn race-dropdown-toggle']")).click();
			Thread.sleep(100);
			// Project space> Message section
			Assert.assertTrue(
					driver.findElement(By.xpath("(//ul[@class='race-tabs no-margin-bottom']/li)[1]")).isDisplayed());
			Assert.assertEquals(
					driver.findElement(By.xpath("(//ul[@class='race-tabs no-margin-bottom']/li)[1]")).getText(),
					selectPropertiesFile(true, locale, "collaboration"));
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[@class='input-group-btn']/button/span")).isDisplayed());
			Assert.assertEquals(driver.findElement(By.xpath("//div[@class='input-group-btn']/button/span")).getText(),
					selectPropertiesFile(true, locale, "all"));
			driver.findElement(By.xpath("//div[@class='input-group-btn']/button/span")).click();
			awaitForElement(driver.findElement(By.xpath("//div[contains(@class,'input-group-btn')]/ul")));
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class,'input-group-btn')]/ul")).isDisplayed());
			countChatMenu = driver.findElements(By.xpath("//div[contains(@class,'input-group-btn')]/ul/li")).size();
			for (int u = 1; u <= countChatMenu; u++) {
				if (u == 1) {
					Assert.assertEquals(driver
							.findElement(By.xpath("(//div[contains(@class,'input-group-btn')]/ul/li/a)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "all"));
				} else if (u == 2) {
					// Assert.assertEquals(driver.findElement(By.xpath("(//div[contains(@class,'input-group-btn')]/ul/li/a)["+u+"]"))
					// .getText(), selectPropertiesFile(true, locale, "todos"));
				} else if (u == 3) {
					Assert.assertEquals(driver
							.findElement(By.xpath("(//div[contains(@class,'input-group-btn')]/ul/li/a)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "pictures"));
				} else if (u == 4) {
					// Assert.assertEquals(driver.findElement(By.xpath("(//div[contains(@class,'input-group-btn')]/ul/li/a)["+u+"]"))
					// .getText(), selectPropertiesFile(true, locale,
					// "videos"));
				} else if (u == 5) {
					Assert.assertEquals(driver
							.findElement(By.xpath("(//div[contains(@class,'input-group-btn')]/ul/li/a)[" + u + "]"))
							.getText(), selectPropertiesFile(true, locale, "files"));
				} else {
					Log.info("No Matching records found");
				}
			}
			driver.findElement(By.xpath("//div[contains(@class,'input-group-btn')]/button/span")).click();
			Thread.sleep(100);
			Assert.assertTrue(driver.findElement(By.xpath("//h3[@class='font-size-18 font-weight-600 text-center']"))
					.getText().contains(selectPropertiesFile(true, locale, "addamessagetostart")));
			Assert.assertTrue(driver.findElement(By.xpath("//h3[@class='font-size-18 font-weight-600 text-center']"))
					.getText().contains(selectPropertiesFile(true, locale, "collaborating")));
			// Task Section
			Assert.assertEquals(driver.findElement(By.xpath("//div[@class='panel-heading']/span")).getText(),
					selectPropertiesFile(true, locale, "taskdetails"));
			Assert.assertEquals(driver.findElement(By.xpath("//div[@class='panel-heading']/button")).getText(),
					selectPropertiesFile(false, locale, "notstarted"));
			for (int u = 1; u <= driver.findElements(By.xpath("(//div[@class='row task-date-details']/div/label)"))
					.size(); u++) {
				if (u == 1) {
					Assert.assertEquals(
							driver.findElement(By.xpath("(//div[@class='row task-date-details']/div/label)[" + u + "]"))
									.getText(),
							selectPropertiesFile(true, locale, "startdate"));
				} else if (u == 2) {
					Assert.assertEquals(
							driver.findElement(By.xpath("(//div[@class='row task-date-details']/div/label)[" + u + "]"))
									.getText(),
							selectPropertiesFile(true, locale, "enddate"));
				} else if (u == 3) {
					Assert.assertEquals(
							driver.findElement(By.xpath("(//div[@class='row task-date-details']/div/label)[" + u + "]"))
									.getText(),
							selectPropertiesFile(true, locale, "duration"));
				} else if (u == 4) {
					Assert.assertEquals(
							driver.findElement(By.xpath("(//div[@class='row task-date-details']/div/label)[" + u + "]"))
									.getText(),
							selectPropertiesFile(true, locale, "loggedtime"));
				} else {
					Log.info("No matching record found");
				}
			}
			Assert.assertEquals(driver
					.findElement(By
							.xpath("//button[@class='btn btn-primary-green padding-right-xs padding-left-xs flex flex-align-center flex-space-between']"))
					.getText(), selectPropertiesFile(true, locale, "edit"));
			Assert.assertEquals(driver
					.findElement(
							By.xpath("//button[@class='btn toggle-select btn-input assign-collaboration']//div[2]"))
					.getText(), selectPropertiesFile(true, locale, "noassignment"));
			System.out.println(
					"hererere" + driver.findElement(By.xpath("//div[@class='media-body media-middle']/h5")).getText());
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='media-body media-middle']/h5")).getText()
					.contains(selectPropertiesFile(true, locale, "approvalgroups")));
			Assert.assertEquals(
					driver.findElement(By.xpath("//div[@class='change-status up race-dropdown']/button")).getText(),
					selectPropertiesFile(true, locale, "changestatus"));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateEditTaskPage() throws Exception {
		try {
			WebDriver driver = DriverFactory.getDriver();
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='modal-content']")).isDisplayed());
			Assert.assertEquals(driver.findElement(By.xpath("//span[@class='has-strip']")).getText(),
					selectPropertiesFile(true, locale, "edittask"));
			Assert.assertEquals(
					driver.findElement(By.xpath("//span[@class='selected-color']/following-sibling::span")).getText(),
					selectPropertiesFile(true, locale, "selecttaskcolor"));
			Assert.assertEquals(
					driver.findElement(By.xpath("((//div[@class='row mobile-view padding-top-sm'])[1]/div)[1]//label"))
							.getText(),
					selectPropertiesFile(true, locale, "taskname"));
			Assert.assertEquals(driver
					.findElement(By.xpath("(((//div[@class='row mobile-view padding-top-sm'])[1]/div)[2]//label)[1]"))
					.getText(), selectPropertiesFile(true, locale, "startdate"));
			Assert.assertEquals(driver
					.findElement(By.xpath("(((//div[@class='row mobile-view padding-top-sm'])[1]/div)[2]//label)[2]"))
					.getText(), selectPropertiesFile(true, locale, "enddate"));
			Assert.assertEquals(driver
					.findElement(By
							.xpath("//button[@class='btn btn-width-block flex-align-center flex-space-between padding-left-sm padding-right-sm input-btn-height btn-input']"))
					.getText(), selectPropertiesFile(true, locale, "pmtask"));
			Assert.assertEquals(driver
					.findElement(By
							.xpath("//div[@class='col-sm-5 flex flex-justify-between flex-align-center autocomplete-column']/label"))
					.getText(), selectPropertiesFile(true, locale, "autocomplete"));
			Assert.assertEquals(driver
					.findElement(
							By.xpath("(//div[@class='switch-field inline-flex']/input/following-sibling::label)[1]"))
					.getText(), selectPropertiesFile(true, locale, "yes"));
			Assert.assertEquals(driver
					.findElement(
							By.xpath("(//div[@class='switch-field inline-flex']/input/following-sibling::label)[2]"))
					.getText(), selectPropertiesFile(true, locale, "no"));
			Assert.assertEquals(
					driver.findElement(By.xpath("//*[@name='informedGroups']/preceding::label[1]")).getText(),
					selectPropertiesFile(true, locale, "inform"));
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='informedGroups']//div[2]")).getText(),
					selectPropertiesFile(true, locale, "selectgroups"));
			Assert.assertEquals(driver
					.findElement(By
							.xpath("(//label[@class='btn btn-primary-green flex flex-space-between flex-align-center full-width-btn input-btn-height'])[1]"))
					.getText(), selectPropertiesFile(true, locale, "adddocuments"));
			Assert.assertEquals(driver
					.findElement(By
							.xpath("(//label[@class='btn btn-primary-green flex flex-space-between flex-align-center full-width-btn input-btn-height'])[2]"))
					.getText(), selectPropertiesFile(true, locale, "adddesign"));
			Assert.assertEquals(driver
					.findElement(By
							.xpath("//button[@class='btn btn-primary-green flex flex-space-between flex-align-center full-width-btn input-btn-height']"))
					.getText(), selectPropertiesFile(true, locale, "adddeliverable"));
			Assert.assertEquals(
					driver.findElement(By.xpath("//*[@name='preDependencies']/preceding::label[1]")).getText(),
					selectPropertiesFile(true, locale, "dependencies"));
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='preDependencies']//div[2]")).getText(),
					selectPropertiesFile(true, locale, "adddependency"));
			Assert.assertEquals(
					driver.findElement(By
							.xpath("//button[@class='btn btn-primary-green btn-width-120 pull-right padding-left-xs padding-right-sm']"))
							.getText(),
					selectPropertiesFile(true, locale, "save") + " " + selectPropertiesFile(true, locale, "task"));
			Assert.assertTrue(common.getCLOSEBUTTON().isDisplayed());
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateDeliverablesScreen() throws Exception {

		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			Assert.assertTrue(dbLocators.DB_ADDDELIVERABLE.isDisplayed());
			dbLocators.DB_ADDDELIVERABLE.click();
			Thread.sleep(500);
			awaitForElement(driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]")));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]")));
			Assert.assertTrue(driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]")).getText()
					.contains(selectPropertiesFile(true, locale, "deliverables")));
			Assert.assertEquals(driver
					.findElement(
							By.xpath("(//*[contains(@class,'btn toggle-select btn-input form-control')])[2]//div[2]"))
					.getText(), selectPropertiesFile(true, locale, "deliverabletype"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//*[@class='btn toggle-select btn-input form-control'])[3]//div[2]"))
							.getText(),
					selectPropertiesFile(true, locale, "approvalpath"));
			Assert.assertEquals(
					driver.findElement(By.xpath("(//*[@class='btn toggle-select btn-input form-control'])[4]//div[2]"))
							.getText(),
					selectPropertiesFile(true, locale, "approvalgroups_deliverable"));
			Assert.assertTrue(common.getCLOSEBUTTON().isDisplayed());
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateDesignScreen() throws Exception {

		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			dbLocators.DB_ADDDESIGN.click();
			awaitForElement(driver.findElement(By.xpath("(//div[@class='modal-header'])[2]/span")));
			Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='modal-header'])[2]/span")).getText(),
					selectPropertiesFile(true, locale, "addnewdesignreview"));
			System.out
					.println("hereeeee" + driver
							.findElement(By
									.xpath("(//div[@class='modal-body no-padding-bottom'])[2]/form/div/div/div/label"))
							.getText());
			System.out.println(selectPropertiesFile(true, locale, "name"));
			Assert.assertEquals(driver
					.findElement(By.xpath("(//div[@class='modal-body no-padding-bottom'])[2]/form/div/div/div/label"))
					.getText(), selectPropertiesFile(true, locale, "name"));
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='loop']//div[2]")).getText(),
					selectPropertiesFile(true, locale, "approvalpath"));
			Assert.assertEquals(driver.findElement(By.xpath("//*[@name='loop']/following::div[2]//div[2]")).getText(),
					selectPropertiesFile(true, locale, "approvalgroups_deliverable"));
			Assert.assertEquals(
					driver.findElement(By.xpath("//button[@class='btn btn-primary-green btn-width-120']")).getText(),
					selectPropertiesFile(true, locale, "save"));
			Assert.assertTrue(driver.findElement(By.xpath("(//div[@class='button close']/span)[2]")).isDisplayed());
			driver.findElement(By.xpath("(//div[@class='button close']/span)[2]")).click();
			Thread.sleep(500);
			Assert.assertTrue(common.getCLOSEBUTTON().isDisplayed());
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProjectDetailsPageAddElements() throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			System.out.println(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size());
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[contains(@class,'project-heading')]/h3")));
			dbLocators.DB_PROJECTHEADING.isDisplayed();
			awaitForElement(common.PROJECTLINKINTABLE);
			common.PROJECTLINKINTABLE.isDisplayed();
			Log.info("Text is :   " + common.PROJECTLINKINTABLE.getText());
			common.PROJECTLINKINTABLE.click();
			awaitForElement(common.PROJECTSPACEMULTIADDBUTTTON);
			dbLocators.PROJECTSPACE_DETAILSPAGE.isDisplayed();
			dbLocators.SELECTEDPRODUCTTEXT.isDisplayed();
			Assert.assertTrue(common.PROJECTSPACEMULTIADDBUTTTON.isDisplayed());
			Thread.sleep(1000);
			common.PROJECTSPACEMULTIADDBUTTTON.click();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateTaskCounts() throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			// This is to match the numbers fork the open tasks numbers from box
			// and
			// below heading
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}

			Log.info("Open Tasks");
			List<WebElement> BOXOPENTASKCOUNTS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[1]"));
			if (BOXOPENTASKCOUNTS.size() > 0 && Integer.parseInt(dbLocators.BOXOPENTASKCOUNT.getText()) != 0) {
				dbLocators.BOXOPENTASKS.click();
				Log.info("Text is :   " + dbLocators.BOXOPENTASKCOUNT.getText());
				String OpenTaskCount = dbLocators.TASKSCOUNT.getText();
				Log.info("Text is :   " + OpenTaskCount);
				OpenTaskCount = OpenTaskCount.replaceAll("[^\\d]", "");
				Assert.assertEquals(dbLocators.BOXOPENTASKCOUNT.getText(), OpenTaskCount);
			} else {
				Log.info("No Open Tasks");
			}
			Log.info("Tasks  Ending");
			List<WebElement> BOXENDINGTASKSS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[2]"));
			if (BOXENDINGTASKSS.size() > 0 && Integer.parseInt(dbLocators.BOXENDINGTASKCOUNT.getText()) != 0) {
				dbLocators.BOXENDINGTASKS.click();
				awaitForElement(common.getPIECHART());
				Log.info("Text is :   " + dbLocators.BOXENDINGTASKCOUNT.getText());
				String EndingTaskCount = dbLocators.TASKSCOUNT.getText();
				Log.info(EndingTaskCount);
				EndingTaskCount = EndingTaskCount.replaceAll("[^\\d]", "");
				Assert.assertEquals(dbLocators.BOXENDINGTASKCOUNT.getText(), EndingTaskCount);
			} else {
				Log.info("No Task Ending");
			}
			Log.info("Tasks  Delayed");
			List<WebElement> BOXTASKSDELAYEDS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[3]"));
			if (BOXTASKSDELAYEDS.size() > 0 && Integer.parseInt(dbLocators.BOXTASKDELAYEDCOUNT.getText()) != 0) {
				dbLocators.BOXTASKSDELAYED.click();
				awaitForElement(dbLocators.BARGRAPH);
				Log.info("Text is :   " + dbLocators.BOXTASKDELAYEDCOUNT.getText());
				String DelayedTaskCount = dbLocators.TASKSCOUNT.getText();
				Log.info(DelayedTaskCount);
				DelayedTaskCount = DelayedTaskCount.replaceAll("[^\\d]", "");
				Assert.assertEquals(dbLocators.BOXTASKDELAYEDCOUNT.getText(), DelayedTaskCount);
			} else {
				Log.info("No Task Delayed");
			}
			Log.info("Pending Specs");

			List<WebElement> BOXPENDINGCPECS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[4]"));
			if (BOXPENDINGCPECS.size() > 0 && Integer.parseInt(dbLocators.BOXNEWPENDINGSPECCOUNT.getText()) != 0) {
				dbLocators.BOXPENDINGSPECS.click();
				Log.info("Text is :   " + dbLocators.BOXNEWPENDINGSPECCOUNT.getText());
				String PendinSpecCount = dbLocators.TASKSCOUNT.getText();
				Log.info(PendinSpecCount);
				PendinSpecCount = PendinSpecCount.replaceAll("[^\\d]", "");
				Assert.assertEquals(dbLocators.BOXNEWPENDINGSPECCOUNT.getText(), PendinSpecCount);
			} else {
				Log.info("No Pending Specs");
			}
			Log.info("New Submissions");
			List<WebElement> NEWSUB = driver.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[5]"));
			if (NEWSUB.size() > 0 && Integer.parseInt(dbLocators.BOXNEWSUBMISSIONCOUNT.getText()) != 0) {
				dbLocators.BOXNEWSUBMISSIONS.click();
				awaitForElement(common.getPIECHART());
				Log.info("Text is :   " + dbLocators.BOXNEWSUBMISSIONCOUNT.getText());
				String NewSubmissionCount = dbLocators.TASKSCOUNT.getText();
				Log.info(NewSubmissionCount);
				NewSubmissionCount = NewSubmissionCount.replaceAll("[^\\d]", "");
				Assert.assertEquals(dbLocators.BOXNEWSUBMISSIONCOUNT.getText(), NewSubmissionCount);
			} else {
				Log.info("No Recent Submissions");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateTableMyDashboardTab() throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					dbLocators.DB_PROJECTHEADING);
			dbLocators.DB_PROJECTHEADING.isDisplayed();
			Assert.assertTrue(
					driver.findElement(By.xpath("//table[@class='table table-striped bordered']")).isDisplayed());
			for (int i = 1; i <= driver
					.findElements(By.xpath("//table[@class='table table-striped bordered']/thead/tr/th")).size(); i++) {
				if (i == 1) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "status"));
				} else if (i == 2) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "project_1") + " " + "ID");
				} else if (i == 3) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "projectname"));
				} else if (i == 4) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "category"));
				} else if (i == 5) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "products"));
				} else if (i == 6) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "relevanttask"));
				} else if (i == 7) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "targetlaunch"));
				} else if (i == 8) {
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "countdown"));
				} else if (i == 9) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]")));
					Assert.assertEquals(driver
							.findElement(
									By.xpath("//table[@class='table table-striped bordered']/thead/tr/th[" + i + "]"))
							.getText(), selectPropertiesFile(true, locale, "actions"));
				}
			}
			List<WebElement> elements = driver
					.findElements(By.xpath("//table[@class='table table-striped bordered']/tbody/tr/td"));
			java.util.Iterator<WebElement> i = elements.iterator();
			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info("Text is :   " + row.getText());
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateTaskSection() throws Throwable {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			List<WebElement> BOXOPENTASKCOUNTS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[1]"));
			if (BOXOPENTASKCOUNTS.size() > 0 && Integer.parseInt(dbLocators.BOXOPENTASKCOUNT.getText()) != 0) {
				dbLocators.BOXOPENTASKS.click();
				Log.info("Open Tasks Data  Starts");
				getTaskSection();
				Log.info("Open Tasks Data  Ends");
			} else {
				Log.info("No Open Tasks");
			}
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//*[@class='row no-margin task-row']")));
			List<WebElement> BOXENDCOUNTS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[2]"));
			if (BOXENDCOUNTS.size() > 0 && Integer.parseInt(dbLocators.BOXENDINGTASKCOUNT.getText()) != 0) {
				Log.info("Tasks Ending Data  Starts");
				dbLocators.BOXENDINGTASKS.click();
				getTaskSection();
				Log.info("Tasks Ending Data  Ends");
			} else {
				Log.info("No Ending Task");
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//*[@class='row no-margin task-row']")));
			List<WebElement> BOXDELCOUNTS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[3]"));
			if (BOXDELCOUNTS.size() > 0 && Integer.parseInt(dbLocators.BOXTASKDELAYEDCOUNT.getText()) != 0) {
				// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",BOXTASKSDELAYED);
				dbLocators.BOXTASKSDELAYED.click();
				// Thread.sleep(1000);
				Log.info("Delayed Tasks Data  Starts");
				awaitForElement(dbLocators.BARGRAPH);
				getTaskSection();
				Log.info("Delayed Tasks Data  Ends");
			} else {
				Log.info("No Task Delayed");
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//*[@class='row no-margin task-row']")));
			List<WebElement> BOXPENCOUNTS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[4]"));
			if (BOXPENCOUNTS.size() > 0 && Integer.parseInt(dbLocators.BOXNEWPENDINGSPECCOUNT.getText()) != 0) {
				dbLocators.BOXPENDINGSPECS.click();
				Log.info("New Pending Specs Data  Starts");
				getTaskSection();
				Log.info("Pending Specs Data Ends");
			} else {
				Log.info("No Pending Specs");
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//*[@class='row no-margin task-row']")));
			List<WebElement> BOXSUBCOUNTS = driver
					.findElements(By.xpath("(//*[@class='row no-margin task-row']/div)[4]"));
			if (BOXSUBCOUNTS.size() > 0 && Integer.parseInt(dbLocators.BOXNEWSUBMISSIONCOUNT.getText()) != 0) {
				dbLocators.BOXNEWSUBMISSIONS.click();
				Log.info("New Submissions Data  Starts");
				getTaskSection();
				Log.info("New Submissions Data Ends");
			} else {
				Log.info("No New Submissions");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateSuppliersTabRecomendedProducts() throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(dbLocators.SUPPLIERSTAB);
			dbLocators.SUPPLIERSTAB.click();
			Thread.sleep(1000);
			awaitForElement(dbLocators.SUPPLIERSTABBARGRAPH);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					dbLocators.RECOMENDEDPRODUCTSHEADING);
			Assert.assertTrue(dbLocators.RECOMENDEDPRODUCTSHEADING.isDisplayed());
			Assert.assertEquals(dbLocators.RECOMENDEDPRODUCTSHEADING.getText(),
					selectPropertiesFile(true, locale, "newproductrecommendation"));
			Assert.assertTrue(dbLocators.RECOMENDEDPRODUCTSECTION.isDisplayed());
			if (driver.findElements(By.xpath("//div[@class='supplier-partner-details relative flex flex-column']"))
					.size() == 0) {
				Log.info("No Recomended Products");
			} else {
				Log.info(
						"Number of recomended Products :  " + driver
								.findElements(
										By.xpath("//div[@class='supplier-partner-details relative flex flex-column']"))
								.size());
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateSuppliersTabBestPartners() throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {

			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(dbLocators.SUPPLIERSTAB);
			dbLocators.SUPPLIERSTAB.click();
			awaitForElement(dbLocators.SUPPLIERSTABBARGRAPH);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					dbLocators.BESTPARTNERSECTIONHEADING);
			dbLocators.BESTPARTNERSECTIONHEADING.isDisplayed();
			Assert.assertEquals(dbLocators.BESTPARTNERSECTIONHEADING.getText(),
					selectPropertiesFile(true, locale, "bestpartners"));
			System.out.println(
					"herere" + driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size());
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() == 1) {
				Log.info("No Best Partner Data");
			} else {
				Log.info(" Best Partner Data");
				for (int i = 1; i <= 10; i++) {
					awaitForElement(driver.findElement(By
							.xpath("(//ul[@class='slide-indicators no-padding flex flex-center flex-align-center']/li/a)["
									+ i + "]")));
					driver.findElement(By
							.xpath("(//ul[@class='slide-indicators no-padding flex flex-center flex-align-center']/li/a)["
									+ i + "]"))
							.click();
					jsWait();
					Thread.sleep(5000);
					if (driver.findElements(By.xpath("(//div[@class='img-block']/img)[1]")).size() == 0) {
						Log.info("No supplier Image");
					} else {
						Log.info("supplier has Image");
						isImageLoaded(driver.findElement(By.xpath("(//div[@class='img-block']/img)[1]")));
					}
					awaitForElement(
							driver.findElement(By.xpath("//h4[@class='font-size-16 font-weight-600 margin-xs']")));
					Assert.assertTrue(
							driver.findElement(By.xpath("//h4[@class='font-size-16 font-weight-600 margin-xs']"))
									.isDisplayed());
					Assert.assertEquals(driver
							.findElement(By.xpath("//h4[@class='font-size-16 font-weight-600 margin-xs']")).getText(),
							selectPropertiesFile(true, locale, "supplierinfo"));
					WebElement SupInfo = driver.findElement(By.xpath("//div[@class='supplier-partner-details']"));
					Log.info("Supplier info   :" + SupInfo.getText());
					WebElement ContactInfo = driver
							.findElement(By.xpath("//div[contains(@class,'supplier-info-details')][2]"));
					Log.info("Suppplier contact info   :" + ContactInfo.getText());
					Assert.assertEquals(driver
							.findElement(By.xpath("//h4[@class='font-size-16 font-weight-600 margin-top']")).getText(),
							selectPropertiesFile(true, locale, "topproducts"));
					if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[2]")).size() == 0) {
						Log.info("No Top Products Added");
					} else {
						Log.info("Top Products Available");
						int numberofproducts = driver
								.findElements(
										By.xpath("//div[@class='block top-product text-center margin-right-xxs']"))
								.size();
						Log.info("Number of Top products by supplier:  " + numberofproducts);
					}
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateSuppliersTab() throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {

			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			dbLocators.SUPPLIERSTAB.click();
			awaitForElement(dbLocators.SUPPLIERSTABBARGRAPH);
			Assert.assertTrue(dbLocators.SUPPLIERSTABBARGRAPH.isDisplayed());
			Assert.assertEquals(common.NOOFCATEGORIES.getText(),
					"# " + selectPropertiesFile(true, locale, "ofcategories"));
			Assert.assertEquals(dbLocators.SUPPLIERSTAB.getText(), selectPropertiesFile(true, locale, "supplier"));
			Assert.assertEquals(dbLocators.SUPPLIERSBYCAT.getText(),
					selectPropertiesFile(true, locale, "suppliersbycategories"));
			Assert.assertEquals(dbLocators.SUPPLIERSINVITED.getText(),
					selectPropertiesFile(true, locale, "suppliersinvitedlastweek"));
			dbLocators.SUPPLIERSTABBARGRAPH.isDisplayed();
			if (driver
					.findElements(By
							.xpath("//div[@class='table-responsive border-light-grey ps-container ps-theme-default']"))
					.size() != 0) {
				Log.info("Data");
			} else {
				Log.info("No Data");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProjectDetailsPage() throws Exception {
		common = new CommonLocators();
		dbLocators = new DashboardLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {

			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[contains(@class,'project-heading')]/h3")));
			Assert.assertTrue(dbLocators.DB_PROJECTHEADING.isDisplayed());
			Assert.assertTrue(
					dbLocators.DB_PROJECTHEADING.getText().contains(selectPropertiesFile(true, locale, "projects")));
			common.PROJECTLINKINTABLE.isDisplayed();
			Log.info("Text is herererere:   " + common.PROJECTLINKINTABLE.getText());
			common.PROJECTLINKINTABLE.click();
			awaitForElement(dbLocators.PROJECTNAME_DETAILSPAGE);
			Assert.assertTrue(dbLocators.PROJECTSPACE_DETAILSPAGE.isDisplayed());
			Assert.assertTrue(dbLocators.PROJECTNAME_DETAILSPAGE.getText() != null);
			Log.info("Task Details Section");
			awaitForElement(dbLocators.TD_HEADING);
			// wait.until(ExpectedConditions.visibilityOf(dbLocators.TD_HEADING));
			// Thread.sleep(3000);
			dbLocators.TD_HEADING.isDisplayed();
			Assert.assertEquals(dbLocators.TD_HEADING.getText(), selectPropertiesFile(true, locale, "taskdetails"));
			awaitForElement(dbLocators.TD_BUTTON);
			dbLocators.TD_BUTTON.isDisplayed();
			Log.info("Text is :   " + dbLocators.TD_BUTTON.getText());
			getPhasesData();
			getColabScreenData();
			getTaskDetailsInfo();
			getDeliverablesData();
			getInformedData();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getTaskSection() throws Exception {

		dbLocators = new DashboardLocators();
		try {
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].scrollIntoView(true);",
			// dbLocators.TASKSECTION);
			awaitForElement(dbLocators.TASKSECTION);
			dbLocators.TASKSECTION.isDisplayed();
			List<WebElement> rows = dbLocators.TASKSECTION.findElements(By.tagName("div"));
			Log.info("Text is " + rows.size());
			java.util.Iterator<WebElement> i = rows.iterator();
			Log.info("----------DATA STARTS-------------------");

			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info("Text is :   " + row.getText());
				Assert.assertTrue(row.getText() != null);
			}
			Log.info("----------DATA ENDS-------------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getDeliverablesData() {
		WebDriver driver = DriverFactory.getDriver();
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div/textarea")));
			List<WebElement> mm = driver.findElements(By.xpath("(//*[contains(@class,'deliverable-slot')])"));
			System.out.println(mm.size() + "hdhdlkhldlkhdl");
			for (int m = 1; m <= mm.size(); m++) {
				WebElement my = driver.findElement(By.xpath("(//*[contains(@class,'deliverable-slot')])[" + m + "]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true)", my);
				List<WebElement> rows = my.findElements(By.tagName("span"));
				java.util.Iterator<WebElement> i = rows.iterator();
				Log.info("----------DATA STARTS-------------------");
				while (i.hasNext()) {
					WebElement row = i.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
				}
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true)", driver
						.findElement(By.xpath("(//*[contains(@class,'deliverable-slot')])[" + m + "]" + "//div")));
			}
			WebElement del = driver.findElement(By.xpath("(//h5[@class='toggle-collapse'])[1]/following-sibling::div"));
			List<WebElement> rows = del.findElements(By.tagName("span"));
			java.util.Iterator<WebElement> i = rows.iterator();
			Log.info("----------DATA STARTS-------------------");
			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info("Text is :   " + row.getText());
				Assert.assertTrue(row.getText() != null);
			}
			Log.info("----------DATA ENDS-------------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getInformedData() {
		WebDriver driver = DriverFactory.getDriver();
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("(//h5)[4]")));
			List<WebElement> rows = driver.findElements(By.xpath("(//h5)[4]/following-sibling::span"));
			java.util.Iterator<WebElement> i = rows.iterator();
			Log.info("----------DATA STARTS-------------------");
			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info("Text is :   " + row.getText());
				Assert.assertTrue(row.getText() != null);
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("(//h5)[4]")));
			Log.info("----------DATA STARTS-------------------");
			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info("Text is :   " + row.getText());
				Assert.assertTrue(row.getText() != null);
			}
			Log.info("----------DATA ENDS-------------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getDocumentsData() {
		WebDriver driver = DriverFactory.getDriver();
		try {
			List<WebElement> uu = driver.findElements(By.xpath("//ul[@class='document-list']"));
			if (uu.size() != 0) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						driver.findElement(By.xpath("//h5[text()='Documents:']")));
				WebElement del = driver.findElement(By.xpath("//ul[@class='document-list']"));
				List<WebElement> rows = del.findElements(By.tagName("li"));
				java.util.Iterator<WebElement> i = rows.iterator();
				Log.info("----------DATA STARTS-------------------");
				while (i.hasNext()) {
					WebElement row = i.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
				}
				Log.info("----------DATA ENDS-------------------");
			} else {
				Log.info("Nothing");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getTaskDetailsInfo() {
		WebDriver driver = DriverFactory.getDriver();
		String TaskDetailsElement = "//div[@class='row task-date-details']/div";
		try {
			List<WebElement> TaskDetails = driver.findElements(By.xpath(TaskDetailsElement));
			for (int m = 1; m <= TaskDetails.size(); m++) {
				WebElement taskDetails = driver.findElement(By.xpath(TaskDetailsElement + "[" + m + "]"));
				List<WebElement> rows = taskDetails.findElements(By.tagName("label"));
				List<WebElement> cols = taskDetails.findElements(By.tagName("p"));
				java.util.Iterator<WebElement> i = rows.iterator();
				java.util.Iterator<WebElement> j = cols.iterator();
				Log.info("----------DATA STARTS-------------------");
				while (i.hasNext() && j.hasNext()) {
					WebElement row = i.next();
					WebElement col = j.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
					Log.info("Text is :   " + col.getText());
					Assert.assertTrue(col.getText() != null);
				}
				Log.info("----------DATA ENDS-------------------");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getColabScreenData() {
		WebDriver driver = DriverFactory.getDriver();
		String ColabScreenElement = "//div[contains(@class,'panel transparent')]";
		try {
			WebElement ColabScreen = driver.findElement(By.xpath(ColabScreenElement));
			List<WebElement> rows = ColabScreen.findElements(By.tagName("li"));
			java.util.Iterator<WebElement> i = rows.iterator();
			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info("Text is :   " + row.getText());
				Assert.assertTrue(row.getText() != null);
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getPhasesData() {
		WebDriver driver = DriverFactory.getDriver();
		String Phases = "//div[contains(@class,'phase-block ng-tns')]";
		try {
			List<WebElement> ListPhases = driver.findElements(By.xpath(Phases));
			for (int m = 1; m <= ListPhases.size(); m++) {
				WebElement phase = driver.findElement(By.xpath(Phases + "[" + m + "]"));
				Log.info("----------DATA STARTS-------------------");
				List<WebElement> rows = phase.findElements(By.tagName("p"));
				java.util.Iterator<WebElement> i = rows.iterator();
				while (i.hasNext()) {
					WebElement row = i.next();
					if (row.getText() != null) {
						Log.info("Text is :   " + row.getText());
						Assert.assertTrue(row.getText() != null);
					} else {
						Log.info("No Data");
					}
				}
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true)",
						driver.findElement(By.xpath(Phases + "[" + m + "]")));
				Log.info("----------DATA ENDS-------------------");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getStatusButtonwithouAssignment() throws InterruptedException {
		try {
			WebDriver driver = DriverFactory.getDriver();
			common = new CommonLocators();
			dbLocators = new DashboardLocators();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//*[@class='form-control']")));
			driver.findElement(By.xpath("//*[@class='form-control']"))
					.sendKeys(configProperities.getProperty("projectname"));
			// Thread.sleep(10000);
			Assert.assertEquals(driver
					.findElement(By.xpath("(//*[@class='table table-striped bordered']/tbody/tr/td)[1]")).getText(),
					"Not started");
			common.PROJECTLINKINTABLE.click();
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(By.xpath("//*[@class='panel transparent ng-trigger ng-trigger-message']"))));
			dbLocators.PROJECTSPACE_DETAILSPAGE.isDisplayed();
			dbLocators.SELECTEDPRODUCTTEXT.isDisplayed();
			Assert.assertTrue(
					driver.findElement(By.xpath("//*[@class='panel transparent ng-trigger ng-trigger-message']"))
							.isDisplayed());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[@class='media-body media-middle']/h5")));
			List<WebElement> buttonPresent = driver.findElements(By.xpath("//*[text()='Change Status']"));
			if (buttonPresent.size() == 0) {
				Assert.assertTrue(buttonPresent.size() == 0);
				Log.info("PM, Colaborator Logged in");
			} else {
				Log.info("Admin logged in");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getMultiAddButtononProjectSpace() throws InterruptedException {
		try {
			WebDriver driver = DriverFactory.getDriver();
			common = new CommonLocators();
			dbLocators = new DashboardLocators();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//*[@class='form-control']")));
			driver.findElement(By.xpath("//*[@class='form-control']"))
					.sendKeys(configProperities.getProperty("projectname"));
			// Thread.sleep(10000);
			Assert.assertEquals(driver
					.findElement(By.xpath("(//*[@class='table table-striped bordered']/tbody/tr/td)[1]")).getText(),
					"Not started");
			common.PROJECTLINKINTABLE.click();
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(By.xpath("//*[@class='panel transparent ng-trigger ng-trigger-message']"))));
			dbLocators.PROJECTSPACE_DETAILSPAGE.isDisplayed();
			dbLocators.SELECTEDPRODUCTTEXT.isDisplayed();
			List<WebElement> buttonPresent = driver
					.findElements(By.xpath("//*[@class='btn btn-primary-green btn-round race-dropdown-toggle']"));
			if (buttonPresent.size() == 0) {
				Log.info("Collaborator logged in");
			} else {
				Log.info("PM, Admin Logged in");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
}
