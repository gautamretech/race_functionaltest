package com.race.qa.pages.ui;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.util.Log;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.SupplierLocators;

public class UISupplierPage extends TestBase {
	CommonLocators common;
	SupplierLocators spLocators;
	WebDriver driver;

	public UISupplierPage(WebDriver driver) {
		this.driver = driver;
		this.driver = DriverFactory.getDriver();

	}

	public void AddNewSupplierInProjectDetailsPage() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		try {
			awaitForElement(spLocators.ADDNEWSUPPLIEROPTION);
			spLocators.ADDNEWSUPPLIEROPTION.click();
			awaitForElement(common.getPOPUPHEADING());
			common.getPOPUPHEADING().isDisplayed();
			getAllTextInPopup();
			common.DDSELECTSUPPLIER.isDisplayed();
			Log.info("Text is :  " + common.DDSELECTSUPPLIER.getText());
			Assert.assertTrue(common.DDSELECTSUPPLIER.getText() != null);
			common.DDSELECTSUPPLIER.click();
			getDropdownData(spLocators.DROPDOWNDATA, "li");
			common.DDSELECTSUPPLIER.click();
			common.DDSELECTSUPPLIER.click();
			driver.findElement(By.xpath("//div[contains(@class,'race-popup-container')]/div/ul/li[1]")).click();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(spLocators.SELECTFACTORYDD)));
			spLocators.SELECTFACTORYDD.isDisplayed();
			Log.info("Text is :  " + spLocators.SELECTFACTORYDD.getText());
			Assert.assertTrue(spLocators.SELECTFACTORYDD.getText() != null);
			awaitForElement(spLocators.SELECTFACTORYDD);
			spLocators.SELECTFACTORYDD.click();
			Thread.sleep(1000);
			// getDropdownData(DROPDOWNDATA, "li");
			spLocators.SELECTFACTORYDD.click();
			spLocators.DDCONTACTS.isDisplayed();
			Log.info("Text is :  " + spLocators.DDCONTACTS.getText());
			Assert.assertTrue(spLocators.DDCONTACTS.getText() != null);
			spLocators.DDCONTACTS.click();
			// getDropdownData(DROPDOWNDATA, "li");
			spLocators.DDCONTACTS.click();
			spLocators.SELECTTASKDD.isDisplayed();
			Log.info("Text is :  " + spLocators.SELECTTASKDD.getText());
			Assert.assertTrue(spLocators.SELECTTASKDD.getText() != null);
			spLocators.SELECTTASKDD.click();
			// getDropdownData(DROPDOWNDATA, "li");
			spLocators.SELECTTASKDD.click();
			/*
			 * wait.until(ExpectedConditions.refreshed(ExpectedConditions.
			 * visibilityOf(spLocators.ADDSUPPLIERBUTTONINPROJECTSPAGE)));
			 * spLocators.ADDSUPPLIERBUTTONINPROJECTSPAGE.isDisplayed();
			 * Log.info("Text is :  " +
			 * spLocators.ADDSUPPLIERBUTTONINPROJECTSPAGE.getText());
			 * Assert.assertTrue(spLocators.ADDSUPPLIERBUTTONINPROJECTSPAGE.
			 * getText() != null);
			 */

			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateSupplierLandingPage() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_SUPPLIERS());
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			common.getMENU_SUPPLIERS().click();
			awaitForElement(spLocators.SUPPLIERSTAB);
			// wait.until(ExpectedConditions.visibilityOf(spLocators.SUPPLIERSTAB));
			spLocators.SUPPLIERSTAB.isDisplayed();
			Log.info("Text is :" + spLocators.SUPPLIERSTAB.getText());
			Assert.assertTrue(spLocators.SUPPLIERSTAB.getText() != null);
			spLocators.COMMUNICATIONSTAB.isDisplayed();
			Log.info("Text is :" + spLocators.COMMUNICATIONSTAB.getText());
			Assert.assertTrue(spLocators.COMMUNICATIONSTAB.getText() != null);
			spLocators.ADDSUPPLIERBUTTON.isDisplayed();
			Log.info("Text is :" + spLocators.ADDSUPPLIERBUTTON.getText());
			Assert.assertTrue(spLocators.ADDSUPPLIERBUTTON.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.HEADINGSUPPLIERS);
			spLocators.HEADINGSUPPLIERS.isDisplayed();
			Log.info("Text is :" + spLocators.HEADINGSUPPLIERS.getText());
			Assert.assertTrue(spLocators.HEADINGSUPPLIERS.getText() != null);
			spLocators.FILTERBUTTON.isDisplayed();
			verifySupplierTable();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getAddedSupplierDetails() throws Throwable {
		try {
			common = new CommonLocators();
			spLocators = new SupplierLocators();
			WebDriver driver = DriverFactory.getDriver();
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.HEADINGSUPPLIERS);
			spLocators.HEADINGSUPPLIERS.isDisplayed();
			spLocators.FILTERBUTTON.isDisplayed();
			spLocators.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.SELECTCATEGORY));
			// TEXTSERACHFILTER.sendKeys(TESTSUPNAME);
			common.getAPPLYFILTERBUTTON().click();
			Thread.sleep(3000);
			verifySupplierTable();
			// driver.findElement(By.xpath("//a[text()='"+TESTSUPNAME+"']")).click();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void addContact_Supplier(boolean isPrimary) throws Throwable {
		try {
			common = new CommonLocators();
			spLocators = new SupplierLocators();
			WebDriver driver = DriverFactory.getDriver();
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(spLocators.OVERVIEWTAB);
			spLocators.CONTACTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
					"//ul[@class='unstyled no-padding no-margin pull-right padding-right-sm btn-ul']/li/button"))));

		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateAddSupplierScreen() throws Throwable {

		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_SUPPLIERS());
			common.getMENU_SUPPLIERS().click();
			awaitForElement(spLocators.SUPPLIERSTAB);
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			spLocators.SUPPLIERSTAB.isDisplayed();
			spLocators.ADDSUPPLIERBUTTON.isDisplayed();
			spLocators.ADDSUPPLIERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(common.getPOPUPHEADING()));
			common.getPOPUPHEADING().isDisplayed();
			Log.info("Text is   :" + common.getPOPUPHEADING().getText());
			Assert.assertTrue(common.getPOPUPHEADING().getText() != null);
			getAllTextInPopup();
			spLocators.UPLOADLOGOLINK.isDisplayed();
			Log.info("Text is   :" + spLocators.UPLOADLOGOLINK.getText());
			Assert.assertTrue(spLocators.UPLOADLOGOLINK.getText() != null);
			spLocators.SUPPLIERNAME.isDisplayed();
			spLocators.SELECTPARENTCOMPANY.isDisplayed();
			spLocators.SELECTPARENTCOMPANY.click();
			getDropdownData(spLocators.DROPDOWNDATA, "li");
			spLocators.SELECTPARENTCOMPANY.click();
			spLocators.SUPPLIERID.isDisplayed();
			spLocators.DDSTATUS.isDisplayed();
			spLocators.DDSTATUS.click();
			// getDropdownData();
			spLocators.DDSTATUS.click();
			spLocators.INPUTCONTACTADDRESS.isDisplayed();
			spLocators.INPUTCONTACTCITY.isDisplayed();
			spLocators.DDCOUNTRY.isDisplayed();
			spLocators.DDCOUNTRY.click();
			getDropdownData(spLocators.DROPDOWNDATA, "li");
			spLocators.DDCOUNTRY.click();
			spLocators.INPUTSTATE.isDisplayed();
			spLocators.INPUTZIP.isDisplayed();
			spLocators.SELECTYEAR.isDisplayed();
			spLocators.SELECTYEAR.click();
			getDropdownData(spLocators.DROPDOWNDATA, "li");
			spLocators.SELECTYEAR.click();
			spLocators.SUPPLIERTAXID.isDisplayed();
			spLocators.SUPPLIERDNB.isDisplayed();
			// RADIOPUBLICCOMP.isDisplayed();
			spLocators.NEXTBUTTON.isDisplayed();
			Log.info("Text is :" + spLocators.NEXTBUTTON.getText());
			Assert.assertTrue(spLocators.NEXTBUTTON.getText() != null);
			spLocators.NEXTBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(common.getPOPUPHEADING()));
			common.getPOPUPHEADING().isDisplayed();
			Log.info("Text is :" + common.getPOPUPHEADING().getText());
			Assert.assertTrue(common.getPOPUPHEADING().getText() != null);
			// PREVIOUSBUTTON.isDisplayed();
			supplierSetting();
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
			// FINALADDSUPPLIERBUTTON.isDisplayed();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void addSupplier(boolean raceAccess) throws Throwable {
		try {
			common = new CommonLocators();
			spLocators = new SupplierLocators();
			WebDriver driver = DriverFactory.getDriver();
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_SUPPLIERS());
			common.getMENU_SUPPLIERS().click();
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			// SUPPLIERSTAB.isDisplayed();
			wait.until(ExpectedConditions.visibilityOf(spLocators.ADDSUPPLIERBUTTON));
			spLocators.ADDSUPPLIERBUTTON.isDisplayed();
			spLocators.ADDSUPPLIERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(common.getPOPUPHEADING()));
			// getRandom_sup();
			/*
			 * SUPPLIERNAME.sendKeys(TESTSUPNAME);
			 * SUPPLIERID.sendKeys(TESTSUPID);
			 * SUPPLIERADDRESS.sendKeys(TESTSUPADD);
			 * SUPPLIERCITY.sendKeys(TESTSUPCITY);
			 * SUPPLIERSTATE.sendKeys(TESTSUPSTATE);
			 * SUPPLIERZIPCODE.sendKeys(TESTSUPZIP);
			 * SUPPLIERTAXID.sendKeys(TESTSUPTAXID);
			 * SUPPLIERDNBNUMBER.sendKeys(TESTSUPDNB);
			 */
			spLocators.DDCOUNTRY.click();
			spLocators.COUNTRYFILTER.sendKeys(configProperities.getProperty("c_country"));
			Thread.sleep(1000);
			driver.findElement(By.xpath("//div[@class='race-select-body ps-container ps-theme-default']/ul/li[2]"))
					.click();
			spLocators.NEXTBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(common.getPOPUPHEADING()));
			if (raceAccess) {
				spLocators.RADIOBUTTONACCESSRACE_YES.click();
			}
			spLocators.FINALADDSUPPLIERBUTTON.click();
			Thread.sleep(30000);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateFilterPopupScreen() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_SUPPLIERS());
			common.getMENU_SUPPLIERS().click();
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			awaitForElement(spLocators.SUPPLIERSTAB);
			spLocators.SUPPLIERSTAB.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.HEADINGSUPPLIERS);
			spLocators.HEADINGSUPPLIERS.isDisplayed();
			spLocators.FILTERBUTTON.isDisplayed();
			Log.info("Text is :" + spLocators.FILTERBUTTON.getText());
			Assert.assertTrue(spLocators.FILTERBUTTON.getText() != null);
			spLocators.FILTERBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.SELECTCATEGORY));
			getAllTextInPopup();
			spLocators.SELECTCATEGORY.isDisplayed();
			spLocators.SELECTCATEGORY.click();
			getDropdownData(spLocators.DROPDOWNDATA, "li");
			spLocators.SELECTCATEGORY.click();
			common.getCLEARFILTERBUTTON().isDisplayed();
			Log.info("Text is :" + common.getCLEARFILTERBUTTON().getText());
			Assert.assertTrue(common.getCLEARFILTERBUTTON().getText() != null);
			common.getCANCELFILTERBUTTON().isDisplayed();
			Log.info("Text is :" + common.getCANCELFILTERBUTTON().getText());
			Assert.assertTrue(common.getCANCELFILTERBUTTON().getText() != null);
			common.getAPPLYFILTERBUTTON().isDisplayed();
			Log.info("Text is :" + common.getAPPLYFILTERBUTTON().getText());
			Assert.assertTrue(common.getAPPLYFILTERBUTTON().getText() != null);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateSupplierDetailsOverviewTab() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_SUPPLIERS());
			common.getMENU_SUPPLIERS().click();
			awaitForElement(spLocators.SUPPLIERSTAB);
			spLocators.SUPPLIERSTAB.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.HEADINGSUPPLIERS);
			spLocators.HEADINGSUPPLIERS.isDisplayed();
			awaitForElement(spLocators.SUPPLIERLINK);
			spLocators.SUPPLIERLINK.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERLINK.getText());
			Assert.assertTrue(spLocators.SUPPLIERLINK.getText() != null);
			// wait.until(ExpectedConditions.visibilityOf(SUPPLIERLINK));
			spLocators.SUPPLIERLINK.click();
			awaitForElement(spLocators.OVERVIEWTAB);
			spLocators.OVERVIEWTAB.isDisplayed();
			Log.info("Text is  :" + spLocators.OVERVIEWTAB.getText());
			Assert.assertTrue(spLocators.OVERVIEWTAB.getText() != null);
			getSupplierPropertyData();
			/*
			 * Log.info("Text is  :" + FIRSTDIVELEMENT1.getText());
			 * Assert.assertTrue(FIRSTDIVELEMENT1.getText() != null); Log.info(
			 * "Text is  :" + FIRSTDIVELEMENT2.getText());
			 * Assert.assertTrue(FIRSTDIVELEMENT2.getText() != null); Log.info(
			 * "Text is  :" + FIRSTDIVELEMENT3.getText());
			 * Assert.assertTrue(FIRSTDIVELEMENT3.getText() != null); Log.info(
			 * "Text is  :" + FIRSTDIVELEMENT4.getText());
			 * Assert.assertTrue(FIRSTDIVELEMENT4.getText() != null);
			 */
			// EDITSUPPLIERBUTTON.isDisplayed();
			// Log.info("Text is :"+EDITSUPPLIERBUTTON.getText());
			// Assert.assertTrue(EDITSUPPLIERBUTTON.getText()!=null);
			spLocators.RECENTNEWSTAB.isDisplayed();
			Log.info("Text is  :" + spLocators.RECENTNEWSTAB.getText());
			Assert.assertTrue(spLocators.RECENTNEWSTAB.getText() != null);
			getRecentData();
			spLocators.RECENTACTIVITYTAB.isDisplayed();
			Log.info("Text is  :" + spLocators.RECENTACTIVITYTAB.getText());
			Assert.assertTrue(spLocators.RECENTACTIVITYTAB.getText() != null);
			spLocators.RECENTACTIVITYTAB.click();
			getRecentActivityData();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.SUPPLIERPAGEPROJECTSTAB);
			spLocators.SUPPLIERPAGEPROJECTSTAB.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERPAGEPROJECTSTAB.getText());
			Assert.assertTrue(spLocators.SUPPLIERPAGEPROJECTSTAB.getText() != null);
			verifyTable();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					spLocators.SUPPLIERPAGEPRODUCTSTAB);
			spLocators.SUPPLIERPAGEPRODUCTSTAB.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERPAGEPRODUCTSTAB.getText());
			Assert.assertTrue(spLocators.SUPPLIERPAGEPRODUCTSTAB.getText() != null);
			spLocators.SUPPLIERPAGEPRODUCTSTAB.click();
			verifyTable();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateAddCustomCriteriaMax6Count() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			common.getMENU_SUPPLIERS().click();
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			awaitForElement(spLocators.SUPPLIERSTAB);
			spLocators.SUPPLIERSTAB.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[contains(@class,'project-heading')]//h3")));
			// common.getPOPUPHEADING().isDisplayed();
			spLocators.SUPPLIERLINK.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERLINK.getText());
			Assert.assertTrue(spLocators.SUPPLIERLINK.getText() != null);
			// wait.until(ExpectedConditions.visibilityOf(SUPPLIERLINK));
			spLocators.SUPPLIERLINK.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.OVERVIEWTAB));
			spLocators.OVERVIEWTAB.isDisplayed();
			spLocators.RATINGSTAB.isDisplayed();
			Log.info("Text is :" + spLocators.RATINGSTAB.getText());
			Assert.assertTrue(spLocators.RATINGSTAB.getText() != null);
			spLocators.RATINGSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.RATINGSAREA));
			spLocators.RATINGSAREA.isDisplayed();
			List<WebElement> visibleButton = driver.findElements(By.xpath(
					"//*[@class='btn btn-primary-green btn-width-120 flex flex-align-center padding-left-xs padding-right-xs flex-space-between ng-star-inserted']"));
			if (visibleButton.size() == 0) {
				Log.info("The supplier does not have a complete projects");
			} else {
				spLocators.ADDRATINGSBUTTON.isDisplayed();
				Log.info("Text is :" + spLocators.ADDRATINGSBUTTON.getText());
				Assert.assertTrue(spLocators.ADDRATINGSBUTTON.getText() != null);
				spLocators.ADDRATINGSBUTTON.click();
				wait.until(ExpectedConditions.visibilityOf(spLocators.DEFAULTCRITERIAS));
				spLocators.DEFAULTCRITERIAS.isDisplayed();
				// Add custom criteria
				spLocators.ADDNEWCRITIRIA.isDisplayed();
				for (int i = 1; i <= 6; i++) {
					spLocators.ADDNEWCRITIRIA.click();
					wait.until(ExpectedConditions.visibilityOf(spLocators.INPUTCRITERIANAME));
					spLocators.INPUTCRITERIANAME.isDisplayed();
					Assert.assertTrue(spLocators.CRITERIACROSSBUTTON.isDisplayed());
					Assert.assertTrue(spLocators.CRITERIACROSSBUTTON.isEnabled());
					// Assert.assertTrue(CRITERIACHECKBOX.isDisplayed());
					Assert.assertTrue(spLocators.CRITERIACHECKBOX.isEnabled());
					WebElement CUSTOM = driver.findElement(By
							.xpath("(//*[@class='row no-margin-left no-margin-right margin-lg ng-star-inserted'])[7]"));
					Log.info("Text is   :" + CUSTOM.getText());
					Assert.assertTrue(CUSTOM.getText() != null);
				}
				Assert.assertFalse(spLocators.ADDNEWCRITIRIA.isEnabled());
				common.getCLOSEBUTTON().isDisplayed();
				common.getCLOSEBUTTON().click();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateAddCustomCriteria() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {

			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			common.getMENU_SUPPLIERS().click();
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			spLocators.SUPPLIERSTAB.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", common.getPOPUPHEADING());
			common.getPOPUPHEADING().isDisplayed();
			spLocators.SUPPLIERLINK.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERLINK.getText());
			Assert.assertTrue(spLocators.SUPPLIERLINK.getText() != null);
			// wait.until(ExpectedConditions.visibilityOf(SUPPLIERLINK));
			spLocators.SUPPLIERLINK.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.OVERVIEWTAB));
			spLocators.OVERVIEWTAB.isDisplayed();
			spLocators.RATINGSTAB.isDisplayed();
			Log.info("Text is :" + spLocators.RATINGSTAB.getText());
			Assert.assertTrue(spLocators.RATINGSTAB.getText() != null);
			spLocators.RATINGSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.RATINGSAREA));
			spLocators.RATINGSAREA.isDisplayed();
			List<WebElement> visibleButton = driver.findElements(By.xpath(
					"//*[@class='btn btn-primary-green btn-width-120 flex flex-align-center padding-left-xs padding-right-xs flex-space-between ng-star-inserted']"));
			if (visibleButton.size() == 0) {
				Log.info("The supplier does not have a complete projects");
			} else {
				spLocators.ADDRATINGSBUTTON.isDisplayed();
				Log.info("Text is :" + spLocators.ADDRATINGSBUTTON.getText());
				Assert.assertTrue(spLocators.ADDRATINGSBUTTON.getText() != null);
				spLocators.ADDRATINGSBUTTON.click();
				wait.until(ExpectedConditions.visibilityOf(spLocators.DEFAULTCRITERIAS));
				spLocators.DEFAULTCRITERIAS.isDisplayed();
				// Add custom criteria
				spLocators.ADDNEWCRITIRIA.isDisplayed();
				spLocators.ADDNEWCRITIRIA.click();
				wait.until(ExpectedConditions.visibilityOf(spLocators.INPUTCRITERIANAME));
				spLocators.INPUTCRITERIANAME.isDisplayed();
				Assert.assertTrue(spLocators.CRITERIACROSSBUTTON.isDisplayed());
				Assert.assertTrue(spLocators.CRITERIACROSSBUTTON.isEnabled());
				// Assert.assertTrue(CRITERIACHECKBOX.isDisplayed());
				Assert.assertTrue(spLocators.CRITERIACHECKBOX.isEnabled());
				WebElement CUSTOM = driver.findElement(
						By.xpath("(//*[@class='row no-margin-left no-margin-right margin-lg ng-star-inserted'])[7]"));
				Log.info("Text is   :" + CUSTOM.getText());
				Assert.assertTrue(CUSTOM.getText() != null);
				common.getCLOSEBUTTON().isDisplayed();
				common.getCLOSEBUTTON().click();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateAddRatingsScreen() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {

			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			common.getMENU_SUPPLIERS().click();
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			spLocators.SUPPLIERSTAB.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", common.getPOPUPHEADING());
			common.getPOPUPHEADING().isDisplayed();
			spLocators.SUPPLIERLINK.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERLINK.getText());
			Assert.assertTrue(spLocators.SUPPLIERLINK.getText() != null);
			// wait.until(ExpectedConditions.visibilityOf(SUPPLIERLINK));
			spLocators.SUPPLIERLINK.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.OVERVIEWTAB));
			spLocators.OVERVIEWTAB.isDisplayed();
			spLocators.RATINGSTAB.isDisplayed();
			Log.info("Text is :" + spLocators.RATINGSTAB.getText());
			Assert.assertTrue(spLocators.RATINGSTAB.getText() != null);
			spLocators.RATINGSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.RATINGSAREA));
			spLocators.RATINGSAREA.isDisplayed();
			// ADDRATINGSBUTTON.isDisplayed();

			List<WebElement> visibleButton = driver.findElements(By.xpath(
					"//*[@class='btn btn-primary-green btn-width-120 flex flex-align-center padding-left-xs padding-right-xs flex-space-between ng-star-inserted']"));
			if (visibleButton.size() == 0) {
				Log.info("The supplier does not have a complete projects");
			} else {
				Log.info("Text is :" + spLocators.ADDRATINGSBUTTON.getText());
				Assert.assertTrue(spLocators.ADDRATINGSBUTTON.getText() != null);
				spLocators.ADDRATINGSBUTTON.click();
				wait.until(ExpectedConditions.visibilityOf(spLocators.DEFAULTCRITERIAS));
				spLocators.DEFAULTCRITERIAS.isDisplayed();
				List<WebElement> criteria = driver.findElements(
						By.xpath("//*[@class='row no-margin-left no-margin-right margin-lg ng-star-inserted']"));
				Log.info("Text is :   " + criteria.size());
				java.util.Iterator<WebElement> i = criteria.iterator();
				Log.info("-----------Data Starts-------------");

				while (i.hasNext()) {
					WebElement Data = i.next();
					Log.info("Text is :   " + Data.getText());
					Assert.assertTrue(Data.getText() != null);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Data);

				}
				Log.info("-----------Data Ends-------------");
				spLocators.ADDNEWCRITIRIA.isDisplayed();
				Log.info("Text is :   " + spLocators.ADDNEWCRITIRIA.getText());
				Assert.assertTrue(spLocators.ADDNEWCRITIRIA.getText() != null);
				spLocators.SAVERATINGS.isDisplayed();
				Log.info("Text is :   " + spLocators.SAVERATINGS.getText());
				Assert.assertTrue(spLocators.SAVERATINGS.getText() != null);
				common.getCLOSEBUTTON().isDisplayed();
				common.getCLOSEBUTTON().click();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateProductTabInSupplierScreen() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {

			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_SUPPLIERS());
			common.getMENU_SUPPLIERS().click();
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			awaitForElement(spLocators.SUPPLIERSTAB);
			Assert.assertTrue(spLocators.SUPPLIERSTAB.isDisplayed());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[contains(@class,'project-heading')]/h3")));
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class,'project-heading')]/h3")).isDisplayed());
			spLocators.SUPPLIERLINK.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERLINK.getText());
			Assert.assertTrue(spLocators.SUPPLIERLINK.getText() != null);
			// wait.until(ExpectedConditions.visibilityOf(SUPPLIERLINK));
			spLocators.SUPPLIERLINK.click();
			awaitForElement(spLocators.OVERVIEWTAB);
			// wait.until(ExpectedConditions.visibilityOf(spLocators.OVERVIEWTAB));
			spLocators.OVERVIEWTAB.isDisplayed();
			spLocators.PRODUCTSTABINSUPPLIERMENU.isDisplayed();
			Log.info("Text is :" + spLocators.PRODUCTSTABINSUPPLIERMENU.getText());
			Assert.assertTrue(spLocators.PRODUCTSTABINSUPPLIERMENU.getText() != null);
			spLocators.PRODUCTSTABINSUPPLIERMENU.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.PRODUCTSTABHEADING));
			Log.info("Text is :" + spLocators.PRODUCTSTABHEADING.getText());
			Assert.assertTrue(spLocators.PRODUCTSTABHEADING.getText() != null);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateRatingsTab() throws Throwable {
		common = new CommonLocators();
		spLocators = new SupplierLocators();
		WebDriver driver = DriverFactory.getDriver();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			common.getMENU_SUPPLIERS().click();
			awaitForElement(spLocators.SUPPLIERSTAB);
			// wait.until(ExpectedConditions.visibilityOf(common.getPIECHART()));
			spLocators.SUPPLIERSTAB.isDisplayed();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[contains(@class,'project-grid')]/div/h3")));
			// common.getPOPUPHEADING().isDisplayed();
			spLocators.SUPPLIERLINK.isDisplayed();
			Log.info("Text is  :" + spLocators.SUPPLIERLINK.getText());
			Assert.assertTrue(spLocators.SUPPLIERLINK.getText() != null);
			// wait.until(ExpectedConditions.visibilityOf(SUPPLIERLINK));
			spLocators.SUPPLIERLINK.click();
			wait.until(ExpectedConditions.visibilityOf(spLocators.OVERVIEWTAB));
			spLocators.OVERVIEWTAB.isDisplayed();
			spLocators.RATINGSTAB.isDisplayed();
			Log.info("Text is :" + spLocators.RATINGSTAB.getText());
			Assert.assertTrue(spLocators.RATINGSTAB.getText() != null);
			spLocators.RATINGSTAB.click();
			awaitForElement(common.getPIECHART());
			List<WebElement> visibleButton = driver.findElements(By.xpath(
					"//*[@class='btn btn-primary-green btn-width-120 flex flex-align-center padding-left-xs padding-right-xs flex-space-between ng-star-inserted']"));
			Log.info("" + visibleButton.size());
			if (visibleButton.size() == 0) {
				Log.info("The project is not in complete state, hence Add ratings button not displayed");
			} else {
				Log.info("Text is :" + spLocators.PROJECTNAMEASHEADING.getText());
				Assert.assertTrue(spLocators.PROJECTNAMEASHEADING.getText() != null);
				Log.info("Text is :" + spLocators.PROJECTHEADING.getText());
				Assert.assertTrue(spLocators.PROJECTHEADING.getText() != null);
				Log.info("Text is :" + spLocators.PROJECTMANGERHEADING.getText());
				Assert.assertTrue(spLocators.PROJECTMANGERHEADING.getText() != null);
				Log.info("Text is :" + spLocators.PROJECTMANAGERNAME.getText());
				Assert.assertTrue(spLocators.PROJECTMANAGERNAME.getText() != null);
				// From Ratings Area
				Log.info("Text is :" + spLocators.CURRENTPROJECTNAME.getText());
				Assert.assertTrue(spLocators.CURRENTPROJECTNAME.getText() != null);
				Log.info("Text is :" + spLocators.CURRENTPROJECTSCORE.getText());
				Assert.assertTrue(spLocators.CURRENTPROJECTSCORE.getText() != null);
				Log.info("Text is :" + spLocators.CURRENTPROJECTSCOREDATE.getText());
				Assert.assertTrue(spLocators.CURRENTPROJECTSCOREDATE.getText() != null);
				Log.info("Text is :" + spLocators.LASTPROJECTNAME.getText());
				Assert.assertTrue(spLocators.LASTPROJECTNAME.getText() != null);
				Log.info("Text is :" + spLocators.LASTPROJECTSCORE.getText());
				Assert.assertTrue(spLocators.LASTPROJECTSCORE.getText() != null);
				Log.info("Text is :" + spLocators.LASTPROJECTSCOREDATE.getText());
				Assert.assertTrue(spLocators.LASTPROJECTSCOREDATE.getText() != null);
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void verifySupplierTable() throws Throwable {
		try {
			WebDriver driver = DriverFactory.getDriver();
			WebElement Table = driver.findElement(By
					.xpath("//h3[@class='font-size-16 font-weight-400 text-2e3d51 no-margin']/following::table/tbody"));

			List<WebElement> rows = Table.findElements(By.tagName("tr"));
			java.util.Iterator<WebElement> i = rows.iterator();
			Log.info("----------DATA STARTS-------------------");
			while (i.hasNext()) {
				WebElement row = i.next();
				Log.info("Text is :   " + row.getText());
				Assert.assertTrue(row.getText() != null);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
			}
			Log.info("----------DATA ENDS-------------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void supplierSetting() throws Throwable {
		try {
			WebDriver driver = DriverFactory.getDriver();
			List<WebElement> rows_1 = driver.findElements(
					By.xpath("//*[@class='blue-border-box margin-bottom-lg']/*[@class='row single-settings']"));
			Log.info("Text i s  :" + rows_1.size());
			List<WebElement> rows_2 = driver.findElements(
					By.xpath("//*[@class='blue-border-box margin-top-lg']/*[@class='row single-settings']"));
			Log.info("Text i s  :" + rows_2.size());
			for (int i = 1; i <= rows_1.size(); i++) {
				WebElement row_1 = driver.findElement(
						By.xpath("//*[@class='blue-border-box margin-bottom-lg']/*[@class='row single-settings'][" + i
								+ "]//h4"));
				Log.info("Text is :" + row_1.getText());
				Assert.assertTrue(row_1.getText() != null);
				WebElement row_2 = driver.findElement(
						By.xpath("//*[@class='blue-border-box margin-bottom-lg']/*[@class='row single-settings'][" + i
								+ "]//p"));
				Log.info("Text is :" + row_2.getText());
				Assert.assertTrue(row_2.getText() != null);
			}
			for (int i = 1; i <= rows_2.size(); i++) {
				WebElement row_1 = driver.findElement(By.xpath(
						"//*[@class='blue-border-box margin-top-lg']/*[@class='row single-settings'][" + i + "]//h4"));
				Log.info("Text is :" + row_1.getText());
				Assert.assertTrue(row_1.getText() != null);
				WebElement row_2 = driver.findElement(By.xpath(
						"//*[@class='blue-border-box margin-top-lg']/*[@class='row single-settings'][" + i + "]//p"));
				Log.info("Text is :" + row_2.getText());
				Assert.assertTrue(row_2.getText() != null);
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getRecentData() throws Throwable {
		try {
			WebDriver driver = DriverFactory.getDriver();
			List<WebElement> rows = driver
					.findElements(By.xpath("//div[@class='scroll-container ps-container ps-theme-default']/ul/li"));
			if (rows.size() <= 1) {
				Log.info("No Data");
			} else {
				for (int i = 1; i <= rows.size(); i++) {
					WebElement row = driver.findElement(By.xpath(
							"//div[@class='scroll-container ps-container ps-theme-default']/ul/li[" + i + "]/p"));
					Log.info("Text is  :" + row.getText());
					Assert.assertTrue(row.getText() != null);
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getRecentActivityData() throws Throwable {
		try {
			WebDriver driver = DriverFactory.getDriver();
			List<WebElement> rows = driver
					.findElements(By.xpath("//div[@class='scroll-container ps-container ps-theme-default']/ul/li"));
			Log.info("Text is  :" + rows.size());
			if (rows.size() <= 1) {
				Log.info("No Data");
			} else {
				for (int i = 1; i <= rows.size(); i++) {
					WebElement row = driver.findElement(By.xpath(
							"//div[@class='scroll-container ps-container ps-theme-default']/ul/li[" + i + "]/p"));
					Log.info("Text is  :" + row.getText());
					Assert.assertTrue(row.getText() != null);
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void getSupplierPropertyData() throws Throwable {
		try {
			WebDriver driver = DriverFactory.getDriver();
			List<WebElement> rows = driver.findElements(By.xpath("//div[contains(@class,'supplier-property')]"));
			Log.info("Text is  :" + rows.size());
			if (rows.size() <= 1) {
				Log.info("No Data");
			} else {
				for (int i = 1; i <= 5; i++) {
					WebElement row = driver
							.findElement(By.xpath("//div[contains(@class,'supplier-property')][" + i + "]/span"));
					WebElement row1 = driver
							.findElement(By.xpath("//div[contains(@class,'supplier-property')][" + i + "]/h5"));
					Log.info("Text is  :" + row.getText());
					Assert.assertTrue(row.getText() != null);
					Log.info("Text is  :" + row1.getText());
					Assert.assertTrue(row1.getText() != null);
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void verifyTable() throws Throwable {
		try {
			WebDriver driver = DriverFactory.getDriver();
			WebElement Table = driver.findElement(By.xpath("//*[@class='table table-striped bordered']/tbody"));
			List<WebElement> rows = Table.findElements(By.tagName("tr"));
			if (rows.size() == 0) {
				Log.info("No Data");
			} else {
				java.util.Iterator<WebElement> i = rows.iterator();
				Log.info("----------DATA STARTS-------------------");
				while (i.hasNext()) {
					WebElement row = i.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
				}
			}
			Log.info("----------DATA ENDS-------------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getAnnualTable() throws Throwable {
		try {
			WebDriver driver = DriverFactory.getDriver();
			WebElement Table = driver.findElement(By.xpath("//*[@class='table table-striped table-hover'][1]/tbody"));
			List<WebElement> rows = Table.findElements(By.tagName("tr"));
			if (rows.size() <= 1) {
				Log.info("No Data");
			} else {
				java.util.Iterator<WebElement> i = rows.iterator();
				Log.info("----------DATA STARTS-------------------");
				while (i.hasNext()) {
					WebElement row = i.next();
					Log.info("Text is :   " + row.getText());
					Assert.assertTrue(row.getText() != null);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
				}
			}
			Log.info("----------DATA ENDS-------------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
}
