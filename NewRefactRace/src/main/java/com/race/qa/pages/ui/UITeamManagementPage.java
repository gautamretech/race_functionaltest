package com.race.qa.pages.ui;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.util.Log;
import com.race.qa.locator.CommonLocators;
import com.race.qa.locator.TeamManagementLocators;

public class UITeamManagementPage extends TestBase {
	WebDriver driver;
	protected CommonLocators common;
	protected TeamManagementLocators tml;
	public UITeamManagementPage(WebDriver driver) {
		this.driver=driver;
		this.driver=DriverFactory.getDriver();
	}

	public void validateTeamTab(String userType) throws Throwable{
		try{
		WebDriver driver = DriverFactory.getDriver();
		common=new CommonLocators();
		tml = new TeamManagementLocators();
		if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
			Log.info("PIE chart available");
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed());
			}
			else{
				Log.info("PIE CHART not available");
			}
		awaitForElement(common.getMENU_TEAMMANGEMENT());
		common.getMENU_TEAMMANGEMENT().click();
		awaitForElement((tml.TEAMTAB));
		tml.TEAMTAB.isDisplayed();
		Log.info(tml.TEAMTAB.getText());
		Assert.assertTrue(tml.TEAMTAB.getText() != null);
		Assert.assertTrue(tml.TEAMHEADING.isDisplayed());
		Assert.assertTrue(tml.SEARCHTEAMMEMBERINPUT.isDisplayed());
		Assert.assertTrue(tml.SEARHMEMBERSBUTTON.isDisplayed());
		if (userType == "Admin") {
			tml.INVITETEAMMEMBUTTON.isDisplayed();
			Log.info(tml.INVITETEAMMEMBUTTON.getText());
			Assert.assertTrue(tml.INVITETEAMMEMBUTTON.getText() != null);
			verifyUserTable();
			Assert.assertTrue(tml.HEADINGPENDINGINVITES.isDisplayed());
			Assert.assertTrue(tml.SEARCHPENDINGINVITES.isDisplayed());
			Assert.assertTrue(tml.SEARCHPENDINGBUTTON.isDisplayed());
			verifyPendingInvitesTable();
		} else if (userType == "Collaborator") {
			verifyUserTable();
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void inviteTeamMemberScreen() throws Throwable{
		try{
		WebDriver driver = DriverFactory.getDriver();
		common=new CommonLocators();
		tml = new TeamManagementLocators();
		if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
				}
				else{
					Log.info("PIE CHART not available");
				}
		awaitForElement(common.getMENU_TEAMMANGEMENT());
		common.getMENU_TEAMMANGEMENT().click();
		awaitForElement((tml.TEAMTAB));
		tml.TEAMTAB.isDisplayed();
		tml.INVITETEAMMEMBUTTON.isDisplayed();
		Log.info("Text is   " + tml.INVITETEAMMEMBUTTON.getText());
		Assert.assertTrue(tml.INVITETEAMMEMBUTTON.getText() != null);
		tml.INVITETEAMMEMBUTTON.click();
		wait.until(ExpectedConditions.visibilityOf(tml.HEADINGPOP));
		tml.HEADINGPOP.isDisplayed();
		Log.info("Text is   " + tml.HEADINGPOP.getText());
		Assert.assertTrue(tml.HEADINGPOP.getText() != null);
		getAllTextInPopup();
		tml.INPUTFIRSTNAME.isDisplayed();
		tml.INPUTSURNAME.isDisplayed();
		tml.INPUTEMAIL.isDisplayed();
		tml.SELECTROLE.isDisplayed();
		tml.SELECTROLE.click();
		// wait.until(ExpectedConditions.visibilityOf(DROPDOWNDATA));
		// getDropdownData();
		tml.SELECTROLE.click();
		tml.BUTTONSENDINVITE.isDisplayed();
		Log.info("Text is :" + tml.BUTTONSENDINVITE.getText());
		Assert.assertTrue(tml.BUTTONSENDINVITE.getText() != null);
		common.getCLOSEBUTTON().isDisplayed();
		common.getCLOSEBUTTON().click();
		Thread.sleep(500);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateGroupsTabScreen() throws Throwable{
		try{
		common=new CommonLocators();
		tml = new TeamManagementLocators();
		WebDriver driver = DriverFactory.getDriver();
		if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
			Log.info("PIE chart available");
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed());
			}
			else{
				Log.info("PIE CHART not available");
			}
		awaitForElement(common.getMENU_TEAMMANGEMENT());
		common.getMENU_TEAMMANGEMENT().click();
		awaitForElement((tml.TEAMTAB));
		tml.TEAMTAB.isDisplayed();
		tml.GROUPSTAB.isDisplayed();
		Log.info("Text is :" + tml.GROUPSTAB.getText());
		Assert.assertTrue(tml.GROUPSTAB.getText() != null);
		tml.GROUPSTAB.click();
		wait.until(ExpectedConditions.visibilityOf(tml.HEADINGUSERGROUP));
		tml.HEADINGUSERGROUP.isDisplayed();
		Log.info("Test is :" + tml.HEADINGUSERGROUP.getText());
		Assert.assertTrue(tml.HEADINGUSERGROUP.getText() != null);
		verifyGroupTable();
		tml.GROUPSEARCHINPUT.isDisplayed();
		tml.GROUPSEARCHBUTTON.isDisplayed();
		Log.info("Test is :" + tml.GROUPSEARCHBUTTON.getText());
		Assert.assertTrue(tml.GROUPSEARCHBUTTON.getText() != null);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", tml.BUTTONDOWNLOADSUMMARY);
		Assert.assertTrue(tml.BUTTONDOWNLOADSUMMARY.isDisplayed());
		Log.info(tml.BUTTONDOWNLOADSUMMARY.getText());
		Assert.assertTrue(tml.BUTTONDOWNLOADSUMMARY.getText() != null);
		tml.BUTTONDOWNLOADSUMMARY.click();
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateAddGroupScreen() throws Throwable{
		try{
		WebDriver driver = DriverFactory.getDriver();
		common=new CommonLocators();
		tml = new TeamManagementLocators();
		System.out.println(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0);
		if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
			Log.info("PIE chart available");
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed());
			}
			else{
				Log.info("PIE CHART not available");
			}
		awaitForElement(common.getMENU_TEAMMANGEMENT());
		common.getMENU_TEAMMANGEMENT().click();
		awaitForElement((tml.TEAMTAB));
		tml.TEAMTAB.isDisplayed();
		tml.GROUPSTAB.isDisplayed();
		tml.GROUPSTAB.click();
		wait.until(ExpectedConditions.visibilityOf(tml.HEADINGUSERGROUP));
		tml.ADDGROUPBUTTON.isDisplayed();
		Log.info("Text is :" + tml.ADDGROUPBUTTON.getText());
		Assert.assertTrue(tml.ADDGROUPBUTTON.getText() != null);
		tml.ADDGROUPBUTTON.click();
		wait.until(ExpectedConditions.visibilityOf(tml.HEADINGPOP));
		Log.info(tml.HEADINGPOP.getText());
		Assert.assertTrue(tml.HEADINGPOP.getText() != null);
		getAllTextInPopup();
		tml.INPUTNAME.isDisplayed();
		//tml.SELECTCOLOR.isDisplayed();
		tml.SELECTBANNER.isDisplayed();
		tml.SELECTBANNER.click();
		// getDropdownData();
		tml.SELECTBANNER.click();
		tml.SELECTCATEGORIES.isDisplayed();
		tml.SELECTCATEGORIES.click();
		// getDropdownData();
		tml.SELECTCATEGORIES.click();
		common.getCLOSEBUTTON().isDisplayed();
		common.getSAVEBUTTON().isDisplayed();
		Assert.assertTrue(common.getSAVEBUTTON().getText() != null);
		common.getCLOSEBUTTON().click();
		Thread.sleep(500);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void validateColorPicker() throws Throwable{
		try{
		WebDriver driver = DriverFactory.getDriver();
		common=new CommonLocators();
		tml = new TeamManagementLocators();
		if(driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size()==0){
			Log.info("PIE chart available");
			awaitForElement(common.getPIECHART());
			Assert.assertTrue(common.getPIECHART().isDisplayed());
			}
			else{
				Log.info("PIE CHART not available");
			}
		awaitForElement(common.getMENU_TEAMMANGEMENT());
		common.getMENU_TEAMMANGEMENT().click();
		awaitForElement((tml.TEAMTAB));
		tml.TEAMTAB.isDisplayed();
		tml.GROUPSTAB.isDisplayed();
		tml.GROUPSTAB.click();
		wait.until(ExpectedConditions.visibilityOf(tml.HEADINGUSERGROUP));
		tml.ADDGROUPBUTTON.isDisplayed();
		tml.ADDGROUPBUTTON.click();
		wait.until(ExpectedConditions.visibilityOf(tml.HEADINGPOP));
		tml.SELECTCOLOR.isDisplayed();
		tml.SELECTCOLOR.click();
		wait.until(ExpectedConditions.visibilityOf(tml.OPTIONCUSTOMCOLOR));
		Log.info("Text is  :" + tml.OPTIONCUSTOMCOLOR.getText());
		Assert.assertTrue(tml.OPTIONCUSTOMCOLOR.getText() != null);
		tml.OPTIONCUSTOMCOLOR.click();
		tml.COLORPICKER.isDisplayed();
		tml.SELECTCOLOR.click();
		common.getCLOSEBUTTON().click();
		Thread.sleep(500);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
	
	public void verifyUserTable() throws Throwable{
		try {
		WebElement Table = driver.findElement(By.xpath("//h3[@class='font-size-18 font-weight-400 text-2e3d51']/following::table[1]/tbody"));
		List<WebElement> rows = Table.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> i = rows.iterator();
		Log.info("----------DATA STARTS-------------------");
		while (i.hasNext()) {
		WebElement row = i.next();
		Log.info("Text is :   " + row.getText());
		Assert.assertTrue(row.getText() != null);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
		}
		Log.info("----------DATA ENDS-------------------");
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void verifyPendingInvitesTable() throws Throwable {
		try{
		WebElement INVITETABLE = driver
		.findElement(By.xpath("//h3[@class='font-size-16 font-weight-400 text-2e3d51']/following::table/tbody"));
		List<WebElement> rows = INVITETABLE.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> i = rows.iterator();
		Log.info("----------DATA STARTS-------------------");
		while (i.hasNext()) {
		WebElement row = i.next();
		Log.info("Text is :   " + row.getText());
		Assert.assertTrue(row.getText() != null);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
		}
		Log.info("----------DATA ENDS-------------------");
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void verifyGroupTable() throws Throwable{
		try {
		WebDriver driver = DriverFactory.getDriver();
		WebElement Table = driver.findElement(
				By.xpath("//h3[@class='font-size-16 font-weight-400 text-2e3d51']/following::table/tbody"));
		List<WebElement> rows = Table.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> i = rows.iterator();
		Log.info("----------DATA STARTS-------------------");
		while (i.hasNext()) {
		WebElement row = i.next();
		Log.info("Text is :   " + row.getText());
				Assert.assertTrue(row.getText() != null);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", row);
			}
			Log.info("----------DATA ENDS-------------------");
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}
}
