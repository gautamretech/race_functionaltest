package com.race.qa.pages.ui;


import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.AdministrativeLocators;
import com.race.qa.locator.CommonLocators;
import com.race.qa.util.Log;

public class UIAdministrationPage extends TestBase {
	
	WebDriver driver;	
	CommonLocators common;
	AdministrativeLocators adl;
	//test
	public UIAdministrationPage(WebDriver driver) {
		this.driver=driver;
		this.driver=DriverFactory.getDriver();
		//next
		
	}
	
	public void validateGeneralTab() throws Exception {
		common = new CommonLocators();
		adl = new AdministrativeLocators();
		//test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				//test.get().log(Status.INFO,"PIE chart available"); 
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
				//test.get().log(Status.INFO,"PIE CHART not available");
			}
			awaitForElement(common.getMENU_ADMINISTRATION());
			common.getMENU_ADMINISTRATION().click();
			test.get().log(Status.INFO,"Administration side Menu clicked");
			
			awaitForElement((adl.GENERALTAB));
			adl.GENERALTAB.isDisplayed();
			Assert.assertEquals(adl.GENERALTAB.getText(), selectPropertiesFile(true, locale, "general"));
			adl.INPUTCOMPANYNAME.isDisplayed();
			Assert.assertEquals(adl.INPUTCOMPANYNAME.getText(), selectPropertiesFile(true, locale, "companyname"));
			adl.INPUTCEONAME.isDisplayed();
			Assert.assertEquals(adl.INPUTCEONAME.getText(), selectPropertiesFile(true, locale, "CEO"));
			adl.SELECTFOUNDEDYEAR.isDisplayed();
			Assert.assertEquals(adl.SELECTFOUNDEDYEAR.getText(), selectPropertiesFile(true, locale, "founded"));
			adl.INPUTHQADDRESS.isDisplayed();
			Assert.assertEquals(adl.INPUTHQADDRESS.getText(),selectPropertiesFile(true, locale, "headquartersaddress"));
			adl.INPUTCITY.isDisplayed();
			Assert.assertEquals(adl.INPUTCITY.getText(),selectPropertiesFile(false, locale, "city"));
			adl.SELECTCOUNTRY.isDisplayed();
			Log.info("Text is :" + adl.SELECTCOUNTRY.getText());
			Assert.assertEquals(adl.SELECTCOUNTRY.getText(),selectPropertiesFile(false, locale, "country"));
			adl.SELECTINDUSTRYTYPE.isDisplayed();
			Assert.assertEquals(adl.SELECTINDUSTRYTYPE.getText(),selectPropertiesFile(true, locale, "industriescategories"));
			adl.DDSELECTINDUSTRYTYPE.click();
			getDropdownData(adl.DROPDOWNDATA, "li");
			adl.DDSELECTINDUSTRYTYPE.click();
			adl.INPUTWEBSITE.isDisplayed();
			Assert.assertEquals(adl.INPUTWEBSITE.getText(),selectPropertiesFile(false, locale, "website"));
			adl.ISPUBLICCOMPANY.isDisplayed();
			Assert.assertEquals(adl.LABELPUBLICCOMPANY.getText(),selectPropertiesFile(true, locale, "publiccompany"));
			Assert.assertEquals(adl.LABELSTOCKSYMBOL.getText(),selectPropertiesFile(true, locale, "stocksymbol"));
			//test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void validatePlatformSettingsTab() throws Exception {
		//test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		common = new CommonLocators();
		adl = new AdministrativeLocators();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_ADMINISTRATION());
			common.getMENU_ADMINISTRATION().click();
			awaitForElement(adl.GENERALTAB);
			adl.GENERALTAB.isDisplayed();
			adl.PLATFORMSETTINGSTAB.isDisplayed();
			Assert.assertEquals(adl.PLATFORMSETTINGSTAB.getText(), selectPropertiesFile(true, locale, "platformsettings"));
			awaitForElement(adl.PLATFORMSETTINGSTAB);
			adl.PLATFORMSETTINGSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(adl.SELECTDEFAULTLANGUAGE));
			// COMPANYLOGO.isDisplayed();// Need to update the logic
			// LOGOREMOVELINK.isDisplayed();
			// LOGOREUPLOADLINK.isDisplayed();
			adl.SELECTDEFAULTLANGUAGE.isDisplayed();
			Log.info("Text is   :" + adl.SELECTDEFAULTLANGUAGE.getText());
			Assert.assertTrue(adl.SELECTDEFAULTLANGUAGE.getText() != null);
			adl.DDSELECTDEFAULTLANGUAGE.click();
			adl.DDSELECTDEFAULTLANGUAGE.click();
			adl.SELECTDEFAULTCURRENCY.isDisplayed();
			Log.info("Text is   :" + adl.SELECTDEFAULTCURRENCY.getText());
			Assert.assertTrue(adl.SELECTDEFAULTCURRENCY.getText() != null);
			adl.DDSELECTDEFAULTCURRENCY.click();
			adl.DDSELECTDEFAULTCURRENCY.click();
			Thread.sleep(500);
			getPlatformDivData();
			adl.INPUTINACTIVITYINTERVAL.isDisplayed();
			//test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void validateProductsTab() throws Exception {
		test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		common = new CommonLocators();
		adl = new AdministrativeLocators();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_ADMINISTRATION());
			common.getMENU_ADMINISTRATION().click();
			test.get().log(Status.INFO,"Administration side Menu clicked");
			awaitForElement(adl.GENERALTAB);
			adl.GENERALTAB.isDisplayed();
			Log.info("Text Is  " + adl.GENERALTAB.getText());
			adl.PRODUCTSTAB.isDisplayed();
			Log.info(adl.PRODUCTSTAB.getText());
			Assert.assertTrue(adl.PRODUCTSTAB.getText() != null);
			adl.PRODUCTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(adl.SELECTCURRENTPRODUCT));
			adl.SELECTCURRENTPRODUCT.isDisplayed();
			Log.info("Text is   :" + adl.SELECTCURRENTPRODUCT.getText());
			adl.SELECTCURRENTPRODUCT.click();
			getDropdownData(adl.SELECTCURRENTPRODUCTDD, "li");
			adl.SELECTCURRENTPRODUCT.click();
			adl.INPUTSEARCH.isDisplayed();
			adl.SEARCHBUTTON.isDisplayed();
			adl.ADDNEWBUTTON.isDisplayed();
			Log.info(adl.ADDNEWBUTTON.getText());
			Assert.assertTrue(adl.ADDNEWBUTTON.getText() != null);
			adl.TABLEACTIONEDITBUTTON.isDisplayed();
			adl.TABLEACTIONDELETEBUTTON.isDisplayed();
			// clicking and verifying Add New screen
			adl.ADDNEWBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(adl.HEADINGADDSCREEN));
			getAllTextInPopup();
			adl.HEADINGADDSCREEN.isDisplayed();
			Log.info("Text is  :" + adl.HEADINGADDSCREEN.getText());
			adl.UPLOADFILELINK.isDisplayed();
			// adl.INPUTBASEVERSION.isDisplayed();
			adl.INPUTNAME.isDisplayed();
			common.getCLOSEBUTTON().isDisplayed();
			common.getSAVEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
			//test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		}
	

	public void validateCategoryStructure() throws Exception{
		try{
		test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		common = new CommonLocators();
		adl = new AdministrativeLocators();
		
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_ADMINISTRATION());
			common.getMENU_ADMINISTRATION().click();
			awaitForElement((adl.GENERALTAB));
			adl.GENERALTAB.isDisplayed();
			adl.PRODUCTSTAB.isDisplayed();
			Log.info(adl.PRODUCTSTAB.getText());
			Assert.assertTrue(adl.PRODUCTSTAB.getText() != null);
			adl.PRODUCTSTAB.click();
			wait.until(ExpectedConditions.visibilityOf(adl.SELECTCURRENTPRODUCT));
			adl.SELECTCURRENTPRODUCT.isDisplayed();
			Log.info("Text is   :" + adl.SELECTCURRENTPRODUCT.getText());
			adl.SELECTCURRENTPRODUCT.click();
			searchForProductData("Categories");
			Log.info("Text is   :" + adl.SELECTCURRENTPRODUCT.getText());
			wait.until(ExpectedConditions.visibilityOf(adl.INPUTSEARCH));
			adl.ADDNEWBUTTON.isDisplayed();
			// getTableData(STRUCTUREHEADER, STRUCTUREBODY, "th", "tr");
			//adl.STRUCTUREDD.isDisplayed();
			adl.ADDNEWBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(adl.HEADINGADDSCREEN));
			getAllTextInPopup();
			List<WebElement> text = driver.findElements(By.xpath("//div[@class='modal-body no-padding-bottom']//h5"));
			Log.info("::::" + text.size());
			for (int j = 1; j <= text.size(); j++) {
				Assert.assertTrue(
						driver.findElement(By.xpath("//div[@class='modal-body no-padding-bottom']//h5")).isDisplayed());
				Log.info("Text Is :"
						+ driver.findElement(By.xpath("//div[@class='modal-body no-padding-bottom']//h5")).getText());

			}
			List<WebElement> boxes = driver.findElements(By.xpath("//input[@name='textItem']"));

			for (int i = 1; i <= boxes.size(); i++) {
				Log.info("Text box found, text box number :" + i);
				Assert.assertTrue(driver.findElement(By.xpath("//input[@name='textItem']")).isDisplayed());
			}
			common.getSAVEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().click();
			// drop down section and table changes
			wait.until(ExpectedConditions.visibilityOf(adl.STRUCTUREDD));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", adl.STRUCTUREDD);
			// STRUCTUREDD.click();
			List<WebElement> dropDown = driver.findElements(
					By.xpath("//div[@class='race-select-body ps-container ps-theme-default ps-active-y']/ul/li"));
			Log.info("::::::" + dropDown.size());
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", adl.STRUCTUREDD);
			// STRUCTUREDD.click();
			for (int k = 1; k <= 5; k++) {
				// STRUCTUREDD.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", adl.STRUCTUREDD);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver
						.findElement(By.xpath("//div[@class='race-select-body ps ps--active-y']/ul/li[" + k + "]")));
				Log.info(adl.STRUCTUREDD.getText() + " Data starts from Here");
				// getTableData(STRUCTUREHEADER, STRUCTUREBODY, "th", "tr");
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", adl.STRUCTUREDD);
				Log.info(adl.STRUCTUREDD.getText() + " Data ends here from Here");
				test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
			}
			}catch(Exception e){
				test.get().log(Status.ERROR, e.getClass().getSimpleName()+"From Method Name   :"+
			Thread.currentThread().getStackTrace()[1].getMethodName());
				throw e;
			}
		
	}

	public void validateSupplierTab() throws Exception {
		//test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		common = new CommonLocators();
		adl = new AdministrativeLocators();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_ADMINISTRATION());
			common.getMENU_ADMINISTRATION().click();
			awaitForElement((adl.GENERALTAB));
			adl.GENERALTAB.isDisplayed();
			adl.SUPPLIERTAB.isDisplayed();
			Log.info(adl.SUPPLIERTAB.getText());
			Assert.assertTrue(adl.SUPPLIERTAB.getText() != null);
			adl.SUPPLIERTAB.click();
			wait.until(ExpectedConditions.visibilityOf(adl.SELECTCURRENTPRODUCT));
			adl.SELECTCURRENTPRODUCT.isDisplayed();
			Log.info(adl.SELECTCURRENTPRODUCT.getText());
			Assert.assertTrue(adl.SELECTCURRENTPRODUCT.getText() != null);
			// getTableData(SUPPLIERTABLEHEADER, SUPPLIERTABLEBODY, "th", "tr");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", adl.SELECTCURRENTPRODUCT);
			adl.SELECTCURRENTPRODUCT.click();
			// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='race-select-body
			// ps ps--active-y']"))));
			getDropdownData(adl.SELECTSUPPPRODUCT, "li");
			adl.SELECTCURRENTPRODUCT.click();
			adl.INPUTSEARCH.isDisplayed();
			adl.SEARCHBUTTON.isDisplayed();
			adl.ADDNEWBUTTON.isDisplayed();
			adl.TABLEACTIONEDITBUTTON.isDisplayed();
			adl.TABLEACTIONDELETEBUTTON.isDisplayed();
			// clicking and verifying Add New screen
			adl.ADDNEWBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(adl.HEADINGADDSCREEN));
			adl.HEADINGADDSCREEN.isDisplayed();
			adl.HEADINGADDSCREEN.click();
			getAllTextInPopup();
			adl.HEADINGADDSCREEN.click();
			// INPUTCUSTOMID.isDisplayed();
			adl.INPUTNAME.isDisplayed();
			common.getCLOSEBUTTON().isDisplayed();
			common.getSAVEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
			//test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void validateFormTab() throws Exception {
		//test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		common = new CommonLocators();
		adl = new AdministrativeLocators();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_ADMINISTRATION());
			common.getMENU_ADMINISTRATION().click();
			//test.get().log(Status.INFO,"Administration side Menu clicked");
			awaitForElement((adl.GENERALTAB));
			adl.GENERALTAB.isDisplayed();
			adl.FORMSTAB.isDisplayed();
			Log.info(adl.FORMSTAB.getText());
			Assert.assertTrue(adl.FORMSTAB.getText() != null);
			adl.FORMSTAB.click();
			awaitForElement(adl.DDFORMLIBRARY);
			Assert.assertTrue(adl.DDFORMLIBRARY.isDisplayed());
			if (driver.findElements(By.xpath("//div[@class='no-data']")).size() == 1) {
				Log.info("No Data");
			} else {
				Log.info("Data");
				awaitForElement(adl.TABLEACTIONCOPY);
				adl.TABLEACTIONCOPY.isDisplayed();
				adl.TABLEACTIONDELETEBUTTON.isDisplayed();
				adl.TABLEACTIONEDITBUTTON.isDisplayed();
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", adl.ADDNEWBUTTON);
			adl.ADDNEWBUTTON.isDisplayed();
			adl.ADDNEWBUTTON.click();
			wait.until(ExpectedConditions.visibilityOf(adl.HEADINGADDSCREEN));
			adl.HEADINGADDSCREEN.isDisplayed();
			Log.info(adl.HEADINGADDSCREEN.getText());
			Assert.assertTrue(adl.HEADINGADDSCREEN.getText() != null);
			getAllTextInPopup();
			adl.INPUTNAME.isDisplayed();
			adl.SELECTTYPE.isDisplayed();
			adl.SELECTTYPE.click();
			// Thread.sleep(1000);
			// getDropdownData(SELECTTYPEDD, "li");
			adl.SELECTTYPE.click();
			common.getCLOSEBUTTON().isDisplayed();
			// common.getSAVEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().click();
			// Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", adl.FORMCREATEVIEW);
			adl.FORMCREATEVIEW.isDisplayed();
			Log.info(adl.FORMCREATEVIEW.getText());
			Assert.assertTrue(adl.FORMCREATEVIEW.getText() != null);
			//test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}

	}

	public void validateReviewTab() throws Exception {
		test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		common = new CommonLocators();
		adl = new AdministrativeLocators();
		try {
			if (driver.findElements(By.xpath("(//span[@class='empty text-center'])[1]")).size() == 0) {
				Log.info("PIE chart available");
				awaitForElement(common.getPIECHART());
				Assert.assertTrue(common.getPIECHART().isDisplayed());
			} else {
				Log.info("PIE CHART not available");
			}
			awaitForElement(common.getMENU_ADMINISTRATION());
			common.getMENU_ADMINISTRATION().click();
			awaitForElement((adl.GENERALTAB));
			adl.GENERALTAB.isDisplayed();
			adl.REVIEWTAB.isDisplayed();
			Log.info(adl.REVIEWTAB.getText());
			Assert.assertTrue(adl.REVIEWTAB.getText() != null);
			adl.REVIEWTAB.click();
			//wait.until(ExpectedConditions.elementToBeClickable(adl.ADDNEWBUTTON));
			Assert.assertTrue(adl.HEADINGREVIEWTAB.isDisplayed());
			// getTableData(REVIEWTABLEHEADER, REVIEWTABLEBODY, "th", "tr");
			// Asserting Archieve check box
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].scrollIntoView(true);",
			// adl.SHOWARCHIVE);
			// adl.SHOWARCHIVE.isDisplayed();
			// adl.CHECKBOX.click();
			List<WebElement> l = driver
					.findElements(By.xpath("//div[@class='table-responsive no-border']/table/tbody/tr/td[4]"));
			java.util.Iterator<WebElement> i = l.iterator();
			while (i.hasNext()) {
				WebElement row = i.next();
				Assert.assertTrue(row.getText() != "YES");
			}
			// Add New
			driver.findElement(By.xpath("(//input[@name='textItem']/following::button[2])[2]")).isDisplayed();
			//
			driver.findElement(By.xpath("(//input[@name='textItem']/following::button[2])[2]")).click();
			wait.until(ExpectedConditions.visibilityOf(adl.HEADINGADDSCREEN));
			adl.HEADINGADDSCREEN.isDisplayed();
			Log.info(adl.HEADINGADDSCREEN.getText());
			Assert.assertTrue(adl.HEADINGADDSCREEN.getText() != null);
			// INPUTNAME.click();
			// getAllTextInPopup();
			adl.INPUTNAME.isDisplayed();
			adl.DELIVERABLETYPE.isDisplayed();
			adl.DELIVERABLETYPE.click();
			// Thread.sleep(1000);
			// getDropdownData(SELECTTYPEDD, "li");
			adl.DELIVERABLETYPE.click();
			common.getCLOSEBUTTON().isDisplayed();
			common.getSAVEBUTTON().isDisplayed();
			common.getCLOSEBUTTON().click();
			Thread.sleep(500);
			test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "+Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public void getPlatformDivData() throws Exception{
		try{
		//test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver
				.findElement(By.xpath("//div[contains(@class,'blue-border-box margin-left-xs margin-right-xs')]")));
		List<WebElement> Div1 = driver
				.findElements(By.xpath("(//div[contains(@class,'single-settings-content')])//h4"));
		for (int i = 1; i <= Div1.size(); i++) {
			Log.info("Text is :"
					+ driver.findElement(By.xpath("(//div[contains(@class,'single-settings-content')])[" + i + "]//h4"))
							.getText());
			Log.info("Text is :"
					+ driver.findElement(By.xpath("(//div[contains(@class,'single-settings-content')])[" + i + "]//p"))
							.getText());
		}
		/*
		 * List<WebElement> Div2 = driver .findElements(By.xpath(
		 * "//div[contains(@class,'row border-top-lg multifactor-section')]/div//h4"
		 * )); for (int i = 1; i <= Div2.size(); i++) { Log.info("Text is :" +
		 * driver .findElement(By.xpath(
		 * "//div[contains(@class,'row border-top-lg multifactor-section')]/div["
		 * + i + "]//h4")) .getText()); Log.info("Text is :" + driver
		 * .findElement(By .xpath(
		 * "//div[contains(@class,'row border-top-lg multifactor-section')]/div["
		 * + i + "]//p")) .getText()); }
		 */
		//test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}		

	public void getSelectData() throws Exception{
		test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		List<WebElement> Datas = adl.DATA.findElements(By.tagName("li"));
		java.util.Iterator<WebElement> i = Datas.iterator();
		Log.info("-----------Data Starts-------------");

		while (i.hasNext()) {
			WebElement Data = i.next();
			Log.info("Text is :   " + Data.getText());
			Assert.assertTrue(Data.getText() != null);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Data);

		}
		Log.info("-----------Data Ends-------------");
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
	}
	}

	public void searchForProductData(String productdataname) throws Exception{
		try{
		test.get().log(Status.INFO, "inside "+Thread.currentThread().getStackTrace()[1].getMethodName());
		awaitForElement(driver.findElement(By.xpath("//div[@class='race-select-body ps ps--active-y']")));
		Assert.assertTrue(
				driver.findElement(By.xpath("//div[@class='race-select-body ps ps--active-y']")).isDisplayed());
		for (int i = 1; i <= driver.findElements(By.xpath("//div[@class='race-select-body ps ps--active-y']/ul/li"))
				.size(); i++) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//div[@class='race-select-body ps ps--active-y']/ul/li[" + i + "]")));
			if (driver.findElement(By.xpath("//div[@class='race-select-body ps ps--active-y']/ul/li[" + i + "]"))
					.getText().equals(productdataname)) {
				Log.info("Matching Data found");
				driver.findElement(By.xpath("//div[@class='race-select-body ps ps--active-y']/ul/li[" + i + "]"))
						.click();
			} else {
				Log.info("Not found");
			}
		}
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName()+"Fro Method Name  :"
	+Thread.currentThread().getStackTrace()[1].getMethodName());
		throw e;
	}
	}
}

