/*
 ******************************************************************************************************************
 *                                   RACE AUTOMATED TEST FRAMEWORK                                                *
 *                                         TECHNOLOGY STACK														  *
 *                                              JAVA 8															  *
 *                                              TESTNG                                                            *
 *                                          MAVEN BUILD TOOL                                                      *
 *                                     CONTAINERIZATION  DOCKER                                                   *
 *                                     REPORTS - EXTENT REPORT 3.0                                                *
 *                                                                                                                *
 *                                               AUTHOR                                                           *
 *											GAUTAM CHAKRABORTY							                          *
 ******************************************************************************************************************
 */
package com.race.qa.base;

import org.openqa.selenium.WebDriver;

/*
 * DriverFactory class is used to get and set the instance of the WebDriver as Multi-Thread.
 */
public class DriverFactory {
	//Thread Local WebDriver
	private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();

	//WebDriver Get Method
	public static WebDriver getDriver() {
		return webDriver.get();
	}

	//WebDriver Remove method for Multi-Thread and Multi Instance
	public static void removeDriver() {
		webDriver.get().quit();
		webDriver.remove();
		webDriver.set(null);
	}

	//Setting the driver with Different Browser or Environment
	public static void setDriver(WebDriver driver) {
		webDriver.set(driver);
	}
}
