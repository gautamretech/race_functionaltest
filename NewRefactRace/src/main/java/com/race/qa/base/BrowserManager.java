package com.race.qa.base;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/*
 * BrowserManager is Executed to get the value of the Browser for which the WebDriver instance will be Set
 */
public class BrowserManager {

	/*
	 * This Method return the instance of the browser driver. This Method uses
	 * two parameters: Browser(Provided by TestNG.xml document) and
	 * EnvironmentProvided by TestNG.xml document Example values, Firefox,
	 * Chrome, Remote and Local
	 */
	synchronized static WebDriver getInastance(String browser, String env) throws Exception {
		// Node URL is for Instantiation of Remote Webdriver like Selenium Grid
		String nodeURL = "http://169.254.252.218:4444/wd/hub/";
		WebDriver driver = null;
		try {
			if (env.toLowerCase().contains("remote") && browser.toLowerCase().contains("firefox")) {
				System.out.println("Type -1 ");
				driver = new RemoteWebDriver(new URL(nodeURL), getFireFoxOptions());
			} else if (env.toLowerCase().contains("local") && browser.toLowerCase().contains("firefox")) {
				System.out.println("Type -2 ");
				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/browserDrivers/geckodriver.exe");
				driver = new FirefoxDriver(getFireFoxOptions());
			} else if (env.toLowerCase().contains("remote") && browser.toLowerCase().contains("chrome")) {
				System.out.println("Type -3 ");
				driver = new RemoteWebDriver(new URL(nodeURL), getChromeOptions());
			} else if (env.toLowerCase().contains("local") && browser.toLowerCase().contains("chrome")) {
				System.out.println("Type -4 ");
				ChromeDriverService service = new ChromeDriverService.Builder()
						.usingDriverExecutable(
								new File((System.getProperty("user.dir") + "/browserDrivers/chromedriver.exe")))
						.usingAnyFreePort().build();
				driver = new ChromeDriver(service, getChromeOptions());
			}
		} catch (Exception e) {
			throw e;
		}
		return driver;
	}

	// Setting the Download file path to a specific directory
	public static synchronized String createProjectDir() {
		String DirectoryName = (System.getProperty("user.dir") + "/ProjectDownloadsDir");
		File Directory = new File(DirectoryName);
		if (!Directory.exists()) {
			Directory.mkdir();
		}
		return System.getProperty("user.dir") + "\\ProjectDownloadsDir\\";
	}

	// Setting the Firefox Options
	private synchronized static FirefoxOptions getFireFoxOptions() {
		DesiredCapabilities caps = null;
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAssumeUntrustedCertificateIssuer(false);
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/x-msexcel;application/excel;application/x-excel;application/vnd.ms-excel;application/octet-stream;application/json");
		profile.setPreference("browser.download.dir", createProjectDir());
		System.out.println(createProjectDir());
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.panel.shown", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.useDownloadDir", true);
		profile.setPreference("browser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.closeWhenDone", true);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
		profile.setPreference("pdfjs.disabled", false);
		FirefoxOptions ffOptions = new FirefoxOptions();
		caps = DesiredCapabilities.firefox();
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setAcceptInsecureCerts(true);
		caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		ffOptions.merge(caps);
		ffOptions.setProfile(profile);
		return ffOptions;
	}

	// Sets the Chrome Options
	private static ChromeOptions getChromeOptions() {
		DesiredCapabilities caps = null;
		caps = DesiredCapabilities.chrome();
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("download.default_directory", createProjectDir());
		System.out.println(createProjectDir());
		Map<String, String> mobileEmulation = new HashMap<String, String>();
		// mobileEmulation.put("deviceName",
		// configProperities.getProperty("deviceName"));
		ChromeOptions chromeOptions = new ChromeOptions();
		HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
		chromeOptions.setExperimentalOption("prefs", chromePrefs);
		chromeOptions.addArguments("test-type", System.getProperty("user.dir") + "//customFolder");
		caps.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setAcceptInsecureCerts(true);
		caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		return chromeOptions;
	}

}