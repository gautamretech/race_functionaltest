package com.race.qa.base;

import static org.awaitility.Awaitility.await;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.race.qa.locator.CommonLocators;
import com.race.qa.util.Log;
import com.race.qa.util.WebEventListners;
import com.race.qa.util.extentreports.ExtentManager;
import com.sun.org.apache.xpath.internal.operations.Plus;

/*
 * Base Class
 * 1. Load all configuration Related Files
 * 2. Instantiate Browser
 * 3. Sets the Suite Level and Test case level Extent reporting
 * 4. Re-uable Methods
 * 5. Locale Selection
 */
public class TestBase {

	protected static Properties configProperities = null;
	protected static Properties zhFrontendProperties = null;
	protected static Properties enFrontendProperties = null;
	protected static Properties enBackendProperties = null;
	protected static Properties jpFrontendProperties = null;
	protected static Properties zhBackendProperties = null;
	protected static Properties jpBackendProperties = null;
	protected static Properties koFrontendProperties = null;
	protected static Properties koBackendProperties = null;
	protected String STARTDATE;
	protected String LAUNCHDATE;
	protected String locale;
	protected EventFiringWebDriver e_driver = null;
	private WebEventListners eventListner = null;
	private static String loginMethod = "/login";
	protected static FluentWait<WebDriver> wait = null;
	protected CommonLocators TBcommon = null;;
	private static ExtentReports extent;
	protected static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
	protected static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
	protected static String downloadFilepath;
	private static XSSFWorkbook workbook;
	private static XSSFSheet firstSheet;
	private static FileInputStream inputStream;
	private static Row nextRow;
    private static Cell cell;
	

	public TestBase() {
	}

	//Creates Extent Reports Folder 
	@BeforeSuite(alwaysRun = true)
	public synchronized void beforeSuite() {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		extent = ExtentManager.createInstance(System.getProperty("user.dir") + "/ExtentReports/ExtentReports" + dateName + ".html");
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(
				System.getProperty("user.dir") + "/ExtentReports/ExtentReports" + dateName + ".html");
		extent.attachReporter(htmlReporter);

	}

	//Sets the Extent report - Parent at the class level
	@BeforeClass(alwaysRun = true)
	public synchronized void beforeClass() throws Exception {
		try {
			ExtentTest parent = extent.createTest(getClass().getName());
			parentTest.set(parent);
			parentTest.get().log(Status.PASS, "Before Class Setup Successfull");
		} catch (Exception e) {
			parentTest.get().log(Status.FAIL, e.getClass().getSimpleName());
			throw e;
		}
	}

	/*
	 * 1. Instantiate Browser - Gets the value from the TestNG.xml file using ITestContext class 
	 * 2. Sets the Test case level Extent reports - Logging
	 * 3. Sets the Download File Path
	 * 4. Window Maximise, event Firing WebDriver, Waits
	 */
	@BeforeMethod(alwaysRun = true)
	public synchronized void initialization(Method method, ITestContext context) throws Exception {
		try {
			//String browser="chrome";
			//String env="local";
			String browser=context.getCurrentXmlTest().getParameter("browser");
			String env =context.getCurrentXmlTest().getParameter("env");
			WebDriver driver = BrowserManager.getInastance(browser, env);
			DriverFactory.setDriver(driver);
			downloadFilepath = BrowserManager.createProjectDir();
			ExtentTest child = parentTest.get().createNode(method.getName());
			test.set(child);
			test.get().log(Status.INFO, MarkupHelper.createLabel("BROWSER: "+browser.toUpperCase()+" and Environment Chosen: "+env.toUpperCase(), ExtentColor.PURPLE));
			configProperities = loadConfigFile();
			locale = getBrowserLocale();
			loadLanguageSpecificFiles(locale);
			startEventFiringDriver(driver);
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			wait = fluentWait(driver, 30, 1000);
			driver.navigate().to(configProperities.getProperty("url") + loginMethod);
			test.get().log(Status.PASS,
					"Initial Test Setup configured For :" + Thread.currentThread().getStackTrace()[1].getMethodName());
		} catch (Exception e) {
			test.get().log(Status.FAIL, e.getClass().getSimpleName());
			throw e;
		}
	}

	/*
	 * Reporting in terms of test status at the end of test and Logged in report. Removes the WebDriver instance
	 */
	@AfterMethod(alwaysRun = true)
	public synchronized void tearDown(ITestResult iTestResult) throws Exception {
		try {
			WebDriver driver = DriverFactory.getDriver();
			if (iTestResult.getStatus() == ITestResult.FAILURE) {
				String screenShotPath = getScreenshot(driver, "screenshot");
				test.get().log(Status.FAIL, MarkupHelper.createLabel(
						iTestResult.getName() + " Test case FAILED due to below issues:", ExtentColor.RED));
				test.get().fail(iTestResult.getThrowable());
				test.get().fail("Snapshot below: " + test.get().addScreenCaptureFromPath(screenShotPath));
			}

			else if (iTestResult.getStatus() == ITestResult.SKIP) {
				test.get().log(Status.FAIL, MarkupHelper.createLabel(
						iTestResult.getName() + " Test case SKIPPED due to below issues:", ExtentColor.ORANGE));
				test.get().skip(iTestResult.getThrowable());
			} else
				test.get().pass(MarkupHelper.createLabel(iTestResult.getName() + "Test PASSED", ExtentColor.GREEN));

			extent.flush();

			DriverFactory.removeDriver();
		} catch (Exception e) {
			test.get().log(Status.FAIL, "After Method Failed" + e.getClass().getSimpleName());
			extent.flush();
		}
	}

	public String getScreenshot(WebDriver driver, String screenshotName) throws Exception {
		String destination = null;
		try {
			// below line is just to append the date format with the screenshot
			// name to avoid duplicate names
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			// after execution, you could see a folder "FailedTestsScreenshots"
			// under src folder
			destination = System.getProperty("user.dir") + "/FailedScreenShots/" + screenshotName + dateName + ".png";
			File finalDestination = new File(destination);
			FileUtils.copyFile(source, finalDestination);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
		}
		// Returns the captured file path
		return destination;
	}

	@SuppressWarnings("deprecation")
	public synchronized FluentWait<WebDriver> fluentWait(WebDriver driver, int totalWait, int pollTime) {
		driver = DriverFactory.getDriver();
		FluentWait<WebDriver> localWait;
		localWait = new FluentWait<WebDriver>(driver).ignoring(StaleElementReferenceException.class)
				.pollingEvery(totalWait, TimeUnit.SECONDS).withTimeout(pollTime, TimeUnit.MILLISECONDS);
		System.out.println("End time     " + System.currentTimeMillis());
		return localWait;
	}

	// Load properties files
	public synchronized Properties loadConfigFile() throws Exception {
		Properties localConfig = new Properties();
		FileInputStream configFile=null;
		try {
			// gets the file path from the project directory - Relative path
			String configFilePath = System.getProperty("user.dir") + "/PropertiesFiles/config.properties";
			configFile= new FileInputStream(configFilePath);
			localConfig.load(new InputStreamReader(configFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			configFile.close();
		}
		return localConfig;

	}

	public synchronized Properties loadZhFrontendFile() throws Exception {
		Properties localzhFrontend = new Properties();
		FileInputStream zhFrontFile=null;
		try {
			String zhFrontFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/zhfrontend.properties");
			zhFrontFile= new FileInputStream(new File(zhFrontFilePath));
			localzhFrontend.load(new InputStreamReader(zhFrontFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			zhFrontFile.close();
		}
		
		return localzhFrontend;
	}

	public synchronized Properties loadEnFrontendFile() throws Exception {
		Properties localenFrontend = new Properties();
		FileInputStream enFrontFile=null;
		try {
			String enFrontFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/enfrontend.properties");
			enFrontFile = new FileInputStream(new File(enFrontFilePath));
			localenFrontend.load(new InputStreamReader(enFrontFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			enFrontFile.close();
		}
		return localenFrontend;
	}

	public synchronized Properties loadEnBackendFile() throws FileNotFoundException, IOException {
		Properties localenBackend = new Properties();
		FileInputStream enBackFile=null;
		try {
			String enBackFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/enBackend.properties");
			enBackFile= new FileInputStream(new File(enBackFilePath));
			localenBackend.load(new InputStreamReader(enBackFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			enBackFile.close();
		}
		return localenBackend;
	}

	public synchronized Properties loadJpFrontendFile() throws FileNotFoundException, IOException {
		Properties localenjpFrontend = new Properties();
		FileInputStream jpFrontFile=null;
		try {
			String jpFrontFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/jpfrontend.properties");
			jpFrontFile= new FileInputStream(new File(jpFrontFilePath));
			localenjpFrontend.load(new InputStreamReader(jpFrontFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			jpFrontFile.close();
		}
		return localenjpFrontend;
	}

	public synchronized Properties loadZhBackendFile() throws FileNotFoundException, IOException {
		Properties localzhBackend = new Properties();
		FileInputStream zhBackFile=null;
		try {
			String zhBackFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/zhbackend.properties");
			zhBackFile= new FileInputStream(zhBackFilePath);
			localzhBackend.load(new InputStreamReader(zhBackFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			zhBackFile.close();
		}
		return localzhBackend;
	}

	public synchronized Properties loadJpBackendFile() throws FileNotFoundException, IOException {
		Properties localjpBackend = new Properties();
		FileInputStream jpBackFile=null;
		try {
			String jpBackFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/jpbackend.properties");
			jpBackFile = new FileInputStream(jpBackFilePath);
			localjpBackend.load(new InputStreamReader(jpBackFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			jpBackFile.close();
		}
		return localjpBackend;
	}

	public synchronized Properties loadKoFrontendFile() throws FileNotFoundException, IOException {
		Properties localkoFrontend = new Properties();
		FileInputStream koFrontFile=null;
		try {
			String koFrontFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/kofrontend.properties");
			koFrontFile= new FileInputStream(new File(koFrontFilePath));
			localkoFrontend.load(new InputStreamReader(koFrontFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			koFrontFile.close();
		}
		return localkoFrontend;
	}

	public synchronized Properties loadKoBackendFile() throws FileNotFoundException, IOException {
		Properties localkoBackend = new Properties();
		FileInputStream koBackFile=null;
		try {
			String koBackFilePath = (System.getProperty("user.dir") + "/PropertiesFiles/kobackend.properties");
			koBackFile= new FileInputStream(koBackFilePath);
			localkoBackend.load(new InputStreamReader(koBackFile, Charset.forName("UTF-8")));
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			koBackFile.close();
		}
		return localkoBackend;
	}

	// Get browser Locale
	public synchronized String getBrowserLocale() throws Exception {
		try {
			configProperities = loadConfigFile();
			Locale currentLocale = Locale.getDefault();
			if (!currentLocale.getLanguage().toUpperCase().equals(configProperities.getProperty("language"))
					&& configProperities.getProperty("default").equals("false")) {
				locale = configProperities.getProperty("language");
			} else if (!currentLocale.getLanguage().toUpperCase().equals(configProperities.getProperty("language"))
					&& configProperities.getProperty("default").equals("true")) {
				test.get().log(Status.ERROR, "Locale Selection not correct, please check Config File for details...");
			} else if (currentLocale.getLanguage().toUpperCase().equals(configProperities.getProperty("language"))
					&& configProperities.getProperty("default").equals("true")
					|| configProperities.getProperty("default").equals("false")) {
			locale = currentLocale.getLanguage().toUpperCase();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
		return locale;
	}

	public synchronized boolean isFileDownloaded(String downloadPath, String fileName) throws Exception {
		boolean flag = false;
		try {
			Path filePath = Paths.get(downloadPath, fileName);
			System.out.println(fileName);
			System.out.println(downloadPath);
			await("Not downloaded").atMost(1, TimeUnit.MINUTES).ignoreExceptions()
					.until(() -> filePath.toFile().exists());
			File dir = new File(downloadPath);
			File[] dir_contents = dir.listFiles();
			for (int i = 0; i < dir_contents.length; i++) {
				if (dir_contents[i].getName().equals(fileName))
					return flag = true;
				Log.info("File Found Inside the Folder");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
		return flag;
	}

	public synchronized void deleteFile(String FILENAME) {
		try {
			File file = new File(System.getProperty("user.dir") + "/ProjectDownloadsDir/" + FILENAME);
			if (!file.exists()) {
				test.get().log(Status.ERROR, "File does not exists in folder");
			} else {
				test.get().log(Status.PASS,"File already exist");
				file.delete();
				test.get().log(Status.PASS,"File Deleted Successfully");
		}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public synchronized void ExcelReader(String FILENAME) throws IOException {
		String excelFilePath = (System.getProperty("user.dir")) + "/ProjectDownloadsDir/" + FILENAME;
		inputStream= new FileInputStream(new File(excelFilePath));
		try {
			workbook = new XSSFWorkbook(inputStream);
			firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();
			Log.info("Total Number of Rows:" + iterator);
			firstSheet.getRow(0).getPhysicalNumberOfCells();
			while (iterator.hasNext()) {
				nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				Log.info("--------Column Data Starts-----------");
				while (cellIterator.hasNext()) {
					cell = cellIterator.next();
					if (cell.getCellTypeEnum() == CellType.STRING) {
						Log.info(cell.getStringCellValue());
					} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
						System.out.print(cell.getBooleanCellValue());
						Log.info(" - ");
					} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
						System.out.print(cell.getNumericCellValue());
						Log.info(" - ");
					}
				}
				Log.info("--------Column Data Ends-----------");
			}
			workbook.close();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}finally{
			inputStream.close();
		}
	}

	public synchronized void loadLanguageSpecificFiles(String Locale) throws Exception {
		try {
			Locale = getBrowserLocale();
			if (Locale.equals("CN")) {
				zhFrontendProperties = loadZhBackendFile();
				zhBackendProperties = loadZhBackendFile();
			} else if (Locale.equals("JP")) {
				jpBackendProperties = loadJpBackendFile();
				jpFrontendProperties = loadJpFrontendFile();
			} else if (Locale.equals("EN")) {
				enBackendProperties = loadEnBackendFile();
				enFrontendProperties = loadEnFrontendFile();
			} else if (Locale.equals("KO")) {
				koBackendProperties = loadKoBackendFile();
				koFrontendProperties = loadKoFrontendFile();
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  From The method Name "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public synchronized void getAllTextInPopup() throws Exception {
		try {
			WebDriver driver = DriverFactory.getDriver();
			List<WebElement> texts = driver.findElements(By.tagName("label"));
			System.out.println(texts.size());
			java.util.Iterator<WebElement> i = texts.iterator();
			Log.info("-----------Data Starts-------------");
			while (i.hasNext()) {
				WebElement text = i.next();
				Log.info("Text is :   " + text.getText());
				Assert.assertTrue(text.getText() != null);
			}
			Log.info("-----------Data Ends-------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public synchronized void getDropdownData(WebElement element, String tag) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
			List<WebElement> Datas = element.findElements(By.tagName(tag));
			java.util.Iterator<WebElement> i = Datas.iterator();
			Log.info("-----------Data Starts-------------");

			while (i.hasNext()) {
				WebElement Data = i.next();
				Log.info("Text is :   " + Data.getText());
				Assert.assertTrue(Data.getText() != null);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Data);
			}
			Log.info("-----------Data Ends-------------");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public static class Randomizer {
		public static int generate(int min, int max) {
			return min + (int) (Math.random() * ((max - min) + 1));
		}
	}

	public synchronized String selectPropertiesFile(boolean isFrontend, String Locale, String key) throws Exception {
		String properties = null;
		Locale = getBrowserLocale();
		try {

			if (Locale.equals("CN")) {
				if (isFrontend) {
					zhFrontendProperties = loadZhFrontendFile();
					properties = zhFrontendProperties.getProperty(key);
				} else {
					zhBackendProperties = loadZhBackendFile();
					properties = zhBackendProperties.getProperty(key);
				}
			} else if (Locale.equals("JP")) {
				if (isFrontend) {
					jpFrontendProperties = loadJpFrontendFile();
					properties = jpFrontendProperties.getProperty(key);
				} else {
					jpBackendProperties = loadJpBackendFile();
					properties = jpBackendProperties.getProperty(key);
				}
			}
			if (Locale.equals("EN")) {
				if (isFrontend) {
					enFrontendProperties = loadEnFrontendFile();
					properties = enFrontendProperties.getProperty(key);
				} else {
					enBackendProperties = loadEnBackendFile();
					properties = enBackendProperties.getProperty(key);
				}
			}
			if (Locale.equals("KO")) {
				if (isFrontend) {
					koFrontendProperties = loadKoFrontendFile();
					properties = koFrontendProperties.getProperty(key);
				} else {
					koBackendProperties = loadKoBackendFile();
					properties = koBackendProperties.getProperty(key);
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
		return properties;

	}

	public synchronized void awaitForElement(WebElement webelement) throws Exception {
		try {
			await("Element not found").atMost(1, TimeUnit.MINUTES).ignoreExceptions()
					.until(() -> webelement.isDisplayed() || webelement.isEnabled());
		} catch (StaleElementReferenceException e) {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(webelement)));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName() + "  from   "
					+ Thread.currentThread().getStackTrace()[1].getMethodName());
			throw e;
		}
	}

	public synchronized boolean selectOptionFromDropdown(WebElement dropdown, WebElement selectOptions, WebElement container,
			WebElement toSelectOption, WebElement inputSearch, String inputOption) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		boolean Flag = true;
		String locale = getBrowserLocale();
		try {
			awaitForElement(dropdown);
			Assert.assertTrue(dropdown.isDisplayed());
			wait.until(ExpectedConditions.elementToBeClickable(dropdown));
			dropdown.click();
			System.out.println("1");
			awaitForElement(container);
			System.out.println("2");
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(inputSearch)));
			inputSearch.sendKeys(inputOption);
			System.out.println("3");
			Thread.sleep(3000);
			if (driver.findElements(By.xpath("//li[contains(@class,'no-options')]")).size() == 1) {
				Log.info("No values found");
				Assert.assertEquals(driver.findElement(By.xpath("//li[contains(@class,'no-options')]")).getText(),
						selectPropertiesFile(true, locale, "nooptionsavailable"));
				Flag = false;
			} else {
				System.out.println("control here");
				Assert.assertTrue(selectOptions.isDisplayed());
				toSelectOption.click();
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		return Flag;
	}

	public synchronized void returnToDashboardPage() throws Exception {
		TBcommon = new CommonLocators();
		try {
			wait.until(ExpectedConditions.elementToBeClickable(TBcommon.HOMEPAGELINK));
			Assert.assertTrue(TBcommon.HOMEPAGELINK.isDisplayed());
			TBcommon.HOMEPAGELINK.click();
			waitForPieChartData();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public synchronized void waitForProgressBar(WebElement element) {
		int bar;
		do {
			String prog = element.getAttribute("style");
			prog = prog.replaceFirst("width: ", "").replace("%", "").replace(";", "").trim();
			bar = Integer.parseInt(prog);
			System.out.print("]"+"\r."+"["+prog);
		} while (bar != 100);
		Assert.assertTrue(bar == 100);

	}

	public void confirmSelection(WebElement element, String text) {
		if (element.getText() != text) {

		}
	}

	public void waitForElement(List<WebElement> element) throws InterruptedException {
		do {
			Thread.sleep(10);
			System.out.println(element.size());
		} while (element.size() != 1);
	}

	public synchronized void moveToElement(WebElement parentElement, WebElement childElement) {
		WebDriver driver = DriverFactory.getDriver();
		Actions action = new Actions(driver);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(parentElement)));
		action.moveToElement(parentElement).perform();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(childElement)));
		action.moveToElement(childElement).click().build().perform();
	}

	public synchronized void mouseHoverOnElement(WebElement parentElement) {
		WebDriver driver = DriverFactory.getDriver();
		Actions action = new Actions(driver);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(parentElement)));
		action.moveToElement(parentElement).perform();
	}

	public void waitForDatainDriopdown(List<WebElement> element) throws InterruptedException {
		do {
			Thread.sleep(1000);
			System.out.println(element.size()+"Herererererere");
		} while (element.size() != 1);
	}

	public synchronized void clickSideNavigationMenu(String menuName) throws Exception {
		TBcommon = new CommonLocators();
		try {
			WebDriver driver = DriverFactory.getDriver();
			//waitForPieChart();
			Assert.assertTrue(TBcommon.getPIECHART().isDisplayed());
			Assert.assertTrue(TBcommon.COMPLETESIDEMENU.isDisplayed());
			Log.info("Side Menu is Displaying");
			if (menuName == "Strategy") {
				Assert.assertEquals(TBcommon.MENU_STRATEGY_TEXT.getText(),
						selectPropertiesFile(true, locale, "strategy"));
				TBcommon.MENU_STRATEGY_LINK.click();
				waitForScreen(TBcommon.COMMONTOPMENULOCATOR, selectPropertiesFile(true, locale, "strategy"));
				awaitForElement(TBcommon.COMMONTOPMENULOCATOR);
				waitForScreen(TBcommon.COMMONTOPMENULOCATOR, selectPropertiesFile(true, locale, "strategy"));
				Assert.assertEquals(TBcommon.COMMONTOPMENULOCATOR.getText(),
						selectPropertiesFile(true, locale, "strategy"));
				Log.info("Clicked on Strategy Menu");
			} else if (menuName == "Products") {
				System.out.println("qqqqqq");
				waitForElementToAppear(driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[4]/a/span")), driver);
				Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[4]/a/span")).getText(),
						selectPropertiesFile(true, locale, "products"));
				driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[4]/a")).click();
				System.out.println("qqqqqq111111");
				waitForPieChartData();
				System.out.println("qqqqqq222222");
				Assert.assertTrue(TBcommon.getPIECHART().isDisplayed());
				awaitForElement(TBcommon.COMMONTOPMENULOCATOR);
				System.out.println("qqqqqq3333333");
				waitForScreen(TBcommon.COMMONTOPMENULOCATOR, selectPropertiesFile(true, locale, "products"));
				System.out.println("qqqqqq5555555");
				Assert.assertEquals(TBcommon.COMMONTOPMENULOCATOR.getText(),
						selectPropertiesFile(true, locale, "products"));
				Log.info("Clicked on Products Menu");
			} else if (menuName == "Suppliers") {
				waitForElementToAppear(driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[5]/a/span")), driver);
				//awaitForElement(driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[5]/a/span")));
				Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[5]/a/span")).getText(),
						selectPropertiesFile(true, locale, "supplier"));
				System.out.println("AAAA");
				driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[5]/a")).click();
				System.out.println("BBBB");
				waitForPieChartData();
				System.out.println("CCC");
				Assert.assertTrue(TBcommon.getPIECHART().isDisplayed());
				waitForScreen(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")),
						selectPropertiesFile(true, locale, "supplier"));
				awaitForElement(TBcommon.COMMONTOPMENULOCATOR);
				Assert.assertEquals(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")).getText(),
						selectPropertiesFile(true, locale, "supplier"));
				Log.info("Clicked on Suppliers Menu");
			} else if (menuName == "TeamManagement") {
				Assert.assertEquals(
						driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[6]/a/span")).getText(),
						selectPropertiesFile(true, locale, "teammanagement"));
				driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[6]/a")).click();
				awaitForElement(TBcommon.getPIECHART());
				Assert.assertTrue(TBcommon.getPIECHART().isDisplayed());
				awaitForElement(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)")));
				waitForScreen(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")),
						selectPropertiesFile(true, locale, "team"));
				Assert.assertEquals(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")).getText(),
						selectPropertiesFile(true, locale, "team"));
				Log.info("Clicked on Team Management Menu");
			} else if (menuName == "ProjectTemplates") {
				waitForElementToAppear(driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[7]/a/span")), driver);
				Assert.assertEquals(driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[7]/a/span")).getText(),
						selectPropertiesFile(true, locale, "projecttemplates"));
				driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[7]/a")).click();
				waitForRoundLoadertoDisappear();
				awaitForElement(TBcommon.TEMPLATEGRAPH);
				Assert.assertTrue(TBcommon.TEMPLATEGRAPH.isDisplayed());
				Assert.assertEquals(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")).getText(),
						selectPropertiesFile(true, locale, "existingtemplates"));
				Log.info("Clicked on Project Template Menu");
			} else if (menuName == "Administration") {
				Assert.assertEquals(
						driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[8]/a/span")).getText(),
						selectPropertiesFile(true, locale, "administration"));
				driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[8]/a")).click();
				awaitForElement(TBcommon.getPIECHART());
				Assert.assertTrue(TBcommon.getPIECHART().isDisplayed());
				awaitForElement(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)")));
				waitForScreen(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")),
						selectPropertiesFile(true, locale, "general"));
				Assert.assertEquals(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")).getText(),
						selectPropertiesFile(true, locale, "general"));
				Log.info("Clicked on Administration Menu");
			} else if (menuName == "Reports") {
				Assert.assertEquals(
						driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[9]/a/span")).getText(),
						selectPropertiesFile(true, locale, "reports"));
				driver.findElement(By.xpath("(//div[@class='main-nav-items']/ul/li)[9]/a")).click();
				awaitForElement(TBcommon.getPIECHART());
				Assert.assertTrue(TBcommon.getPIECHART().isDisplayed());
				awaitForElement(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)")));
				waitForScreen(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")),
						selectPropertiesFile(true, locale, "team"));
				Assert.assertEquals(driver.findElement(By.xpath("(//ul[@class='race-tabs']/li/a)[1]")).getText(),
						selectPropertiesFile(true, locale, "team"));
				Log.info("Clicked on Reports Menu");
			} else {
				Log.info("No Matching Records Found");
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	public void waitForScreen(WebElement element, String text) throws InterruptedException {
		try {
			while (!element.getText().equals(text)) {
				Log.info(element.getText());
				Log.info(text);
				Thread.sleep(100);
			}
		} catch (StaleElementReferenceException e) {
			Thread.sleep(10);
		}

	}

	public synchronized boolean isImageLoaded(WebElement element) {
		WebDriver driver = DriverFactory.getDriver();
		boolean flag = false;
		boolean loaded = ((JavascriptExecutor) driver).executeScript(
				"return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
				element) != null;
		if (loaded == true) {
			Log.info("Image Loaded");
			flag = true;
		} else {
			Log.info("Image Not Loaded");
		}
		return flag;
	}

	public boolean waitForTableContents(List<WebElement> element) throws InterruptedException {
		boolean found = false;
		while (element.size() == 0) {
			Thread.sleep(100);
			Log.info("Item not displayed yet");
		}
		found = true;
		return found;
	}

	public synchronized String getStartDate() {
		String startDate = "";
		String startDay = LocalDate.now().getDayOfWeek().toString();
		if (startDay == "MONDAY") {
			startDate = LocalDate.now().toString();
		} else if (startDay == "TUESDAY") {
			startDate = LocalDate.now().toString();
		} else if (startDay == "WEDNESDAY") {
			startDate = LocalDate.now().toString();
		} else if (startDay == "THURSDAY") {
			startDate = LocalDate.now().toString();
		} else if (startDay == "FRIDAY") {
			startDate = LocalDate.now().toString();
		} else if (startDay == "SATURDAY") {
			startDate = LocalDate.now().plusDays(2).toString();
		} else if (startDay == "SUNDAY") {
			startDate = LocalDate.now().plusDays(1).toString();
		}
		STARTDATE = startDate;
		return STARTDATE;
	}

	public synchronized String getLaunchDate() {
		String launchDate = "";
		String LaunchDay = LocalDate.now().getDayOfWeek().toString();

		if (LaunchDay == "MONDAY") {
			launchDate = LocalDate.now().plusDays(1).toString();
		} else if (LaunchDay == "TUESDAY") {
			launchDate = LocalDate.now().plusDays(1).toString();
		} else if (LaunchDay == "WEDNESDAY") {
			launchDate = LocalDate.now().plusDays(1).toString();
		} else if (LaunchDay == "THURSDAY") {
			launchDate = LocalDate.now().plusDays(1).toString();
		} else if (LaunchDay == "FRIDAY") {
			launchDate = LocalDate.now().plusDays(3).toString();
		} else if (LaunchDay == "SATURDAY") {
			launchDate = LocalDate.now().plusDays(3).toString();
		} else if (LaunchDay == "SUNDAY") {
			launchDate = LocalDate.now().plusDays(2).toString();
		}
		LAUNCHDATE = launchDate;
		return LAUNCHDATE;

	}

	public synchronized void languageSelection(String Locale) throws Exception {
		TBcommon = new CommonLocators();
		try {
			this.locale = Locale;
			this.locale = getBrowserLocale();
			wait.until(ExpectedConditions.elementToBeClickable(TBcommon.getSELECTLANGUAGE()));
			if (TBcommon.getSELECTEDLANGUAGE().getText().equals("English (US)")) {
				Log.info("Default Language is English");
				if (Locale.equals("EN")) {
					Log.info("Locale Selected --- English");
					Assert.assertEquals("English (US)", TBcommon.getSELECTEDLANGUAGE().getText());
					Log.info("Language is not Changed");
				} else if (Locale.equals("CN")) {
					Log.info("Language Selected is Chinese");
					TBcommon.getSELECTLANGUAGE().click();
					Thread.sleep(1000);
					// awaitForElement(common.getSELECTCHINESE());
					wait.until(ExpectedConditions.visibilityOf(TBcommon.getSELECTCHINESE()));
					Assert.assertEquals(TBcommon.getSELECTCHINESE().getText(), "Simplified Chinese");
					TBcommon.getSELECTCHINESE().click();
					awaitForElement(TBcommon.getSELECTEDLANGUAGE());
					Assert.assertEquals(TBcommon.getSELECTEDLANGUAGE().getText(),
							selectPropertiesFile(false, Locale, "simplifiedchinese"));
				} else if (Locale.equals("JP")) {
					Log.info("Language Selected is Japanese");
					TBcommon.getSELECTLANGUAGE().click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(TBcommon.getSELECTJAPANESE()));
					Assert.assertEquals(TBcommon.getSELECTJAPANESE().getText(), "Japanese");
					TBcommon.getSELECTJAPANESE().click();
					awaitForElement(TBcommon.getSELECTEDLANGUAGE());
					Thread.sleep(500);
					Assert.assertEquals(TBcommon.getSELECTEDLANGUAGE().getText(),
							selectPropertiesFile(false, Locale, "japanese"));
				} else if (Locale.equals("KO")) {
					Log.info("Language Selected is Korean");
					TBcommon.getSELECTLANGUAGE().click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(TBcommon.getSELECTKOREAN()));
					Assert.assertEquals(TBcommon.getSELECTKOREAN().getText(), "Korean");
					TBcommon.getSELECTKOREAN().click();
					awaitForElement(TBcommon.getSELECTEDLANGUAGE());
					//jsWait();
					Thread.sleep(1500);
					Assert.assertEquals(TBcommon.getSELECTEDLANGUAGE().getText(),
							selectPropertiesFile(false, Locale, "korean"));
				}
			}
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
		}
	}
	public void jsWait(){
		
	}

		public void startEventFiringDriver(WebDriver driver) {
		driver = DriverFactory.getDriver();
		e_driver = new EventFiringWebDriver(driver);
		eventListner = new WebEventListners();
		e_driver.register(eventListner);
		driver = e_driver;
	}
		public void waitForElementToAppear(WebElement element, WebDriver driver) {
			try{
			(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			    public Boolean apply(WebDriver d) {
			        return element.isDisplayed() ||element.isEnabled();
		    }
			});
			}catch(StaleElementReferenceException e){
				(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
				    public Boolean apply(WebDriver d) {
				        return element.isDisplayed() ||element.isEnabled();
			    }
				});
			}
			}
		public void waitForTextToAppear(WebElement element) throws InterruptedException{
			int i=0;
			do{
				Thread.sleep(100);
				System.out.println("waiting..."+i);
				i=i+1;
			}while(element.getAttribute("value").length()!=0 || i<=10);
		}
		
		public void waitForElementToAppearWithText(WebElement element, WebDriver driver) throws InterruptedException{
			
			(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			    public Boolean apply(WebDriver d) {
			        return element.getAttribute("value").length() != 0;
			        
			    }
			    
			});
			
			}
public void waitForElementToDisappear(List<WebElement> element) throws InterruptedException{
			while(element.size()!=0){
				System.out.print("\r.");
				System.out.println(element.size()+"waitForElementToDisappear");
				Thread.sleep(1000);
			}
			
			}
				
		
	public void waitForLandingPage() throws InterruptedException{
		WebDriver driver = DriverFactory.getDriver();
		TBcommon = new CommonLocators(); 
		try{
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
		    public Boolean apply(WebDriver d) {
		        return TBcommon.LANDINGPAGEMENUATTRIBUTE.getAttribute("value").length() != 0;
		    }
		});
		Integer size = driver.findElements(By.xpath("//div[@class='main-nav-items']/ul/li")).size();
		if(size.equals(9)){
			test.get().log(Status.INFO, "Administration Login");
		}
		else if(size.equals(7)){
			test.get().log(Status.INFO, "PM User Login");
		}
		else if(size.equals(6)){
			test.get().log(Status.INFO, "Colaborator User Login");
		}
		else if(size.equals(3)){
			test.get().log(Status.INFO, "Supplier User Login");
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
		}
		
	}
	public void waitForPieChartData() throws InterruptedException{
		TBcommon=new CommonLocators();
		WebDriver driver =DriverFactory.getDriver();
		try{
		int i=0;
		String xpath_1="(//span[@class='empty text-center'])";
		String xpath_2="//*[@class='main-canvas']";
		while(driver.findElements(By.xpath("//*[@class='main-canvas']")).size()==0 || i<=10){
			System.out.print("{"+"\r.}");
			Thread.sleep(100);
			i=i+1;
			continue;
		}
		if(driver.findElements(By.xpath(xpath_1+"[2]")).size()==0 && driver.findElements(By.xpath(xpath_1+"[1]")).size()==0
				&& driver.findElements(By.xpath(xpath_2)).size()!=0){
			test.get().log(Status.INFO,"LOGIN WITH All Data");
		}
		else if(driver.findElements(By.xpath(xpath_1+"[2]")).size()==0 && driver.findElements(By.xpath(xpath_1+"[1]")).size()!=0
				&& driver.findElements(By.xpath(xpath_2)).size()!=0){
			test.get().log(Status.INFO,"LOGIN with Pie Data");
		}
		else if(driver.findElements(By.xpath(xpath_1+"[2]")).size()!=0 && driver.findElements(By.xpath(xpath_1+"[1]")).size()==0
				&& driver.findElements(By.xpath(xpath_2)).size()==0){
			test.get().log(Status.INFO,"Login without Pie Data");
		}
		else if(driver.findElements(By.xpath(xpath_1+"[2]")).size()!=0 && driver.findElements(By.xpath(xpath_1+"[1]")).size()!=0
				&& driver.findElements(By.xpath(xpath_2)).size()==0){
			test.get().log(Status.INFO,"Login WITHOUT DATA");
		}
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
		}
		
	}
	public void waitForElementToPresentSingleTime(List<WebElement> element) throws InterruptedException{
		do{
			Thread.sleep(100);
			System.out.println(element.size()+"I Am struck Here");
		}while(element.size()!=1);
	}
	public void waitForRoundLoadertoDisappear() throws InterruptedException{
		WebDriver driver=DriverFactory.getDriver();
		do{
			Thread.sleep(100);
			System.out.println(driver.findElements(By.xpath("//div[contains(@class,'round-loader')]")).size());
			System.out.println(driver.findElements(By.xpath("//div[contains(@class,'round-loader')]")).size()==0);
			
		}while(driver.findElements(By.xpath("//div[contains(@class,'round-loader')]")).size()==0);
	}
	}