package com.race.qa.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import com.race.qa.base.DriverFactory;


public class AdministrativeLocators{
		
	// All the Locators from the administration page
	@FindBy(xpath = "//*[@class='race-select-body ps-container ps-theme-default']/ul")
	public WebElement DATA;
	@FindBy(xpath = "//div[@class='race-select-body ps ps--active-y']/ul")
	public WebElement DROPDOWNDATA;
	@FindBy(xpath = "//*[@class='table-responsive border-light-grey']/table")
	public WebElement PRODUCTTABLEHEADER;
	@FindBy(xpath="//*[@class='table-responsive border-light-grey overflow-hidden']/table")
	public WebElement STRUCTUREHEADER;
	@FindBy(xpath = "//*[@class='table-responsive border-light-grey overflow-hidden']/table/tbody")
	public WebElement PRODUCTTABLEBODY;
	@FindBy(xpath="//*[@class='table-responsive border-light-grey overflow-hidden']/table/tbody")
	public WebElement STRUCTUREBODY;
	// General Tab
	@FindBy(xpath = "//*[@routerlink='general']")
	public WebElement GENERALTAB;
	@FindBy(xpath = "//input[@name='name']/preceding-sibling::label")
	public WebElement INPUTCOMPANYNAME;
	@FindBy(xpath = "//input[@name='ceo']/preceding-sibling::label")
	public WebElement INPUTCEONAME;
	@FindBy(xpath = "//*[@name='foundedYear']/preceding-sibling::label")
	public WebElement SELECTFOUNDEDYEAR;
	@FindBy(xpath = "//input[@name='headquartersAddress']/preceding-sibling::label")
	public WebElement INPUTHQADDRESS;
	@FindBy(xpath = "//input[@name='city']/preceding-sibling::label")
	public WebElement INPUTCITY;
	@FindBy(xpath = "//*[@name='country']/preceding-sibling::label")
	public WebElement SELECTCOUNTRY;
	@FindBy(xpath = "//*[@name='industryCategories']/preceding-sibling::label")
	public WebElement SELECTINDUSTRYTYPE;
	@FindBy(xpath = "//*[@name='industryCategories']")
	public WebElement DDSELECTINDUSTRYTYPE;
	@FindBy(xpath = "//input[@name='website']/preceding-sibling::label")
	public WebElement INPUTWEBSITE;
	@FindBy(xpath = "//div[@class='switch-field']")
	public WebElement ISPUBLICCOMPANY;
	// Platform Settings Tab
	@FindBy(xpath = "//*[@routerlink='platform-settings']")
	public WebElement PLATFORMSETTINGSTAB;
	@FindBy(xpath = "//*[@name='defaultLanguage']/preceding-sibling::label")
	public WebElement SELECTDEFAULTLANGUAGE;
	@FindBy(xpath = "//*[@name='defaultLanguage']")
	public WebElement DDSELECTDEFAULTLANGUAGE;
	@FindBy(xpath = "//*[@name='defaultCurrency']/preceding-sibling::label")
	public WebElement SELECTDEFAULTCURRENCY;
	@FindBy(xpath = "//*[@name='defaultCurrency']")
	public WebElement DDSELECTDEFAULTCURRENCY;
	@FindBy(xpath = "//input[@name='maxInactivityInterval']")
	public WebElement INPUTINACTIVITYINTERVAL;
	// Products Tab
	@FindBy(xpath = "//*[@routerlink='product-data']")
	public WebElement PRODUCTSTAB;
	@FindBy(xpath = "//*[@name='currentProduct']")
	public WebElement SELECTCURRENTPRODUCT;
	@FindBy(xpath = "//div[@class='race-select-body ps ps--active-y']/ul")
	public WebElement SELECTCURRENTPRODUCTDD;
	@FindBy(xpath = "//input[@name='textItem']")
	public WebElement INPUTSEARCH;
	@FindBy(xpath = "//input[@name='textItem']/following::button[1]")
	public WebElement SEARCHBUTTON;
	@FindBy(xpath="//div[@class='form-group no-padding no-margin']/input")
	public WebElement INPUTSEARCHREVIEWTAB;
	@FindBy(xpath = "//input[@name='textItem']/following::button[2]")
	public WebElement ADDNEWBUTTON;
	@FindBy(xpath = "//button/img[@alt='Edit']")
	public WebElement TABLEACTIONEDITBUTTON;
	@FindBy(xpath = "//button/img[@alt='Delete']")
	public WebElement TABLEACTIONDELETEBUTTON;
	// Add New page
	@FindBy(xpath = "//*[@class='modal-header']/span")
	public WebElement HEADINGADDSCREEN;
	@FindBy(xpath = "//label[@class='upload-link']")
	public WebElement UPLOADFILELINK;
	
	@FindBy(xpath = "//*[@name='tier1Id']/div/input")
	public WebElement INPUTDEPARTMENTID;
	@FindBy(xpath="//*[@name='tier1Name']/div/input")
	public WebElement INPUTDEPARTMENTNAME;
	@FindBy(xpath = "//*[@name='tier2Id']/div/input")
	public WebElement INPUTFAMILYID;
	@FindBy(xpath="//*[@name='tier2Name']/div/input")
	public WebElement INPUTFAMILYNAME;
	@FindBy(xpath = "//*[@name='tier3Id']/div/input")
	public WebElement INPUTCATEGORYID;
	@FindBy(xpath="//*[@name='tier3Name']/div/input")
	public WebElement INPUTCATEGORYNAME;
	@FindBy(xpath = "//*[@name='tier4Id']/div/input")
	public WebElement INPUTSUBCATEGORYID;
	@FindBy(xpath="//*[@name='tier4Name']/div/input")
	public WebElement INPUTSUBCATEGORYNAME;
	@FindBy(xpath = "//*[@name='tier5Id']/div/input")
	public WebElement INPUTSEGMENTID;
	@FindBy(xpath="//*[@name='tier5Name']/div/input")
	public WebElement INPUTSEGMENTNAME;
	@FindBy(xpath="//*[@class='margin-top-xs ng-star-inserted']")
	public WebElement IMAGEBLUETICK;
	@FindBy(xpath="//*[@class='pointer ng-star-inserted']/img")
	public WebElement IMAGEPLUS;
	
	
	@FindBy(xpath = "//input[@class='form-control ng-pristine ng-valid ng-touched']")
	public WebElement INPUTBASEVERSION;
	@FindBy(xpath = "//input[@class='form-control ng-untouched ng-pristine ng-invalid']")
	public WebElement INPUTNAME;
	@FindBy(xpath = "//*[@class='modal-footer border-top-lg']/button[2]")
	public WebElement DELETEBUTTON;
	@FindBy(xpath = "//h4")
	public WebElement HEADINGDELETEPOP;
	@FindBy(xpath = "//h4/following-sibling::p")
	public WebElement DELETEWARNING;
	@FindBy(xpath="//*[@name='categories']")
	public WebElement DDCATEGORY;
	// Supplier Page
	@FindBy(xpath = "//*[@routerlink='supplier-data']")
	public WebElement SUPPLIERTAB;
	@FindBy(xpath = "//div[@class='table-responsive border-light-grey overflow-hidden']/table")
	public WebElement SUPPLIERTABLEHEADER;
	@FindBy(xpath = "//div[@class='table-responsive border-light-grey overflow-hidden']/table/tbody")
	public WebElement SUPPLIERTABLEBODY;
	@FindBy(xpath = "//div[contains(@class,'race-popup-container')]/div/ul")
	public WebElement SELECTSUPPPRODUCT;
	// Forms Tab
	@FindBy(xpath = "//div[@class='table-responsive overflow-hidden']/table")
	public WebElement FORMTABLEHEADER;
	@FindBy(xpath = "//div[@class='table-responsive overflow-hidden']/table/tbody")
	public WebElement FORMTABLEBODY;
	@FindBy(xpath = "//*[@routerlink='form-builder']")
	public WebElement FORMSTAB;
	@FindBy(xpath="(//h3[@class='section-heading'])[1]")
	public WebElement DDFORMLIBRARY;
	@FindBy(xpath = "//button/img[@alt='Copy']")
	public WebElement TABLEACTIONCOPY;
	@FindBy(xpath = "//div[@class='collapsing-block disabled']/div/h3")
	public WebElement FORMCREATEVIEW;
	@FindBy(xpath = "//*[@name='types']")
	public WebElement SELECTTYPE;
	@FindBy(xpath = "//div[contains(@class,'race-select-body ps-container ps-theme-default ps-active-y')]/ul")
	public WebElement SELECTTYPEDD;
	// Review Tab
	@FindBy(xpath = "//*[@routerlink='review-process']")
	public WebElement REVIEWTAB;
	@FindBy(xpath = "//h3[@class='section-heading']")
	public WebElement HEADINGREVIEWTAB;
	@FindBy(xpath = "//span[@class='margin-right-sm relative']")
	public WebElement SHOWARCHIVE;
	@FindBy(xpath = "//span[@class='margin-right-sm relative']/following-sibling::race-checkbox")
	public WebElement CHECKBOX;
	@FindBy(xpath = "//*[@class='table-responsive no-border']/table")
	public WebElement REVIEWTABLEHEADER;
	@FindBy(xpath = "//*[@class='table-responsive no-border']/table/tbody")
	public WebElement REVIEWTABLEBODY;
	@FindBy(xpath = "//*[@name='form']")
	public WebElement DELIVERABLETYPE;
	// Add new review
	@FindBy(xpath="//*[@placeholder='Enter Stage Name']")
	public WebElement INPUTSTAGENAME;
	@FindBy(xpath="//div[@class='race-select-container form-select']/button")
	public WebElement DDAPPROVALGROUP;
	@FindBy(xpath="//div[@class='text-right padding-sm']/button")
	public WebElement STAGESAVEBUTTON;
	@FindBy(xpath="//*[@name='depthFilter']/div/button")
	public WebElement STRUCTUREDD;
	@FindBy(xpath="//input[@name='website']/preceding-sibling::label")
	public WebElement INPUTLABELWEBSITE;
	@FindBy(xpath="//div[contains(@class,'public-company')]/label")
	public WebElement LABELPUBLICCOMPANY;
	@FindBy(xpath="//div[contains(@class,'public-company')]/following::div/label")
	public WebElement LABELSTOCKSYMBOL;
	
	
	
	public AdministrativeLocators(){
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
}
