package com.race.qa.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.race.qa.base.DriverFactory;

public class ProjectTemplateLocators{
	
	@FindBy(xpath="((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[1]")
	public WebElement TEMPLATEEDITACTION;
	@FindBy(xpath="((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[2]")
	public WebElement TEMPLATEARCHIVEACTION;
	@FindBy(xpath="((//table[@class='table table-striped']/tbody/tr/td[6])[1]/div/button)[3]")
	public WebElement TEMPLATEDELETEACTION;
	@FindBy(xpath="//div[@class='panel-heading transparent mobile-view-tab']//ul/li[2]/a")
	public WebElement LINKNEWTEMPLATE;
	@FindBy(xpath="//div[@class='panel-heading transparent mobile-view-tab']//ul/li[1]/a")
	public WebElement LINKEXISTINGTEMPLATE;
	@FindBy(xpath="//input[@name='name']/preceding-sibling::label")
	public WebElement LABELINPUTTEMPLATENAME;
	@FindBy(xpath="//input[@name='name']")
	public WebElement INPUTTEMPLATENAME;
	@FindBy(xpath="//*[@name='categories']")
	public WebElement DDCATEGORIES;
	@FindBy(xpath="//div[@class='race-select-body ps']/ul/li[1]")
	public WebElement DDSELECTTEMPLATECATEGORYOPTION;
	@FindBy(xpath="//*[@name='type']")
	public WebElement DDTEMPLATETYPE;
	@FindBy(xpath="//div[@class='race-select-body ps']/ul/li[1]")
	public WebElement DDSELECTTEPLATETYPEOPTION;
	@FindBy(xpath="//button[@class='btn btn-input']")
	public WebElement TEMPLATECANCELBUTTON;
	@FindBy(xpath="//button[@class='btn btn-primary-blue']")
	public WebElement TEMPLATESAVEASDRAFTBUTTON;
	@FindBy(xpath="//button[@class='btn btn-primary-green']")
	public WebElement TEMPLATESAVEBUTTON;
	
	//from UI
	@FindBy(xpath = "//button/img[@alt='Edit']")
	public WebElement TABLEACTIONEDITBUTTON;
	@FindBy(xpath = "//button/img[@alt='Delete']")
	public WebElement TABLEACTIONDELETEBUTTON;
	@FindBy(xpath="//div[@class='race-select-body ps-container ps-theme-default']/ul/li")
	public WebElement DDCATEGORYDATA;
	@FindBy(xpath="//input[@placeholder='Filter']")
	public WebElement DDFILTERINPUT;
	@FindBy(xpath = "//*[@class='text-2e3d51 font-weight-400 no-margin inline-block']")
	public WebElement HEADING;
	@FindBy(xpath = "//*[@class='race-select-body ps-container ps-theme-default ps-active-y']/ul")
	public WebElement DROPDOWNDATA;
	@FindBy(xpath="//div[@class='race-select-body ps ps--active-y']/ul")
	public WebElement CATEGORYDD;
	
	@FindBy(xpath = "//a[@routerlink='existing']")
	public WebElement EXISTINGTEMPLATETAB;
	@FindBy(xpath = "//a[@routerlink='new']")
	public WebElement NEWTEMPLATETAB;
	@FindBy(xpath = "//*[@class='font-size-18 font-weight-400 text-2e3d51']")
	public WebElement HEADINGEXISTINGTEMPLATE;
	@FindBy(xpath = "//div[contains(@class,'chart')]")
	public WebElement BARCHART;
	// New Template
	
	@FindBy(xpath = "//*[@name='categories']")
	public WebElement SELECTCATEGORIES;
	@FindBy(xpath = "//*[@name='type']")
	public WebElement SELECTTEMPLATETYPE;
	@FindBy(xpath = "//textarea[@name='description']")
	public WebElement DESCRIPTION;
	@FindBy(xpath = "//h3[@class='font-size-16 font-weight-400 text-2e3d51 no-margin']")
	public WebElement HEADINGPHASES;
	@FindBy(xpath = "//div[contains(@class,'modal-footer')]/button")
	public WebElement PHASESAVEBUTTON;
	@FindBy(xpath = "//h3/following-sibling::button")
	public WebElement ADDNEWPHASEBUTTON;
	@FindBy(xpath = "//div[@class='modal-header']/span")
	public WebElement HEADINGPOP;
	@FindBy(xpath = "//span[@class='selected-color']")
	public WebElement CHECKBOXPHASECOLOR;
	@FindBy(xpath="//span[@class='selected-color']/following-sibling::span")
	public WebElement LABELSELECTCOLOR;
	@FindBy(xpath="//div[@class='modal-body no-padding-bottom']//input[@name='name']/preceding-sibling::label")
	public WebElement LABELINPUTPHASENAME;
	@FindBy(xpath="//div[@class='modal-body no-padding-bottom']//*[@name='description']/preceding-sibling::label")
	public WebElement LABELINPUTDESCRIPTION;
	@FindBy(xpath="//div[@class='header padding-sm']/h4")
	public WebElement ADDEDPHASEHEADING;
	@FindBy(xpath="//img[@alt='Edit']")
	public WebElement EDITBUTTON;
	@FindBy(xpath="//img[@alt='Delete']")
	public WebElement DELETEBUTTON;
	//Add Task
	@FindBy(xpath="//div[contains(@class,'accordion-panel')]/div/button")
	public WebElement ADDTASKBUTTON;
	@FindBy(xpath="//span[@class='selected-color']/following-sibling::span")
	public WebElement SELECTCOLOR;
	@FindBy(xpath="//*[@name='approvalGroups']")
	public WebElement DDAPROVALGROUPS;
	@FindBy(xpath="//*[@name='informedGroups']")
	public WebElement DDINFORMGROUPS;
	@FindBy(xpath="//*[@name='preDependencies']")
	public WebElement DDADDDEPENDENCY;
	//to be added the deliverable type option dd
	@FindBy(xpath="//input[@placeholder='Deliverable Name']")
	public WebElement INPUTDELVERABLENAME;
	@FindBy(xpath="//*[@class='close margin-top-xs']/span")
	public WebElement DELIVERABLECLOSEBUTTON;
	@FindBy(xpath="//div[@class='form-group no-padding-right no-padding-left']/input")
	public WebElement INPUTSEARCH;
	@FindBy(xpath="//div[@class='table-responsive border-light-grey ng-trigger ng-trigger-fade']/table/tbody")
	public WebElement TABLEBODY;
	@FindBy(xpath="//div[@class='table-responsive border-light-grey ng-trigger ng-trigger-fade']/table")
	public WebElement TABLEHEADER;
	@FindBy(xpath="//div[@class='form-container task-modal']/div/div/div//input[@name='name']")
	public WebElement INPUTNAME;
	@FindBy(xpath="//div[@class='form-container task-modal']/div/div/div//*[@name='description']")
	public WebElement INPUTDESCRIPTION;
	@FindBy(xpath="//div[@class='modal-footer no-padding-top']/button")
	public WebElement BUTTONCREATETASK;
	@FindBy(xpath="//input[@name='duration']")
	public WebElement INPUTDURATION;
	@FindBy(xpath="//h4[@class='text-2e3d51 font-weight-400 no-margin inline-block']")
	public WebElement HEADINGTOPTEMPLATES;
	@FindBy(xpath="(//button[@class='btn toggle-select form-control btn-primary'])[1]")
	public WebElement DDNOOFCATEGORIES;
	
	
	
	public ProjectTemplateLocators(){
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
	
}
