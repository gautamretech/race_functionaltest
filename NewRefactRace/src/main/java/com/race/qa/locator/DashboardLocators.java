package com.race.qa.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import com.race.qa.base.DriverFactory;


public class DashboardLocators {
	//MyDashboard
	@FindBy(xpath="(//ul[@class='race-tabs']/li/a)[1]")
	public WebElement MYDASHBOARD;

	// StartNewProject
	@FindBy(xpath="//*[@class='font-size-16 text-2e3d51 font-weight-400 no-margin']")
	public WebElement DB_PROJECTHEADING;
	@FindBy(xpath = "(//button[contains(@class,'btn btn-primary-green')])[2]")
	private WebElement DB_STARTPROJECTBUTTON;

	public WebElement getDB_STARTPROJECTBUTTON() {
		return DB_STARTPROJECTBUTTON;
	}

	@FindBy(xpath = "//*[@name='category']")
	private WebElement DB_DDCATEGORY;

	public WebElement getDB_DDCATEGORY() {
		return DB_DDCATEGORY;
	}
	@FindBy(xpath = "//*[@name='suppliers']")
	private WebElement DB_DDSUPPLIER;

	public WebElement getDB_DDSUPPLIER() {
		return DB_DDSUPPLIER;
	}

	@FindBy(xpath = "//*[@name='template']")
	private WebElement DB_DDTEMPLATE;

	public WebElement getDB_DDTEMPLATE() {
		return DB_DDTEMPLATE;
	}

	@FindBy(xpath = "//*[@name='name']")
	private WebElement DB_INPUTPROJECTNAME;

	public WebElement getDB_INPUTPROJECTNAME() {
		return DB_INPUTPROJECTNAME;
	}

	@FindBy(xpath = "//*[@name='manager']")
	private WebElement DB_DDTEAMMANEGER;

	public WebElement getDB_DDTEAMMANEGER() {
		return DB_DDTEAMMANEGER;
	}

	@FindBy(xpath = "//div[@class='row']/div/button")
	private WebElement DB_BUTTONNEXT;

	public WebElement getDB_BUTTONNEXT() {
		return DB_BUTTONNEXT;
	}

	@FindBy(xpath = "//ul[@class='race-tabs']/li/a")
	private WebElement TEXTNEWPROJECT;

	public WebElement getTEXTNEWPROJECT() {
		return TEXTNEWPROJECT;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[1]")
	private WebElement LABELCATEGORY;

	public WebElement getLABELCATEGORY() {
		return LABELCATEGORY;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[2]")
	private WebElement LABELTEMPLATE;

	public WebElement getLABELTEMPLATE() {
		return LABELTEMPLATE;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[3]")
	private WebElement LABELPROJECTNAME;

	public WebElement getLABELPROJECTNAME() {
		return LABELPROJECTNAME;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[4]")
	private WebElement LABELPROJECTMANAGER;

	public WebElement getLABELPROJECTMANAGER() {
		return LABELPROJECTMANAGER;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[5]")
	private WebElement LABELSUPPLIERS;

	public WebElement getLABELSUPPLIERS() {
		return LABELSUPPLIERS;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[6]")
	private WebElement LABELSTARTDATE;

	public WebElement getLABELSTARTDATE() {
		return LABELSTARTDATE;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[7]")
	private WebElement LABELENDDATE;

	public WebElement getLABELENDDATE() {
		return LABELENDDATE;
	}

	@FindBy(xpath = "//*[@name='startDate']")
	public WebElement DB_STARTDATE;
	@FindBy(xpath = "//*[@name='launchDate']")
	public WebElement DB_ENDDATE;
	@FindBy(xpath = "//*[@name='category']/div/button/div/div/img")
	WebElement IMGDDCATEGORY;
	@FindBy(xpath = "//*[@name='category']/div/button/div/div[2]")
	public WebElement TEXTSELECTCATEGORY;
	@FindBy(xpath = "//*[@name='template']/div/button/div/div/img")
	WebElement IMGDDTEMPLATE;
	@FindBy(xpath = "//*[@name='template']/div/button/div/div[2]")
	public WebElement TEXTSELECTTEMPLATE;
	@FindBy(xpath = "//*[@name='manager']/div/button/div/div/img")
	WebElement IMGDDMANAGER;
	@FindBy(xpath = "//*[@name='manager']/div/button/div/div[2]")
	public WebElement TEXTSELECTMANAGER;
	@FindBy(xpath = "//*[@name='suppliers']/div/button/div/div/img")
	WebElement IMGDDSUPPLIERS;
	@FindBy(xpath = "//*[@name='suppliers']/div/button/div/div[2]")
	public WebElement TEXTSELECTSUPPLIERS;
	@FindBy(xpath = "//*[@name='suppliers']")
	public WebElement SELECTSUPPLIER;
	@FindBy(xpath = "//table[@class='table table-striped bordered']//tr/td[3]/div/div/a")
	public WebElement PROJECTLINKINTABLE;
	@FindBy(xpath = "//span[@class='status draft']")
	public WebElement PROJECTTASKSTATUS;
	@FindBy(xpath = "//span[@class='task-title']")
	public WebElement TASKNAME;
	@FindBy(xpath = "//div[@class='panel-body bg-white margin-bottom-sm status-column']")
	public WebElement TASKLISTSECTION;
	@FindBy(xpath = "//*[@class='btn toggle-select btn-input assign-collaboration']")
	public WebElement DDASSIGNTASK;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[1]/div/h2")
	public WebElement BOXOPENTASKCOUNT;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[1]")
	public WebElement BOXOPENTASKS;
	@FindBy(xpath = "//div[contains(@class,'recent-submission')]/h2[contains(@class,'no-margin font-size-18 font-weight-400 text-2e3d51 ng-')]")
	public WebElement TASKSCOUNT;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[2]")
	public WebElement BOXENDINGTASKS;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[2]/div/h2")
	public WebElement BOXENDINGTASKCOUNT;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[3]/div/h2")
	public WebElement BOXTASKDELAYEDCOUNT;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[3]")
	public WebElement BOXTASKSDELAYED;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[4]")
	public WebElement BOXPENDINGSPECS;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[5]")
	public WebElement BOXNEWSUBMISSIONS;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[5]/div/h2")
	public WebElement BOXNEWSUBMISSIONCOUNT;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[1]/div/h2")
	public WebElement BOXOPENDELIVERABLE;
	@FindBy(xpath = "(//*[@class='row no-margin task-row']/div)[4]/div/h2")
	public WebElement BOXNEWPENDINGSPECCOUNT;
	@FindBy(xpath = "//div[@class='panel-heading']/span")
	public WebElement TD_BUTTON;
	@FindBy(xpath="//div[@class='change-status up race-dropdown']/button")
	public WebElement CHANGESTATUSBUTTON;
	@FindBy(xpath="(//div[@class='change-status up race-dropdown open']/ul/li/a)[1]")
	public WebElement OPTIONSTARTTASK;
	@FindBy(xpath="//div[@class='change-status up race-dropdown open']/ul/li/a")
	public WebElement OPTIONCOMPLETETASK;
	@FindBy(xpath="//div[@class='media-body media-middle']/following-sibling::div/button")
	public WebElement EDITTASKBUTTON;
	@FindBy(xpath="//input[@name='canAutocomplete'][1]/following-sibling::label[1]")
	public WebElement AUTOCOMPLETEYES;
	@FindBy(xpath="//*[@class='btn toggle-select form-control btn-primary']/div/div[2]")
	public WebElement EDITTASKPHASEDD;
	@FindBy(xpath="//*[@name='informedGroups']")
	public WebElement DB_DDINFORMEDGROUP;
	@FindBy(xpath="//button[contains(@class,'btn btn-primary-green flex flex-space-between')]")
	public WebElement DB_ADDDELIVERABLE;
	@FindBy(xpath="(//*[contains(@class,'ng-untouched ng-pristine ng-invalid')])[2]")
	public WebElement DB_DDDELIVERABLETYPE;
	@FindBy(xpath="//input[@class='form-control ng-untouched ng-pristine ng-invalid']")
	public WebElement DB_INPUTDELIVERABLENAME;
	@FindBy(xpath="(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])[1]")
	public WebElement DB_DDDELIVERABLELOOP;
	@FindBy(xpath="(//div[@class='task-deliverable']/div[@class='row']/div/div[@class='form-group']/race-select[contains(@class,'ng-untouched ng-pristine')])[2]")
	public WebElement DB_DDDELIVERABLEGROUP;
	@FindBy(xpath="(//*[contains(@class,'btn btn-primary-green flex flex-space-between flex-align-center full-width-btn input-btn-height')])[3]")
	public WebElement DB_ADDDESIGN;
	@FindBy(xpath="(//*[contains(@class,'btn btn-primary-green flex flex-space-between flex-align-center full-width-btn input-btn-height')])[2]")
	public WebElement DB_ADDDESIGN_SUPPLIER;
	@FindBy(xpath="//div[@class='task-deliverable']//input")
	public WebElement DB_INPUTDESIGNNAME;
	@FindBy(xpath="(//div[@class='task-deliverable']//*[contains(@class,'ng-untouched ng-pristine ng-valid')])[1]")
	public WebElement DB_DDDESIGNLOOP;
	@FindBy(xpath="(//div[@class='task-deliverable']//*[contains(@class,'ng-untouched ng-pristine ng-valid')])[2]")
	public WebElement DB_DDDESIGNGROUP;
	@FindBy(xpath="//div[contains(@class,'modal-footer')]/button")
	public WebElement DB_DESIGNSAVEBUTTON;
	//Loop assignment for deliverable
	@FindBy(xpath="//*[contains(@class,'ng-untouched ng-pristine ng-valid ng-star-inserted')]")
	public WebElement DB_ASSIGNDROPDOWNDELIVERABLELOOP;
	@FindBy(xpath="//div[@class='form-group no-margin-bottom margin-top-sm']/label")
	public WebElement DB_ASSIGNLABELDELIVERABLE;
	//Notifications related
	@FindBy(xpath="//button[@class='btn notification-icon race-dropdown-toggle']")
	public WebElement DB_NOTIFICATIONBUTTON;
	@FindBy(xpath="//button[@class='btn notification-icon race-dropdown-toggle']/span")
	public WebElement DB_NOTIFICATIONCOUNT;
	@FindBy(xpath="//table[@class='table notifications']/tbody/tr[1]/td[2]/span/a")
	public WebElement DB_READNOTIFICATION_1;
	@FindBy(xpath="//table[@class='table notifications']/tbody/tr[1]/td[2]/span/span")
	public WebElement DB_READNOTIFICATION_2;
	@FindBy(xpath = "//div[@class='bar-graph-wrapper ng-trigger ng-trigger-fade']")
	public WebElement BARGRAPH;
	@FindBy(xpath = "//a[@routerlink='suppliers']")
	public WebElement SUPPLIERSTAB;
	@FindBy(xpath = "//div[@class='graph no-right-border no-margin-top']")
	public WebElement SUPPLIERSTABBARGRAPH;
	@FindBy(xpath = "//*[@class='font-size-20 font-weight-400 no-margin padding-sm border-bottom-lg relative']")
	public WebElement RECOMENDEDPRODUCTSHEADING;
	@FindBy(xpath = "//*[@class='row no-margin padding-top-sm product-list']")
	public WebElement RECOMENDEDPRODUCTSECTION;
	@FindBy(xpath = "//h3[@class='font-size-20 font-weight-400 no-margin padding-sm border-bottom-lg']")
	public WebElement BESTPARTNERSECTIONHEADING;
	@FindBy(xpath = "//h3[@class='font-size-16 font-weight-400 text-2e3d51 no-margin']")
	public WebElement SUPPLIERSBYCAT;
	@FindBy(xpath = "//div[@class='panel-body no-padding-top no-padding-right no-padding-left']/h3")
	public WebElement SUPPLIERSINVITED;
	@FindBy(xpath = "//div[@class='race-select-container projects']//div/div[2]")
	public WebElement PROJECTNAME_DETAILSPAGE;
	@FindBy(xpath = "//span[contains(@class,'toolbar-header project-space')]")
	public WebElement PROJECTSPACE_DETAILSPAGE;
	@FindBy(xpath = "//div[@class='panel-heading']/span")
	public WebElement TD_HEADING;
	@FindBy(xpath = "//ul[contains(@class,'unstyled no-padding margin-top-sm border-top tab-left ps')]/li")
	public WebElement TASKSECTION;
	@FindBy(xpath = "//*[@alt='Bar Code']/following-sibling::span[1]")
	public WebElement SELECTEDPRODUCTTEXT;
	@FindBy(xpath="//span[@class='user-name hidden-sm hidden-xs']")
	public WebElement LOGGEDUSER;
	
	//new ones
	@FindBy(xpath="//button[@class='btn btn-block btn-primary-green']")
	public WebElement INFORMEDGROUPDONEBUTOON;
	@FindBy(xpath="//span[contains(@class,'group ')]/preceding::h5[1]")
	public WebElement INFORMEDGROUPHEADINGPROJECTSPACE;
	@FindBy(xpath="//span[contains(@class,'group ')]")
	public WebElement INFORMEDGROUPVALUEPROJECTSPACE;
	
	@FindBy(xpath="//div[@class='modal-footer']/button")
	public WebElement TASKSAVEBUTTON_EDITTASK;
	@FindBy(xpath = "(//div[contains(@class,'race-select-top')]/following-sibling::div/ul/li/a)")
	public WebElement INFORMEDCONTAINEROPTION;
	@FindBy(xpath = "//*[@class='btn toggle-select btn-input assign-collaboration']/div/div[2]")
	public WebElement ASSIGNEDTEXT;
	@FindBy(xpath = "//span[@class='crop']")
	public WebElement ASSIGMENTOPTION;
	@FindBy(xpath="//div[contains(@class,'race-select-top')]/following-sibling::div/ul/li/a/race-checkbox")
	public WebElement INFORMEDDOPTION;
	
	
	
	
	
	
	
	@FindBy(xpath="//*[@name='loop']")
	public WebElement SUPPLIERDESIGNLOOPDD;
	@FindBy(xpath="//input[@name='name']")
	public WebElement SUPPLIERDESIGNNAMEINPUT;
	@FindBy(xpath="(//div[@class='form-group no-margin']//*[contains(@class,'ng-untouched ng-pristine ng-valid')])[2]")
	public WebElement SUPPLIERDESIGNGROUPDD;
	
	
	
	
	public DashboardLocators() {
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
}
