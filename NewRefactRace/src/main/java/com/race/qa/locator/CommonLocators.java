package com.race.qa.locator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import com.race.qa.base.DriverFactory;


public class CommonLocators{
	
	@FindBy(xpath="//div[@class='main-nav-items']/ul/li[2]")
	public WebElement LANDINGPAGEMENUATTRIBUTE;
	@FindBy(xpath = "//div[contains(@class,'race-popup-container')]/div/input")
	private WebElement DDSEARCHINPUT;
	public WebElement getDDSEARCHINPUT() {
		return DDSEARCHINPUT;
	}
	@FindBy(xpath = "//span[@class='navigation ng-star-inserted']/ul")
	private WebElement PAGES;
	public WebElement getPAGES() {
		return PAGES;
	}
	@FindBy(xpath = "//*[@class='avatar']/following-sibling::span")
	private WebElement DISPLAYNAME;
	public WebElement getDISPLAYNAME() {
		return DISPLAYNAME;
	}
	// Login Page Objects
	@FindBy(xpath = "//img[@alt='Retrieve Password']")
	private WebElement IMAGERETRIVEPASSWORD;
	public WebElement getIMAGERETRIVEPASSWORD() {
		return IMAGERETRIVEPASSWORD;
	}
	@FindBy(xpath = "//div[@class='login-field btn-options']/div/a")
	private WebElement LINKFORGOTPASSWORD;
	public WebElement getLINKFORGOTPASSWORD() {
		return LINKFORGOTPASSWORD;
	}
	@FindBy(xpath = "//input[@name='email']")
	private WebElement INPUTUSERNAME;
	public WebElement getINPUTUSERNAME() {
		return INPUTUSERNAME;
	}
	@FindBy(xpath = "//div[@class='login-field btn-options']/div/button")
	private WebElement BUTTONRETRIVEPASSWORD;
	public WebElement getBUTTONRETRIVEPASSWORD() {
		return BUTTONRETRIVEPASSWORD;
	}
	@FindBy(xpath = "//div[@class='login-field btn-options']/div/a")
	private WebElement LINKBACK2LOGIN;
	public WebElement getLINKBACK2LOGIN() {
		return LINKBACK2LOGIN;
	}
	@FindBy(xpath = "//*[@alt='Sign In']")
	private WebElement SIGNIN_IMAGE;
	public WebElement getSIGNIN_IMAGE() {
		return SIGNIN_IMAGE;
	}
	@FindBy(xpath = "//img[@class='responsive-img']")
	private WebElement IMAGERESPONSIVE;
	public WebElement getIMAGERESPONSIVE() {
		return IMAGERESPONSIVE;
	}
	@FindBy(xpath = "//*[@alt='reTech Labs']")
	private WebElement RETECHLOGO;
	public WebElement getRETECHLOGO() {
		return RETECHLOGO;
	}
	@FindBy(xpath = "//input[@name='password']")
	private WebElement INPUTPASSWORD;
	public WebElement getINPUTPASSWORD() {
		return INPUTPASSWORD;
	}
	@FindBy(xpath = "//button[@type='submit']")
	private WebElement SIGNINBUTTON;
	public WebElement getSIGNINBUTTON() {
		return SIGNINBUTTON;
	}
	@FindBy(xpath = "//div[@class='reg-language']/h4")
	private WebElement TEXTLANGUAGESELECT;
	public WebElement getTEXTLANGUAGESELECT() {
		return TEXTLANGUAGESELECT;
	}
	@FindBy(xpath = "//div[@class='race-select-container left']")
	private WebElement SELECTLANGUAGE;
	public WebElement getSELECTLANGUAGE() {
		return SELECTLANGUAGE;
	}
	@FindBy(xpath = "//div[@class='icon logout']/following-sibling::span")
	private WebElement LOGOUTBUTTON;
	public WebElement getLOGOUTBUTTON() {
		return LOGOUTBUTTON;
	}
	@FindBy(xpath = "//a[@class='btn btn-primary-blue btn-proceed']")
	private WebElement RELOGINBUTTON;
	public WebElement getRELOGINBUTTON() {
		return RELOGINBUTTON;
	}
	@FindBy(xpath = "//div[@class='race-select-container left']/button/following-sibling::div/div/ul/li[2]/a")
	private WebElement SELECTCHINESE;
	public WebElement getSELECTCHINESE() {
		return SELECTCHINESE;
	}
	@FindBy(xpath = "//div[@class='select-info-body media-middle']")
	private WebElement SELECTEDLANGUAGE;
	public WebElement getSELECTEDLANGUAGE() {
		return SELECTEDLANGUAGE;
	}
	@FindBy(xpath = "//div[@class='race-select-container left']/button/following-sibling::div/div/ul/li[3]/a")
	private WebElement SELECTJAPANESE;
	public WebElement getSELECTJAPANESE() {
		return SELECTJAPANESE;
	}
	@FindBy(xpath = "//div[@class='race-select-container left']/button/following-sibling::div/div/ul/li[4]/a")
	private WebElement SELECTKOREAN;
	public WebElement getSELECTKOREAN() {
		return SELECTKOREAN;
	}
	// Common All Pages
	@FindBy(xpath = "//button[@class='btn btn-primary-green']")
	private WebElement APPLYFILTERBUTTON;
	public WebElement getAPPLYFILTERBUTTON() {
		return APPLYFILTERBUTTON;
	}
	@FindBy(xpath = "//button[@class='btn btn-input']")
	private WebElement CANCELFILTERBUTTON;
	public WebElement getCANCELFILTERBUTTON() {
		return CANCELFILTERBUTTON;
	}
	@FindBy(xpath = "//button[@class='btn btn-blank btn-danger']")
	private WebElement CLEARFILTERBUTTON;
	public WebElement getCLEARFILTERBUTTON() {
		return CLEARFILTERBUTTON;
	}
	@FindBy(xpath = "//button/img[@alt='Previous']")
	private WebElement PAGEPREVIOUSBUTTON;
	public WebElement getPAGEPREVIOUSBUTTON() {
		return PAGEPREVIOUSBUTTON;
	}
	@FindBy(xpath = "//button[@class='btn btn-input next']")
	private WebElement PAGENEXTBUTTON;
	public WebElement getPAGENEXTBUTTON() {
		return PAGENEXTBUTTON;
	}
	@FindBy(xpath = "//*[@class='mid']")
	private WebElement TOTALRECORDSINFO;
	public WebElement getTOTALRECORDSINFO() {
		return TOTALRECORDSINFO;
	}
	@FindBy(xpath = "//div[@class='button close']/span")
	private WebElement CLOSEBUTTON;
	public WebElement getCLOSEBUTTON() {
		return CLOSEBUTTON;
	}
	@FindBy(xpath = "//div[contains(@class,'modal-footer')]/button")
	private WebElement SAVEBUTTON;
	public WebElement getSAVEBUTTON() {
		return SAVEBUTTON;
	}
	@FindBy(xpath = "//*[@class='modal-header']/span")
	private WebElement POPUPHEADING;
	public WebElement getPOPUPHEADING() {
		return POPUPHEADING;
	}
	// DashBoard Page objects
	@FindBy(xpath = "//*[@alt='Account Settings']")
	private WebElement ACCOUNTSETTINGSLINK;
	public WebElement getACCOUNTSETTINGSLINK() {
		return ACCOUNTSETTINGSLINK;
	}
	@FindBy(xpath = "//*[@alt='Settings Menu']")
	private WebElement SETTINGSMENU;
	public WebElement getSETTINGSMENU() {
		return SETTINGSMENU;
	}
	@FindBy(xpath = "//*[contains(@class,'section-name')]/h2")
	private WebElement HEADINGDASHBOARDPAGE;
	public WebElement getHEADINGDASHBOARDPAGE() {
		return HEADINGDASHBOARDPAGE;
	}

	// DashBoard
	// Menus
	@FindBy(xpath = "//*[contains(@class,'left-nav-top')]")
	private WebElement TEXTNAVIGATIONMENU;

	public WebElement getTEXTNAVIGATIONMENU() {
		return TEXTNAVIGATIONMENU;
	}

	@FindBy(xpath = "//*[@alt='Dashboard']/following-sibling::span")
	private WebElement MENU_DASHBOARD;

	public WebElement getMENU_DASHBOARD() {
		return MENU_DASHBOARD;
	}

	@FindBy(xpath = "//*[@alt='Products']/following-sibling::span")
	private WebElement MENU_PRODUCTS;

	public WebElement getMENU_PRODUCTS() {
		return MENU_PRODUCTS;
	}

	@FindBy(xpath = "//*[@alt='Suppliers']/following-sibling::span")
	private WebElement MENU_SUPPLIERS;

	public WebElement getMENU_SUPPLIERS() {
		return MENU_SUPPLIERS;
	}

	@FindBy(xpath = "//*[@alt='Team Management']/following-sibling::span")
	private WebElement MENU_TEAMMANGEMENT;

	public WebElement getMENU_TEAMMANGEMENT() {
		return MENU_TEAMMANGEMENT;
	}

	@FindBy(xpath = "//*[@alt='Project Templates']/following-sibling::span")
	private WebElement MENU_PROJECTTEMPLATES;

	public WebElement getMENU_PROJECTTEMPLATES() {
		return MENU_PROJECTTEMPLATES;
	}

	@FindBy(xpath = "//*[@alt='Administration']/following-sibling::span")
	private WebElement MENU_ADMINISTRATION;

	public WebElement getMENU_ADMINISTRATION() {
		return MENU_ADMINISTRATION;
	}

	@FindBy(xpath = "//*[@alt='Audits']/following-sibling::span")
	private WebElement MENU_REPORTS;

	public WebElement getMENU_REPORTS() {
		return MENU_REPORTS;
	}

	@FindBy(xpath = "//*[@class='main-canvas']")
	private WebElement PIECHART;

	public WebElement getPIECHART() {
		return PIECHART;
	}

	@FindBy(xpath = "//div[@class='navbar-brand']/a/img")
	private WebElement IMAGEDASHBOARDRESPONSIVE;

	public WebElement getIMAGEDASHBOARDRESPONSIVE() {
		return IMAGEDASHBOARDRESPONSIVE;
	}

	@FindBy(xpath = "//div[@class='notification-bar']")
	private WebElement AREANOTIFICATIONBAR;

	public WebElement getAREANOTIFICATIONBAR() {
		return AREANOTIFICATIONBAR;
	}

	@FindBy(xpath = "//div[@class='ticker-container ng-trigger ng-trigger-tickerAnimation as-placeholder']")
	private WebElement DISPLAYNOTIFICATIONS;

	public WebElement getDISPLAYNOTIFICATIONS() {
		return DISPLAYNOTIFICATIONS;
	}

	@FindBy(xpath = "//img[@alt='Clock']")
	private WebElement IMAGECLOCK;

	public WebElement getIMAGECLOCK() {
		return IMAGECLOCK;
	}

	@FindBy(xpath = "//div[@class='current-time']/span[2]")
	private WebElement TEXTTIME;

	public WebElement getTEXTTIME() {
		return TEXTTIME;
	}

	@FindBy(xpath = "//div[@class='avatar']/div")
	private WebElement IMAGEAVATAR;

	public WebElement getIMAGEAVATAR() {
		return IMAGEAVATAR;
	}

	@FindBy(xpath = "//span[contains(@class,'user-name hidden-sm hidden-xs')]")
	private WebElement LOGGEDINUSERNAME;

	public WebElement getLOGGEDINUSERNAME() {
		return LOGGEDINUSERNAME;
	}

	@FindBy(xpath = "//button[@class='btn notification-icon race-dropdown-toggle']/img")
	private WebElement IMAGENOTIFICATIONBELL;

	public WebElement getIMAGENOTIFICATIONBELL() {
		return IMAGENOTIFICATIONBELL;
	}

	@FindBy(xpath = "//button[@class='btn notification-icon race-dropdown-toggle']/span")
	private WebElement COUNTNOTIFICATION;

	public WebElement getCOUNTNOTIFICATION() {
		return COUNTNOTIFICATION;
	}

	// Account Settings
	@FindBy(xpath = "//div[@class='modal-header bg-off-white hidden-xs no-padding-bottom no-border']/span")
	private WebElement HEADINGACCSETTINGS;

	public WebElement getHEADINGACCSETTINGS() {
		return HEADINGACCSETTINGS;
	}

	@FindBy(xpath = "(//div[@class='mobile-view-tab']/ul/li/a)[1]")
	private WebElement TAB_AS_PASSWORD;

	public WebElement getTAB_AS_PASSWORD() {
		return TAB_AS_PASSWORD;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)")
	private WebElement AS_LABEL_OLDPASSWORD;

	public WebElement getAS_LABEL_OLDPASSWORD() {
		return AS_LABEL_OLDPASSWORD;
	}

	@FindBy(xpath = "(//div[@class='form-group']/input)[1]")
	private WebElement AS_INPUT_OLDPASSWORD;

	public WebElement getAS_INPUT_OLDPASSWORD() {
		return AS_INPUT_OLDPASSWORD;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[2]")
	private WebElement AS_LABEL_NEWPASSWORD;

	public WebElement getAS_LABEL_NEWPASSWORD() {
		return AS_LABEL_NEWPASSWORD;
	}

	@FindBy(xpath = "(//div[@class='form-group']/input)[2]")
	private WebElement AS_INPUT_NEWPASSWORD;

	public WebElement getAS_INPUT_NEWPASSWORD() {
		return AS_INPUT_NEWPASSWORD;
	}

	@FindBy(xpath = "(//div[@class='form-group']/label)[3]")
	private WebElement AS_LABEL_CNFPASSWORD;

	public WebElement getAS_LABEL_CNFPASSWORD() {
		return AS_LABEL_CNFPASSWORD;
	}

	@FindBy(xpath = "(//div[@class='form-group']/input)[3]")
	private WebElement AS_INPUT_CNFPASSWORD;

	public WebElement getAS_INPUT_CNFPASSWORD() {
		return AS_INPUT_CNFPASSWORD;
	}

	@FindBy(xpath = "//*[@class='ng-star-inserted']/label")
	private WebElement AS_TEXTCANTREMEMBER;

	public WebElement getAS_TEXTCANTREMEMBER() {
		return AS_TEXTCANTREMEMBER;
	}

	@FindBy(xpath = "//*[@class='ng-star-inserted']/ button")
	private WebElement AS_BUTTONRESETPASSWORD;

	public WebElement getAS_BUTTONRESETPASSWORD() {
		return AS_BUTTONRESETPASSWORD;
	}

	// LanguageTAB
	@FindBy(xpath = "(//div[@class='mobile-view-tab']/ul/li)[2]")
	private WebElement TAB_LANGUAGE;

	public WebElement getTAB_LANGUAGE() {
		return TAB_LANGUAGE;
	}

	@FindBy(xpath = "//div[@class='form-group']/label")
	private WebElement LABELLANGUAGE;

	public WebElement getLABELLANGUAGE() {
		return LABELLANGUAGE;
	}

	@FindBy(xpath = "//*[@name='language']//div[@class='select-info-body media-middle']")
	private WebElement AS_SELECTEDLANGUAGE;

	public WebElement getAS_SELECTEDLANGUAGE() {
		return AS_SELECTEDLANGUAGE;
	}

	@FindBy(xpath = "//*[@name='language']//button[@class='btn toggle-select form-control btn-primary']")
	private WebElement AS_DD_LANGUAGESELECT;

	public WebElement getAS_DD_LANGUAGESELECT() {
		return AS_DD_LANGUAGESELECT;
	}

	@FindBy(xpath = "//div[contains(@class,'race-popup-container')]")
	private WebElement CONTAINER;

	public WebElement getCONTAINER() {
		return CONTAINER;
	}

	@FindBy(xpath = "(//div[contains(@class,'race-select-top')]/following-sibling::div/ul/li/a)")
	public WebElement CONTAINEROPTION;

	// Notification Tab
	@FindBy(xpath = "(//div[@class='mobile-view-tab']/ul/li)[3]")
	private WebElement TAB_NOTIFICATION;

	public WebElement getTAB_NOTIFICATION() {
		return TAB_NOTIFICATION;
	}
	@FindBy(xpath="//div[contains(@class,'race-select-top')]/following-sibling::div/ul/li/a/race-checkbox")
	public WebElement SELECTEDOPTION;

	@FindBy(xpath = "(//div[contains(@class,'single-settings-content')]/div/h4)[1]")
	private WebElement LABEL_NOTIFICATION_EMAIL;

	public WebElement getLABEL_NOTIFICATION_EMAIL() {
		return LABEL_NOTIFICATION_EMAIL;
	}

	@FindBy(xpath = "(//div[contains(@class,'single-settings-content')]/div/p)[1]")
	private WebElement TEXT_NOTIFICATION_EMAIL;

	public WebElement getTEXT_NOTIFICATION_EMAIL() {
		return TEXT_NOTIFICATION_EMAIL;
	}

	@FindBy(xpath = "(//input[@name='switch_3']/following-sibling::label)[1]")
	private WebElement BUTTON_YES_EMAIL;

	public WebElement getBUTTON_YES_EMAIL() {
		return BUTTON_YES_EMAIL;
	}

	@FindBy(xpath = "(//input[@name='switch_3']/following-sibling::label)[2]")
	private WebElement BUTTON_NO_EMAIL;

	public WebElement getBUTTON_NO_EMAIL() {
		return BUTTON_NO_EMAIL;
	}

	@FindBy(xpath = "(//div[contains(@class,'single-settings-content')]/div/h4)[2]")
	private WebElement LABEL_NOTIFICATION_POPUP;

	public WebElement getLABEL_NOTIFICATION_POPUP() {
		return LABEL_NOTIFICATION_POPUP;
	}

	@FindBy(xpath = "(//div[contains(@class,'single-settings-content')]/div/p)[2]")
	private WebElement TEXT_NOTIFICATION_POPUP;

	public WebElement getTEXT_NOTIFICATION_POPUP() {
		return TEXT_NOTIFICATION_POPUP;
	}

	@FindBy(xpath = "(//input[@name='switch_4']/following-sibling::label)[1]")
	private WebElement BUTTON_YES_POPUP;

	public WebElement getBUTTON_YES_POPUP() {
		return BUTTON_YES_POPUP;
	}

	@FindBy(xpath = "(//input[@name='switch_4']/following-sibling::label)[2]")
	private WebElement BUTTON_NO_POPUP;

	public WebElement getBUTTON_NO_POPUP() {
		return BUTTON_NO_POPUP;
	}

	// MultiFactor
	@FindBy(xpath = "(//div[@class='mobile-view-tab']/ul/li)[4]")
	private WebElement TAB_MULTIFACTOR;

	public WebElement getTAB_MULTIFACTOR() {
		return TAB_MULTIFACTOR;
	}

	@FindBy(xpath = "//div[@class='switch-field']/label[1]")
	private WebElement SWITCH_MF_DISABLE;

	public WebElement getSWITCH_MF_DISABLE() {
		return SWITCH_MF_DISABLE;
	}

	@FindBy(xpath = "//div[@class='switch-field']/label[2]")
	private WebElement SWITCH_MF_ENABLE;

	public WebElement getSWITCH_MF_ENABLE() {
		return SWITCH_MF_ENABLE;
	}

	@FindBy(xpath = "//div[@class='media-body']")
	private WebElement TEXT_MF_DISABLE;

	public WebElement getTEXT_MF_DISABLE() {
		return TEXT_MF_DISABLE;
	}

	@FindBy(xpath = "//div[@class='form-group']/label")
	private WebElement LABEL_MF_AUTHTYPE;

	public WebElement getLABEL_MF_AUTHTYPE() {
		return LABEL_MF_AUTHTYPE;
	}
	@FindBy(xpath="//input[@name='textItem']")
	public WebElement INPUTSEARCHFILTER;
	@FindBy(xpath="//button[@class='btn btn-block btn-primary-green']")
	public WebElement BUTTONDONE;
	//Project space MultiAdd button and corresponding elements
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']")
	public WebElement PROJECTSPACEMULTIADDBUTTTON;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul")
	public WebElement MULTIBUTTONDROPDOWN;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul/li[2]")
	public WebElement MULTIADD_EDITSUPPLIEROPTION;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-round race-dropdown-toggle']/following-sibling::ul/li[4]")
	public WebElement MULTIADD_NEWPRODUCT;
	@FindBy(xpath="//div[@class='modal-header']/span")
	public WebElement MODALHEADING;
	@FindBy(xpath="//*[@name='supplier']")
	public WebElement DDSELECTSUPPLIER;
	@FindBy(xpath="(//div[@class='race-select-body ps']/ul/li/a)[1]")
	public WebElement DDSUPPLIEROPTION;
	@FindBy(xpath="//div[@class='modal-footer task-modal-footer no-padding-top']/button")
	public WebElement ADDSUPPLIERBUTTON; 
	@FindBy(xpath="(//p[@class='text-ellipsis no-margin-bottom'])[1]")
	public WebElement PROJECTSUPPLIERSIDEMENU;
	//Newe Product
	@FindBy(xpath="(//div[@class='inline-flex']/label)[1]")
	public WebElement LABELEXISTINGPRODUCT;
	@FindBy(xpath="(//div[@class='inline-flex']/label)[2]")
	public WebElement LABELNEWPRODUCT;
	@FindBy(xpath="//div[@class='switch-field inline-flex']/input[@name='newProduct'][1]")
	public WebElement INPUTYESNEWPRODUCT;
	@FindBy(xpath="//div[@class='switch-field inline-flex']/input[@name='newProduct'][2]")
	public WebElement INPUTNONEWPRODUCT;
	@FindBy(xpath="//div[@class='switch-field inline-flex']/input[@name='existingProduct'][1]")
	public WebElement INPUTYESEXISTPRODUCT;
	@FindBy(xpath="//div[@class='switch-field inline-flex']/input[@name='existingProduct'][2]")
	public WebElement INPUTNOEXISTPRODUCT;
	@FindBy(xpath="//div[@class='form-container task-modal']//h5")
	public WebElement TITLEPRODUCT;
	@FindBy(xpath="//div[@class='form-container task-modal']//p")
	public WebElement ADDPRODUCTINFO;
	//ExistingProducts
	@FindBy(xpath="//*[@name='newExistingProduct']")
	public WebElement DDEXISTINGPRODUCT;
	@FindBy(xpath="//*[@name='filterCategory']")
	public WebElement CATEGORYDROPDOWN;
	@FindBy(xpath="//*[@name='filterBrand']")
	public WebElement BRANDDROPDOWN;
	@FindBy(xpath="//*[@name='newExistingProduct']")
	public WebElement EXISTINGPRODUCTDROPDOWN;
	@FindBy(xpath="(//div[@class='race-select-body ps']/ul/li/a)[1]")
	public WebElement DDEXISTINGPRODUCTOPTION;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-width-120 pull-right']")
	public WebElement BUTTONCONFIRM;
	@FindBy(xpath="//div[@class='product-item ng-star-inserted']/div/div[1]")
	public WebElement ADDEDPRODUCT;
	@FindBy(xpath="//div[@class='product-item ng-star-inserted']/div/div[2]/button")
	public WebElement ADDEDPRODUCT_CLOSEBUTTON;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-block ng-star-inserted']")
	public WebElement SECONDADDPRODUCTBUTTON;
	//New Product
	@FindBy(xpath="//*[@name='projects']")
	public WebElement PROJECTDROPDOWN;
	@FindBy(xpath="//*[@name='projects']/div/button/div/div[2]")
	public WebElement DDPROJECTNAME;
	@FindBy(xpath="//*[@name='projects']/div/button")
	public WebElement DDPROJECTBUTTON;
	@FindBy(xpath="//input[@name='name']")
	public WebElement INPUTPRODUCTNAME;
	@FindBy(xpath="//input[@name='code']")
	public WebElement INPUTPRODUCTID;
	@FindBy(xpath="//div[contains(@class,'pointer link back')]/span")
	public WebElement BUTTONBACKTORATING;
	@FindBy(xpath="//*[@alt='Bar Code']/following-sibling::span[1]")
	public WebElement SELECTEDPRODUCT;
	@FindBy(xpath="//tr[@class='hidden-row ng-star-inserted']")
	public WebElement PRERATINGSAREA;
	@FindBy(xpath="//div[@class='product-rating-wrapper']")
	public WebElement PRODUCTSRATINGSAREA;
	@FindBy(xpath="//*[@name='country']")
	public WebElement DDCOUNTRY;
	@FindBy(xpath="//div[@class='race-select-body ps']/ul/li[1]")
	public WebElement DDSELECTOPTION;
	@FindBy(xpath="//div[@class='bar-graph-wrapper ng-trigger ng-trigger-fade']")
	public WebElement TEMPLATEGRAPH;
	@FindBy(xpath = "//*[@class='select-info-body media-middle']")
	public WebElement NOOFCATEGORIES;
	@FindBy(xpath = "//table[@class='table table-striped bordered']/tbody/tr[1]/td[3]/div/div/a/strong")
	public WebElement PROJECTLINKINTABLE;
	//Notifications
	@FindBy(xpath="//div[@class='notifications-title text-center']/following-sibling::div")
	public WebElement NOTIFICATIONICONMODEL;
	@FindBy(xpath="//table[@class='table notifications table-fixed']/tbody/tr[1]/td[2]")
	public WebElement COMPLETEFIRSTNOTIFICATON;
	@FindBy(xpath="//table[@class='table notifications table-fixed']/tbody/tr[1]/td[2]/span")
	public WebElement COMPLETEFIRSTNOTIFICATONTEXT;
	@FindBy(xpath="(//td[@class='remove'])[1]/div")
	public WebElement REMOVEFIRSTNOTIFICATION;
	
	//Side Menu
	//@FindBy(xpath="")
	//public WebElement
	//new
	@FindBy(xpath="//table[@class='table table-striped bordered']/tbody/tr")
	public WebElement TABLEROWDATA;
	@FindBy(xpath="//h4[@class='status completed']")
	public WebElement PROJECTSTATUSCOMPLETE;
	@FindBy(xpath="//div[@class='count-column completed-column-count active']")
	public WebElement PROJECTSTATUSCOMPLETESELECTED;
	@FindBy(xpath="//button[@class='btn btn-search']")
	public WebElement INPUTSEARCHFILTER_BUTTON;
	@FindBy(xpath="//p[@class='text-ellipsis no-margin-bottom']")
	public WebElement SUPPLIERLANDINGPAGE;
	@FindBy(xpath="//div[@class='panel-heading']/span")
	public WebElement SUPPLIERHEADINGPJOJECTSPACE;
	@FindBy(xpath="//li[contains(@class,'no-options')]")
	public WebElement DROPDOWNNOOPTIONSTEXT;
	@FindBy(xpath="(//a[@routerlink='/'])[1]")
	public WebElement HOMEPAGELINK;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@FindBy(xpath="//div[@class='main-nav-items']/ul")
	public WebElement COMPLETESIDEMENU;
	@FindBy(xpath="(//div[@class='main-nav-items']/ul/li)[3]/a/span")
	public WebElement MENU_STRATEGY_TEXT;
	@FindBy(xpath="(//div[@class='main-nav-items']/ul/li)[3]/a")
	public WebElement MENU_STRATEGY_LINK;
	@FindBy(xpath="(//ul[@class='race-tabs']/li/a)[1]")
	public WebElement COMMONTOPMENULOCATOR;
	@FindBy(xpath="//span[@class='message']")
	public WebElement COMMONMESSAGELOCATOR;
	@FindBy(xpath="//a[contains(@class,'removeAll')]")
	public WebElement NOTIFICATION_DELETEALLBUTTON;
	@FindBy(xpath="//a[contains(@class,'readAll')]")
	public WebElement NOTIFICATION_READALLBUTTON;
	@FindBy(xpath="//div[@class='no-notification']")
	public WebElement NO_NOTICATIONTEXT;
	
	
	
	
	
	
	
	public CommonLocators() {
		
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
	
}
