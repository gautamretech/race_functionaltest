package com.race.qa.locator;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.race.qa.base.DriverFactory;


public class TeamManagementLocators{

	@FindBy(xpath = "//div[contains(@class,'race-popup-container')]/div/input/following-sibling::span/race-checkbox")
	public WebElement SELECTALLCHECKBOX;
	@FindBy(xpath = "//*[@name='users']")
	public WebElement DDUSERSLIST;
	@FindBy(xpath = "//*[@name='groupLead']")
	public WebElement DDGROUPLEADLIST;
	@FindBy(xpath = "//h3[@class='font-size-16 font-weight-400 text-2e3d51']")
	public WebElement HEADINGUSERGROUP;
	@FindBy(xpath = "//*[@class='race-select-body ps-container ps-theme-default ps-active-y']/ul")
	public WebElement DROPDOWNDATA;
	
	@FindBy(xpath = "//*[@routerlink='management']")
	public WebElement TEAMTAB;
	@FindBy(xpath = "//*[@routerlink='group-management']")
	public WebElement GROUPSTAB;
	// Team Tab
	
	@FindBy(xpath = "//ul[@class='race-tabs']/li/button")
	public WebElement INVITETEAMMEMBUTTON;
	
	// Invite User
	@FindBy(xpath = "//div[@class='modal-header']/span")
	public WebElement HEADINGPOP;
	@FindBy(xpath = "//input[@name='firstName']")
	public WebElement INPUTFIRSTNAME;
	@FindBy(xpath = "//input[@name='lastName']")
	public WebElement INPUTSURNAME;
	@FindBy(xpath = "//input[@name='email']")
	public WebElement INPUTEMAIL;
	@FindBy(xpath = "//*[@name='role']")
	public WebElement SELECTROLE;
	
	@FindBy(xpath = "//div[@class='modal-footer no-padding-top']/button")
	public WebElement BUTTONSENDINVITE;
	// Group Tab
	@FindBy(xpath = "//a[contains(@class,'btn btn-input pull-right')]")
	public WebElement BUTTONDOWNLOADSUMMARYINCHINESE;
	@FindBy(xpath = "//h3/following::div//input")
	public WebElement GROUPSEARCHINPUT;
	@FindBy(xpath = "(//h3/following::div//button)[1]")
	public WebElement GROUPSEARCHBUTTON;
	// Add Group
	@FindBy(xpath = "//ul[@class='race-tabs']/li/button")
	public WebElement ADDGROUPBUTTON;
	@FindBy(xpath = "//input[@name='name']")
	public WebElement INPUTNAME;
	@FindBy(xpath = "//button[@class='btn btn-search race-dropdown-toggle']")
	public WebElement SELECTCOLOR;
	@FindBy(xpath = "//*[@class='btn btn-input custom-color']")
	public WebElement OPTIONCUSTOMCOLOR;
	@FindBy(xpath = "//div[@class='color-picker']")
	public WebElement COLORPICKER;
	@FindBy(xpath = "//*[@name='banners']")
	public WebElement SELECTBANNER;
	@FindBy(xpath = "//*[@name='categories']")
	public WebElement SELECTCATEGORIES;
	@FindBy(xpath = "//div[@class='cursor']")
	public WebElement CURSOR;
	@FindBy(xpath = "//div[@class='form-group']/input")
	public WebElement INPUTSEARCH;
	@FindBy(xpath = "//*[@class='table-responsive border-light-grey']/table")
	public WebElement TABLEHEADER;
	@FindBy(xpath = "//*[@class='table-responsive border-light-grey']/table/tbody")
	public WebElement TABLEBODY;
	@FindBy(xpath = "//button/img[@alt='Edit']")
	public WebElement TABLEACTIONEDITBUTTON;
	@FindBy(xpath = "//button/img[@alt='Delete']")
	public WebElement TABLEACTIONDELETEBUTTON;
	@FindBy(xpath="(//h3[@class='font-size-16 font-weight-400 text-2e3d51'])[1]")
	public WebElement TEAMHEADING;
	@FindBy(xpath="(//input[@name='textItem'])[1]")
	public WebElement SEARCHTEAMMEMBERINPUT;
	@FindBy(xpath="(//button[@class='btn btn-search'])[1]")
	public WebElement SEARHMEMBERSBUTTON;
	@FindBy(xpath="//h3[@class='font-size-16 font-weight-400 text-2e3d51']")
	public WebElement HEADINGPENDINGINVITES;
	@FindBy(xpath="(//input[@name='textItem'])[2]")
	public WebElement SEARCHPENDINGINVITES;
	@FindBy(xpath="(//button[@class='btn btn-search'])[2]")
	public WebElement SEARCHPENDINGBUTTON;
	@FindBy(xpath="//a[contains(@class,'btn btn-input pull-right')]")
	public WebElement BUTTONDOWNLOADSUMMARY;
	
	public TeamManagementLocators(){
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
}
