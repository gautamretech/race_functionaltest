package com.race.qa.locator;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.race.qa.base.DriverFactory;


public class ProductsPageLocators{
	
	@FindBy(xpath="//*[@name='categoryTiersdepartment']")
	public WebElement PRODUCTPAGESELECTDEPARTMENT;
	@FindBy(xpath="//*[@name='categoryTiersfamily']")
	public WebElement PRODUCTPAGESELECTFAMILY;
	@FindBy(xpath="//*[@name='categoryTierscategory']")
	public WebElement PRODUCTPAGESELECTCATEGORY;
	@FindBy(xpath="//*[@name='category']")
	public WebElement FILTERCATEGORYDROPDOWN;
	@FindBy(xpath="//*[@name='categoryTierssubCategory']")
	public WebElement PRODUCTPAGESELECTSUBCATEGORY;
	@FindBy(xpath="//*[@name='modelcategory']")
	public WebElement PRODUCTPAGESELECTSEGMENT;
	@FindBy(xpath = "//table[@class='table table-striped table-hover']/tbody/tr")
	public WebElement PRODUCTTABLE;
	@FindBy(xpath = "//table[@class='table table-striped table-hover no-margin-bottom']/tbody/tr")
	public WebElement VARIANTTABLE;

	@FindBy(xpath = "//*[contains(@class,'filter-box')]")
	public WebElement FILTERPOPUP;
	@FindBy(xpath = "//div[contains(@class,'race-popup-container')]/div/following-sibling::div/ul")
	public WebElement VARDATA;
	@FindBy(xpath = "//*[@class='race-select-body ps ps--active-y']/ul")
	public WebElement DROPDOWNDATA;
	@FindBy(xpath = "//div[contains(@class,'upload-container')]/div/strong")
	public WebElement UPLOADCONTAINER;
	@FindBy(xpath = "//label[@class='upload-link']")
	public WebElement UPLOADLINK;
	@FindBy(xpath = "//a[@routerlink='projects']")
	public WebElement PROJECTSTAB;

	@FindBy(xpath = "//div[@class='panel-heading transparent no-padding-right']/ul//button")
	public WebElement ADDPRODUCTBUTTON;
	@FindBy(xpath = "//ul[@class='unstyled no-margin pull-right']/li/button")
	public WebElement ADDPRODUCTBUTTONINGENTAB;

	@FindBy(xpath = "//div[@class='modal-header']/span")
	public WebElement POPUPHEADINGPRODUCT;
	@FindBy(xpath = "//*[@name='projects']")
	public WebElement PRODUCTPAGESELECTPROJECT;
	@FindBy(xpath = "//input[@name='name']")
	public WebElement PRODUCTPAGEPRODUCTNAME;
	@FindBy(xpath = "//input[@name='code']")
	public WebElement PRODUCTPAGEPRODUCTID;
	@FindBy(xpath = "//*[@name='modelbrand']")
	public WebElement PRODUCTPAGESELCTBRAND;
	@FindBy(xpath = "//input[@name='description']")
	public WebElement PRODUCTPAGEDESCRIPTION;
	@FindBy(xpath = "//input[@name='marketPositioning']")
	public WebElement PRODUCTPAGEMARKETPOSITION;
	@FindBy(xpath = "//*[@name='countriesSold']")
	public WebElement PRODUCTPAGESELECTCOUNTRY;
	@FindBy(xpath = "//*[@name='bannersSold']")
	public WebElement PRODUCTPAGESELECTBANNER;
	@FindBy(xpath = "//*[@name='status']")
	public WebElement PRODUCTPAGESELECTSTATUS;
	@FindBy(xpath = "//*[@name='specificationsType']")
	public WebElement PRODUCTPAGESELECTSPECIFICATION;
	@FindBy(xpath = "//div[@class='next-btn']/button")
	public WebElement PRODUCTPAGENEXTBUTTON;

	// Products Page Filter popup
	@FindBy(xpath = "//div[@class='filter-container flex flex-align-center']/button")
	public WebElement PRODUCTSPAGEFILTERBUTTON;
	@FindBy(xpath = "//input[@name='productCode']")
	public WebElement FILTERPRODUCTCODE;
	@FindBy(xpath = "//input[@name='ingredient']")
	public WebElement FILTERPRODUCTINGID;
	@FindBy(xpath = "//h3[contains(@class,'font-size-16 font-weight')]")
	public WebElement HEADING;

	// Products Page #Categories
	
	// Product details page
	// General Tabs
	@FindBy(xpath = "//table[@class='table table-striped table-hover']/tbody/tr[1]/td[5]/a")
	public WebElement PRODUCTLINK;
	@FindBy(xpath = "//a[@routerlink='general']")
	public WebElement GENERALTAB;
	@FindBy(xpath = "//div[@class='overview-top-section']/div/h5")
	public WebElement HEADINGOVERVIEW;
	@FindBy(xpath = "//div[@class='overview-top-section']/div/button")
	public WebElement EDITBUTTON;
	@FindBy(xpath = "//img[@alt='Upload']")
	public WebElement UPLOADFILEIMAGE;
	
	@FindBy(xpath = "(//h5[contains(@class,'no-margin font-weight-600 padding-xs padding-left-xs bg-linear-gradient')])[1]")
	public WebElement COUNTRYSOLD;
	@FindBy(xpath = "(//h5[contains(@class,'no-margin font-weight-600 padding-xs padding-left-xs bg-linear-gradient')])[2]")
	public WebElement BANNERSOLD;
	// EDIT PAGE
	@FindBy(xpath = "//div[@class='modal-header']/span")
	public WebElement HEADINGPRODUCTEDITPAGE;

	// Varients and project page
	@FindBy(xpath = "//*[@routerlink='variants']")
	public WebElement VARIENTSTAB;
	@FindBy(xpath = "//li[@class='btn-addon pull-right hidden-xs']/button")
	public WebElement ADDVARIENTBUTTON;
	// Edit Varient Screen
	@FindBy(xpath = "//table[@class='table table-striped table-hover no-margin-bottom']/tbody/tr[1]/td[1]")
	public WebElement VARIENTNAMELINK;
	@FindBy(xpath = "//*[@class='modal-header']/span")
	public WebElement HEADINGEDITVARIENT;
	@FindBy(xpath = "//div[@class='row mobile-view']//label[@class='upload-link']")
	public WebElement VARIANTUPLOADIMAGE;
	@FindBy(xpath = "//input[@name='name']")
	public WebElement VARIANTNAME;
	@FindBy(xpath = "//input[@name='internalId']")
	public WebElement VARIANTID;
	@FindBy(xpath = "//*[@alt='Add']/following-sibling::span")
	public WebElement ADDANOTHERVARIANTID;
	@FindBy(xpath = "//input[@name='estAnnualQty']")
	public WebElement ESTANNUALQUANTITY;
	@FindBy(xpath = "//input[@name='numberOfUnits']")
	public WebElement NUMBEROFUNITS;
	@FindBy(xpath = "//input[@name='size']")
	public WebElement SIZE;
	@FindBy(xpath = "//input[@name='sizeUnit']")
	public WebElement SIZEUNIT;
	@FindBy(xpath = "//input[@name='variantWidth']")
	public WebElement WIDTH;
	@FindBy(xpath = "//input[@name='variantHeight']")
	public WebElement HEIGHT;
	@FindBy(xpath = "//input[@name='variantLength']")
	public WebElement LENGTH;
	@FindBy(xpath = "//textarea[@name='description']")
	public WebElement DESCRIPTION;

	// Specs Management Tab
	@FindBy(xpath = "//*[@routerlink='specs-management']")
	public WebElement SPECMANAGEMENTTAB;
	@FindBy(xpath = "//button[contains(@class,'btn btn-primary-green')]")
	public WebElement REQUESTSPECIFICATIONBUTTON;
	@FindBy(xpath = "//div[@class='modal-header']/span[1]")
	public WebElement HEADINGREQUESTSPECIFICATION;
	@FindBy(xpath = "//*[@name='variant']")
	public WebElement SELECTPRODUCTVARIANT;
	@FindBy(xpath = "//*[@name='modelsuppliers']/div/button/div/div[2]")
	public WebElement SELECTSUPPLIER;
	@FindBy(xpath = "//div[@class='col-xs-12 col-sm-3']/button")
	public WebElement CONFIRMBUTTON;
	@FindBy(xpath = "//*[contains(@class,'add-product-btn')]/span")
	public WebElement ADDMORESPECBUTTON;
	@FindBy(xpath = "//div[@class='pull-right']/button")
	public WebElement SUBMITBUTTON;
	@FindBy(xpath = "//*[@class='panel-heading transparent mobile-view-tab no-padding-bottom']/ul/li[1]")
	public WebElement SUBMISSIONTAB;
	@FindBy(xpath = "//*[@class='panel-heading transparent mobile-view-tab no-padding-bottom']/ul/li[2]")
	public WebElement SPECREQUESTTAB;
	@FindBy(xpath = "//input[@name='searchSubmission']")
	public WebElement SEARCHSUBMISSION;
	@FindBy(xpath = "//div[@class='media-body media-middle']/span")
	public WebElement TEXTDROPDOWNALLSUBMISSION;
	@FindBy(xpath = "//button[@class='btn btn-search race-dropdown-toggle']")
	public WebElement DROPDOWNALLSUBMISSION;
	@FindBy(xpath = "//div[@class='row no-margin']/div/h3")
	public WebElement INUSETEXT;
	@FindBy(xpath = "//div[@class='row no-margin']/div/h5")
	public WebElement TOTALINUSETEXT;
	@FindBy(xpath="//*[@id='switchNewProductLeft']/following-sibling::label[1]")
	public WebElement YESNEWPRODUCTRADIO;
	@FindBy(xpath="//input[@id='switchExistingLeft']/following-sibling::label[1]")
	public WebElement YESEXISTINGPRODUCTRADIO;
	@FindBy(xpath="//*[@name='newExistingProduct']")
	public WebElement EXISTINGPRODUCTDROPDOWN;
	@FindBy(xpath="//*[@class='btn btn-primary-green btn-width-120 pull-right']")
	public WebElement EXISTINGPRODUCTCONFIRMBUTTON;
	// Existing Product
	@FindBy(xpath="//*[@name='filterCategory']")
	public WebElement FILTERCATEGORY;
	@FindBy(xpath="//*[@name='filterBrand']")
	public WebElement FILTERBRAND;
	@FindBy(xpath="//img[@alt='New Product']")
	public WebElement ADDNEWPRODUCTOPTION;


	
	public ProductsPageLocators(){
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
	
}
