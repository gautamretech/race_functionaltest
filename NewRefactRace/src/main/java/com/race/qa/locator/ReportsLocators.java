package com.race.qa.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.race.qa.base.DriverFactory;


public class ReportsLocators{
	
	@FindBy(xpath = "(//div[@class='graph race-popup'])[1]")
	public WebElement LINEGRAPH;
	// Tabs
	@FindBy(xpath = "//a[@routerlink='team']")
	public WebElement TEAMTAB;
	@FindBy(xpath = "//a[@routerlink='projects']")
	public WebElement PROJECTSTAB;
	@FindBy(xpath = "//a[@routerlink='products']")
	public WebElement PRODUCTSTAB;
	@FindBy(xpath = "//a[@routerlink='suppliers']")
	public WebElement SUPPLIERSTAB;

	// TEAMTAB
	@FindBy(xpath = "(//h4[@class='graph-title'])[1]")
	public WebElement HEADINGWORKLOAD;
	@FindBy(xpath = "//div[@class='select-container']//div/div/following-sibling::div[contains(@class,'select-info-body')]")
	public WebElement SELECTSTATUSOFTASK;
	@FindBy(xpath = "//*[contains(@class,'font-size-18')]")
	public WebElement HEADING;
	@FindBy(xpath = "//button[contains(@class, 'margin-left-sm')]")
	public WebElement DOWNLOADBUTTON;
	@FindBy(xpath = "//button[contains(@class, 'filter-btn')]")
	public WebElement FILTERBUTTON;
	@FindBy(xpath = "//input[@name='searchUsers']")
	public WebElement SEARCHUSERSINPUTBOX;
	@FindBy(xpath = "//button[@type='submit']")
	public WebElement SEARCHBUTTON;
	@FindBy(xpath = "//img[@class='icon no-img']")
	public WebElement GROUPSICON;
	@FindBy(xpath = "//th[contains(@class,'ng-tns')]")
	String TABLEHEADERGROUP;

	// User Filter Screen
	@FindBy(xpath = "//*[@name='group']")
	public WebElement SELECTUSERGROUPS;
	@FindBy(xpath = "//*[@name='role']")
	public WebElement SELECTUSERROLE;
	@FindBy(xpath = "//*[@name='status']")
	public WebElement SELECTSTATUS;
	@FindBy(xpath = "//*[@name='projects']")
	public WebElement SELECTUSERPROJECT;
	@FindBy(xpath = "//div[contains(@class,'race-popup-container ')]/div/ul/li[1]")
	public WebElement OPTIONACTIVESTATUS;

	// Projects Tab
	@FindBy(xpath = "//input[@name='searchProjects']")
	public WebElement SEARCHPROJECTINPUT;
	@FindBy(xpath = "//*[@name='categories']")
	public WebElement SELECTCATEGORIES;
	@FindBy(xpath = "//*[@name='templateType']")
	public WebElement SELECTTEMPLATETYPE;
	@FindBy(xpath = "//*[@name='projectManager']")
	public WebElement SELECTMANAGER;
	@FindBy(xpath = "//*[@class='race-select-body ps ps--active-y']/ul/li[2]")
	public WebElement OPTIONINPROGRESSSTATE;
	@FindBy(xpath = "//div[@class='actions']")
	public WebElement ICONPROJSUPPLIER;
	// Products tab and filter
	@FindBy(xpath = "//input[@name='searchProducts']")
	public WebElement SEARCPRODUCTINPUTBOX;
	@FindBy(xpath = "//*[@name='suppliers']")
	public WebElement SELECTSUPPLIER;
	@FindBy(xpath = "//*[@name='ingredient']")
	public WebElement SELECTINGREDIENTS;
	@FindBy(xpath = "//*[@name='allergen']")
	public WebElement SELECTALLERGENS;
	@FindBy(xpath = "//*[@class='race-select-body ps ps--active-y']/ul/li[1]")
	public WebElement OPTIONINPROGRESSSTATUS;
	@FindBy(xpath = "(//div[@class='actions banner-product-popup'])[1]")
	public WebElement ICONCOUNTRYNBANNER;

	// Supplier tab and filters
	@FindBy(xpath = "//input[@name='searchSuppliers']")
	public WebElement SEARCHBOXSUPPLIER;
	@FindBy(xpath = "//*[@name='countries']")
	public WebElement SELECTCOUNTRY;
	@FindBy(xpath = "//*[@name='parentCompany']")
	public WebElement SELECTPARENTCOMPANY;
	@FindBy(xpath = "//*[@class='table-responsive border-light-grey overflow-hidden relative']")
	public WebElement USERTABLE;

	@FindBy(xpath = "//div[@class='race-select-body ps ps--active-y']/ul")
	public WebElement FILTERSECTIONDROPDOWN;
	
	//Projects Tab
	@FindBy(xpath = "//h4[@class='font-weight-400']")
	public WebElement TEXTOVERVIEW;
	@FindBy(xpath = "//*[@name='toggle']//div[2]")
	public WebElement DDTOGGLENAME;
	@FindBy(xpath = "//div[@class='panel-body graph by-completed-graph clickable']")
	public WebElement COMPLETEDPROJECTGRAPHAREA;
	@FindBy(xpath = "//div[@class='panel-body graph by-completed-graph clickable']//span[@class='title']")
	public WebElement COMPLETEDTEXT;
	@FindBy(xpath = "//div[@class='chart-svg']")
	public WebElement COMPLETEDPIECHART;
	@FindBy(xpath = "//div[@class='arc-legend flex-center']")
	public WebElement COMPLETEDLEGENDAREA;
	@FindBy(xpath = "//div[@class='arc-legend flex-center']/div[1]/div[1]")
	public WebElement EARLYBACKGROUNDCOLOR;
	@FindBy(xpath = "//div[@class='arc-legend flex-center']/div[1]/div[3]")
	public WebElement EARLYTEXT;
	@FindBy(xpath = "//div[@class='arc-legend flex-center']/div[2]/div[1]")
	public WebElement ONTIMEBACKGROUNDCOLOR;
	@FindBy(xpath = "//div[@class='arc-legend flex-center']/div[2]/div[3]")
	public WebElement ONTIMETEXT;
	@FindBy(xpath = "//div[@class='arc-legend flex-center']/div[2]/div[1]")
	public WebElement DELAYEDBACKGROUNDCOLOR;
	@FindBy(xpath = "//div[@class='arc-legend flex-center']/div[2]/div[3]")
	public WebElement DELAYEDTEXT;
	
	
	protected ReportsLocators(){
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
	

}
