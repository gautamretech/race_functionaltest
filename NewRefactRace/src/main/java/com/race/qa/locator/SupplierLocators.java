package com.race.qa.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.race.qa.base.DriverFactory;


public class SupplierLocators{

	@FindBy(xpath="(//table[@class='table table-striped']/tbody/tr)[1]/td[4]/a")
	public WebElement SUPPLIERLINKTABLE;
	@FindBy(xpath="(//ul[@class='race-tabs'])[1]")
	public WebElement SUPPLIERSECTIONTABS;
	@FindBy(xpath="//button[@class='btn font-weight-700 padding-left-xs padding-right-xs ng-star-inserted']")
	public WebElement SUPPLIEREDITBUTTON;
	//ADD SUPPLIER
	@FindBy(xpath="//*[@routerlink='dashboard']/following::li/button")
	public WebElement ADDSUPPLIERBUTTON;
	@FindBy(xpath="//div[@class='modal-content']")
	public WebElement ADDPOPUP;
	@FindBy(xpath="//div[@class='modal-header']/span")
	public WebElement HEADINGADD;
	@FindBy(xpath="//div[@class='row form-container']/div/label")
	public WebElement LABELADDSUPPLIERLOGO;
	@FindBy(xpath="//input[@name='name']")
	public WebElement SUPPLIERNAME;
	@FindBy(xpath="//input[@name='vendorNumber']")
	public WebElement SUPPLIERID;
	@FindBy(xpath="//input[@name='address']")
	public WebElement SUPPLIERADDRESS;
	@FindBy(xpath="//input[@name='city']")
	public WebElement SUPPLIERCITY;
	@FindBy(xpath="//input[@name='website']")
	public WebElement SUPPLIERWEBSITE;
	@FindBy(xpath="//input[@name='taxId']")
	public WebElement SUPPLIERTAXID;
	@FindBy(xpath="//input[@name='dunsNumber']")
	public WebElement SUPPLIERDNB;
	@FindBy(xpath="//button[@class='pull-right btn btn-primary-green btn-width-120 flex flex-space-between flex-align-center ng-star-inserted']")
	public WebElement NEXTBUTTON;
	@FindBy(xpath="//div[@class='next-btn']/button")
	public WebElement SAVESUPPLIERBUTTON;
	@FindBy(xpath="//button[@class='btn btn-link ng-star-inserted']")
	public WebElement PREVIOUSBUTTON;
	@FindBy(xpath="//label[@class='upload-link']/input")
	public WebElement UPLOADLOGOLINK;
	@FindBy(xpath="//*[@name='status']")
	public WebElement DDSTATUS;
	@FindBy(xpath="//div[@class='race-select-body ps']/ul/li[1]")
	public WebElement DDSELECTOPTION;
	@FindBy(xpath="//*[@name='country']")
	public WebElement DDCOUNTRY;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[1]/a")
	public WebElement GENERALTAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[2]/a")
	public WebElement BUSINESSTAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[3]/a")
	public WebElement FACTORYTAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[4]/a")
	public WebElement CONTACTTAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[5]/a")
	public WebElement CERTIFICATETAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[6]/a")
	public WebElement MANAGEMENTTAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[7]/a")
	public WebElement CATALOGTAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[8]/a")
	public WebElement RATINGSTAB;
	@FindBy(xpath="(//div[@class='panel-heading transparent mobile-view-tab']/ul/li)[9]/a")
	public WebElement SETTINGTAB;
	@FindBy(xpath="//h5[@class='font-weight-700 no-margin padding-top-5']")
	public WebElement HEADINGOVERVIEW;
	@FindBy(xpath="btn font-weight-700 padding-left-xs padding-right-xs ng-star-inserted")
	public WebElement EDITBUTTON;
	//Business Tab
	@FindBy(xpath="//div[@class='panel-body bg-white no-top-border potential-category-section']/div/div/img")
	public WebElement BUSINESSTABIMAGE;
	@FindBy(xpath="//div[@class='race-select-container form-select']/button//div/div[2]")
	public WebElement TEXTBUSINESSTYPE;
	@FindBy(xpath="//h3[@class='font-size-16 font-weight-400 text-2e3d51 word-break']")
	public WebElement BUSINESSTABSUPPLIERNAME;
	@FindBy(xpath="//*[@name='businessTypes']")
	public WebElement DDBUSINESSTYPE;
	@FindBy(xpath="//div[@class='race-select-body ps']/ul/li/a/race-checkbox")
	public WebElement DDBUSINESSOPTION;
	@FindBy(xpath="//h5[@class='chip-list-header light-text']")
	public WebElement HEADINGBUSINESSTYPESELECTINFO;
	@FindBy(xpath="//span[@class='chip ng-star-inserted']")
	public WebElement BUSINESSCHIPTEXT;
	@FindBy(xpath="//span[@class='chip ng-star-inserted']/button")
	public WebElement BUTTONBUSINESSCHIPCLOSE;
	//Annual sales
	@FindBy(xpath="//div[@class='col-xs-12 flex flex-space-between flex-align-center']/h3")
	public WebElement HEADINGANNUALSALES;
	@FindBy(xpath="//div[@class='col-xs-12 flex flex-space-between flex-align-center']/button")
	public WebElement BUTTONADDANNUNALSALES;
	@FindBy(xpath="(//div[@class='no-data'])[1]")
	public WebElement ANNUALSALES_NODATA;
	@FindBy(xpath="//*[@name='saleCategory']")
	public WebElement DDSALECATEGORY;
	@FindBy(xpath="//*[@name='saleYear']")
	public WebElement DDYEAR;
	@FindBy(xpath="//*[@name='saleYear']/following::td[1]/input")
	public WebElement INPUTSALES;
	@FindBy(xpath="//*[@name='saleYear']/following::td[2]/input")
	public WebElement INPUTPERCENTAGE;
	@FindBy(xpath="//*[@name='saleYear']/following::td[3]/strong")
	public WebElement CALCULATEDVALUE;
	@FindBy(xpath="//*[@name='saleYear']/following::td[4]/div/button")
	public WebElement SALESDELETEBUTTON;
	@FindBy(xpath="(//div[@class='race-select-body ps']/ul/li)[1]")
	public WebElement DDCATEGORYOPTION;
	@FindBy(xpath="//*[@name='saleCategory']//button/div/div[2]")
	public WebElement DDCATEGORYSELECTED;
	@FindBy(xpath="(//div[@class='race-select-body ps ps--active-y']/ul/li)[1]/a")
	public WebElement DDYEAROPTION;
	@FindBy(xpath="//*[@name='saleYear']//button//div[2]")
	public WebElement DDYEARSELECTED;
	//Potential Categories
	@FindBy(xpath="//div[@class='col-xs-12 flex flex-align-center flex-space-between']/h3")
	public WebElement HEADINGPOTENTIALCATEGORIES;
	@FindBy(xpath="//div[@class='col-xs-12 flex flex-align-center flex-space-between']/button")
	public WebElement ADDBUTTONPOTENTALCATEGORY;
	@FindBy(xpath="(//div[@class='no-data'])[2]")
	public WebElement POTENTIALCATEGORIES_NODATA;
	@FindBy(xpath="//*[@name='potentialCategoryCategory']")
	public WebElement DDPOTENTIALCATEGORIES;
	@FindBy(xpath="//*[@name='potentialCategoryYear']")
	public WebElement DDPOTENTIALYEAR;
	@FindBy(xpath="//*[@name='potentialCategoryYear']/following::td[1]/input")
	public WebElement INPUTCAPACITY;
	@FindBy(xpath="//*[@name='potentialCategoryYear']/following::td[2]/input")
	public WebElement INPUTUSED;
	@FindBy(xpath="//div[@class='comment']/input")
	public WebElement INPUTCOMMENT;
	@FindBy(xpath="//*[@name='potentialCategoryYear']/following::td[4]/div/button")
	public WebElement DELETEPOTENTIAL;
	@FindBy(xpath="//*[@name='potentialCategoryCategory']//button/div/div[2]")
	public WebElement DDPOTENTIALCATSELECTED;
	@FindBy(xpath="//*[@name='potentialCategoryYear']//button/div/div[2]")
	public WebElement DDPOTENTIALYEARSELECTED;
	@FindBy(xpath="//div[@class='modal-body']/div/div[2]/p")
	public WebElement CONFIRMDELETEMESSAGE;
	@FindBy(xpath="//button[@class='btn btn-input btn-width-120 margin-left-xs font-size-13 font-weight-700']")
	public WebElement CANCELBUTTON;
	@FindBy(xpath="//button[@class='btn btn-danger btn-width-120 margin-left-xs font-size-13 font-weight-700']")
	public WebElement DELETBUTTON;
	@FindBy(xpath="//div[@class='panel-heading']/span")
	public WebElement TEXTSUPPLIERDETAILS;
	@FindBy(xpath="//div[@class='model-status']")
	public WebElement SUPPLIERSTATUSBUTTON;
	@FindBy(xpath="//div[@class='media-body media-left supplier-about']/h5/a/strong")
	public WebElement BLOCKSUPPLIERNAME;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-block form-control race-dropdown-toggle']")
	public WebElement SUPPLIERCHANGESTATUSBUTTON;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-block form-control race-dropdown-toggle']/following-sibling::ul")
	public WebElement CHANGEOPTIONS;
	@FindBy(xpath="(//button[@class='btn btn-primary-green btn-block form-control race-dropdown-toggle']/following-sibling::ul/li)[4]")
	public WebElement SELECTSUPPLIEROPTION;
	//SINGLE PRERATINGS PAGE
	@FindBy(xpath="(//div[@class='panel-heading transparent']/ul/li)[2]")
	public WebElement HEADINGPRERATINGPAGE;
	@FindBy(xpath="//div[@class='table-responsive margin-bottom-sm ps']")
	public WebElement TABLEPRERATINGPAGE;
	@FindBy(xpath="(//table[@class='table table-striped bordered']/tbody/tr/td)[1]/span")
	public WebElement RATINGSTATUSPRERATINGPAGE;
	@FindBy(xpath="(//table[@class='table table-striped bordered']/tbody/tr/td)[3]/div")
	public WebElement PROJECTNAMEPRERATINGPAGE;
	@FindBy(xpath="(//div[@class='danger add-product text-center font-weight-700']/div)[1]")
	public WebElement TEXTMESSAGEPRERATINGPAGE;
	@FindBy(xpath="(//div[@class='danger add-product text-center font-weight-700']/div)[2]")
	public WebElement CLICKHERELINKPRERATINGPAGE;
	@FindBy(xpath="//tr[@class='hidden-row ng-star-inserted']")
	public WebElement PRERATINGSAREA;
	@FindBy(xpath="//div[@class='product-rating-wrapper']")
	public WebElement PRODUCTSRATINGSAREA;
	@FindBy(xpath="//div[@class='product-rating-wrapper']//div[@class='text-product']")
	public WebElement TITLEPRODUCTRATINGS;
	@FindBy(xpath="(//*[@alt='info ico'])[1]")
	public WebElement PRODUCTRATINGINFO_POPUP;
	@FindBy(xpath="(//div[contains(@class,'race-tooltip')]/div)[1]")
	public WebElement PRODUCTRATINGINFO_POPUPTEXT;
	@FindBy(xpath="(//div[@class='product-rating-wrapper']/div/following-sibling::div)[1]//input")
	public WebElement PRODUCTRATINGRANGE;
	@FindBy(xpath="(//div[@class='product-rating-wrapper']/div/following-sibling::div)[1]//span")
	public WebElement PRODUCTSCORETYPE;
	//Supplier ratings Area
	@FindBy(xpath="//div[@class='product-rating-wrapper margin-bottom-15']")
	public WebElement SUPPLIERRATINGSAREA;
	@FindBy(xpath="//div[@class='product-rating-wrapper margin-bottom-15']/div/div[2]")
	public WebElement TITLESUPPLIERRATINGS;
	@FindBy(xpath="(//*[@alt='info ico'])[2]")
	public WebElement SUPPLIERRATINGINFO_POPUP;
	@FindBy(xpath="(//div[contains(@class,'race-tooltip')]/div)[1]")
	public WebElement SUPPLIERRATINGSINFO_TEXT;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-width-150']")
	public WebElement BUTTONADDCRITERIA;
	@FindBy(xpath="//button[@class='btn btn-primary-green btn-width-90']")
	public WebElement BUTTONSAVESUPPLIERRATINGS;
	@FindBy(xpath="(//table[@class='table table-striped bordered']/tbody/tr/td)[8]/span")
	public WebElement FINALRATINGSCORE;
	@FindBy(xpath="//div[@class='product-errors']/div/div")
	public WebElement PRODUCTERROR;
	@FindBy(xpath="//div[@class='supplier-errors']/div/div")
	public WebElement SUPPLIERERROR;
	// Catalog screen
	@FindBy(xpath="//h3[@class='no-margin font-size-18 font-weight-400 text-2e3d51']")
	public WebElement PRODUCTLISTTITLE;
	@FindBy(xpath="//button[@class='btn btn-green-border text-2e3d51 font-weight-400 flex flex-center flex-align-center margin-left-sm padding-left-xs padding-right-xs']")
	public WebElement ADDPRODUCTBUTTON;
	@FindBy(xpath="//div[@class='placeholder ng-star-inserted']/span")
	public WebElement PRODLISTNODATA;
	@FindBy(xpath="(//div[@class='modal-content']//div)[1]/span")
	public WebElement HEADERADDPRODUCT;
	@FindBy(xpath="(//ul[@class='race-tabs no-margin-bottom']/li)[1]/a")
	public WebElement TABADDNEWPRODUCT;
	@FindBy(xpath="(//ul[@class='race-tabs no-margin-bottom']/li)[2]/a")
	public WebElement TABBATCHUPLOAD;
	@FindBy(xpath="(//h4[@class='font-size-16 font-weight-400'])[1]")
	public WebElement LABELUPLOADPRODUCTIMAGE; 
	@FindBy(xpath="(//label[@class='upload-label']/input)")
	public WebElement UPLOADPRODUCTIMAGE;
	@FindBy(xpath="//input[@name='name']/preceding-sibling::label")
	public WebElement LABELINPUTPRODUCTNAME;
	@FindBy(xpath="(//input[@name='name'])")
	public WebElement INPUTPRODUCTNAME;
	@FindBy(xpath="//input[@name='size']/preceding-sibling::label")
	public WebElement LABELINPUTSIZE;
	@FindBy(xpath="(//input[@name='size'])")
	public WebElement INPUTSIZE;
	@FindBy(xpath="//input[@name='price']/preceding-sibling::label")
	public WebElement LABELINPUTPRICE;
	@FindBy(xpath="(//input[@name='price'])")
	public WebElement INPUTPRICE;
	@FindBy(xpath="//input[@name='packageType']/preceding-sibling::label")
	public WebElement LABELINPUTPACKAGETYPE;
	@FindBy(xpath="(//input[@name='packageType'])")
	public WebElement INPUTPACKAGETYPE;
	@FindBy(xpath="//div[@class='modal-footer footer-btn-container']/button")
	public WebElement SAVEBUTTONCATALOG;
	@FindBy(xpath="(//h4[@class='font-size-16 font-weight-400'])[2]")
	public WebElement LINKADDMOREPRODUCTS;
	@FindBy(xpath="(//div[@class='button close'])[2]")
	public WebElement ADDMODALCLOSEBUTTON;
	@FindBy(xpath="(//div[@class='modal-body no-padding-bottom']/div//div)[2]/a")
	public WebElement TEXTDOWNLOADTEMPLATE;
	@FindBy(xpath="//div[@class='upload-container start']/div/span")
	public WebElement TEXTUPLOADBATCH;
	@FindBy(xpath="//div[@class='upload-container start']/div/input")
	public WebElement INPUTBATCHUPLOAD;
	@FindBy(xpath="//h4[@class='font-size-16 font-weight-400']")
	public WebElement TEXTUPLOADEDFILES;
	@FindBy(xpath="(//div[@class='media-body media-middle']/div/span)[1]")
	public WebElement TITLEFILENAME;
	@FindBy(xpath="(//div[@class='media-body media-middle']/div/span)[2]")
	public WebElement SUBTITLEMESSAGE;
	@FindBy(xpath="//div[@class='modal-footer footer-btn-container']/button")
	public WebElement CLOSEBUTTON;
	@FindBy(xpath="//div[@class='row border-bottom-lg padding-bottom-sm']//button[1]")
	public WebElement REMOVEBUTTON;
	@FindBy(xpath="//div[@class='row border-bottom-lg padding-bottom-sm']//button[2]")
	public WebElement RECOMMENDBUTTON;
	@FindBy(xpath="//div[@class='modal-content']//div/div/p")
	public WebElement ERRORMESSAGEDIV;
	@FindBy(xpath="//div[@class='list']/div/span[2]")
	public WebElement TOPADMINPERMISSIONDENIEDMESSAGE;
	//Product Info
	@FindBy(xpath="(//div[@class='modal-content']//div)[1]/span")
	public WebElement PRODUCTINFOHEADING;
	@FindBy(xpath="(//div[contains(@class,'col-xs-6 no-padding switcher')]/h4)[1]")
	public WebElement LABELOEM;
	@FindBy(xpath="(//div[contains(@class,'col-xs-6 no-padding switcher')]/h4)[2]")
	public WebElement LABELINSTOCK;
	@FindBy(xpath="//div[@class='input-select-group']/div/label")
	public WebElement LABELMOQ;
	@FindBy(xpath="//div[@class='input-select-group']/div/input")
	public WebElement INPUTMOQ;
	@FindBy(xpath="(//div[@class='form-group no-margin']/input)[5]")
	public WebElement INPUTORIGIN;
	@FindBy(xpath="(//div[@class='form-group no-margin']/label)[5]")
	public WebElement LABELORIGIN;
	@FindBy(xpath="//textarea[@class='form-control ng-untouched ng-pristine ng-invalid']")
	public WebElement TEXTAREAREASON;
	@FindBy(xpath="(//label[@class='font-size-14 font-weight-700 text-2e3d51'])[7]")
	public WebElement LABELTEXTAREA;
	@FindBy(xpath="//div[@class='modal-footer footer-btn-container']/button")
	public WebElement CONFIRMBUTTON;
	@FindBy(xpath="(//div[@class='modal-content'])[2]//span")
	public WebElement RECOMENDCONFIRMMESSAGE2;
	@FindBy(xpath="(//div[@class='modal-content'])[2]//p")
	public WebElement RECOMENDCONFIRMMESSAGE1;
	@FindBy(xpath="//div[@class='btn default-cursor ng-star-inserted']")
	public WebElement RECOMENDEDICON;
	@FindBy(xpath="//div[@class='modal-footer border-top-lg']/button[2]")
	public WebElement RECOMENDCONFIRMBUTTON;
	//Factory
	@FindBy(xpath="//button[@class='btn btn-primary-green flex flex-align-center padding-left-xs padding-right-xs flex-space-between']")
	public WebElement ADDBUTTON;
	@FindBy(xpath="//table[@class='table table-striped table-hover bottom-border']/tbody/tr/td/div//div[2]")
	public WebElement FACTORYTABLENODATE;
	@FindBy(xpath="(//div[@class='heading'])[1]")
	public WebElement HEADINGFACTORYMAP;
	@FindBy(xpath="(//div[@class='text-center empty-text'])[1]/span")
	public WebElement NOMAPMESSAGE;
	@FindBy(xpath="(//div[@class='text-center empty-text'])[2]")
	public WebElement FACTORYSELECTEMPTYAREA;
	@FindBy(xpath="(//div[@class='text-center empty-text'])[2]/span[1]")
	public WebElement SELECTFACTORYMESSAGE1;
	@FindBy(xpath="(//div[@class='text-center empty-text'])[2]/span[2]")
	public WebElement SELECTFACTORYMESSAGE2;
	//Add Factory
	@FindBy(xpath="(//div[@class='heading'])[2]")
	public WebElement ADDNEWFACTORYHEADING;
	@FindBy(xpath="//input[@name='name']")
	public WebElement INPUTFACTORYNAME;
	@FindBy(xpath="//*[@name='type']")
	public WebElement DDTYPE;
	@FindBy(xpath="//*[@name='categories']")
	public WebElement DDCATEGORIES;
	@FindBy(xpath="//*[@name='contacts']")
	public WebElement DDCONTACTS;
	@FindBy(xpath="//*[@name='address']")
	public WebElement INPUTADDDRESS;
	@FindBy(xpath="//*[@name='state']")
	public WebElement INPUTSTATE;
	@FindBy(xpath="//*[@name='city']")
	public WebElement INPUTCITY;
	@FindBy(xpath="//*[@name='zip']")
	public WebElement INPUTZIP;
	@FindBy(xpath="//*[@name='capacity']")
	public WebElement INPUTFACTORYCAPACITY;
	@FindBy(xpath="//input[@name='capacityUsageRatio']")
	public WebElement INPUTRATIO;
	@FindBy(xpath="//input[@name='linesCount']")
	public WebElement INPUTLINES;
	@FindBy(xpath="//input[@name='employeesCount']")
	public WebElement INPUTEMPLOYEES;
	@FindBy(xpath="//button[@class='btn btn-block btn-primary-green']")
	public WebElement BUTTONCOMPLETE;
	@FindBy(xpath="(//div[@class='heading'])[2]//button[1]")
	public WebElement FACTORYDELETEBUTTON;
	@FindBy(xpath="(//div[@class='heading'])[2]//button[2]")
	public WebElement FACTORYEDITBUTTON;
	@FindBy(xpath="//table[@class='table table-striped table-hover bottom-border']/tbody/tr/td[2]")
	public WebElement TABLEFACTORYNAME;
	//Contacts
	@FindBy(xpath="//input[@name='textItem']")
	public WebElement CONTACTINPUTSEARCH;
	@FindBy(xpath="(//div[@class='no-data'])")
	public WebElement TABLENODATA;
	//Add Contact
	@FindBy(xpath="//input[@name='firstName']/preceding-sibling::label")
	public WebElement LABELCONTACTFIRSTNAME;
	@FindBy(xpath="//input[@name='firstName']")
	public WebElement INPUTCONTACTFIRSTNAME;
	@FindBy(xpath="//input[@name='lastName']/preceding-sibling::label")
	public WebElement LABELCONTACTLASTNAME;
	@FindBy(xpath="//input[@name='lastName']")
	public WebElement INPUTCONTACTLASTNAME;
	@FindBy(xpath="//input[@name='email']/preceding-sibling::label")
	public WebElement LABELCONTACTEMAIL;
	@FindBy(xpath="//input[@name='email']")
	public WebElement INPUTCONTACTEMAIL;
	@FindBy(xpath="//input[@name='phone']/preceding-sibling::label")
	public WebElement LABELCONTACTPHONE;
	@FindBy(xpath="//input[@name='phone']")
	public WebElement INPUTCONTACTPHONE;
	@FindBy(xpath="//input[@name='title']/preceding-sibling::label")
	public WebElement LABELCONTACTTITLE;
	@FindBy(xpath="//input[@name='title']")
	public WebElement INPUTCONTACTTITLE;
	@FindBy(xpath="//input[@name='linkedin']/preceding-sibling::label")
	public WebElement LABELCONTACTLINEDIN;
	@FindBy(xpath="//input[@name='linkedin']")
	public WebElement INPUTCONTACTLINEDIN;
	@FindBy(xpath="//input[@name='address']/preceding-sibling::label")
	public WebElement LABELCONTACTADDRESS;
	@FindBy(xpath="//input[@name='address']")
	public WebElement INPUTCONTACTADDRESS;
	@FindBy(xpath="//input[@name='city']/preceding-sibling::label")
	public WebElement LABELCONTACTCITY;
	@FindBy(xpath="//input[@name='city']")
	public WebElement INPUTCONTACTCITY;
	@FindBy(xpath="//input[@name='state']/preceding-sibling::label")
	public WebElement LABELCONTACTSTATE;
	@FindBy(xpath="//input[@name='state']")
	public WebElement INPUTCONTACTSTATE;
	@FindBy(xpath="//input[@name='zip']/preceding-sibling::label")
	public WebElement LABELCONTACTZIP;
	@FindBy(xpath="//input[@name='zip']")
	public WebElement INPUTCONTACTZIP;
	@FindBy(xpath="//label[@class='label-padding-side margin-left-md']/preceding-sibling::label/input/following-sibling::div")
	public WebElement CHECKBOXPRIMARYCONTACT;
	@FindBy(xpath="//label[@class='label-padding-side margin-left-md']")
	public WebElement LABELPRIMARYCONTACT;
	@FindBy(xpath="//label[@class='inline-flex label-padding-side']")
	public WebElement LABELACCESS;
	@FindBy(xpath="//div[@class='modal-footer no-padding-top']/button")
	public WebElement CONTACTSAVEBUTTON;
	@FindBy(xpath="//*[@name='country']/preceding::label[1]")
	public WebElement LABELCONTACTCOUNTRY;
	@FindBy(xpath = "//*[@alt='New Supplier']")
	public WebElement ADDNEWSUPPLIEROPTION;
	@FindBy(xpath = "//div[contains(@class,'race-popup-container')]/div/ul")
	public WebElement DROPDOWNDATA;
	@FindBy(xpath = "//*[@name='factories']")
	public WebElement SELECTFACTORYDD;
	@FindBy(xpath = "//*[@name='tasks']")
	public WebElement SELECTTASKDD;
	@FindBy(xpath = "//button[@class='btn btn-primary-green']")
	public WebElement ADDSUPPLIERBUTTONINPROJECTSPAGE;
	@FindBy(xpath = "//*[@routerlink='dashboard']")
	public WebElement SUPPLIERSTAB;
	@FindBy(xpath = "//*[@routerlink='communications']")
	public WebElement COMMUNICATIONSTAB;
	@FindBy(xpath = "//h3[@class='font-size-16 font-weight-400 text-2e3d51 no-margin']")
	public WebElement HEADINGSUPPLIERS;
	@FindBy(xpath = "//div[@class='filter-container flex flex-align-center']/button")
	public WebElement FILTERBUTTON;
	@FindBy(xpath = "//*[@name='category']")
	public WebElement SELECTCATEGORY;
	@FindBy(xpath = "//*[@routerlink='overview/detail/projects']")
	public WebElement OVERVIEWTAB;
	@FindBy(xpath = "//a[text()='Contacts']")
	public WebElement CONTACTSTAB;
	@FindBy(xpath = "//*[@name='parent']")
	public WebElement SELECTPARENTCOMPANY;
	@FindBy(xpath = "//*[@name='yearEstablished']")
	public WebElement SELECTYEAR;
	@FindBy(xpath = "//*[@name='country']/div/button/following-sibling::div/div/input")
	public WebElement COUNTRYFILTER;
	@FindBy(xpath = "//*[@name='switch_1']/following-sibling::label[text()='YES']")
	public WebElement RADIOBUTTONACCESSRACE_YES;
	@FindBy(xpath = "//button[text()='Add supplier']")
	public WebElement FINALADDSUPPLIERBUTTON;
	@FindBy(xpath = "//span[text()='Supplier Settings']")
	public WebElement HEADINGSUPPLIERSETTING;
	@FindBy(xpath = "//*[@class='table table-striped']/tbody/tr[1]/td[4]/a")
	public WebElement SUPPLIERLINK;
	@FindBy(xpath = "//li[@class='border-right-lg']/a")
	public WebElement RECENTNEWSTAB;
	@FindBy(xpath = "//li[@class='border-right-lg']/following-sibling::li/a")
	public WebElement RECENTACTIVITYTAB;
	@FindBy(xpath = "//*[@routerlink='detail/projects']")
	public WebElement SUPPLIERPAGEPROJECTSTAB;
	@FindBy(xpath = "//*[@routerlink='detail/products']")
	public WebElement SUPPLIERPAGEPRODUCTSTAB;
	@FindBy(xpath = "//*[@class='rating-graph-section']")
	public WebElement RATINGSAREA;
	@FindBy(xpath = "//*[@class='btn btn-primary-green btn-width-120 flex flex-align-center padding-left-xs padding-right-xs flex-space-between ng-star-inserted']")
	public WebElement ADDRATINGSBUTTON;
	@FindBy(xpath = "//*[@class='row no-margin-left no-margin-right margin-lg ng-star-inserted']")
	public WebElement DEFAULTCRITERIAS;
	@FindBy(xpath = "//*[@class='btn btn-primary-green btn-width-120 padding-left-xs padding-right-xs ng-star-inserted']")
	public WebElement ADDNEWCRITIRIA;
	@FindBy(xpath = "//*[@name='textItem']")
	public WebElement INPUTCRITERIANAME;
	@FindBy(xpath = "(//*[@class='btn delete-btn ng-star-inserted'])[7]")
	public WebElement CRITERIACROSSBUTTON;
	@FindBy(xpath = "(//*[@class='ng-untouched ng-pristine ng-valid'])[7]")
	public WebElement CRITERIACHECKBOX;
	@FindBy(xpath = "//*[@class='btn btn-primary-green btn-width-120']")
	public WebElement SAVERATINGS;
	@FindBy(xpath = "//*[@routerlink='catalog']")
	public WebElement PRODUCTSTABINSUPPLIERMENU;
	@FindBy(xpath = "//h3[@class='no-margin font-size-18 font-weight-400 text-2e3d51']")
	public WebElement PRODUCTSTABHEADING;
	@FindBy(xpath = "//*[contains(@class,'no-margin font-size-23 font-weight-600')]")
	public WebElement PROJECTNAMEASHEADING;
	@FindBy(xpath = "(//*[@class='font-size-14 font-weight-400 text-gray'])[1]")
	public WebElement PROJECTHEADING;
	@FindBy(xpath = "(//*[@class='font-size-14 font-weight-400 text-gray'])[2]")
	public WebElement PROJECTMANGERHEADING;
	@FindBy(xpath = "(//*[contains(@class,'margin-left-xxs')])[1]")
	public WebElement PROJECTMANAGERNAME;
	@FindBy(xpath = "//*[@class='col-xs-6 total-score-column']/h2")
	public WebElement CURRENTPROJECTNAME;
	@FindBy(xpath = "//*[@class='col-xs-6 total-score-column']/h4")
	public WebElement CURRENTPROJECTSCORE;
	@FindBy(xpath = "//*[@class='col-xs-6 total-score-column']/h5/span")
	public WebElement CURRENTPROJECTSCOREDATE;
	@FindBy(xpath = "//*[@class='col-xs-6 last-score-column']/h2")
	public WebElement LASTPROJECTNAME;
	@FindBy(xpath = "//*[@class='col-xs-6 last-score-column']/h4")
	public WebElement LASTPROJECTSCORE;
	@FindBy(xpath = "(//*[@class='col-xs-6 last-score-column']/h5/span)[2]")
	public WebElement LASTPROJECTSCOREDATE;
	
	public SupplierLocators(){
		WebDriver driver=DriverFactory.getDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
	}
	
	
}
