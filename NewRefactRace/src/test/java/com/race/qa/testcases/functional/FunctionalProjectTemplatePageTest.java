package com.race.qa.testcases.functional;



import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.functional.FunctionalProjectTemplatePage;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.util.Log;

public class FunctionalProjectTemplatePageTest extends TestBase{
	UILoginPage lp;
	FunctionalProjectTemplatePage fpt;
	String message = "You're currently editing this template in another tab, please double check.";
	private String templatename;
	String methodName;

	
	
	/**
	 * Validating error message -  One template is already in edit mode
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_1() throws Throwable{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("adminusername"),
				configProperities.getProperty("commonpassword"));
		Log.info("First Instance");
		clickSideNavigationMenu("ProjectTemplates");
		fpt.validateActiveEditErrorMessage();
		fpt.rightClickonMenu(message);
		lp.logout();
		test.get().log(Status.PASS,"Step 1");
		Log.endTestCase(methodName);
		}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
	/**
	 * Template can be save as draft if no info RACE-5058
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_2() throws Throwable{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			clickSideNavigationMenu("ProjectTemplates");
			templatename=fpt.addTemplate_Data1(false,configProperities.getProperty("familyname"), "Category Reviews", configProperities.getProperty("templatenameasinput")+Randomizer.generate(2000, 5000));
			String savedMessage=fpt.saveAction(true, false);
			Assert.assertEquals(savedMessage, selectPropertiesFile(true, locale, "templatesavedasdraft"));
			//	verifying Data
			returnToDashboardPage();
			clickSideNavigationMenu("ProjectTemplates");
			fpt.getTemplate(templatename);
			Assert.assertEquals(fpt.getTemplateStatus(templatename), "Draft");
			lp.logout();
			test.get().log(Status.PASS,"Step 1");
			}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		
	}
	/**without any Data
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_12() throws Exception{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			clickSideNavigationMenu("ProjectTemplates");
			templatename=fpt.addTemplate_Data1(true,null, null, null);
			String savedMessage=fpt.saveAction(true, false);
			Assert.assertEquals(savedMessage, selectPropertiesFile(true, locale, "cannotsavetemplateasdraftwithouttemplatenameandtype"));
			lp.logout();
			test.get().log(Status.PASS,"Step 1");
			Log.endTestCase(methodName);
			}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
	/**
	 * Save button is disabled and Error message on mouse hover on save button
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_3() throws Exception{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();		
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			clickSideNavigationMenu("ProjectTemplates");
			fpt.addTemplate_Data1(false,configProperities.getProperty("familyname"), "Category Reviews", ("templatenameasinput")+Randomizer.generate(2000, 5000));
			String saveMessage = fpt.saveAction(false, true);
			System.out.println(saveMessage);
			lp.logout();
			test.get().log(Status.PASS,"Step 1");
			Log.endTestCase(methodName);
			}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
	/**
	 * Validate the message on archiving and un-archiving.
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_4() throws Throwable{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
		lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("ProjectTemplates");
		templatename=fpt.createNewTemplate(configProperities.getProperty("familyname"),
				selectPropertiesFile(true, locale, "categoryreviews"), 1, 1, false, null);
		System.out.println(templatename);
		returnToDashboardPage();
		clickSideNavigationMenu("ProjectTemplates");
		fpt.getTemplate(templatename);
		System.out.println(fpt.validateArchiveFunction());
		//checking unarchiving
		returnToDashboardPage();
		clickSideNavigationMenu("ProjectTemplates");
		fpt.getTemplate(templatename);
		System.out.println(fpt.validateArchiveFunction());
		lp.logout();
		test.get().log(Status.PASS,"Step 1");
		Log.endTestCase(methodName);
	}catch (Exception e) {
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}
	/**Create new Template- success
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_5() throws Throwable{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			clickSideNavigationMenu("ProjectTemplates");
			templatename=fpt.createNewTemplate(configProperities.getProperty("familyname"),
				selectPropertiesFile(true, locale, "categoryreviews"), 1, 1, false, null);
			System.out.println(templatename);
			lp.logout();
			test.get().log(Status.PASS,"Step 1");
			Log.endTestCase(methodName);
			}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
	/**
	 * Copy Template
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_6() throws Throwable{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			clickSideNavigationMenu("ProjectTemplates");
			templatename=fpt.createNewTemplate(configProperities.getProperty("familyname"),
				selectPropertiesFile(true, locale, "categoryreviews"), 1, 1, false, null);
			System.out.println("From Test case"+templatename);
			returnToDashboardPage();
			clickSideNavigationMenu("ProjectTemplates");
			fpt.getTemplate(templatename);
			String copy=(fpt.validateCopyFunction(templatename));
			Assert.assertEquals(fpt.getTemplateStatus(copy), "Draft");
			lp.logout();
			test.get().log(Status.PASS,"Step 1");
			}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}
	/**deleting template and creating a new one with same name
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void testTemplate_7() throws Throwable{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fpt = new FunctionalProjectTemplatePage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try{
		
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("adminusername"),
				configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("ProjectTemplates");
		templatename=fpt.createNewTemplate(configProperities.getProperty("familyname"),
				selectPropertiesFile(true, locale, "categoryreviews"), 1, 1, false, null);
		System.out.println(templatename);
		returnToDashboardPage();
		clickSideNavigationMenu("ProjectTemplates");
		fpt.getTemplate(templatename);
		fpt.validateDeleteFunction();
		returnToDashboardPage();
		clickSideNavigationMenu("ProjectTemplates");
		//Creating new template with same name
		String duplicateTemplatename =fpt.createNewTemplate(configProperities.getProperty("familyname"),
				selectPropertiesFile(true, locale, "categoryreviews"), 1, 1, true, templatename);
		Assert.assertEquals(templatename, duplicateTemplatename);
		lp.logout();
		test.get().log(Status.PASS,"Step 1");
		
		Log.endTestCase(methodName);
		}catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
	
	
	
	
	

}
