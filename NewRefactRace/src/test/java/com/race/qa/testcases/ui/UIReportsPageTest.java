package com.race.qa.testcases.ui;


import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.pages.ui.UIReportsPage;
import com.race.qa.util.Log;

public class UIReportsPageTest extends TestBase {
	UILoginPage lp;
	UIReportsPage reportPage;
	private String USERFILEPREFIX = "users_";
	private String PROJECTPREFIX = "projects_";
	private String PRODUCTPREFIX = "products_";
	private String SUPPLIERPREFIX = "suppliers_";
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
	private String FILEDATE = dateFormat.format(new Date());
	private String FILEFORMAT = ".xlsx";
	private String FINALUSERFILENAME = USERFILEPREFIX + FILEDATE + FILEFORMAT;
	private String FINALPROJECTFILENAME = PROJECTPREFIX + FILEDATE + FILEFORMAT;
	private String FINALPRODUCTFILENAME = PRODUCTPREFIX + FILEDATE + FILEFORMAT;
	private String FINALSUPPLIERFILENAME = SUPPLIERPREFIX + FILEDATE + FILEFORMAT;
	String methodName;
	String downloadFilepath;

	@Test(groups = { "SANITY" })
	public void validateTeamTabScreenTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		reportPage = new UIReportsPage(driver);
		// System.out.println(reportPage.validateLineGraph_TeamTab(reportPage.currentMonth()));
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateUserTableDataInTeamTabTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateUserTableInTeamTab();
		lp.logout();
		
		Log.startTestCase(methodName);
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateDownloadUserReportTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadUserReport();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALUSERFILENAME));
		ExcelReader(FINALUSERFILENAME);
		deleteFile(FINALUSERFILENAME);
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateDownloadProjectReportTest() throws StackOverflowError, Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadProjectReport();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALPROJECTFILENAME));
		ExcelReader(FINALPROJECTFILENAME);
		deleteFile(FINALPROJECTFILENAME);
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateDownloadProductReportTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadProductReport();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALPRODUCTFILENAME));
		ExcelReader(FINALPRODUCTFILENAME);
		deleteFile(FINALPRODUCTFILENAME);
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}
		catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateDownloadSupplierReportTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadSupplierReport();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALSUPPLIERFILENAME));
		ExcelReader(FINALSUPPLIERFILENAME);
		deleteFile(FINALSUPPLIERFILENAME);
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateUserFilterPopupScreenTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateUserFilterPopupScreen();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateDownloadUserReportWithFilterOnStatusTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadUserReportWithFilterOnStatus();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALUSERFILENAME));
		ExcelReader(FINALUSERFILENAME);
		deleteFile(FINALUSERFILENAME);
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}
	
	@Test(groups = { "SANITY" })
	public void validateDownloadProjectReportWithFilterOnStatusTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadProjectReportWithFilterOnStatus();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALPROJECTFILENAME));
		ExcelReader(FINALPROJECTFILENAME);
		deleteFile(FINALPROJECTFILENAME);
		lp.logout();
		Log.endTestCase(methodName);
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}

	}
	@Test(groups = { "SANITY" })
	public void validateDownloadProductReportWithFilterOnStatusTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadProductReportWithFilterOnStatus();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALPRODUCTFILENAME));
		ExcelReader(FINALPRODUCTFILENAME);
		deleteFile(FINALPRODUCTFILENAME);
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}
		catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateDownloadSupplierReportWithFilterOnStatusTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateDownloadSupplierReportWithFilterOnStatus();
		Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALSUPPLIERFILENAME));
		ExcelReader(FINALSUPPLIERFILENAME);
		deleteFile(FINALSUPPLIERFILENAME);
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateProjectTabScreenTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateProjectTabScreen();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateProductTabScreenTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateProductTabScreen();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateSupplierTabScreenTest() throws Exception {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateSupplierTabScreen();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateProjectFilterPopupScreenTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateProjectFilterPopupScreen();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateProductFilterPopupScreenTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateProductFilterPopupScreen();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateSupplierFilterPopupScreenTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateSupplierFilterPopupScreen();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateProjectTableInTeamTabTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateProjectTableInTeamTab();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
	}catch(Exception e){
		test.get().log(Status.ERROR, e.getClass().getSimpleName());
		throw e;
	}
	}

	@Test(groups = { "SANITY" })
	public void validateProductTableTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateProductTable();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		}
	

	@Test(groups = { "SANITY" })
	public void validateSupplierTableInTeamTabTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..."+Thread.currentThread().getStackTrace()[1].getMethodName());
		try{
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println(methodName + Thread.currentThread().getId());
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		lp = new UILoginPage(driver, locale);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.INFO, "Successful Login");
		reportPage = new UIReportsPage(driver);
		reportPage.validateSupplierTable();
		lp.logout();
		test.get().log(Status.INFO, "completed "+Thread.currentThread().getStackTrace()[1].getMethodName());
		Log.endTestCase(methodName);
		}catch(Exception e){
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
}
