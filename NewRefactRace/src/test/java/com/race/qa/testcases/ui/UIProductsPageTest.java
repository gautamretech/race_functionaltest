package com.race.qa.testcases.ui;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.pages.ui.UIProductsPage;
//import net.lightbody.bmp.core.har.Har;

public class UIProductsPageTest extends TestBase {
	UILoginPage lp;
	UIProductsPage productsPage;
	String methodName;
	
	@Test(groups = { "SANITY" })
	public void validateAddNewProductPageTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Add New Product Modal", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			productsPage = new UIProductsPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Add New Product Modal");
			productsPage.validateAddNewProductPage();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateFilterPopupScreenTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Filter Menu", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			productsPage = new UIProductsPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Filter Menus ");
			productsPage.validateFilterPopupScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateProductTableDataTest() throws Exception {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Products Page Table Data",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		lp = new UILoginPage(driver, locale);
		test.get().log(Status.INFO, "Step 1 : Login ");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.PASS, "Step 1 : Successful Login ");
		productsPage = new UIProductsPage(driver);
		test.get().log(Status.INFO, "Step 2 : Validating  Products Page Table Data");
		productsPage.validateProductTableData();
		test.get().log(Status.PASS, "Step 2 : Validation Complete ");
		test.get().log(Status.INFO, "Step 3 : Logout ");
		lp.logout();
		test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateProductGeneralTabTest() throws Exception {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Products - General tab",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		lp = new UILoginPage(driver, locale);
		test.get().log(Status.INFO, "Step 1 : Login ");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.PASS, "Step 1 : Successful Login ");
		productsPage = new UIProductsPage(driver);
		test.get().log(Status.INFO, "Step 2 : Validating General Tab ");
		productsPage.validateProductGeneralTab();
		test.get().log(Status.PASS, "Step 2 : Validation Complete ");
		test.get().log(Status.INFO, "Step 3 : Logout ");
		lp.logout();
		test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateProductEditPageTest() throws Exception {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Edit Product Modal",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		lp = new UILoginPage(driver, locale);
		test.get().log(Status.INFO, "Step 1 : Login ");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.PASS, "Step 1 : Successful Login ");
		productsPage = new UIProductsPage(driver);
		test.get().log(Status.INFO, "Step 2 : Validating Edit products Modal");
		productsPage.validateProductEditPage();
		test.get().log(Status.PASS, "Step 2 : Validation Complete ");
		test.get().log(Status.INFO, "Step 3 : Logout ");
		lp.logout();
		test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateAddEditVariantTest() throws Exception {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Edit Varients Modal",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		lp = new UILoginPage(driver, locale);
		test.get().log(Status.INFO, "Step 1 : Login ");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.PASS, "Step 1 : Successful Login ");
		productsPage = new UIProductsPage(driver);
		test.get().log(Status.INFO, "Step 2 : Validating Edit Varient");
		productsPage.validateAddEditVariant();
		test.get().log(Status.PASS, "Step 2 : Validation Complete ");
		test.get().log(Status.INFO, "Step 3 : Logout ");
		lp.logout();
		test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateSpecManagementTabAndRequetSpecsTest() throws Exception {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the side Specifications tab",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		lp = new UILoginPage(driver, locale);
		test.get().log(Status.INFO, "Step 1 : Login ");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.PASS, "Step 1 : Successful Login ");
		productsPage = new UIProductsPage(driver);
		test.get().log(Status.INFO, "Step 2 : Validating Edit Varient");
		productsPage.validateSpecManagementTabAndRequetSpecs();
		test.get().log(Status.PASS, "Step 2 : Validation Complete ");
		test.get().log(Status.INFO, "Step 3 : Logout ");
		lp.logout();
		test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
}
