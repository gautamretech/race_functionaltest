package com.race.qa.testcases.functional;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.pages.functional.FunctionalAdministrativePage;
import com.race.qa.pages.functional.FunctionalDashboardPage;
import com.race.qa.pages.functional.FunctionalSupplierPage;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.util.Log;

public class FunctionalSupplierPageTest extends TestBase {

	UILoginPage lp;
	FunctionalSupplierPage fps;
	FunctionalDashboardPage fp;
	FunctionalAdministrativePage fap;
	CommonLocators common;
	private String supplierName;
	private String PROJECTNAME;
	private String methodName;

	public FunctionalSupplierPageTest() {}

	
	/**
	 * Validating the number of Required fields in the Add supplier section
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_1() throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		fps.validateRequiredFields_AddSupplier();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * Add business type, Add annual sales Data and delete annual sales
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_2() throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Business");
		fps.supplierBusinessTab(supplierName);
		fps.addBusinessType(configProperities.getProperty("businesstype"));
		fps.addAnnualSalesData(configProperities.getProperty("familyname"), "2019", 1000, 10.50);
		fps.deleteAnnualSalesData();
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * Add business type, potential categories Delete method needs to be created
	 * 
	 * @throws Exception
	 *//*
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_3() throws Exception {
		lp = new UILoginPage();
		fps = new FunctionalSupplierPage();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Business");
		fps.supplierBusinessTab(supplierName);
		fps.addBusinessType(configProperities.getProperty("businesstype"));
		fps.addPotentialCategoriesData(configProperities.getProperty("familyname"), "2019", 1000, 10);
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	*//**
	 * validating supplier needs ratings message
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_4() throws Throwable {
		WebDriver driver=DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fp = new FunctionalDashboardPage(driver);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		PROJECTNAME = fp.createProjectAPI();
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		returnToDashboardPage();
		fp.getProject(PROJECTNAME, false);
		fp.addSupplierFromProjectScreen(supplierName, false, null, false, false);
		fp.changeTaskStatus("STARTTASK");
		Log.info("Project Started");
		fp.changeTaskStatus("COMPLETETASK");
		Log.info("Project Completed");
		fps.changeSupplierStatus("SELECT", supplierName);
		Assert.assertEquals(driver.findElement(By.xpath("//span[@class='message']")).getText(),
				selectPropertiesFile(true, locale, "supplier_message") + " " + supplierName + " " + "from" + " "
						+ PROJECTNAME + " "
						+ selectPropertiesFile(true, locale, "hasbeenmarkedcompletedpleaseratethesupplier"));
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * RACE-5492 Validating message - currently, message is shown - this is an
	 * issue Blacklisted Supplier
	 * 
	 * @throws Exception
	 *//*
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_5() throws Exception {
		lp = new UILoginPage();
		fp = new FunctionalDashboardPage();
		fps = new FunctionalSupplierPage();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		PROJECTNAME = fp.createProjectAPI();
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "blacklisted"),
				selectPropertiesFile(true, locale, "china"));
		returnToDashboardPage();
		fp.getProject(PROJECTNAME, false);
		fp.addSupplierFromProjectScreen(supplierName, false, null, false, false);
		fp.changeTaskStatus("STARTTASK");
		Log.info("Project Started");
		fp.changeTaskStatus("COMPLETETASK");
		Log.info("Project Completed");
		fps.changeSupplierStatus("SELECT", supplierName);
		Thread.sleep(100);
		Assert.assertTrue(driver.findElements(By.xpath("//span[@class='message']")).size() == 0);
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	*//**
	 * Supplier Ratings - EToE
	 * @throws Throwable 
	 */
		@Test(groups = { "REGRESSION" })
		public void TestSupplier_6() throws Throwable {
			common=new CommonLocators();
			WebDriver driver=DriverFactory.getDriver();
			String locale = getBrowserLocale();
			lp = new UILoginPage(driver,locale);
			fp = new FunctionalDashboardPage(driver);
			fps = new FunctionalSupplierPage(driver);
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			Log.startTestCase(methodName);
		// step 1
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			PROJECTNAME = fp.createProjectAPI();
			clickSideNavigationMenu("Suppliers");
			supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
			returnToDashboardPage();
			fp.getProject(PROJECTNAME, false);
			fp.addSupplierFromProjectScreen(supplierName, false, null, false, false);
			fp.changeTaskStatus("STARTTASK");
			Log.info("Project Started");
			fp.changeTaskStatus("COMPLETETASK");
			Log.info("Project Completed");
			fps.changeSupplierStatus("SELECT", supplierName);
			fps.AssertStatement(common.COMMONMESSAGELOCATOR,selectPropertiesFile(true, locale, "hasbeenmarkedcompletedpleaseratethesupplier"));
			/*Assert.assertEquals(common.COMMONMESSAGELOCATOR.getText(),selectPropertiesFile(true, locale, "supplier_message") + " " + supplierName + " " + "from" + " "
					+ PROJECTNAME + " "+ selectPropertiesFile(true, locale, "hasbeenmarkedcompletedpleaseratethesupplier"));
			*/returnToDashboardPage();
			fp.navigateToRatingTabFromTasks(true, supplierName, PROJECTNAME);
			fps.preRatingsAddProduct_Click(PROJECTNAME, supplierName);
			fp.addProductFromProjectScreen(true, configProperities.getProperty("productname"), PROJECTNAME);
			fps.clickBacktoRatings();
			fps.addProductRatings();
			fps.addSupplierRating(false, 3, configProperities.getProperty("usercriteria"));
		}

	/**
	 * Error Messages
	 * @throws Throwable 
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_7() throws Throwable {
		WebDriver driver=DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fp = new FunctionalDashboardPage(driver);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		PROJECTNAME = fp.createProjectAPI();
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		returnToDashboardPage();
		fp.getProject(PROJECTNAME, false);
		fp.addSupplierFromProjectScreen(supplierName, false, null, false, false);
		fp.changeTaskStatus("STARTTASK");
		Log.info("Project Started");
		fp.changeTaskStatus("COMPLETETASK");
		Log.info("Project Completed");
		fps.changeSupplierStatus("SELECT", supplierName);
		Assert.assertEquals(driver.findElement(By.xpath("//span[@class='message']")).getText(),
		selectPropertiesFile(true, locale, "supplier_message") + " " + supplierName + " " + "from" + " "
		+ PROJECTNAME + " "+ selectPropertiesFile(true, locale, "hasbeenmarkedcompletedpleaseratethesupplier"));
		returnToDashboardPage();
		fp.navigateToRatingTabFromTasks(true, supplierName, PROJECTNAME);
		fps.preRatingsAddProduct_Click(PROJECTNAME, supplierName);
		fp.addProductFromProjectScreen(true, configProperities.getProperty("productname"), PROJECTNAME);
		fps.clickBacktoRatings();
		fps.ratinsPageErrorMessageOnSave();
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * Add catalog as Admin - without batch upload
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_8() throws Exception {
		WebDriver driver=DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Catalog");
		fps.validateCatalogPage();
		fps.addCatalogData(false, null);
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * add catalog - batch upload with correct file
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_9() throws Exception {
		WebDriver driver=DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Catalog");
		fps.validateCatalogPage();
		fps.addCatalogData(true, "CatalogBatchupload.xlsx");
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * catalog batch upload - bad file content
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_10() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Catalog");
		fps.validateCatalogPage();
		fps.addCatalogData(true, "Badbatchupload.xlsx");
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * delete a catalog
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_11() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Catalog");
		fps.validateCatalogPage();
		fps.addCatalogData(false, null);
		fps.deleteCatalogData();
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * Recommened a product as admin
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_12() throws Exception {
		
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Catalog");
		fps.validateCatalogPage();
		fps.addCatalogData(false, null);
		fps.recomendProductAsAdmin();
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * choose a Top product - expected error message
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_13() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		// search supplier
		fps.getSupplier(supplierName);
		fps.supplierTabs("Catalog");
		fps.validateCatalogPage();
		fps.addCatalogData(false, null);
		fps.chooseTopProductAsAdmin();
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * RACE-5490 created Factories and Maps
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_14() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		fps.getSupplier(supplierName);
		fps.supplierTabs("Factories");
		fps.addNewFactorySuccess(configProperities.getProperty("factoryname"), "Active", "Owned", "China");
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * RACE-5940 created Factory without proper address - error message
	 * displayed on map
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_15() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		fps.getSupplier(supplierName);
		fps.supplierTabs("Factories");
		fps.addNewFactorywithoutAddress(configProperities.getProperty("factoryname"), "Active", "Owned", "China");
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * Validate add factory page items
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_16() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		fps.getSupplier(supplierName);
		fps.supplierTabs("Factories");
		fps.validateFactoryPage();
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * Create supplier contact
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_17() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		fps.getSupplier(supplierName);
		fps.supplierTabs("Contacts");
		fps.addContactDetails("China");
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}

	/**
	 * Validate contact page
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_18() throws Exception {
		WebDriver driver= DriverFactory.getDriver();
		String locale=getBrowserLocale();
		lp = new UILoginPage(driver,locale);
		fps = new FunctionalSupplierPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Suppliers");
		supplierName = fps.createSupplier(selectPropertiesFile(false, locale, "active"),
				selectPropertiesFile(true, locale, "china"));
		fps.getSupplier(supplierName);
		fps.supplierTabs("Contacts");
		fps.validateContact();
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
	}
	/*
	@Test(groups = { "REGRESSION" })
	public void TestSupplier_4719_deliverable() throws Exception {
		lp = new UILoginPage();
		fp = new FunctionalDashboardPage();
		fps = new FunctionalSupplierPage();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// Remove supplier notifications
		lp.login(configProperities.getProperty("suppliercontact"),
		configProperities.getProperty("commonpassword"));
		fp.deleteNotificationMessage(true, fp.getNotificationCount());
		returnToDashboardPage(); lp.interim_logout();
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		PROJECTNAME = fp.createProjectAPI();
		fp.getProject(PROJECTNAME, false);
		fp.addSupplierFromProjectScreen(configProperities.getProperty("funcsuppliername"), true,
				configProperities.getProperty("funccontactname"), false, true);
		returnToDashboardPage();
		lp.interim_logout();
		lp.login(configProperities.getProperty("suppliercontact"), configProperities.getProperty("commonpassword"));
		Integer BEFORE_TASKCOUNT = fp.getTaskCounts("taskending"); Log.info(
		"The count before Task is :" + BEFORE_TASKCOUNT);
		fp.deleteNotificationMessage(true, fp.getNotificationCount());
		Assert.assertEquals(fp.getNotificationCount(), 0);
		fp.getProject(PROJECTNAME, true);
		fp.submitDeliverables(1, 1);
		returnToDashboardPage();
		lp.interim_logout();
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, false);
		fp.assignApprovalUserDeliverable("PM", null, 1, false, true, configProperities.getProperty("groupleadname"));
		Log.info("Assigned to PM");
		returnToDashboardPage();
		lp.interim_logout();
		lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, true);
		fp.approvalActionDeliverable("APPROVE", 1, true);// can be used for a
															// non assigned
		// user test case - add this in test
		// sheet
		lp.interim_logout();
		Log.info("Approved by PM");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, true);
		fp.assignApprovalUserDeliverable("COLAB", null, 1, false, true, configProperities.getProperty("groupleadname"));
		lp.interim_logout();
		Log.info("Assigned to Colab");
		lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, true);
		fp.approvalActionDeliverable("APPROVE", 1, true);
		lp.interim_logout();
		Log.info("Approved By Colab");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, true);
		fp.assignApprovalUserDeliverable("ADMIN", "APPROVE", 1, false, true, configProperities.getProperty("groupleadname"));
		returnToDashboardPage();
		lp.logout();
		Log.endTestCase(methodName);
		
	}

	@Test(groups = { "REGRESSION" })
	public void TestSupplier_4719_design() throws Exception {
		lp = new UILoginPage();
		fp = new FunctionalDashboardPage();
		fps = new FunctionalSupplierPage();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// Remove supplier notifications
		lp.login(configProperities.getProperty("suppliercontact"),
		configProperities.getProperty("commonpassword"));
		fp.deleteNotificationMessage(true, fp.getNotificationCount());
		returnToDashboardPage(); 
		lp.interim_logout();
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		PROJECTNAME = fp.createProjectAPI();
		fp.getProject(PROJECTNAME, false);
		fp.addSupplierFromProjectScreen(configProperities.getProperty("funcsuppliername"), true,
				configProperities.getProperty("funccontactname"), true, false);
		returnToDashboardPage();
		lp.interim_logout();
		lp.login(configProperities.getProperty("suppliercontact"), configProperities.getProperty("commonpassword"));
		Integer BEFORE_TASKCOUNT = fp.getTaskCounts("taskending"); Log.info(
		"The count before Task is :" + BEFORE_TASKCOUNT);
		fp.deleteNotificationMessage(true, fp.getNotificationCount());
		Assert.assertEquals(fp.getNotificationCount(), 0);
		fp.getProject(PROJECTNAME, true);
		fp.addDesignFolder(1, 1, false);
		fp.submitDesign(1, 1, false);
		fp.selectDesignVersion(1, 1);
		returnToDashboardPage();
		lp.interim_logout();
		lp.login(configProperities.getProperty("adminusername"),
		configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, true); 
		fp.assignApprovalStepUser("PM",null, 1, 1, false, true, configProperities.getProperty("groupleadname"));
		Log.info("Assigned to PM");
		returnToDashboardPage(); 
		lp.interim_logout();
		lp.login(configProperities.getProperty("pmusername"),
		configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, true); fp.approvalAction(1, 1, "APPROVE",true);// can be used for a non assigned // user test case - add this
		//in test // sheet 
		lp.interim_logout();
		Log.info("Approved by PM");
		lp.login(configProperities.getProperty("adminusername"),
		configProperities.getProperty("commonpassword"));
		fp.getProject(PROJECTNAME, true); fp.assignApprovalStepUser("COLAB",
		 null, 1, 1, false, true, configProperities.getProperty("groupleadname")); 
		lp.interim_logout(); Log.info(
		 "Assigned to Colab");
		 lp.login(configProperities.getProperty("collabusername"),
		 configProperities.getProperty("commonpassword"));
		 fp.getProject(PROJECTNAME, true); fp.approvalAction(1, 1, "APPROVE",
		 true); lp.interim_logout(); Log.info("Approved By Colab");
		 lp.login(configProperities.getProperty("adminusername"),
		 configProperities.getProperty("commonpassword"));
		 fp.getProject(PROJECTNAME, true); fp.assignApprovalStepUser("ADMIN",
		 "APPROVE", 1, 1, false, true, configProperities.getProperty("groupleadname")); 
		 returnToDashboardPage(); 
		 lp.logout();
		 Log.endTestCase(methodName);
		
	}
		  @AfterMethod(alwaysRun = true) public void tearDown(ITestResult
		 iTestResult) { if (iTestResult.getStatus() == ITestResult.FAILURE) {
		  driver.close(); } driver.quit();  This is for browser mob proxy not
		 * in use currently
		 * 
		 * Har har = proxy.getHar(); File harFile = new File(sFileName); try {
		 * har.writeTo(harFile); } catch (IOException ex) { System.out.println
		 * (ex.toString()); System.out.println("Could not find file " +
		 * sFileName); } if (driver != null) { proxy.stop();
		 

	}
*/
}
