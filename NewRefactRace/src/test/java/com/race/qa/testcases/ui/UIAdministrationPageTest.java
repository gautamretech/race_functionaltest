package com.race.qa.testcases.ui;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.locator.CommonLocators;
import com.race.qa.pages.ui.UIAdministrationPage;
import com.race.qa.pages.ui.UILoginPage;
//import net.lightbody.bmp.core.har.Har;

public class UIAdministrationPageTest extends TestBase {
	UILoginPage loginPage;
	UIAdministrationPage administrationPage;
	CommonLocators common;
	String methodName;

	@Test(groups = { "SANITY" })
	public synchronized void validateGeneralTabTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the General tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			loginPage = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			loginPage.login(configProperities.getProperty("adminusername"),
					configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			administrationPage = new UIAdministrationPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating General Tab");
			administrationPage.validateGeneralTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			loginPage.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validatePlatformSettingsTabTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Platform Settings tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "inside " + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			loginPage = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			loginPage.login(configProperities.getProperty("adminusername"),
					configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			administrationPage = new UIAdministrationPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Platform Settings ");
			administrationPage.validatePlatformSettingsTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			loginPage.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateProductsTabTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Products tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "inside " + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			loginPage = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			loginPage.login(configProperities.getProperty("adminusername"),
					configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			administrationPage = new UIAdministrationPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Products Tab");
			administrationPage.validateProductsTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			loginPage.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateCategoryStructureTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Category Structure", ExtentColor.AMBER));
		test.get().log(Status.INFO,
				"Starting Test Case ID " + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			loginPage = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			loginPage.login(configProperities.getProperty("adminusername"),
					configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			administrationPage = new UIAdministrationPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Category Structure");
			administrationPage.validateCategoryStructure();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			loginPage.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateSupplierTabTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Supplier tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "inside " + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			loginPage = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			loginPage.login(configProperities.getProperty("adminusername"),
					configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			administrationPage = new UIAdministrationPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Supplier Tab");
			administrationPage.validateSupplierTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			loginPage.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateFormTabTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Forms tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "inside " + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			loginPage = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			loginPage.login(configProperities.getProperty("adminusername"),
					configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			administrationPage = new UIAdministrationPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Forms Tab");
			administrationPage.validateFormTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			loginPage.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateReviewTabTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Review tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "inside " + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			loginPage = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			loginPage.login(configProperities.getProperty("adminusername"),
					configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			administrationPage = new UIAdministrationPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Review Tab");
			administrationPage.validateReviewTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			loginPage.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
}
