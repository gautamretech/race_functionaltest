package com.race.qa.testcases.ui;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.util.Log;

public class UILoginPageTest extends TestBase {

	UILoginPage loginPage;
	String methodName;

	@Test(groups = { "SANITY" })
	public void validateLoginTest() throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName() + "   " + Thread.currentThread().getId();
		Log.startTestCase(methodName);
		loginPage = new UILoginPage(driver, locale);
		loginPage.login(configProperities.getProperty("adminusername"),
				configProperities.getProperty("commonpassword"));
		loginPage.logout();

	}

	@Test(groups = { "SANITY" })
	public void validateLoginTest1() throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName() + "   " + Thread.currentThread().getId();
		Log.startTestCase(methodName);
		loginPage = new UILoginPage(driver, locale);
		loginPage.login(configProperities.getProperty("adminusername"),
				configProperities.getProperty("commonpassword"));
		loginPage.logout();

	}

	/*
	 * @Test public void validateForgetPasswordPageTest() throws
	 * InterruptedException { String Locale =
	 * configProperities.getProperty("language"); methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); loginPage = new UILoginPage(); try {
	 * loginPage.validateForgetPasswordPage(Locale); } catch (Exception e) { //
	 * TODO Auto-generated catch block e.printStackTrace(); } }
	 */

	
}
