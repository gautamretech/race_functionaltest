/*package com.race.qa.testcases.functional;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.race.qa.base.TestBase;
import com.race.qa.pages.functional.FunctionalProductsPage;
import com.race.qa.pages.functional.FunctionalSupplierPage;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.util.Log;

public class FunctionalProductsPageTest extends TestBase{
	UILoginPage lp;
	FunctionalProductsPage fpp;
	public FunctionalProductsPageTest(){
		super();
	}
	@BeforeMethod(alwaysRun = true)
	public void setUp() {
		try {
			initialization();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test(groups = { "REGRESSION" })
	public void TestProduct_1() throws Exception {
		lp = new UILoginPage();
		fpp = new FunctionalProductsPage();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		clickSideNavigationMenu("Products");
		
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult iTestResult) {
		if (iTestResult.getStatus() == ITestResult.FAILURE) {
			driver.close();
		}
		driver.quit();
		
		 * This is for browser mob proxy not in use currently
		 * 
		 * Har har = proxy.getHar(); File harFile = new File(sFileName); try {
		 * har.writeTo(harFile); } catch (IOException ex) { System.out.println
		 * (ex.toString()); System.out.println("Could not find file " +
		 * sFileName); } if (driver != null) { proxy.stop();
		 

	}

}
*/