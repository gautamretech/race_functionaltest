package com.race.qa.testcases.ui;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.functional.FunctionalDashboardPage;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.pages.ui.UISupplierPage;
import com.race.qa.pages.ui.UIDashboardPage;
import com.race.qa.util.Log;

public class UIDashboardPageTest extends TestBase {
	UISupplierPage sp;
	UILoginPage lp;
	UIDashboardPage ui;
	FunctionalDashboardPage fp;
	protected String PROJECTNAME;
	String methodName;

	@Test(groups = { "REGRESSION" })
	public void uiValidateCreateProjectTest() throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		lp = new UILoginPage(driver, locale);
		ui = new UIDashboardPage(driver);
		Log.startTestCase(methodName);
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		ui.createDuplicateProject(false);
		lp.logout();
		Log.endTestCase(methodName);
	}

	@Test(groups = { "SANITY" })
	public void validateDashboardMenusTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the side Menus on the Navigation tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Navigation Menus ");
			ui.validateDashboardMenus();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateTaskMenusTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Validating Dashboard Menus if have tasks and clickable- Open Tasks, Tasks Ending,"
								+ " Delayed Tasks, Pending Specs and Recent Submissions. Tests also performed on "
								+ " If tasks has count and view more button is displayed and clickable",
						ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Dashboard Menus ");
			ui.validateDashboardTaskMenu();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateProjectStatusTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel(
				"Validating Project Status - UI designs, border colors as per status and Texts", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Project Status ");
			ui.validateProjectStatus();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateDashboardFiltersTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper
				.createLabel("Validating the Dashboard Filter Menus are working and clickable", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Dashboard Filter Menus ");
			ui.validateDashboardFilters();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "REGRESSION" })
	public void validateProjectSpace() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		try {
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			System.out.println(methodName + Thread.currentThread().getId());
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			Log.startTestCase(methodName);
			lp = new UILoginPage(driver, locale);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			// fp = new FunctionalDashboardPage();
			ui = new UIDashboardPage(driver);
			PROJECTNAME = fp.createProject(false);
			fp.returnToDashboardPage();
			fp.getProject(PROJECTNAME, false);
			ui.validateProjectSpace(PROJECTNAME);
			lp.logout();
			test.get().log(Status.INFO, "completed " + Thread.currentThread().getStackTrace()[1].getMethodName());
			Log.endTestCase(methodName);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "REGRESSION" })
	public void validateEditTaskScreentest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		try {
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			System.out.println(methodName + Thread.currentThread().getId());
			Log.startTestCase(methodName);
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			lp = new UILoginPage(driver, locale);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fp = new FunctionalDashboardPage(driver);
			PROJECTNAME = fp.createProject(false);
			returnToDashboardPage();
			fp.getProject(PROJECTNAME, false);
			fp.clickOnEditTaskButton();
			ui = new UIDashboardPage(driver);
			ui.validateEditTaskPage();
			lp.logout();
			test.get().log(Status.INFO, "completed " + Thread.currentThread().getStackTrace()[1].getMethodName());
			Log.endTestCase(methodName);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "REGRESSION" })
	public void validateDeliverablesScreenTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		try {
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			System.out.println(methodName + Thread.currentThread().getId());
			Log.startTestCase(methodName);
			lp = new UILoginPage(driver, locale);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fp = new FunctionalDashboardPage(driver);
			PROJECTNAME = fp.createProject(false);
			returnToDashboardPage();
			fp.getProject(PROJECTNAME, false);
			fp.clickOnEditTaskButton();
			ui = new UIDashboardPage(driver);
			ui.validateDeliverablesScreen();
			lp.logout();
			test.get().log(Status.INFO, "completed " + Thread.currentThread().getStackTrace()[1].getMethodName());
			Log.endTestCase(methodName);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "REGRESSION" })
	public void validateDesignScreenTest() throws Throwable {
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		try {
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			Log.startTestCase(methodName);
			lp = new UILoginPage(driver, locale);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fp = new FunctionalDashboardPage(driver);
			PROJECTNAME = fp.createProject(false);
			returnToDashboardPage();
			fp.getProject(PROJECTNAME, false);
			fp.clickOnEditTaskButton();
			ui = new UIDashboardPage(driver);
			ui.validateDesignScreen();
			lp.logout();
			test.get().log(Status.INFO, "completed " + Thread.currentThread().getStackTrace()[1].getMethodName());
			Log.endTestCase(methodName);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateTaskCountsTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Dashboared Menus Task counts", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Dashboard Menus Task counts");
			ui.validateTaskCounts();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateTableMyDashboardTabTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating Dashboard Project list table", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Dashboard Project List");
			ui.validateTableMyDashboardTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateTaskSectionTest() throws Throwable {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Task section Data", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Task Section");
			ui.validateTaskSection();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateSuppliersTabTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Supplier Dashboard", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Supplier DashBoard");
			ui.validateSuppliersTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateSuppliersTabBestPartnersTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Supplier's Dashboard - Best Partners", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Best Partner's section ");
			ui.validateSuppliersTabBestPartners();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateSuppliersTabRecomendedProductsTest() throws Exception {
		test.get().log(Status.INFO, MarkupHelper
				.createLabel("Validating the Supplier's Dashboard - Recommended Products", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Navigation Menus ");
			ui.validateSuppliersTabRecomendedProducts();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateProjectDetailsPageTest() throws Exception {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Projects Details Page elements", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Navigation Menus ");
			ui.validateProjectDetailsPage();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateAddSupplierInProjectsDetailsPage() throws Throwable {
		test.get().log(Status.INFO, MarkupHelper
				.createLabel("Validating the Supplier addition from Projects Details Page", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			ui = new UIDashboardPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Navigation Menus ");
			ui.validateProjectDetailsPageAddElements();
			sp = new UISupplierPage(driver);
			sp.AddNewSupplierInProjectDetailsPage();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	/*
	 * @Test(groups = { "SANITY" }) public void
	 * validateAddProductInProjectsDetailsPage() throws InterruptedException {
	 * WebDriver driver = DriverFactory.getInstance().getDriver(); String locale
	 * = getBrowserLocale(); configProperities = loadConfigFile(); ProductsPage
	 * productsPage; methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); lp = new LoginPage();
	 * lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword")); ui = new
	 * UIDashboardPage(); ui.validateProjectDetailsPageAddElements();
	 * productsPage = new ProductsPage();
	 * productsPage.AddNewProductInProjectDetailsPage(); lp.logout();
	 * Log.endTestCase(methodName); }
	 */

}
