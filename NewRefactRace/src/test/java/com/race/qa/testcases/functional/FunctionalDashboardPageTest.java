package com.race.qa.testcases.functional;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.functional.FunctionalDashboardPage;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.util.Log;

public class FunctionalDashboardPageTest extends TestBase {

	UILoginPage lp;
	FunctionalDashboardPage fdp;
	private String PROJECTNAME;
	String methodName;

	/**
	 * The "count of Open Task" will not be reduced from Assignee's open tasks,
	 * if the Task is auto-complete with Task status as "Not Started"
	 * 
	 * @throws Exception
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_1() throws Throwable {
		test.get().log(Status.INFO, MarkupHelper.createLabel(
				"Assignee's Open Task count will not reduce, if assigned Task is not started", ExtentColor.AMBER));
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Pre-Requisite - 1. Auto-Complete is 'Yes' 2.Design without Approval/Loop 3. Task not Started",
						ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test Case...");
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		String taskname;
		Integer BEFORE_TASKCOUNT;
		Integer AFTER_TASKCOUNT;
		Integer FINAL_TASKCOUNT;
		String TASKSTATUS;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.INFO,
					"Step 1 : Login as colaborator, get The current Task Count and deleted all existing Notifications");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			Assert.assertEquals(fdp.getNotificationCount(), 0);
			test.get().log(Status.PASS, "Current Task Count is :" + BEFORE_TASKCOUNT
					+ " and Current notification count is " + fdp.getNotificationCount());
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 2 : Login as Admin, create a project, Add a Design with No Approval, Task not started by Admin,"
							+ "Task Assigned to Colaborator User, Delete all existing notifications");
			PROJECTNAME = fdp.createProjectAPI();
			test.get().log(Status.INFO, "Project Created " + PROJECTNAME);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			taskname = fdp.getTaskName();
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			test.get().log(Status.PASS,
					"Project Created with Design Task as No Approval, Task Assigned to Colaborator");
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		test.get().log(Status.INFO,
				"Step 3: Login as Colaborator User, Check for Increased Task Count and Read Notification,");
		try {
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			Integer tempCount = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(tempCount, AFTER_TASKCOUNT, "A mismatch due to Refresh is possible at this stage");
			Log.info("Task count is Increased");
			// Notifications- Expected Assignment notification
			Assert.assertEquals(fdp.getNotificationCount(), 1);
			Assert.assertEquals(fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()), taskname),
					true);
			test.get().log(Status.INFO,
					"Task Assignment Notification - " + fdp.readNotification(fdp.getNotificationCount()));
			fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			Assert.assertEquals(fdp.getNotificationCount(), 0);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 4  : Add Design Folder");
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
			test.get().log(Status.PASS, "Step 4 ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 5  : Submit Design");
			fdp.submitDesign(1, 1, false);
			test.get().log(Status.PASS, "Step 5 ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 6  : Select Design");
			fdp.selectDesignVersion(1, 1);
			test.get().log(Status.PASS, "Step 6");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 7  : Asserting Task Count is not changed and Project Status is Not started");
			returnToDashboardPage();
			FINAL_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Assert.assertEquals(AFTER_TASKCOUNT, FINAL_TASKCOUNT);
			Log.info("Task count did not changed");
			fdp.getProject(PROJECTNAME, false);
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "notstarted"));
			test.get().log(Status.PASS, "Step 7  " + " Task Count before Assignment is  : " + BEFORE_TASKCOUNT
					+ " After Assignment Task Count is : " + AFTER_TASKCOUNT + " After Select Design Task Count : "
					+ FINAL_TASKCOUNT + "Current Task Status : " + TASKSTATUS);
			lp.logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * The "count of Open Task" will be reduced from assignee's tasks, if the
	 * Task is auto-complete with Task status as "In Progress". And the Task
	 * Status will be Updated as "MARK COMPLETE" with the select of design
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_2() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("The count of Open Task will be reduced from assignee's tasks, if the"
						+ " Task is auto-complete with Task status as 'In Progress'. And the Task"
						+ "Status will be Updated as 'MARK COMPLETE' with the select of design", ExtentColor.AMBER));
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Pre-Requisite - 1. Auto-Complete is 'Yes' 2.Design without Approval/Loop 3. Task Started By Assignee",
						ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test Case...");
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		String BEFORE_TASKSTATUS;
		String taskname;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		Integer BEFORE_TASKCOUNT;
		Integer AFTER_TASKCOUNT;
		Integer FINAL_TASKCOUNT;
		String FINAL_TASKSTATUS;
		Integer INTERIM;
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		test.get().log(Status.INFO,
				"Step 1 : Login as colaborator, get The current Task Count and deleted all existing Notifications");
		try {
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			test.get().log(Status.PASS, "Current Task Count is : " + BEFORE_TASKCOUNT
					+ " and Current notification count is " + fdp.getNotificationCount());
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 2 : Login as Admin, create a project, Add a Design with No Approval, Task not started by Admin,"
							+ "Task Assigned to Colaborator User, Delete all existing notifications");
			PROJECTNAME = fdp.createProjectAPI();
			test.get().log(Status.INFO, "Project Created " + PROJECTNAME);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			taskname = fdp.getTaskName();
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			test.get().log(Status.PASS,
					"Project Created with Design Task as No Approval, Task Assigned to Colaborator");
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 3: Login as Colaborator User, Check for Increased Task Count and Read Notification");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			INTERIM = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "This error come due to an Ajax related error");
			Log.info("Task count is Increased");
			test.get().log(Status.INFO,
					"Task Assignment Notification - " + fdp.readNotification(fdp.getNotificationCount()));
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 4 : Starting Design Task as Colaborator");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			test.get().log(Status.INFO, "Task Started By Assignee, Current Task Status is   " + BEFORE_TASKSTATUS);
			// Step 6
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 5 : Add Design Folder");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 6 : Submit Design");
			fdp.submitDesign(1, 1, false);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 7 : Select Design");
			fdp.selectDesignVersion(1, 1);
			FINAL_TASKSTATUS = fdp.getTaskStatus();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		} // Step 7
		try {
			test.get().log(Status.INFO, "Step 8 : Asserting Current Status to be Marked Complete");
			returnToDashboardPage();
			FINAL_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Assert.assertEquals(BEFORE_TASKCOUNT, FINAL_TASKCOUNT);
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "markedcomplete"));
			test.get().log(Status.PASS, "Before Task Count  :" + BEFORE_TASKCOUNT + " After Task count "
					+ FINAL_TASKCOUNT + " Final Task Status is " + FINAL_TASKSTATUS);
			lp.logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * The "count of Open Task" will not be reduced, if the Task is
	 * auto-complete with Task status as "In Progress". And the Task Status will
	 * not be Updated as "MARK COMPLETE" on submit of design
	 * 
	 * @throws Throwable
	 * 
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_3() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("The 'count of Open Task' will not be reduced, if the Task is"
						+ " auto-complete with Task status as 'In Progress'. And the Task Status will"
						+ " not be Updated as 'MARK COMPLETE' on submit of design", ExtentColor.AMBER));
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Pre-Requisite - 1. Auto-Complete is 'Yes' 2.Design without Approval/Loop 3. Task is Started  4. Design is not selected",
						ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test Case...");
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		Integer BEFORE_TASKCOUNT;
		Integer AFTER_TASKCOUNT;
		String BEFORE_TASKSTATUS;
		String FINAL_TASKSTATUS;
		Integer FINAL_TASKCOUNT;
		Integer INTERIM;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		test.get().log(Status.INFO,
				"Step 1 : Login as colaborator, get The current Task Count and deleted all existing Notifications");
		try {
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			test.get().log(Status.PASS, "Current Task Count is :" + BEFORE_TASKCOUNT
					+ " and Current notification count is " + fdp.getNotificationCount());
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		test.get().log(Status.INFO,
				"Step 2 : Login as Admin, create a project, Add a Design with No Approval, Task not started by Admin,"
						+ "Task Assigned to Colaborator User, Delete all existing notifications");
		try {
			PROJECTNAME = fdp.createProjectAPI();
			test.get().log(Status.INFO, "Project Created " + PROJECTNAME);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS,
					"Project Created with Design Task as No Approval, Task Assigned to Colaborator");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		test.get().log(Status.INFO,
				"Step 3: Login as Colaborator User, Check for Increased Task Count and Read Notification,");
		try {
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			INTERIM = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT);
			Log.info("Task count is Increased");
			returnToDashboardPage();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 4  :Starting  Task by Assignee");
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 5  : Add Design Folder");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 6  : Submit Design");
			fdp.submitDesign(1, 1, false);
			test.get().log(Status.INFO, "Design not selected");
			FINAL_TASKSTATUS = fdp.getTaskStatus();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			returnToDashboardPage();
			FINAL_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Assert.assertEquals(AFTER_TASKCOUNT, FINAL_TASKCOUNT);
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS,
					"Task Count before Assignment is  : " + BEFORE_TASKCOUNT + " After Assignment Task Count is : "
							+ AFTER_TASKCOUNT + " After Submit Design Task Count : " + FINAL_TASKCOUNT
							+ "Current Task Status : " + FINAL_TASKSTATUS);
			lp.logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * The Task will not be complete if "Approval Group is used" on select
	 * design and the count will get not decreased from assignee's open Task As
	 * the same task is now available for approval
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_4() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Task will not be Marked Completed for Design with Group, And the Task count will also not be reduced on selection of the design",
						ExtentColor.AMBER));
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Pre-Requisite - 1. Auto-Complete is 'Yes' 2.Design with GROUP 3. Task Started by Assignee",
						ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test Case...");
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		Integer BEFORE_TASKCOUNT;
		Integer AFTER_TASKCOUNT;
		String FINAL_TASKSTATUS;
		Integer INTERIM;
		String BEFORE_TASKSTATUS;
		Log.startTestCase(methodName);
		Integer FINAL_TASKCOUNT;
		try {
			test.get().log(Status.INFO,
					"Step 1 : Login as colaborator, get The current Task Count and deleted all existing Notifications");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 2 : Login as Admin, create a project, Add a Design with No Approval, Task not started by Admin,"
							+ " Task Assigned to Colaborator User, Delete all existing Admin notifications");
			PROJECTNAME = fdp.createProjectAPI();
			test.get().log(Status.INFO, "Project Created " + PROJECTNAME);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("GROUP", 1, null, configProperities.getProperty("groupname"));
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 3: Login as Colaborator User, Check for Increased Task Count and Read Notification");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			INTERIM = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "mismatch due to ajax calls refresh issue");
			Log.info("Task count is Increased");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 4  Start Task");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			test.get().log(Status.INFO, "Current Task Status is  " + BEFORE_TASKSTATUS);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 5 : Add Design Folder");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 6 : Submit Design");
			fdp.submitDesign(1, 1, false);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 7: Select Design Version");
			fdp.selectDesignVersion(1, 1);
			Log.info("Design is selected");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			FINAL_TASKSTATUS = fdp.getTaskStatus();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			returnToDashboardPage();
			FINAL_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "mismatch due to ajax calls refresh issue");
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			lp.logout();
			test.get().log(Status.PASS,
					"Task Count before Assignment is  : " + BEFORE_TASKCOUNT + " After Assignment Task Count is : "
							+ AFTER_TASKCOUNT + " After Submit Design Task Count : " + FINAL_TASKCOUNT
							+ "Current Task Status : " + FINAL_TASKSTATUS);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/*
	 * The Task will not be complete if "Approval Loop is used" on select design
	 * and the count will get decreased from assignee's open Task.
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_5() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Task will not be Marked Completed for Design with Approval LOOP, BUT the Task count will be reduced on selection of the design",
						ExtentColor.AMBER));
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Pre-Requisite - 1. Auto-Complete is 'Yes' 2.Design with LOOP 3. Task Started by Assignee",
						ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test Case...");
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		Integer BEFORE_TASKCOUNT;
		Integer AFTER_TASKCOUNT;
		Integer INTERIM;
		String BEFORE_TASKSTATUS;
		String FINAL_TASKSTATUS;
		Integer FINAL_TASKCOUNT;
		try {
			test.get().log(Status.INFO,
					"Step 1 : Login as colaborator, get The current Task Count and deleted all existing Notifications");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 2 : Login as Admin, create a project, Add a Design with No Approval, Task not started by Admin,"
							+ "Task Assigned to Colaborator User, Delete all existing Admin notifications");
			PROJECTNAME = fdp.createProjectAPI();
			test.get().log(Status.INFO, "Project Created " + PROJECTNAME);
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("LOOP", 1, configProperities.getProperty("pathname"), null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO,
					"Step 3: Login as Colaborator User, Check for Increased Task Count and Read Notification");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			INTERIM = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "WS error related to refresbehd task counts");
			Log.info("Task count is Increased");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 4  Start Task");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			test.get().log(Status.INFO, "Current Task Status is  " + BEFORE_TASKSTATUS);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 5 : Add Design Folder");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 6 : Submit Design");
			fdp.submitDesign(1, 1, false);
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 7: Select Design Version");
			fdp.selectDesignVersion(1, 1);
			Log.info("Design is selected");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.INFO, "Step 8 : Final Step");
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			returnToDashboardPage();
			FINAL_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Assert.assertEquals(BEFORE_TASKCOUNT, FINAL_TASKCOUNT);
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS,
					"Task Count before Assignment is  : " + BEFORE_TASKCOUNT + " After Assignment Task Count is : "
							+ AFTER_TASKCOUNT + " After Select Design Task Count : " + FINAL_TASKCOUNT
							+ "Current Task Status : " + FINAL_TASKSTATUS);
			lp.logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Change Task Status button will not display for assignee, once the
	 * assignee starts the task, user in Group.
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_6() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel(
						"Assignee Task count will not change, on auto - complete design Task, if Design Task is 'No - Approval'"
								+ " and Task is not Started",
						ExtentColor.AMBER));
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String TASKSTATUS;
		boolean taskStatusButton;
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			// Step 1
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("GROUP", 1, null, configProperities.getProperty("groupname"));
			// Step 2
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"),
					configProperities.getProperty("" + "commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			taskStatusButton = fdp.isTaskStausButtonDisplayed();
			Assert.assertEquals(taskStatusButton, true);
			lp.logout();
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Change Task Status button will not display for assignee, once the
	 * assignee starts the task, user in LOOP.
	 * 
	 * @throws Throwable
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_7() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String TASKSTATUS;
		boolean taskStatusButton;
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("LOOP", 1, configProperities.getProperty("pathname"), null); // Step
																						// 2
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			taskStatusButton = fdp.isTaskStausButtonDisplayed();
			Assert.assertEquals(taskStatusButton, true);
			lp.logout();
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Change Task Status button will display for Assigned in no Group/Loop with
	 * submission
	 * 
	 * @throws Throwable
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_8() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String TASKSTATUS;
		boolean taskStatusButton;
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.submitDesign(1, 1, false);
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 5");
			fdp.selectDesignVersion(1, 1);
			test.get().log(Status.PASS, "Step 5");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 5");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			taskStatusButton = fdp.isTaskStausButtonDisplayed();
			Assert.assertEquals(taskStatusButton, true);
			lp.logout();
			test.get().log(Status.PASS, "Step 5");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Change Task Status button will display for Admin user in Group/Loop
	 * without submission/Finalize.
	 * 
	 * @throws Throwable
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_9() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		String TASKSTATUS;
		boolean taskStatusButton;
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("GROUP", 1, null, configProperities.getProperty("groupname"));
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			returnToDashboardPage();
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.submitDesign(1, 1, false);
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 5");
			fdp.selectDesignVersion(1, 1);
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 5");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 6");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			taskStatusButton = fdp.isTaskStausButtonDisplayed();
			Assert.assertEquals(taskStatusButton, false);
			lp.logout();
			test.get().log(Status.PASS, "Step 6");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task will not get Mark complete, if 1 design added, 2 folders added and
	 * submitted on both folders but only one is selected
	 * 
	 * @throws Throwable
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_10() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String TASKSTATUS;
		String FINAL_TASKSTATUS;
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(2, 2, false);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.submitDesign(2, 1, false);
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.selectDesignVersion(1, 1);
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 5");
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			lp.logout();
			test.get().log(Status.PASS, "Step 5");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task will not get Mark complete, if 1 design added, 2 folders added and
	 * submitted in 1 folder and select the design.
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_11() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String TASKSTATUS;
		String FINAL_TASKSTATUS;
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			// Step 2
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(2, 1, false);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.submitDesign(1, 1, false);
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 5");
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			lp.logout();
			test.get().log(Status.PASS, "Step 5");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task will not get Mark complete, if 1 design added, 2 folders added and
	 * submitted in 2 folder .
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_12() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		String TASKSTATUS;
		String FINAL_TASKSTATUS;
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); // Step
																														// 4
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(2, 1, false);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.submitDesign(2, 1, false);
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			lp.logout();
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task will get Mark complete, if 1 design added, 2 folders added and
	 * submitted in 2 folder and 2 selected designs
	 * 
	 * @throws Throwable
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_13() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		String TASKSTATUS;
		String FINAL_TASKSTATUS;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); // Step
																														// 4
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			System.out.println(TASKSTATUS = fdp.getTaskStatus());
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(2, 1, false);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.submitDesign(2, 1, false);
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 5");
			fdp.selectDesignVersion(2, 1);
			test.get().log(Status.PASS, "Step 5");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 6");
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "markedcomplete"));
			lp.logout();
			test.get().log(Status.PASS, "Step 6");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task will get Mark complete, if 1 design added, 1 folders added and
	 * submitted in 1 folder and 1 selected designs in Loop
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_14() throws Throwable {

		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		String BEFORE_TASKSTATUS;
		String new_TASKSTATUS;
		String DESIGNNAME;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);

		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("LOOP", 1, configProperities.getProperty("pathname"), null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); // Step
																														// 5
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 3");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			DESIGNNAME = fdp.addDesignFolder(1, 1, false);
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 4");
			fdp.submitDesign(1, 1, false);
			test.get().log(Status.PASS, "Step 4");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 5");
			fdp.selectDesignVersion(1, 1);
			Log.info("Design is selected");
			new_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(new_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 5");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 6");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.assignApprovalStepUser("PM", null, 1, 1, false, false, configProperities.getProperty("groupleadname"));
			Log.info("Assigned to PM");
			returnToDashboardPage();
			lp.interim_logout();
			lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.approvalAction(1, 1, "APPROVE", false);
			// can be used for a non // assigned // user test case - add this in
			// test sheet
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 6");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.info("Approved by PM");
		try {
			test.get().log(Status.PASS, "Step 7");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.assignApprovalStepUser("COLAB", null, 1, 1, false, false,configProperities.getProperty("groupleadname"));
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 7");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.info("Assigned to Colab");
		try {
			test.get().log(Status.PASS, "Step 8");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.approvalAction(1, 1, "APPROVE", false);
			lp.interim_logout();
			Log.info("Approved By Colab");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.assignApprovalStepUser("ADMIN", "APPROVE", 1, 1, false, false,
					configProperities.getProperty("groupleadname"));
			String FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "markedcomplete"));
			lp.logout();
			test.get().log(Status.PASS, "Step 8");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task will not get Mark complete, if 1 design added, 2 folders added and
	 * submitted in 2 folder and 1 selected designs in Loop
	 * 
	 * @throws Throwable
	 */
	
	  @Test(groups = { "REGRESSION" }) 
	  public void TestDesign_Task_15() throws Throwable {
	  
	  WebDriver driver = DriverFactory.getDriver(); 
	  String locale = getBrowserLocale(); 
	  String BEFORE_TASKSTATUS; String new_TASKSTATUS; String FINAL_TASKSTATUS; 
	  lp = new UILoginPage(driver,locale); 
	  fdp = new FunctionalDashboardPage(driver); 
	  methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
	  Log.startTestCase(methodName); 
	  try{ 
		  test.get().log(Status.PASS, "Step 1"); 
		  PROJECTNAME = fdp.createProjectAPI();
		  lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		  fdp.getProject(PROJECTNAME, false); 
		  fdp.clickOnEditTaskButton();
		  fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
		  fdp.clickOnEditTaskButton(); fdp.addDesigns("LOOP", 1,configProperities.getProperty("pathname"), null);
		  fdp.assignTask(configProperities.getProperty("colabname"));
		  returnToDashboardPage(); 
		  lp.interim_logout(); 
		  test.get().log(Status.PASS, "Step 1"); 
		  }catch (Exception e) { 
			  test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  	try{
	  		test.get().log(Status.PASS,"Step 2");
	  		lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); // Step 5
	  		fdp.getProject(PROJECTNAME, false); 
	  		fdp.changeTaskStatus("STARTTASK");
	  		BEFORE_TASKSTATUS = fdp.getTaskStatus(); Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
	  		test.get().log(Status.PASS,"Step 2"); 
	  		}catch (Exception e) {
	  			test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
	  			throw e; 
	  		}
	   try{ 
		   test.get().log(Status.PASS,"Step 3");
		   returnToDashboardPage(); 
		   fdp.getProject(PROJECTNAME, false);
		   fdp.addDesignFolder(2, 1, false); 
		   test.get().log(Status.PASS,"Step 3");
	   	}catch (Exception e) { 
	   		test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
	   		throw e; 
	   	} 
	   try{
		   test.get().log(Status.PASS,"Step 3"); 
		   fdp.submitDesign(2, 1, false);
		   test.get().log(Status.PASS,"Step 3"); 
		   }catch (Exception e) {
			   test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			   throw e; 
			}
	   try{ 
		   test.get().log(Status.PASS,"Step 4"); 
		   fdp.selectDesignVersion(1, 1);
		   test.get().log(Status.PASS,"Step 4"); 
		   }catch (Exception e) {
			   test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			   throw e; 
		   }
	   try{ 
		   test.get().log(Status.PASS,"Step 5"); Log.info("Design is selected"); 
		   new_TASKSTATUS= fdp.getTaskStatus();
		   Assert.assertEquals(new_TASKSTATUS, selectPropertiesFile(false, locale,"inprogress"));
		   returnToDashboardPage(); 
		   lp.interim_logout();
		   test.get().log(Status.PASS,"Step 5"); 
		   }catch (Exception e) {
			   test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			   throw e; 
		   }
	   try{ 
		   test.get().log(Status.PASS,"Step 6");
		   lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
		   fdp.getProject(PROJECTNAME, false); 
		   fdp.assignApprovalStepUser("PM", null, 1, 1, false, false, configProperities.getProperty("groupleadname")); 
		   lp.interim_logout();
		   test.get().log(Status.PASS,"Step 6"); 
		   }catch (Exception e) {
			   test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			   throw e; 
		   }
	   try{ 
		   test.get().log(Status.PASS,"Step 7");
		   lp.login(configProperities.getProperty("pmusername"),configProperities.getProperty("commonpassword"));
		   fdp.getProject(PROJECTNAME, false); 
		   fdp.approvalAction(1, 1, "APPROVE",false);
		   lp.interim_logout(); 
		   test.get().log(Status.PASS,"Step 7"); 
		   }catch (Exception e) { 
			   test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			   throw e; 
		   } 
	   	try{
	   		test.get().log(Status.PASS,"Step 8");
	   		lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
	   		fdp.getProject(PROJECTNAME, false); 
	   		fdp.assignApprovalStepUser("COLAB", null, 1, 1, false, false,configProperities.getProperty("groupleadname")); lp.interim_logout();
	   		test.get().log(Status.PASS,"Step 8"); 
	   		}catch (Exception e) {
	   			test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
	   			throw e; 
	   		}
	   	try{ 
	   		test.get().log(Status.PASS,"Step 9");
	   		lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword"));
	   		fdp.getProject(PROJECTNAME, false); 
	   		fdp.approvalAction(1, 1, "APPROVE",false); 
	   		lp.interim_logout(); 
	   		test.get().log(Status.PASS,"Step 9"); 
	   		}catch(Exception e) { 
	   			test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
	   			throw e; 
	   		} 
	   	try{
	   		test.get().log(Status.PASS,"Step 10");
	   		lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
	   		fdp.getProject(PROJECTNAME, false); 
	   		fdp.assignApprovalStepUser("ADMIN",	 "APPROVE", 1, 1, false, false,configProperities.getProperty("groupleadname")); returnToDashboardPage();
	   		fdp.getProject(PROJECTNAME, false); 
	   		FINAL_TASKSTATUS= fdp.getTaskStatus(); 
	   		Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress")); 
	   		lp.logout();
	   		test.get().log(Status.PASS,"Step 10"); 
	   		}catch (Exception e) {
	   			test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
	   			throw e; 
	   		}
	   		Log.endTestCase(methodName); 
	   		}
	  
	 /**
		 * Task status will be in progress, if design and deliverable added to a
		 * task and only deliverable is submitted
		 * 
		 * @throws Throwable
		 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_16() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		String BEFORE_TASKSTATUS;
		Integer totalDeliverables;
		String new_TASKSTATUS;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("NOAPPROVAL", 1, null, null);
			fdp.clickOnEditTaskButton();
			fdp.addDeliverables(1, "NOAPPROVAL", null, null);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); // Step
																														// 5
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			totalDeliverables = fdp.getDeliverableCount();
			fdp.submitDeliverables(totalDeliverables, 1);
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			new_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(new_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			lp.logout();
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * RACE-4719
	 * 
	 * @throws Throwable
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDesign_Task_17() throws Throwable {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		String TASKSTATUS;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDesigns("GROUP", 1, null, configProperities.getProperty("groupname"));
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try {
			test.get().log(Status.PASS, "Step 2");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); // Step
																														// 4
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.addDesignFolder(1, 1, false);
			fdp.submitDesign(1, 1, false);
			fdp.selectDesignVersion(1, 1);
			returnToDashboardPage();
			lp.interim_logout();
			lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.approvalGroup(1, 1, true, false);
			TASKSTATUS = fdp.getTaskStatus();
			System.out.println(TASKSTATUS);
			Assert.assertEquals(TASKSTATUS, selectPropertiesFile(false, locale, "markedcomplete"));
			returnToDashboardPage();
			lp.logout();
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task status will be Marked Complete, if deliverable added to a task
	 * without approval loop and assignee submits the deliverable.
	 * 
	 * @throws Throwable
	 */

	@Test(groups = { "REGRESSION" })
	public void TestDeliverable_Task1() throws Throwable {
		int totalDeliverables;
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		Integer BEFORE_TASKCOUNT;
		Integer INTERIM;
		String FINAL_TASKSTATUS;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		try {
			test.get().log(Status.PASS, "Step 1");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		} // step 2 // Creating Project using API
		try {
			test.get().log(Status.PASS, "Step 2");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDeliverables(1, "NOAPPROVAL", null, null);
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		} // Login as colab
		try {
			test.get().log(Status.PASS, "Step 3");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			Integer AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			INTERIM = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "This error come due to an Ajax related error");
			Log.info("Task count is Increased");
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			String BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			totalDeliverables = fdp.getDeliverableCount();
			fdp.submitDeliverables(totalDeliverables, 1);
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "markedcomplete"));
			lp.logout();
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	@Test(groups = { "REGRESSION" })
	public void TestDeliverable_Task3() throws Throwable {
		int totalDeliverables;
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		Integer INTERIM;
		Integer BEFORE_TASKCOUNT;
		Integer AFTER_TASKCOUNT;
		String FINAL_TASKSTATUS;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try {
			test.get().log(Status.PASS, "Step 1");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		try { // step 2
			test.get().log(Status.PASS, "Step 2");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDeliverables(1, "NOAPPROVAL", null, null);
			fdp.clickOnEditTaskButton();
			fdp.addDeliverables(1, "NOAPPROVAL", null, null);
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		} // Login as colab
		try {
			test.get().log(Status.PASS, "Step 3");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			INTERIM = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "This error come due to an Ajax related error");
			Log.info("Task count is Increased");
			// Step 5
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			String BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			totalDeliverables = fdp.getDeliverableCount();
			fdp.submitDeliverables(totalDeliverables, 1);

			// 2nd time
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress"));
			lp.logout();
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task status will be Marked Complete, if deliverable added to a task with
	 * Group and assignee submits the deliverable. And any group member
	 * finalizes
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" })
	public void TestDeliverable_Task2() throws Throwable {
		int totalDeliverables;
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		Integer BEFORE_TASKCOUNT;
		Integer AFTER_TASKCOUNT;
		Integer INTERIM;
		String BEFORE_TASKSTATUS;
		String FINAL_TASKSTATUS;
		lp = new UILoginPage(driver, locale);
		fdp = new FunctionalDashboardPage(driver);
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName);
		// step 1
		try {
			test.get().log(Status.PASS, "Step 1");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT);
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 1");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		} // step 2
		try {
			test.get().log(Status.PASS, "Step 2");
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton();
			fdp.addDeliverables(1, "GROUP", null, configProperities.getProperty("5249_group"));
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 2");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		} // Login as colab
		try {
			test.get().log(Status.PASS, "Step 3");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks");
			Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
			INTERIM = BEFORE_TASKCOUNT + 1;
			Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "This error come due to an Ajax related error");
			Log.info("Task count is Increased"); // Step 5
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			fdp.changeTaskStatus("STARTTASK");
			BEFORE_TASKSTATUS = fdp.getTaskStatus();
			Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			returnToDashboardPage();
			fdp.getProject(PROJECTNAME, false);
			totalDeliverables = fdp.getDeliverableCount();
			fdp.submitDeliverables(totalDeliverables, 1);
			returnToDashboardPage();
			lp.interim_logout();
			test.get().log(Status.PASS, "Step 3");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		} // Login as pm for approval actions
		try {
			test.get().log(Status.PASS, "Step 4");
			lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false);
			totalDeliverables = fdp.getDeliverableCount();
			fdp.approvalGroupDeliver(1, true, false);
			FINAL_TASKSTATUS = fdp.getTaskStatus();
			Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "markedcomplete"));
			lp.logout();
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
		Log.endTestCase(methodName);
	}

	/**
	 * Task Status will in complete, if request updated for a deliverable in
	 * Loop and the task count of assignee will not be reduced
	 * 
	 * @throws Throwable
	 */
	@Test(groups = { "REGRESSION" }) 
	public void TestDeliverable_Task4() throws Throwable { 
		Integer totalDeliverables; 
		WebDriver driver = DriverFactory.getDriver(); 
		String locale = getBrowserLocale(); 
		Integer BEFORE_TASKCOUNT; String BEFORE_TASKSTATUS; Integer AFTER_TASKCOUNT; Integer TASKCOUNT; String FINAL_TASKSTATUS;
		lp = new UILoginPage(driver,locale); 
		fdp = new FunctionalDashboardPage(driver); 
		methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		Log.startTestCase(methodName); 
		try{ 
			test.get().log(Status.PASS,"Step 1");
			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); 
			BEFORE_TASKCOUNT= fdp.getTaskCounts("opentasks"); 
			Log.info("The count before Task is :" + BEFORE_TASKCOUNT); 
			lp.interim_logout(); 
			test.get().log(Status.PASS, "Step 1"); 
			}catch (Exception e) { 
				test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				throw e; 
			} 
		try {
			test.get().log(Status.PASS,"Step 2"); 
			PROJECTNAME = fdp.createProjectAPI();
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			fdp.getProject(PROJECTNAME, false); 
			fdp.clickOnEditTaskButton();
			fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
			fdp.clickOnEditTaskButton(); 
			fdp.addDeliverables(1, "LOOP",configProperities.getProperty("pathname"), null);
			returnToDashboardPage(); 
			fdp.getProject(PROJECTNAME, false);
			fdp.assignTask(configProperities.getProperty("colabname"));
			returnToDashboardPage(); 
			lp.interim_logout(); 
			test.get().log(Status.PASS, "Step 2"); 
			}catch (Exception e) { 
				test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				throw e; 
			}
		 try{
			 test.get().log(Status.PASS,"Step 3");
			 lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword"));
			 fdp.getProject(PROJECTNAME, false); 
			 fdp.changeTaskStatus("STARTTASK");
			 BEFORE_TASKSTATUS= fdp.getTaskStatus(); 
			 Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
			 returnToDashboardPage(); 
			 fdp.getProject(PROJECTNAME, false);
			 totalDeliverables = fdp.getDeliverableCount();
			 fdp.submitDeliverables(totalDeliverables, 1); 
			 returnToDashboardPage();
			 lp.interim_logout(); 
			 test.get().log(Status.PASS,"Step 3"); 
			 }catch(Exception e) { 
				 test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
				 throw e; 
			 }
		 try{
			 test.get().log(Status.PASS,"Step 4");
			 lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			 fdp.getProject(PROJECTNAME, false);
			 fdp.assignApprovalUserDeliverable("COLAB", null, 1, false, false,configProperities.getProperty("groupleadname")); 
			 returnToDashboardPage();
			 lp.interim_logout(); 
		 	}	catch(Exception e){
		 		test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
				 throw e; 
			}
		 try{
			 lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
			 fdp.getProject(PROJECTNAME, false);
			 fdp.approvalActionDeliverable("APPROVE", 1, false);
			 returnToDashboardPage(); 
			 lp.interim_logout(); 
			 test.get().log(Status.PASS,"Step 4"); 
			 }catch (Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 } 
		 try{
			 test.get().log(Status.PASS,"Step 5");
			 lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			 fdp.getProject(PROJECTNAME, false);
			 fdp.assignApprovalUserDeliverable("PM", null, 1, false, false,configProperities.getProperty("groupleadname")); 
			 returnToDashboardPage();
			 lp.interim_logout(); 
			 test.get().log(Status.PASS,"Step 5"); 
			 }catch(Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 }
		 try{
			 test.get().log(Status.PASS,"Step 6");
			 lp.login(configProperities.getProperty("pmusername"),configProperities.getProperty("commonpassword"));
			 fdp.getProject(PROJECTNAME, false);
			 fdp.approvalActionDeliverable("APPROVE", 1, false);
			 returnToDashboardPage(); 
			 lp.interim_logout(); 
			 test.get().log(Status.PASS, "Step 6"); 
			 }catch (Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 }
		 try{
			 test.get().log(Status.PASS,"Step 7");
			 lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			 fdp.getProject(PROJECTNAME, false);
			 fdp.assignApprovalUserDeliverable("ADMIN", "UPDATE", 1, false, false,configProperities.getProperty("groupleadname")); 
			 returnToDashboardPage();
			 lp.interim_logout(); 
			 test.get().log(Status.PASS,"Step 7"); 
			 }catch(Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 } 
		 try{
			 test.get().log(Status.PASS,"Step 8");
			 lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); 
			 AFTER_TASKCOUNT = fdp.getTaskCounts("opentasks"); 
			 TASKCOUNT= (BEFORE_TASKCOUNT + 1);
			 Assert.assertEquals(AFTER_TASKCOUNT, TASKCOUNT); 
			 returnToDashboardPage();
			 fdp.getProject(PROJECTNAME, false); 
			 FINAL_TASKSTATUS= fdp.getTaskStatus(); 
			 Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale, "inprogress")); 
			 lp.logout();
			 test.get().log(Status.PASS,"Step 8"); 
			 }catch (Exception e) {
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 }
		 	Log.endTestCase(methodName); 
		 	}
	  
	 /**
		 * @throws Throwable
	 */
	
	  @Test(groups = { "REGRESSION" }) 
	  public void TestDeliverable_Task5() throws Throwable { 
		  Integer totalDeliverables; 
		  WebDriver driver = DriverFactory.getDriver(); 
		  String locale = getBrowserLocale(); 
		  Integer BEFORE_TASKCOUNT; Integer AFTER_TASKCOUNT; Integer INTERIM; String BEFORE_TASKSTATUS; String FINAL_TASKSTATUS; 
		  lp = new UILoginPage(driver,locale); 
		  fdp = new FunctionalDashboardPage(driver); 
		  methodName =Thread.currentThread().getStackTrace()[1].getMethodName();
		  Log.startTestCase(methodName); 
	  try{ 
		  test.get().log(Status.PASS, "Step 1"); 
		  lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); 
		  BEFORE_TASKCOUNT=  fdp.getTaskCounts("opentasks"); 
		  Log.info("The count before Task is :" + BEFORE_TASKCOUNT); 
		  lp.interim_logout(); 
		  test.get().log(Status.PASS, "Step 1"); 
		  }catch (Exception e) { 
			  test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  try{
		 test.get().log(Status.PASS,"Step 2"); 
		 PROJECTNAME = fdp.createProjectAPI();
		 lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		 fdp.getProject(PROJECTNAME, false); 
		 fdp.clickOnEditTaskButton();
		 fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
		 fdp.clickOnEditTaskButton(); 
		 fdp.addDeliverables(1, "LOOP", configProperities.getProperty("pathname"), null);
		 returnToDashboardPage(); 
		 fdp.getProject(PROJECTNAME, false);
		 fdp.assignTask(configProperities.getProperty("colabname"));
		 returnToDashboardPage(); 
		 lp.interim_logout(); 
		 test.get().log(Status.PASS,"Step 2"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
			 throw e; 
		 }
	  try{
		  test.get().log(Status.PASS,"Step 3");
		  lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); 
		  AFTER_TASKCOUNT= fdp.getTaskCounts("opentasks"); 
		  Log.info("The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
		  INTERIM= BEFORE_TASKCOUNT + 1; 
		  Assert.assertEquals(INTERIM,AFTER_TASKCOUNT, "This error come due to an Ajax related error");
		  Log.info("Task count is Increased"); 
		  returnToDashboardPage();
		  fdp.getProject(PROJECTNAME, false); 
		  fdp.changeTaskStatus("STARTTASK");
		  BEFORE_TASKSTATUS= fdp.getTaskStatus(); 
		  Log.info("Current Task status is  :" + BEFORE_TASKSTATUS);
		  returnToDashboardPage(); 
		  fdp.getProject(PROJECTNAME, false);
		  totalDeliverables = fdp.getDeliverableCount();
		  fdp.submitDeliverables(totalDeliverables, 1); 
		  returnToDashboardPage();
		  lp.interim_logout(); 
		  test.get().log(Status.PASS,"Step 3"); 
		  }catch(Exception e) { 
			  test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  try{
		  test.get().log(Status.PASS,"Step 4");
		  lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
		  fdp.getProject(PROJECTNAME, false); 
		  totalDeliverables =fdp.getDeliverableCount(); 
		  fdp.assignApprovalUserDeliverable("PM", null,1, false, false, configProperities.getProperty("groupleadname"));
		  returnToDashboardPage(); 
		  lp.interim_logout(); 
		  test.get().log(Status.PASS,"Step 4"); 
		  }catch (Exception e) { 
			  test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  try{
		  test.get().log(Status.PASS,"Step 5");
		  lp.login(configProperities.getProperty("pmusername"),configProperities.getProperty("commonpassword"));
		  fdp.getProject(PROJECTNAME, false); 
		  totalDeliverables = fdp.getDeliverableCount(); 
		  fdp.approvalActionDeliverable("APPROVE", 1, false); 
		  returnToDashboardPage(); 
		  lp.interim_logout();
		  test.get().log(Status.PASS,"Step 5"); 
		  }catch (Exception e) {
			  test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  try{ 
		  test.get().log(Status.PASS,"Step 6");
		  lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
		  fdp.getProject(PROJECTNAME, false); 
		  totalDeliverables = fdp.getDeliverableCount(); 
		  fdp.assignApprovalUserDeliverable("COLAB",null, 1, false, false, configProperities.getProperty("groupleadname"));
		  returnToDashboardPage(); 
		  lp.interim_logout(); 
		  test.get().log(Status.PASS,"Step 6"); 
		  }catch (Exception e) { 
			  test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  try{
		  test.get().log(Status.PASS,"Step 7");
		  lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword"));
		  fdp.getProject(PROJECTNAME, false); 
		  totalDeliverables = fdp.getDeliverableCount(); 
		  fdp.approvalActionDeliverable("APPROVE", 1,false); 
		  returnToDashboardPage(); 
		  lp.interim_logout();
		  test.get().log(Status.PASS,"Step 7"); 
		  }catch (Exception e) {
			  test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  try{ 
		  test.get().log(Status.PASS,"Step 8");
		  lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
		  fdp.getProject(PROJECTNAME, false); 
		  totalDeliverables =fdp.getDeliverableCount(); 
		  fdp.assignApprovalUserDeliverable("ADMIN","APPROVE", 1, false, false,configProperities.getProperty("groupleadname")); 
		  FINAL_TASKSTATUS= fdp.getTaskStatus(); 
		  Assert.assertEquals(FINAL_TASKSTATUS,selectPropertiesFile(false, locale, "markedcomplete")); 
		  lp.logout();
		  test.get().log(Status.PASS,"Step 8"); 
		  }catch (Exception e) {
			  test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			  throw e; 
		  }
	  	Log.endTestCase(methodName); 
	  }
	  
	 /*@Test public void validateTaskStartedNotification() throws Throwable {
	 * WebDriver driver = DriverFactory.getInstance().getDriver(); String locale
	 * = getBrowserLocale(); String taskname;
	 * 
	 * 
	 * lp = new UILoginPage(driver,locale); fdp = new
	 * FunctionalDashboardPage(driver); methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); // Step 1 try{ test.get().log(Status.PASS,
	 * "Step 1"); PROJECTNAME = fdp.createProjectAPI();
	 * lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); taskname= fdp.getTaskName();
	 * fdp.changeTaskStatus("STARTTASK"); // Step 2 //
	 * fdp.analyseMessageType(fdp.readNotification(fdp.getNotificationCount()),
	 * // taskname); lp.logout(); test.get().log(Status.PASS,"Step 1"); }catch
	 * (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } Log.endTestCase(methodName); }
	 * 
	 *//**
		 * Notifications to be added
		 * 
		 * @throws Throwable
		 */
	/*
	 * @Test public void not() throws Throwable { WebDriver driver =
	 * DriverFactory.getInstance().getDriver(); String locale =
	 * getBrowserLocale();
	 * 
	 * 
	 * 
	 * 
	 * lp = new UILoginPage(driver,locale); fdp = new
	 * FunctionalDashboardPage(driver); methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); try{ test.get().log(Status.PASS,"Step 1");
	 * PROJECTNAME = fdp.createProjectAPI();
	 * lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword"));
	 * 
	 * fdp.getProject(PROJECTNAME, false); fdp.clickOnEditTaskButton();
	 * fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
	 * fdp.clickOnEditTaskButton(); fdp.addDeliverables(1, "LOOP",
	 * configProperities.getProperty("pathname"), null);
	 * returnToDashboardPage(); fdp.getProject(PROJECTNAME, false);
	 * fdp.assignTask(configProperities.getProperty("colabname"));
	 * returnToDashboardPage(); lp.interim_logout(); test.get().log(Status.PASS,
	 * "Step 1"); }catch (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } try{
	 * test.get().log(Status.PASS,"Step 2");
	 * lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); String mm = fdp.getTaskName(); //
	 * fdp.readNotification(mm, "TASKASSIGN"); lp.logout();
	 * test.get().log(Status.PASS,"Step 2"); Log.endTestCase(methodName); }catch
	 * (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } }
	 * 
	 *//**
		 * if no informed group is used and no assignment is done. Project will
		 * not be visible except the project owner
		 * 
		 * @throws Throwable
		 */
	
	  @Test(groups = { "REGRESSION" }) 
	  public void Test5249_1() throws Throwable { 
		  WebDriver driver = DriverFactory.getDriver();
		  String locale = getBrowserLocale();
		  lp = new UILoginPage(driver,locale); 
		  fdp = new FunctionalDashboardPage(driver); 
		  methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		  Log.startTestCase(methodName); 
		  try{ 
			  test.get().log(Status.PASS,"Step 1");
			  PROJECTNAME = fdp.createProjectAPI();
			  lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
			  fdp.getProject(PROJECTNAME, false); 
			  fdp.clickOnEditTaskButton();
			  fdp.modifyTask(true, false, null); 
			  returnToDashboardPage();
			  lp.interim_logout(); 
			  test.get().log(Status.PASS,"Step 1"); 
			  }catch (Exception e) { 
				  test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				  throw e; 
			  } 
		  	try{
		  		test.get().log(Status.PASS,"Step 2");
		  		lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword"));
		  		fdp.getProject(PROJECTNAME, false); 
		  		returnToDashboardPage();
		  		lp.interim_logout(); 
		  		test.get().log(Status.PASS,"Step 2"); 
		  		}catch (Exception e) { 
		  			test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
		  			throw e; 
		  		} 
		  		try{
		  			test.get().log(Status.PASS,"Step 3");
		  			lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
		  			fdp.getProject(PROJECTNAME, false); 
		  			returnToDashboardPage(); 
		  			lp.logout();
		  			test.get().log(Status.PASS,"Step 3"); 
		  			}catch (Exception e) {
		  				test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
		  				throw e; 
		  			}
		  		Log.endTestCase(methodName); 
		  		}
	  
	  
	/*
	 * Group User = Auto_group Informed - Colab user is Team Lead Task
		 * assigned to PM user is group member Pm user login - Assign Drop down
		 * will display 2 options a) Colab user - Team Lead b) Pm user - self
		 * 
		 * @throws Throwable
	*/	 
	
	 @Test(groups = { "REGRESSION" }) 
	 public void Test5249_3() throws Throwable { 
		 WebDriver driver = DriverFactory.getDriver();
		 String locale = getBrowserLocale();
		 lp = new UILoginPage(driver,locale); 
		 fdp = new FunctionalDashboardPage(driver); 
		 methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		 Log.startTestCase(methodName); 
	 try{ 
		 test.get().log(Status.PASS,"Step 1");
		 PROJECTNAME = fdp.createProjectAPI();
		 lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
		 fdp.getProject(PROJECTNAME, false); 
		 fdp.clickOnEditTaskButton();
		 fdp.modifyTask(true, true, configProperities.getProperty("5249_group"));
		 returnToDashboardPage(); 
		 fdp.getProject(PROJECTNAME, false);
		 fdp.assignTask(configProperities.getProperty("pmname"));
		 lp.interim_logout(); 
		 test.get().log(Status.PASS,"Step 1"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } 
	 try{
		 test.get().log(Status.PASS,"Step 2");
		 lp.login(configProperities.getProperty("pmusername"),configProperities.getProperty("commonpassword"));
		 fdp.getProject(PROJECTNAME, false);
		 Assert.assertEquals(fdp.getInformedDropDown(), true);
		 fdp.getReassignUserlist(fdp.getInformedDropDown());
		 returnToDashboardPage(); 
		 lp.logout(); 
		 test.get().log(Status.PASS,"Step 2"); 
		 Log.endTestCase(methodName); 
		 }catch (Exception e) {
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
			 } 
	 }
	  
	 /**
		 * If assignment is done to a non-lead member, Lead login will not see a
		 * drop-down for re-assignment Group User = Auto_group Informed
		 * 
		 * @throws Throwable
		 */
	
	 @Test(groups = { "REGRESSION" }) 
	 public void Test5249_4() throws Throwable {
		 WebDriver driver = DriverFactory.getDriver(); 
		 String locale = getBrowserLocale();
		 lp = new UILoginPage(driver,locale); 
		 fdp = new FunctionalDashboardPage(driver); 
		 methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		 Log.startTestCase(methodName); 
	 try{ 
		 test.get().log(Status.PASS,"Step 1");
		 PROJECTNAME = fdp.createProjectAPI();
		 lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		 fdp.getProject(PROJECTNAME, false); 
		 fdp.clickOnEditTaskButton();
		 fdp.modifyTask(true, true, configProperities.getProperty("5249_group"));
		 returnToDashboardPage(); 
		 fdp.getProject(PROJECTNAME, false);
		 fdp.assignTask(configProperities.getProperty("pmname"));
		 lp.interim_logout(); 
		 test.get().log(Status.PASS,"Step 1"); 
		 }catch(Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } 
	 try{
		 test.get().log(Status.PASS,"Step 2");
		 lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword"));
		 fdp.getProject(PROJECTNAME, false);
		 Assert.assertEquals(fdp.getInformedDropDown(), false);
		 returnToDashboardPage(); 
		 lp.logout();
		 test.get().log(Status.PASS,"Step 2"); 
		 Log.endTestCase(methodName); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } 
	 }
	 /**
		 * Group User = Auto_group Informed - Colab user is Team Lead Task
		 * assigned to PM user is group member Pm user login - Assign Drop down
		 * will display 2 options a) Colab user - Team Lead b) Pm user - member
		 * c) Admin user member
		 * 
		 * @throws Throwable
		 */
	
	 @Test(groups = { "REGRESSION" }) 
	 public void Test5249_5() throws Throwable { 
		 WebDriver driver = DriverFactory.getDriver();
		 String locale = getBrowserLocale();
		 lp = new UILoginPage(driver,locale); 
		 fdp = new FunctionalDashboardPage(driver); 
		 methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		 Log.startTestCase(methodName); 
	 try{ 
		 test.get().log(Status.PASS,"Step 1");
		 PROJECTNAME = fdp.createProjectAPI();
		 lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword"));
		 fdp.getProject(PROJECTNAME, false); 
		 fdp.clickOnEditTaskButton();
		 fdp.modifyTask(true, true, configProperities.getProperty("5249_group"));
		 returnToDashboardPage(); 
		 fdp.getProject(PROJECTNAME, false);
		 fdp.assignTask(configProperities.getProperty("colabname"));
		 lp.interim_logout(); 
		 test.get().log(Status.PASS,"Step 1"); 
		 }catch(Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } 
	 try{
		 test.get().log(Status.PASS,"Step 2");
		 lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword"));
		 fdp.getProject(PROJECTNAME, false);
		 Assert.assertEquals(fdp.getInformedDropDown(), true);
		 fdp.getReassignUserlist(fdp.getInformedDropDown());
		 returnToDashboardPage(); 
		 lp.logout(); 
		 test.get().log(Status.PASS,"Step 2"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
			 throw e; 
		 } 
	 	Log.endTestCase(methodName); 
	 	}
	 /**
		 * 1. Task Assigned to Team Lead - colab user 2. Team Lead re-assigns to
		 * team member - pm user 3. Check for the notifications Group user -
		 * Autogroup_informed
		 * 
		 * @throws Throwable
		 * 
		 */
	
	 @Test(groups = { "REGRESSION" }) 
	 public void Test5249_6() throws Throwable { 
		 WebDriver driver = DriverFactory.getDriver();
		 String locale = getBrowserLocale(); 
		 lp = new UILoginPage(driver,locale);
		 fdp = new FunctionalDashboardPage(driver); 
		 String taskname = null; String currentuser = null; 
		 methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		 Log.startTestCase(methodName);
		 // Step 1 // Delete Notification from All user 
		 // Colab user 
		 try{ 
			 test.get().log(Status.PASS,"Step 1");
			 lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); 
			 // Deleting exiting Notification and count will be 0 
			 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			 Assert.assertEquals(fdp.getNotificationCount(), 0);
			 returnToDashboardPage(); 
			 lp.interim_logout(); 
			 test.get().log(Status.PASS,"Step 1"); 
			 }catch (Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 } // PM user 
		 try{
			 test.get().log(Status.PASS,"Step 2");
			 lp.login(configProperities.getProperty("pmusername"),configProperities.getProperty("commonpassword")); 
			 // Deleting exiting Notification and count will be 0 
			 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			 Assert.assertEquals(fdp.getNotificationCount(), 0);
			 returnToDashboardPage(); 
			 lp.interim_logout(); 
			 test.get().log(Status.PASS,"Step 2"); 
			 }catch (Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 } 
		 	// Create project 
		 try{
			 test.get().log(Status.PASS,"Step 3"); 
			 PROJECTNAME = fdp.createProjectAPI();
			 lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword")); 
			 // Deleting exiting Notification and count will be 0 
			 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			 Assert.assertEquals(fdp.getNotificationCount(), 0); 
			 // Task Edit and add informed group 
			 fdp.getProject(PROJECTNAME, false); 
			 taskname = fdp.getTaskName(); 
			 fdp.clickOnEditTaskButton(); 
			 fdp.modifyTask(true, true, configProperities.getProperty("5249_group"));
			 returnToDashboardPage(); 
			 fdp.getProject(PROJECTNAME, false); 
			 // Assign task to Group Lead
			 fdp.assignTask(configProperities.getProperty("colabname"));
			 lp.interim_logout(); 
			 test.get().log(Status.PASS,"Step 3"); 
			 }catch(Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 } 
		 try{
			 test.get().log(Status.PASS,"Step 4");
			 lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); 
			 // Check the notifications 
			 Assert.assertEquals(fdp.getNotificationCount(), 1);
			 fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()),taskname); 
			 fdp.getProject(PROJECTNAME, false);
			 System.out.println(fdp.currentAssignedUser());
			 fdp.reAssignTask(fdp.currentAssignedUser(), true, configProperities.getProperty("5249member1"), true); 
			 // deleting the notification from group Lead 
			 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			 Assert.assertEquals(fdp.getNotificationCount(), 0);
			 returnToDashboardPage(); 
			 lp.interim_logout(); 
			 test.get().log(Status.PASS, "Step 4"); 
			 }catch (Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 } 
		 try{ 
			 // Check re-assign message* for project owner 
			 test.get().log(Status.PASS,"Step 5");
			 lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword")); 
			 // get the current assigned user 
			 fdp.getProject(PROJECTNAME, false); 
			 currentuser = fdp.currentAssignedUser(); 
			 // Check the notifications
			 Assert.assertEquals(fdp.getNotificationCount(), 1);
			 fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()),taskname, currentuser);
			 // delete the notification
			 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			 Assert.assertEquals(fdp.getNotificationCount(), 0);
			 returnToDashboardPage(); 
			 lp.interim_logout(); 
			 test.get().log(Status.PASS, "Step 5"); 
			 }catch (Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
			 } 
		 	// Group member assigned message 
		 try{ 
			 test.get().log(Status.PASS,"Step 6");
			 lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword")); 
			 // Check the notifications 
			 Assert.assertEquals(fdp.getNotificationCount(), 1);
			 fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()),taskname); 
			 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
			 Assert.assertEquals(fdp.getNotificationCount(), 0);
			 returnToDashboardPage(); 
			 lp.logout(); 
			 test.get().log(Status.PASS,"Step 6"); 
			 }catch (Exception e) { 
				 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
				 throw e; 
				 } 
		 }
	 /**
		 * 1. Task Assigned to Team Member - pm user 2. Team member re-assigns
		 * to team Lead - collab user 3. Check for the notifications Group user
		 * - Autogroup_informed
		 * 
		 * @throws Throwable
		 * 
		 */
	 @Test(groups = { "REGRESSION" }) 
	 public void Test5249_7() throws Throwable { 
		 WebDriver driver = DriverFactory.getDriver();
		 String locale = getBrowserLocale();
		 lp = new UILoginPage(driver,locale); 
		 fdp = new FunctionalDashboardPage(driver); 
		 String taskname = null; String currentuser = null; 
		 methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		 Log.startTestCase(methodName); 
		 // Step 1 Delete Notification from All user 
		 // Colab user 
	 try{ 
		 test.get().log(Status.PASS,"Step 1");
		 lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); 
		 // Deleting exiting Notification and count will be 0 
		 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
		 Assert.assertEquals(fdp.getNotificationCount(), 0);
		 returnToDashboardPage(); 
		 lp.interim_logout(); 
		 test.get().log(Status.PASS, "Step 1"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } 
	 	// PM user 
	 try{
		 test.get().log(Status.PASS,"Step 2");
		 lp.login(configProperities.getProperty("pmusername"),configProperities.getProperty("commonpassword")); 
		 // Deleting exiting Notification and count will be 0 
		 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
		 Assert.assertEquals(fdp.getNotificationCount(), 0);
		 returnToDashboardPage(); 
		 lp.interim_logout(); 
		 test.get().log(Status.PASS, "Step 2"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } 
	 	// Create project 
	 try{
		 test.get().log(Status.PASS,"Step 3"); 
		 PROJECTNAME = fdp.createProjectAPI();
		 lp.login(configProperities.getProperty("adminusername"),configProperities.getProperty("commonpassword")); 
		 // Deleting exiting Notification and count will be 0 
		 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
		 Assert.assertEquals(fdp.getNotificationCount(), 0); 
		 // Task Edit and add informed group 
		 fdp.getProject(PROJECTNAME, false); 
		 taskname = fdp.getTaskName(); 
		 fdp.clickOnEditTaskButton(); 
		 fdp.modifyTask(true, true, configProperities.getProperty("5249_group"));
		 returnToDashboardPage(); 
		 fdp.getProject(PROJECTNAME, false);
		 // Assign task to Group Lead
		 fdp.assignTask(configProperities.getProperty("pmname"));
		 lp.interim_logout(); 
		 test.get().log(Status.PASS,"Step 3"); 
		 }catch(Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } // Strp 2 
	 try{
		 test.get().log(Status.PASS,"Step 4");
		 lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword")); 
		 // Check the notifications 
		 Assert.assertEquals(fdp.getNotificationCount(), 1);
		 fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()),taskname); 
		 fdp.getProject(PROJECTNAME, false); 
		 // Current user
		 System.out.println(fdp.currentAssignedUser()); 
		 // Assign Task to pm user
		 fdp.reAssignTask(fdp.currentAssignedUser(), false,configProperities.getProperty("5249lead"), true); 
		 // deleting the notification from group member 
		 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
		 Assert.assertEquals(fdp.getNotificationCount(), 0);
		 returnToDashboardPage(); 
		 lp.interim_logout(); 
		 test.get().log(Status.PASS, "Step 4"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 }
	 	// Check re-assign message for project owner 
	 try{ 
		 test.get().log(Status.PASS,"Step 5");
		 lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword")); 
		 // get the current assigned user 
		 fdp.getProject(PROJECTNAME, false); 
		 currentuser = fdp.currentAssignedUser(); 
		 // Check the notifications
		 Assert.assertEquals(fdp.getNotificationCount(), 1);
		 fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()),taskname, currentuser); 
		 // delete the notification
		 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
		 Assert.assertEquals(fdp.getNotificationCount(), 0);
		 returnToDashboardPage(); 
		 lp.interim_logout(); 
		 test.get().log(Status.PASS, "Step 5"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); throw e; }
	 	// Group member assigned message 
	 	try{ 
	 		test.get().log(Status.PASS,"Step 6");
	 		lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); 
	 		// Check the notifications 
	 		Assert.assertEquals(fdp.getNotificationCount(), 1);
	 		fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()),taskname); 
	 		fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
	 		Assert.assertEquals(fdp.getNotificationCount(), 0);
	 		returnToDashboardPage(); 
	 		lp.logout(); 
	 		test.get().log(Status.PASS,"Step 6" ); 
	 		}catch (Exception e) { 
	 			test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
	 			throw e; 
	 			} 
	 	}
	 
	 @Test(groups = { "REGRESSION" }) 
	 public void Test4719_2() throws Throwable { 
		 WebDriver driver = DriverFactory.getDriver();
		 String locale = getBrowserLocale(); 
		 String FINAL_TASKSTATUS; Integer BEFORE_TASKCOUNT; Integer AFTER_TASKCOUNT; Integer INTERIM; 
		 String BEFORE_TASKSTATUS; Integer FINAL_TASKCOUNT; 
		 lp = new UILoginPage(driver,locale); 
		 fdp = new FunctionalDashboardPage(driver);
		 methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		 Log.startTestCase(methodName); 
		 // Delete all notifications
		 //Colab user
	 try{ 
		 test.get().log(Status.PASS,"Step 1");
		 lp.login(configProperities.getProperty("collabusername"),configProperities.getProperty("commonpassword")); 
		 // Deleting exiting Notification and count will be 0 
		 BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks"); 
		 Log.info("The count before Task is :" + BEFORE_TASKCOUNT); 
		 fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
		 Assert.assertEquals(fdp.getNotificationCount(), 0);
		 returnToDashboardPage(); 
		 lp.interim_logout(); 
		 test.get().log(Status.PASS, "Step 1"); 
		 }catch (Exception e) { 
			 test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
			 throw e; 
		 } // PM user 
	 	try{
	 		test.get().log(Status.PASS,"Step 2");
	 		lp.login(configProperities.getProperty("pmusername"), configProperities.getProperty("commonpassword")); 
	 		// Deleting exiting Notification and count will be 0 
	 		fdp.deleteNotificationMessage(true,fdp.getNotificationCount());
	 		Assert.assertEquals(fdp.getNotificationCount(), 0);
	 		returnToDashboardPage(); 
	 		lp.interim_logout(); 
	 		test.get().log(Status.PASS,"Step 2"); 
	 		}catch (Exception e) { 
	 			test.get().log(Status.ERROR,e.getClass().getSimpleName()); 
	 			throw e; 
	 		} // Step 2 
	 	try{
	 		test.get().log(Status.PASS,"Step 3"); 
	 		PROJECTNAME = fdp.createProjectAPI();
	 		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
	 		fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
	 		Assert.assertEquals(fdp.getNotificationCount(), 0);
	 		fdp.getProject(PROJECTNAME, false); fdp.clickOnEditTaskButton();
	 		fdp.modifyTask(true, true, configProperities.getProperty("5249_group"));
	 		fdp.clickOnEditTaskButton(); fdp.addDesigns("LOOP", 1,configProperities.getProperty("4719path"), null); 
	 		// Step 3 // Task Assign to Group Lead
	 		fdp.assignTask(configProperities.getProperty("colabname"));
	 		returnToDashboardPage(); 
	 		lp.interim_logout(); 
	 		test.get().log(Status.PASS,"Step 3"); 
	 		}catch (Exception e) { 
	 			test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
	 			throw e; 
	 		} // Step 4 
	 	try{
	 		test.get().log(Status.PASS,"Step 4");
	 		lp.login(configProperities.getProperty("collabusername"), configProperities.getProperty("commonpassword")); 
	 		AFTER_TASKCOUNT=fdp.getTaskCounts("opentasks"); 
	 		Log.info( "The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
	 		INTERIM= BEFORE_TASKCOUNT + 1; 
	 		Assert.assertEquals(INTERIM, AFTER_TASKCOUNT, "WS error related to refresbehd task counts"); 
	 		Log.info("Task count is Increased"); 
	 		// here 
	 		// Step 5 
	 		returnToDashboardPage();
	 		fdp.getProject(PROJECTNAME, false); 
	 		fdp.changeTaskStatus("STARTTASK");
	 		BEFORE_TASKSTATUS= fdp.getTaskStatus(); 
	 		Log.info("Current Task status is  :" + BEFORE_TASKSTATUS); 
	 		// Step 6
	 		returnToDashboardPage(); 
	 		fdp.getProject(PROJECTNAME, false);
	 		fdp.addDesignFolder(1, 1, false); 
	 		fdp.submitDesign(1, 1, false);
	 		fdp.selectDesignVersion(1, 1); 
	 		Log.info("Design is selected");
	 		FINAL_TASKSTATUS= fdp.getTaskStatus(); 
	 		// Step 7 
	 		returnToDashboardPage();
	 		FINAL_TASKCOUNT= fdp.getTaskCounts("opentasks"); 
	 		Assert.assertEquals(INTERIM, FINAL_TASKCOUNT);
	 		Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale,"inprogress")); 
	 		lp.logout(); 
	 		Log.endTestCase(methodName);
	 		test.get().log(Status.PASS,"Step 4"); 
	 		}catch (Exception e) {
	 			test.get().log(Status.ERROR, e.getClass().getSimpleName()); 
	 			throw e; 
	 		} 
	 	}
	 /* @Test public void notificationTest() throws InterruptedException { lp =
	 * new UILoginPage(); fdp = new FunctionalDashboardPage(); methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName);
	 * lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword"));
	 * System.out.println(fdp.getNotificationCount());
	 * System.out.println(fdp.readNotification(fdp.getNotificationCount()));
	 * fdp.deleteNotificationMessage(false, fdp.getNotificationCount());
	 * System.out.println(fdp.getNotificationCount()); }
	 * 
	 * @Test(groups = { "REGRESSION" }) public void TestDesign_Task_4719_1()
	 * throws Throwable { String taskname = null; WebDriver driver =
	 * DriverFactory.getInstance().getDriver(); String locale =
	 * getBrowserLocale();
	 * 
	 * 
	 * lp = new UILoginPage(driver,locale); fdp = new
	 * FunctionalDashboardPage(driver); methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); // Step 1 // Delete Notification from All
	 * user // Colab user try{ test.get().log(Status.PASS,"Step 1");
	 * lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword")); // Deleting exiting
	 * Notification and count will be 0 fdp.deleteNotificationMessage(true,
	 * fdp.getNotificationCount());
	 * Assert.assertEquals(fdp.getNotificationCount(), 0);
	 * returnToDashboardPage(); lp.interim_logout(); test.get().log(Status.PASS,
	 * "Step 1"); }catch (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } // PM user try{
	 * test.get().log(Status.PASS,"Step 2");
	 * lp.login(configProperities.getProperty("pmusername"),
	 * configProperities.getProperty("commonpassword")); // Deleting exiting
	 * Notification and count will be 0 fdp.deleteNotificationMessage(true,
	 * fdp.getNotificationCount());
	 * Assert.assertEquals(fdp.getNotificationCount(), 0);
	 * returnToDashboardPage(); lp.interim_logout(); test.get().log(Status.PASS,
	 * "Step 2"); }catch (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } // Step 2 try{
	 * test.get().log(Status.PASS,"Step 3"); PROJECTNAME =
	 * fdp.createProjectAPI();
	 * lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
	 * Assert.assertEquals(fdp.getNotificationCount(), 0);
	 * fdp.getProject(PROJECTNAME, false); fdp.clickOnEditTaskButton(); taskname
	 * = fdp.getTaskName(); fdp.modifyTask(true, true,
	 * configProperities.getProperty("groupname")); fdp.clickOnEditTaskButton();
	 * fdp.addDesigns("LOOP", 1, configProperities.getProperty("4719path"),
	 * null); // Step 3
	 * fdp.assignTask(configProperities.getProperty("colabname"));
	 * returnToDashboardPage(); fdp.getProject(PROJECTNAME, false);
	 * fdp.changeTaskStatus("STARTTASK"); String BEFORE_TASKSTATUS =
	 * fdp.getTaskStatus(); Log.info("Current Task status is  :" +
	 * BEFORE_TASKSTATUS); returnToDashboardPage();
	 * Assert.assertEquals(fdp.messageTypeAnalyse(fdp.readNotification(fdp.
	 * getNotificationCount()), taskname), true); lp.interim_logout();
	 * test.get().log(Status.PASS,"Step 3"); }catch (Exception e) {
	 * test.get().log(Status.ERROR, e.getClass().getSimpleName()); throw e; } //
	 * Step 4 try{ test.get().log(Status.PASS,"Step 4");
	 * lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword")); // Step 5
	 * fdp.getProject(PROJECTNAME, false); taskname = fdp.getTaskName();
	 * Assert.assertEquals(fdp.messageTypeAnalyse(fdp.readNotification(fdp.
	 * getNotificationCount()), taskname), true);
	 * fdp.deleteNotificationMessage(true, fdp.getNotificationCount()); // Step
	 * 6 returnToDashboardPage(); fdp.getProject(PROJECTNAME, false); String
	 * designname = fdp.addDesignFolder(1, 1, false); fdp.submitDesign(1, 1,
	 * false); fdp.selectDesignVersion(1, 1); Log.info("Design is selected");
	 * Assert.assertEquals(fdp.messageTypeAnalyse(fdp.readNotification(fdp.
	 * getNotificationCount()), taskname), true);
	 * fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
	 * returnToDashboardPage(); lp.interim_logout(); test.get().log(Status.PASS,
	 * "Step 4"); }catch (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } // Step 7 try{
	 * test.get().log(Status.PASS,"Step 5");
	 * lp.login(configProperities.getProperty("pmusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); // Self assignment by pm User and
	 * approval fdp.assignApprovalStepUser("PM", null, 1, 1, true, false,
	 * configProperities.getProperty("5249lead")); Log.info(
	 * "self Assigned by PM"); returnToDashboardPage();
	 * Assert.assertEquals(fdp.messageTypeAnalyse(fdp.readNotification(fdp.
	 * getNotificationCount()), taskname), true);
	 * fdp.deleteNotificationMessage(true, fdp.getNotificationCount());
	 * returnToDashboardPage(); lp.interim_logout(); test.get().log(Status.PASS,
	 * "Step 5"); }catch (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } Log.info("Approved by PM");
	 * try{ test.get().log(Status.PASS,"Step 5");
	 * lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword")); Assert.assertEquals(
	 * fdp.messageTypeAnalyse(fdp.readNotification(fdp.getNotificationCount()),
	 * taskname, "my PM my PM"), true); fdp.deleteNotificationMessage(true,
	 * fdp.getNotificationCount()); // fdp.getProject(PROJECTNAME);
	 * lp.interim_logout(); test.get().log(Status.PASS,"Step 5"); }catch
	 * (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } // pm login and approve try{
	 * test.get().log(Status.PASS,"Step 6");
	 * lp.login(configProperities.getProperty("pmusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); fdp.approvalAction(1, 1, "APPROVE",
	 * false);// can be used for a non // assigned returnToDashboardPage();
	 * lp.interim_logout(); test.get().log(Status.PASS,"Step 6"); }catch
	 * (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } try{
	 * test.get().log(Status.PASS,"Step 7");
	 * lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword"));
	 * Assert.assertEquals(fdp.messageTypeAnalyse(fdp.readNotification(fdp.
	 * getNotificationCount()), taskname), true); lp.logout();
	 * Log.endTestCase(methodName); test.get().log(Status.PASS,"Step 7"); }catch
	 * (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } }
	 * 
	 * @Test(groups = { "REGRESSION" }) public void TestDeliverable_4719_2()
	 * throws Throwable { WebDriver driver =
	 * DriverFactory.getInstance().getDriver(); String locale =
	 * getBrowserLocale();
	 * 
	 * 
	 * 
	 * int totalDeliverables; lp = new UILoginPage(driver,locale); fdp = new
	 * FunctionalDashboardPage(driver); methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); // step 1 try{ test.get().log(Status.PASS,
	 * "Step 1"); lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword")); Integer
	 * BEFORE_TASKCOUNT = fdp.getTaskCounts("opentasks"); Log.info(
	 * "The count before Task is :" + BEFORE_TASKCOUNT); lp.interim_logout();
	 * test.get().log(Status.PASS,"Step 1"); }catch (Exception e) {
	 * test.get().log(Status.ERROR, e.getClass().getSimpleName()); throw e; } //
	 * step 2 try{ test.get().log(Status.PASS,"Step 2"); PROJECTNAME =
	 * fdp.createProjectAPI();
	 * lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); fdp.clickOnEditTaskButton();
	 * fdp.modifyTask(true, true, configProperities.getProperty("groupname"));
	 * fdp.clickOnEditTaskButton(); fdp.addDeliverables(1, "LOOP",
	 * configProperities.getProperty("4719path"), null);
	 * returnToDashboardPage(); fdp.getProject(PROJECTNAME, false);
	 * fdp.assignTask(configProperities.getProperty("colabname"));
	 * returnToDashboardPage(); lp.interim_logout(); test.get().log(Status.PASS,
	 * "Step 2"); }catch (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } // Login as colab try{
	 * test.get().log(Status.PASS,"Step 3");
	 * lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword")); Integer AFTER_TASKCOUNT
	 * = fdp.getTaskCounts("opentasks"); Log.info(
	 * "The count of Open Task after task assignment is :" + AFTER_TASKCOUNT);
	 * //Integer INTERIM = BEFORE_TASKCOUNT + 1; //Assert.assertEquals(INTERIM,
	 * AFTER_TASKCOUNT, "This error come due to an Ajax related error");
	 * Log.info("Task count is Increased"); // Step 5 returnToDashboardPage();
	 * fdp.getProject(PROJECTNAME, false); fdp.changeTaskStatus("STARTTASK");
	 * String BEFORE_TASKSTATUS = fdp.getTaskStatus(); Log.info(
	 * "Current Task status is  :" + BEFORE_TASKSTATUS);
	 * returnToDashboardPage(); fdp.getProject(PROJECTNAME, false);
	 * totalDeliverables = fdp.getDeliverableCount();
	 * fdp.submitDeliverables(totalDeliverables, 1); returnToDashboardPage();
	 * lp.interim_logout(); test.get().log(Status.PASS,"Step 2"); }catch
	 * (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; } // assignment by admin try{
	 * lp.login(configProperities.getProperty("pmusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); totalDeliverables =
	 * fdp.getDeliverableCount(); fdp.assignApprovalUserDeliverable("PM", null,
	 * 1, true, false, configProperities.getProperty("5249lead"));
	 * returnToDashboardPage(); lp.interim_logout(); // by pm
	 * lp.login(configProperities.getProperty("pmusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); totalDeliverables =
	 * fdp.getDeliverableCount(); fdp.approvalActionDeliverable("APPROVE", 1,
	 * false); returnToDashboardPage(); lp.interim_logout(); }catch (Exception
	 * e) { test.get().log(Status.ERROR, e.getClass().getSimpleName()); throw e;
	 * } // admin try{ lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); totalDeliverables =
	 * fdp.getDeliverableCount(); fdp.assignApprovalUserDeliverable("COLAB",
	 * null, 1, false, false, configProperities.getProperty("5249lead"));
	 * returnToDashboardPage(); lp.interim_logout(); }catch (Exception e) {
	 * test.get().log(Status.ERROR, e.getClass().getSimpleName()); throw e; } //
	 * colab try{ lp.login(configProperities.getProperty("collabusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); totalDeliverables =
	 * fdp.getDeliverableCount(); fdp.approvalActionDeliverable("APPROVE", 1,
	 * false); returnToDashboardPage(); lp.interim_logout();
	 * 
	 * // admin approve lp.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword"));
	 * fdp.getProject(PROJECTNAME, false); totalDeliverables =
	 * fdp.getDeliverableCount(); fdp.assignApprovalUserDeliverable("ADMIN",
	 * "APPROVE", 1, true, false, configProperities.getProperty("5249lead"));
	 * 
	 * String FINAL_TASKSTATUS = fdp.getTaskStatus();
	 * Assert.assertEquals(FINAL_TASKSTATUS, selectPropertiesFile(false, locale,
	 * "markedcomplete")); lp.logout(); Log.endTestCase(methodName); }catch
	 * (Exception e) { test.get().log(Status.ERROR,
	 * e.getClass().getSimpleName()); throw e; }
	 * 
	 * }
	 */}
