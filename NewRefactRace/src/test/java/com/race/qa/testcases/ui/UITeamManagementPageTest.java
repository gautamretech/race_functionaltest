package com.race.qa.testcases.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.pages.ui.UITeamManagementPage;
//import net.lightbody.bmp.core.har.Har;

public class UITeamManagementPageTest extends TestBase {
	UILoginPage lp;
	UITeamManagementPage teamManagementPage;
	private String GROUPPREFIX = "groups_";
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private String FILEDATE = dateFormat.format(new Date());
	private String FILEFORMAT = ".xlsx";
	private String FINALFILENAME = GROUPPREFIX + FILEDATE + FILEFORMAT;
	String methodName;

	/**
	 * Color picker for Group removed RACE-5525 - 1 test case less
	 * 
	 * @throws Throwable
	 */
	/*
	 * @Test(groups = { "1SANITY" }) public void validateColorPickerTest()
	 * throws InterruptedException { methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); loginPage = new UILoginPage();
	 * loginPage.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword")); teamManagementPage =
	 * new UITeamManagementPage(); teamManagementPage.validateColorPicker(); try
	 * { loginPage.logout(); } catch (InterruptedException e) {
	 * e.printStackTrace(); } Log.endTestCase(methodName); }
	 */
	/*@Test(groups = { "SANITY" })
	public void validateAddGroupScreenTest() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Group Tab - Add Groups", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			teamManagementPage = new UITeamManagementPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Group Tab - Add Group Modal ");
			teamManagementPage.validateAddGroupScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateTeamTabTest() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Team tab as Admin User", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			teamManagementPage = new UITeamManagementPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Team Tab as Admin user ");
			teamManagementPage.validateTeamTab("Admin");
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void inviteTeamMemberScreenTest() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Team Member Screen Test", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			teamManagementPage = new UITeamManagementPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating the Team Member Screen Test");
			teamManagementPage.inviteTeamMemberScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

*/	
	@Test(groups = { "SANITY" })
	public void validateGroupsTabScreenTest() throws Throwable {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Groups Screen and Download File", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			teamManagementPage = new UITeamManagementPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Group Tab Screen and Download file ");
			teamManagementPage.validateGroupsTabScreen();
			Assert.assertTrue(isFileDownloaded(downloadFilepath, FINALFILENAME));
			test.get().log(Status.INFO, "Step 2 -a : File Downloaded, File Name is : "+FINALFILENAME);
			ExcelReader(FINALFILENAME);
			test.get().log(Status.INFO, "Step 2 -b : Read the contents of the downloaded File");
			deleteFile(FINALFILENAME);
			test.get().log(Status.INFO, "Step 2 -c : Delete the file from the location for reusability");
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
}
