package com.race.qa.testcases.ui;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.pages.ui.UISupplierPage;
//import net.lightbody.bmp.core.har.Har;

public class UISupplierPageTest extends TestBase {
	UILoginPage lp;
	UISupplierPage supplierPage;
	String methodName;

	@Test(groups = { "SANITY" })
	public void validateSupplierLandingPageTest() throws Throwable {
		test.get().log(Status.INFO,
				MarkupHelper.createLabel("Validating the Supplier Landing Page Elements", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			supplierPage = new UISupplierPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating the Supplier Landing Page Elements");
			supplierPage.validateSupplierLandingPage();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateAddSupplierScreenTest() throws Throwable {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating the Add Supplier Modal", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			supplierPage = new UISupplierPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Supplier Add Modal ");
			supplierPage.validateAddSupplierScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateFilterPopupScreenTest() throws Throwable {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Supplier Filter Screen",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			supplierPage = new UISupplierPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Supplier Filter ");
			supplierPage.validateFilterPopupScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateSupplierDetailsOverviewTabTest() throws Throwable {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Supplier's OverView Tab",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			supplierPage = new UISupplierPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Suppliers Overview Tab");
			supplierPage.validateSupplierDetailsOverviewTab();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	/*
	 * @Test(groups = { "SANITY1" }) public void validateRatingsTabTest() throws
	 * InterruptedException {
	 * 
	 * methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); loginPage = new UILoginPage();
	 * loginPage.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword")); supplierPage = new
	 * UISupplierPage(); supplierPage.validateRatingsTab(); try {
	 * loginPage.logout(); } catch (InterruptedException e) {
	 * e.printStackTrace(); } Log.endTestCase(methodName);
	 * 
	 * }
	 * 
	 * @Test(groups = { "SANITY1" }) public void validateAddRatingsScreenTest()
	 * throws InterruptedException { methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); loginPage = new
	 * UILoginPage(driver,locale);
	 * loginPage.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword")); supplierPage = new
	 * UISupplierPage(driver); supplierPage.validateAddRatingsScreen(); try {
	 * loginPage.logout(); } catch (InterruptedException e) {
	 * e.printStackTrace(); } Log.endTestCase(methodName);
	 * 
	 * }
	 * 
	 * @Test(groups = { "SANITY1" }) public void validateAddCustomCriteriaTest()
	 * throws InterruptedException { methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); loginPage = new
	 * UILoginPage(driver,locale);
	 * loginPage.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword")); supplierPage = new
	 * UISupplierPage(driver); supplierPage.validateAddCustomCriteria(); try {
	 * loginPage.logout(); } catch (InterruptedException e) {
	 * e.printStackTrace(); } Log.endTestCase(methodName);
	 * 
	 * }
	 */
	
	@Test(groups = { "SANITY" })
	public void validateProductTabInSupplierScreenTest() throws Throwable {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Supplier's Product Tab tab",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			supplierPage = new UISupplierPage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Supplier's Product Tab ");
			supplierPage.validateProductTabInSupplierScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}
	/*
	 * @Parameters("browser")
	 * 
	 * @Test(groups = { "SANITY" }) public void
	 * validateAddCustomCriteriaMax6CountTest() throws Exception { WebDriver
	 * driver = DriverFactory.getInstance().getDriver(); String locale =
	 * getBrowserLocale(); configProperities = loadConfigFile(); methodName =
	 * Thread.currentThread().getStackTrace()[1].getMethodName();
	 * Log.startTestCase(methodName); loginPage = new
	 * UILoginPage(driver,locale);
	 * loginPage.login(configProperities.getProperty("adminusername"),
	 * configProperities.getProperty("commonpassword")); supplierPage = new
	 * UISupplierPage(driver);
	 * supplierPage.validateAddCustomCriteriaMax6Count(); try {
	 * loginPage.logout(); } catch (InterruptedException e) {
	 * e.printStackTrace(); } Log.endTestCase(methodName);
	 * 
	 * }
	 */

}
