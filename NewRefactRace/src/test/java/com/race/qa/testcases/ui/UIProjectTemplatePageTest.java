package com.race.qa.testcases.ui;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.race.qa.base.DriverFactory;
import com.race.qa.base.TestBase;
import com.race.qa.pages.ui.UILoginPage;
import com.race.qa.pages.ui.UIProjectTemplatePage;
import com.race.qa.util.Log;
//import net.lightbody.bmp.core.har.Har;

public class UIProjectTemplatePageTest extends TestBase {
	UILoginPage lp;
	UIProjectTemplatePage projectTemplatePage;
	String methodName;

	@Test(groups = { "SANITY" })
	public void validateExistingTemplateTabScreenTest() throws Throwable {
		test.get().log(Status.INFO, MarkupHelper.createLabel("Validating Existing Template tab", ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
			WebDriver driver = DriverFactory.getDriver();
			String locale = getBrowserLocale();
			configProperities = loadConfigFile();
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			projectTemplatePage = new UIProjectTemplatePage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating Existing Template ");
			projectTemplatePage.validateExistingTemplateTabScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateNewTemplateTabScreenTest() throws Throwable {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the New Template tab",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		try {
			methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			Log.startTestCase(methodName);
			lp = new UILoginPage(driver, locale);
			test.get().log(Status.INFO, "Step 1 : Login ");
			lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
			test.get().log(Status.PASS, "Step 1 : Successful Login ");
			projectTemplatePage = new UIProjectTemplatePage(driver);
			test.get().log(Status.INFO, "Step 2 : Validating New Template ");
			projectTemplatePage.validateNewTemplateTabScreen();
			test.get().log(Status.PASS, "Step 2 : Validation Complete ");
			test.get().log(Status.INFO, "Step 3 : Logout ");
			lp.logout();
			test.get().log(Status.PASS, "Step 3 : Successful Logout ");
			} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

	@Test(groups = { "SANITY" })
	public void validateAddNewPhaseScreenTest() throws Throwable {
		test.get().log(Status.INFO,MarkupHelper.createLabel("Validating the Add New Phase Screen",ExtentColor.AMBER));
		test.get().log(Status.INFO, "Starting Test..." + Thread.currentThread().getStackTrace()[1].getMethodName());
		try {
		WebDriver driver = DriverFactory.getDriver();
		String locale = getBrowserLocale();
		configProperities = loadConfigFile();
		lp = new UILoginPage(driver, locale);
		test.get().log(Status.INFO, "Step 1 : Login ");
		lp.login(configProperities.getProperty("adminusername"), configProperities.getProperty("commonpassword"));
		test.get().log(Status.PASS, "Step 1 : Successful Login ");
		projectTemplatePage = new UIProjectTemplatePage(driver);
		test.get().log(Status.INFO, "Step 2 : Validating Add New Phase Screen");
		projectTemplatePage.validateAddNewPhaseScreen();
		test.get().log(Status.PASS, "Step 2 : Validation Complete ");
		test.get().log(Status.INFO, "Step 3 : Logout ");
		lp.logout();
		test.get().log(Status.PASS, "Step 3 : Successful Logout ");
		} catch (Exception e) {
			test.get().log(Status.ERROR, e.getClass().getSimpleName());
			throw e;
		}
	}

}
